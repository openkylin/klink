#!/bin/bash

#获取头文件路径
include_dir=$1
#需要链接头文件的路径
root_path=$2
# 0 创建文件夹 1 不创建文件夹
flag=$3

mkdir $include_dir/include/ 1>/dev/null 2>/dev/null

dirstr=${root_path##*/}

if [ ! "$dirstr" = "include" ]; then
    if [ $flag -eq 0 ]; then
        mkdir $include_dir/include/$dirstr 1>/dev/null 2>/dev/null
    fi
fi

# 使用 for 循环遍历目录下的所有文件
for filepath in `find $root_path -name "*.h" -o -name "*.hpp"` ; do
    if [ -f $filepath ]; then
        if [ $flag -eq 0 ]; then
            includepath=$include_dir/include/$dirstr/${filepath##*/}
        else
            includepath=$include_dir/include/${filepath##*/}
        fi
        # 创建软链接
        ln -s $filepath $includepath 1>/dev/null 2>/dev/null
    fi
done