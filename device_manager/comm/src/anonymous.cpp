/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "anonymous.h"
#include "mbedtls/md.h"
#include "securec.h"
#include "softbus_adapter_log.h"

namespace {
constexpr int SHA_HASH_LEN = 32;
constexpr int SHORT_DEVICE_ID_HASH_LENGTH = 8;
constexpr int HEXIFY_UNIT_LEN = 2;
constexpr int HEX_DIGIT_MAX_NUM = 16;
constexpr int DEC_MAX_NUM = 10;
#define HEXIFY_LEN(len) ((len) * HEXIFY_UNIT_LEN + 1)
constexpr uint32_t MAX_MESSAGE_LEN = 40 * 1024 * 1024;
}  // namespace

std::string GetAnonyString(const std::string &value)
{
    const int32_t INT32_SHORT_ID_LENGTH = 20;
    const int32_t INT32_PLAINTEXT_LENGTH = 4;
    const int32_t INT32_MIN_ID_LENGTH = 3;

    std::string tmpStr("******");
    size_t strLen = value.length();
    if (strLen < INT32_MIN_ID_LENGTH) {
        return tmpStr;
    }

    std::string res;
    if (strLen <= INT32_SHORT_ID_LENGTH) {
        res += value[0];
        res += tmpStr;
        res += value[strLen - 1];
    } else {
        res.append(value, 0, INT32_PLAINTEXT_LENGTH);
        res += tmpStr;
        res.append(value, strLen - INT32_PLAINTEXT_LENGTH, INT32_PLAINTEXT_LENGTH);
    }

    return res;
}

std::string GetAnonyInt32(const int32_t value)
{
    std::string tempString = std::to_string(value);
    size_t length = tempString.length();
    if (length == 0x01) {
        tempString[0] = '*';
        return tempString;
    }
    for (size_t i = 1; i < length - 1; i++) {
        tempString[i] = '*';
    }
    return tempString;
}

bool IsNumberString(const std::string &inputString)
{
    LOG_INFO("IsNumberString for DeviceManagerNapi");
    if (inputString.length() == 0) {
        LOG_ERR("inputString is Null");
        return false;
    }
    const int32_t MIN_ASCLL_NUM = 48;
    const int32_t MAX_ASCLL_NUM = 57;
    for (size_t i = 0; i < inputString.length(); i++) {
        int num = (int)inputString[i];
        if (num >= MIN_ASCLL_NUM && num <= MAX_ASCLL_NUM) {
            continue;
        } else {
            return false;
        }
    }
    return true;
}

bool IsString(const nlohmann::json &jsonObj, const std::string &key)
{
    bool res = jsonObj.contains(key) && jsonObj[key].is_string() && jsonObj[key].size() <= MAX_MESSAGE_LEN;
    if (!res) {
        LOG_ERR("the key %s in jsonObj is invalid.", key.c_str());
    }
    return res;
}

bool IsInt32(const nlohmann::json &jsonObj, const std::string &key)
{
    bool res = jsonObj.contains(key) && jsonObj[key].is_number_integer() && jsonObj[key] >= INT32_MIN &&
               jsonObj[key] <= INT32_MAX;
    if (!res) {
        LOG_ERR("the key %s in jsonObj is invalid.", key.c_str());
    }
    return res;
}

bool IsInt64(const nlohmann::json &jsonObj, const std::string &key)
{
    bool res = jsonObj.contains(key) && jsonObj[key].is_number_integer() && jsonObj[key] >= INT64_MIN &&
               jsonObj[key] <= INT64_MAX;
    if (!res) {
        LOG_ERR("the key %s in jsonObj is invalid.", key.c_str());
    }
    return res;
}

bool IsArray(const nlohmann::json &jsonObj, const std::string &key)
{
    bool res = jsonObj.contains(key) && jsonObj[key].is_array();
    if (!res) {
        LOG_ERR("the key %s in jsonObj is invalid.", key.c_str());
    }
    return res;
}

bool IsBool(const nlohmann::json &jsonObj, const std::string &key)
{
    bool res = jsonObj.contains(key) && jsonObj[key].is_boolean();
    if (!res) {
        LOG_ERR("the key %s in jsonObj is invalid.", key.c_str());
    }
    return res;
}

int32_t GenerateStrHash(const unsigned char *str, uint32_t len, unsigned char *hash)
{
    if (str == nullptr || hash == nullptr || len == 0) {
        return ERR_DEV_CENTER_INPUT_PARA_INVALID;
    }

    mbedtls_md_context_t ctx;
    const mbedtls_md_info_t *info = NULL;
    mbedtls_md_init(&ctx);

    info = mbedtls_md_info_from_type(MBEDTLS_MD_SHA256);
    if (mbedtls_md_setup(&ctx, info, 0) != 0) {
        mbedtls_md_free(&ctx);
        return ERR_FAILED;
    }
    if (mbedtls_md_starts(&ctx) != 0) {
        mbedtls_md_free(&ctx);
        return ERR_FAILED;
    }
    if (mbedtls_md_update(&ctx, str, len) != 0) {
        mbedtls_md_free(&ctx);
        return ERR_FAILED;
    }
    if (mbedtls_md_finish(&ctx, hash) != 0) {
        mbedtls_md_free(&ctx);
        return ERR_FAILED;
    }

    mbedtls_md_free(&ctx);
    return DEV_CENTER_OK;
}

int32_t ConvertBytesToHexString(char *outBuf, uint32_t outBufLen, const unsigned char *inBuf, uint32_t inLen)
{
    if ((outBuf == nullptr) || (inBuf == nullptr) || (outBufLen < HEXIFY_LEN(inLen))) {
        return ERR_DEV_CENTER_INPUT_PARA_INVALID;
    }

    while (inLen > 0) {
        unsigned char h = *inBuf / HEX_DIGIT_MAX_NUM;
        unsigned char l = *inBuf % HEX_DIGIT_MAX_NUM;
        if (h < DEC_MAX_NUM) {
            *outBuf++ = '0' + h;
        } else {
            *outBuf++ = 'a' + h - DEC_MAX_NUM;
        }
        if (l < DEC_MAX_NUM) {
            *outBuf++ = '0' + l;
        } else {
            *outBuf++ = 'a' + l - DEC_MAX_NUM;
        }
        ++inBuf;
        inLen--;
    }
    return DEV_CENTER_OK;
}

int32_t GenRandInt(int32_t randMin, int32_t randMax)
{
    srand(time(NULL));
    return (randMin + rand() % (randMax - randMin));
}

int32_t GetUdidHash(const std::string &udid, unsigned char *udidHash)
{
    char hashResult[SHA_HASH_LEN] = {0};
    int32_t ret = GenerateStrHash((const uint8_t *)udid.c_str(), strlen(udid.c_str()) + 1, (uint8_t *)hashResult);
    if (ret != DEV_CENTER_OK) {
        LOG_ERR("GenerateStrHash failed");
        return ret;
    }
    ret = ConvertBytesToHexString((char *)udidHash, SHORT_DEVICE_ID_HASH_LENGTH + 1, (const uint8_t *)hashResult,
                                  SHORT_DEVICE_ID_HASH_LENGTH / HEXIFY_UNIT_LEN);
    if (ret != DEV_CENTER_OK) {
        LOG_ERR("ConvertBytesToHexString failed");
        return ret;
    }
    return DEV_CENTER_OK;
}

uint16_t GenRandUint(uint16_t randMin, uint16_t randMax)
{
    srand(time(NULL));
    return (randMin + rand() % (randMax - randMin));
}

int64_t GenRandLongLong(int64_t randMin, int64_t randMax)
{
    static int64_t reqId = randMin;
    reqId++;
    reqId = reqId % randMax + 1;
    return reqId;
}

int32_t WriteAuthDeviceInfo(OHOS::Parcel &data, const AuthDeviceInfo &info)
{
    int32_t infoLen;
    infoLen = sizeof(AuthDeviceInfo);
    if (!data.WriteInt32(infoLen)) {
        LOG_ERR("WriteAuthDeviceInfo write node size failed!");
        return ERR_FAILED;
    }
    if (!data.WriteUnpadBuffer((void *)(&info), infoLen)) {
        LOG_ERR("WriteAuthDeviceInfo write node info failed!");
        return ERR_FAILED;
    }
    return DEV_CENTER_OK;
}

int32_t ReadAuthDeviceInfo(OHOS::Parcel &data, AuthDeviceInfo &info)
{
    (void)memset_s(&info, sizeof(AuthDeviceInfo), 0, sizeof(AuthDeviceInfo));
    int32_t infoLen;
    infoLen = sizeof(AuthDeviceInfo);

    int32_t bufferSize = data.ReadInt32();
    if (bufferSize != infoLen) {
        LOG_ERR("ReadAuthDeviceInfo read node size failed!");
        return ERR_FAILED;
    }

    const void *infoPtr = data.ReadUnpadBuffer(infoLen);
    if (infoPtr == nullptr) {
        LOG_ERR("ReadAuthDeviceInfo read node failed!");
        return ERR_FAILED;
    }
    if (memcpy_s(&info, infoLen, infoPtr, infoLen) != EOK) {
        LOG_ERR("ReadAuthDeviceInfo memcpy_s failed!");
        return ERR_FAILED;
    }
    return DEV_CENTER_OK;
}