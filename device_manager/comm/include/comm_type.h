/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef COMM_TYPE_H
#define COMM_TYPE_H

#include "devicemgr_comm.h"

enum DeviceMgrFuncId {
    SERVER_REG_STATE_CB = 0,
    SERVER_UNREG_STATE_CB,
    SERVER_REG_UI_CB,
    SERVER_UNREG_UI_CB,
    SERVER_DESTORY_CLIENT,

    SERVER_GET_LOCAL_DEVICE_INFO = 100,
    SERVER_GET_TRUSTED_DEVICE_LIST,
    SERVER_GET_DEVICE_INFO,
    SERVER_PUBLISH_DEVICE_DISCOVERY,
    SERVER_UNPUBLISH_DEVICE_DISCOVERY,
    SERVER_START_DEVICE_DISCOVERY,
    SERVER_STOP_DEVICE_DISCOVERY,
    SERVER_AUTH_DEVICE,
    SERVER_UNAUTH_DEVICE,

    CLIENT_ON_DEVICE_FOUND = 200,
    CLIENT_ON_DISCOVERY_SUCC,
    CLIENT_ON_DISCOVERY_FAIL,
    CLIENT_ON_PUBLISH_RESULT,
    CLIENT_ON_DEVICE_CHANGED,
    CLIENT_ON_SHOW_AUTH_STATE,
    CLIENT_ON_SHOW_AUTH_INFO,
    CLIENT_ON_USER_OPERATION,
    CLIENT_ON_CANCEL_DISPLAY,
    CLIENT_ON_INPUT_AUTH_INFO,
};

#endif  // COMM_TYPE_H