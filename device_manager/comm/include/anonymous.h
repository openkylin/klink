/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANONYMOUS_H
#define ANONYMOUS_H

#include <stdio.h>
#include <string>
#include "parcel.h"
#include "comm_type.h"
#include "nlohmann/json.hpp"

std::string GetAnonyString(const std::string &value);
std::string GetAnonyInt32(const int32_t value);
bool IsNumberString(const std::string &inputString);
bool IsString(const nlohmann::json &jsonObj, const std::string &key);
bool IsInt32(const nlohmann::json &jsonObj, const std::string &key);
bool IsInt64(const nlohmann::json &jsonObj, const std::string &key);
bool IsArray(const nlohmann::json &jsonObj, const std::string &key);
bool IsBool(const nlohmann::json &jsonObj, const std::string &key);
int32_t GenRandInt(int32_t randMin, int32_t randMax);
uint16_t GenRandUint(uint16_t randMin, uint16_t randMax);
int32_t GetUdidHash(const std::string &udid, unsigned char *udidHash);
int64_t GenRandLongLong(int64_t randMin, int64_t randMax);

int32_t WriteAuthDeviceInfo(OHOS::Parcel &data, const AuthDeviceInfo &info);
int32_t ReadAuthDeviceInfo(OHOS::Parcel &data, AuthDeviceInfo &info);

#endif  // ANONYMOUS_H
