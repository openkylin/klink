/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "devicemanagersdk.h"

#include <unistd.h>
#include <map>
#include "devicemgripc.h"
#include "comm_type.h"
#include "anonymous.h"

#define DEV_SDK_LOG(fmt, args...) printf("[DeviceMgrClient][%s:%u]" fmt "\n", __func__, __LINE__, ##args);

static bool s_isInit = false;
static const char *s_pkgName = NULL;
static const ISoftbusStateCallback *s_deviceStateCb = NULL;
static const IUIStateUpdateCallback *s_uiStateUpdateCb = NULL;
static const ISoftbusPublishCallback *s_publishCb = NULL;
static const ISoftbusDiscoveryCallback *s_discoveryCb = NULL;

using DeviceMgrClientFunc = int32_t (*)(OHOS::Parcel &data, OHOS::Parcel &reply);
static std::map<uint32_t, DeviceMgrClientFunc> s_memberFuncMap;

static int32_t ClientOnPublishResult(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    if (s_publishCb == NULL && s_publishCb->OnPublishResult == NULL) {
        DEV_SDK_LOG("publish result callback is NULL");
        return ERR_DEV_CENTER_POINT_NULL;
    }
    const char *pkgName = data.ReadCString();
    if (pkgName == NULL && (strcmp(pkgName, s_pkgName) != 0)) {
        DEV_SDK_LOG("Invalid package name");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t publishId = -1;
    int32_t publishResult = -1;
    if (!data.ReadInt32(publishId)) {
        DEV_SDK_LOG("read publishId failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.ReadInt32(publishResult)) {
        DEV_SDK_LOG("read publishResult failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    s_publishCb->OnPublishResult(publishId, publishResult);

    return DEV_CENTER_OK;
}

static int32_t ClientOnDeviceFound(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    if (s_discoveryCb == NULL && s_discoveryCb->OnDeviceFound == NULL) {
        DEV_SDK_LOG("discovery result callback is NULL");
        return ERR_DEV_CENTER_POINT_NULL;
    }
    const char *pkgName = data.ReadCString();
    if (pkgName == NULL && (strcmp(pkgName, s_pkgName) != 0)) {
        DEV_SDK_LOG("Invalid package name");
        return ERR_DEV_CENTER_FAILED;
    }
    bool isOnline = false;
    AuthDeviceInfo info;
    if (!data.ReadBool(isOnline)) {
        DEV_SDK_LOG("read Online status failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (ReadAuthDeviceInfo(data, info) != DEV_CENTER_OK) {
        DEV_SDK_LOG("read info failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    s_discoveryCb->OnDeviceFound(info, isOnline);

    return DEV_CENTER_OK;
}

static int32_t ClientOnDiscoverySuccess(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    if (s_discoveryCb == NULL && s_discoveryCb->OnDiscoverySuccess == NULL) {
        DEV_SDK_LOG("discovery result callback is NULL");
        return ERR_DEV_CENTER_POINT_NULL;
    }
    const char *pkgName = data.ReadCString();
    if (pkgName == NULL && (strcmp(pkgName, s_pkgName) != 0)) {
        DEV_SDK_LOG("Invalid package name");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t subscribeId = -1;
    if (!data.WriteInt32(subscribeId)) {
        DEV_SDK_LOG("read subscribeId failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    s_discoveryCb->OnDiscoverySuccess(subscribeId);

    return DEV_CENTER_OK;
}

static int32_t ClientOnDiscoveryFailed(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    if (s_discoveryCb == NULL && s_discoveryCb->OnDiscoveryFailed == NULL) {
        DEV_SDK_LOG("discovery result callback is NULL");
        return ERR_DEV_CENTER_POINT_NULL;
    }
    const char *pkgName = data.ReadCString();
    if (pkgName == NULL && (strcmp(pkgName, s_pkgName) != 0)) {
        DEV_SDK_LOG("Invalid package name");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t subscribeId = -1;
    int32_t failedReason = -1;
    if (!data.WriteInt32(subscribeId)) {
        DEV_SDK_LOG("read subscribeId failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.WriteInt32(failedReason)) {
        DEV_SDK_LOG("read failedReason failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    s_discoveryCb->OnDiscoveryFailed(subscribeId, failedReason);

    return DEV_CENTER_OK;
}

static int32_t ClientOnDeviceChanged(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    if (s_deviceStateCb == NULL && s_deviceStateCb->OnDeviceChanged == NULL) {
        DEV_SDK_LOG("device state callback is NULL");
        return ERR_DEV_CENTER_POINT_NULL;
    }
    const char *pkgName = data.ReadCString();
    if (pkgName == NULL && (strcmp(pkgName, s_pkgName) != 0)) {
        DEV_SDK_LOG("Invalid package name");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t state = -1;
    AuthDeviceInfo info;
    if (!data.ReadInt32(state)) {
        DEV_SDK_LOG("read state failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (ReadAuthDeviceInfo(data, info) != DEV_CENTER_OK) {
        DEV_SDK_LOG("read info failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    s_deviceStateCb->OnDeviceChanged((AuthDeviceState)state, info);

    return DEV_CENTER_OK;
}

static int32_t ClientOnShowAuthState(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    if (s_uiStateUpdateCb == NULL && s_uiStateUpdateCb->OnShowAuthState == NULL) {
        DEV_SDK_LOG("ui callback is NULL");
        return ERR_DEV_CENTER_POINT_NULL;
    }
    const char *pkgName = data.ReadCString();
    if (pkgName == NULL && (strcmp(pkgName, s_pkgName) != 0)) {
        DEV_SDK_LOG("Invalid package name");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t msgType = -1;
    if (!data.ReadInt32(msgType)) {
        DEV_SDK_LOG("read msgType failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    s_uiStateUpdateCb->OnShowAuthState(msgType);

    return DEV_CENTER_OK;
}

static int32_t ClientOnShowAuthInfo(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    if (s_uiStateUpdateCb == NULL && s_uiStateUpdateCb->OnShowAuthInfo == NULL) {
        DEV_SDK_LOG("ui callback is NULL");
        return ERR_DEV_CENTER_POINT_NULL;
    }
    const char *pkgName = data.ReadCString();
    if (pkgName == NULL && (strcmp(pkgName, s_pkgName) != 0)) {
        DEV_SDK_LOG("Invalid package name");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t code = -1;
    if (!data.ReadInt32(code)) {
        DEV_SDK_LOG("read code failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    s_uiStateUpdateCb->OnShowAuthInfo(code);

    return DEV_CENTER_OK;
}

static int32_t ClientOnUserOperation(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    if (s_uiStateUpdateCb == NULL && s_uiStateUpdateCb->OnUserOperation == NULL) {
        DEV_SDK_LOG("ui callback is NULL");
        return ERR_DEV_CENTER_POINT_NULL;
    }
    const char *pkgName = data.ReadCString();
    if (pkgName == NULL && (strcmp(pkgName, s_pkgName) != 0)) {
        DEV_SDK_LOG("Invalid package name");
        return ERR_DEV_CENTER_FAILED;
    }
    const char *remoteName = data.ReadCString();
    if (remoteName == NULL) {
        DEV_SDK_LOG("read remoteName failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t retReply = s_uiStateUpdateCb->OnUserOperation(remoteName);
    if (!reply.WriteInt32(retReply)) {
        DEV_SDK_LOG("write reply failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    return DEV_CENTER_OK;
}

static int32_t ClientOnCancelDisplay(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    if (s_uiStateUpdateCb == NULL && s_uiStateUpdateCb->OnCancelDisplay == NULL) {
        DEV_SDK_LOG("ui callback is NULL");
        return ERR_DEV_CENTER_POINT_NULL;
    }
    const char *pkgName = data.ReadCString();
    if (pkgName == NULL && (strcmp(pkgName, s_pkgName) != 0)) {
        DEV_SDK_LOG("Invalid package name");
        return ERR_DEV_CENTER_FAILED;
    }

    s_uiStateUpdateCb->OnCancelDisplay();

    return DEV_CENTER_OK;
}

static int32_t ClientOnInputAuthInfo(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    if (s_uiStateUpdateCb == NULL && s_uiStateUpdateCb->OnInputAuthInfo == NULL) {
        DEV_SDK_LOG("ui callback is NULL");
        return ERR_DEV_CENTER_POINT_NULL;
    }
    const char *pkgName = data.ReadCString();
    if (pkgName == NULL && (strcmp(pkgName, s_pkgName) != 0)) {
        DEV_SDK_LOG("Invalid package name");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t retReply = s_uiStateUpdateCb->OnInputAuthInfo();
    if (!reply.WriteInt32(retReply)) {
        DEV_SDK_LOG("write reply failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    return DEV_CENTER_OK;
}

static void InitMemberSdkFuncMap()
{
    s_memberFuncMap[CLIENT_ON_DEVICE_FOUND] = &ClientOnDeviceFound;
    s_memberFuncMap[CLIENT_ON_DISCOVERY_SUCC] = &ClientOnDiscoverySuccess;
    s_memberFuncMap[CLIENT_ON_DISCOVERY_FAIL] = &ClientOnDiscoveryFailed;
    s_memberFuncMap[CLIENT_ON_PUBLISH_RESULT] = &ClientOnPublishResult;
    s_memberFuncMap[CLIENT_ON_DEVICE_CHANGED] = &ClientOnDeviceChanged;
    s_memberFuncMap[CLIENT_ON_SHOW_AUTH_STATE] = &ClientOnShowAuthState;
    s_memberFuncMap[CLIENT_ON_SHOW_AUTH_INFO] = &ClientOnShowAuthInfo;
    s_memberFuncMap[CLIENT_ON_USER_OPERATION] = &ClientOnUserOperation;
    s_memberFuncMap[CLIENT_ON_CANCEL_DISPLAY] = &ClientOnCancelDisplay;
    s_memberFuncMap[CLIENT_ON_INPUT_AUTH_INFO] = &ClientOnInputAuthInfo;
}

static void OnRequest(int32_t code, OHOS::Parcel &data, OHOS::Parcel &reply)
{
    DEV_SDK_LOG("Client, code = %d", code);
    if (s_memberFuncMap.find(code) != s_memberFuncMap.end()) {
        int32_t retReply = s_memberFuncMap[code](data, reply);
        if ((retReply != DEV_CENTER_OK) && !reply.WriteInt32(retReply)) {
            DEV_SDK_LOG("Request:%d write reply failed!", code);
            return;
        }
    }

    return;
}

static DeviceIpcRequest s_ipcRequest = {.OnRequest = OnRequest};

void InitDeviceManagerClient(const char *pkgName)
{
    if (s_isInit) {
        return;
    }
    s_pkgName = pkgName;
    InitMemberSdkFuncMap();
    InitIpc(false, &s_ipcRequest);
    s_isInit = true;
    DEV_SDK_LOG("init client success!");
}

int32_t RegisterDeviceStateCallback(const char *pkgName, const ISoftbusStateCallback *callback)
{
    if (pkgName == NULL) {
        DEV_SDK_LOG("pkgName is invalid");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!s_isInit) {
        InitDeviceManagerClient(pkgName);
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(pkgName)) {
        DEV_SDK_LOG("write pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.WriteInt32((int32_t)getpid())) {
        DEV_SDK_LOG("write pid failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t ret = SendRequest(SERVER_REG_STATE_CB, ResultOption::TF_SYNC, data, reply);
    if (ret != DEV_CENTER_OK) {
        DEV_SDK_LOG("SendRequest failed! ret = %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    if (!reply.ReadInt32(ret)) {
        DEV_SDK_LOG("read ret failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (ret == DEV_CENTER_OK) {
        s_deviceStateCb = callback;
    }
    return ret;
}

int32_t UnRegisterDeviceStateCallback(const char *pkgName)
{
    if (pkgName == NULL) {
        DEV_SDK_LOG("pkgName is invalid");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!s_isInit) {
        InitDeviceManagerClient(pkgName);
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(pkgName)) {
        DEV_SDK_LOG("write pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t ret = SendRequest(SERVER_UNREG_STATE_CB, ResultOption::TF_SYNC, data, reply);
    if (ret != DEV_CENTER_OK) {
        DEV_SDK_LOG("SendRequest failed! ret = %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    if (!reply.ReadInt32(ret)) {
        DEV_SDK_LOG("read ret failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (ret == DEV_CENTER_OK) {
        s_deviceStateCb = NULL;
    }
    return ret;
}

int32_t RegisterUIStateCallback(const char *pkgName, const IUIStateUpdateCallback *callback)
{
    if (pkgName == NULL) {
        DEV_SDK_LOG("pkgName is invalid");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!s_isInit) {
        InitDeviceManagerClient(pkgName);
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(pkgName)) {
        DEV_SDK_LOG("write pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.WriteInt32((int32_t)getpid())) {
        DEV_SDK_LOG("write pid failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t ret = SendRequest(SERVER_REG_UI_CB, ResultOption::TF_SYNC, data, reply);
    if (ret != DEV_CENTER_OK) {
        DEV_SDK_LOG("SendRequest failed! ret = %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    if (!reply.ReadInt32(ret)) {
        DEV_SDK_LOG("read ret failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (ret == DEV_CENTER_OK) {
        s_uiStateUpdateCb = callback;
    }
    return ret;
}

int32_t UnRegisterUIStateCallback(const char *pkgName)
{
    if (pkgName == NULL) {
        DEV_SDK_LOG("pkgName is invalid");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!s_isInit) {
        InitDeviceManagerClient(pkgName);
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(pkgName)) {
        DEV_SDK_LOG("write pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t ret = SendRequest(SERVER_UNREG_UI_CB, ResultOption::TF_SYNC, data, reply);
    if (ret != DEV_CENTER_OK) {
        DEV_SDK_LOG("SendRequest failed! ret = %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    if (!reply.ReadInt32(ret)) {
        DEV_SDK_LOG("read ret failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (ret == DEV_CENTER_OK) {
        s_uiStateUpdateCb = NULL;
    }
    return ret;
}

int32_t GetLocalDeviceInfo(const char *pkgName, AuthDeviceInfo &info)
{
    if (pkgName == NULL) {
        DEV_SDK_LOG("pkgName is invalid");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!s_isInit) {
        InitDeviceManagerClient(pkgName);
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(pkgName)) {
        DEV_SDK_LOG("write pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t ret = SendRequest(SERVER_GET_LOCAL_DEVICE_INFO, ResultOption::TF_SYNC, data, reply);
    if (ret != DEV_CENTER_OK) {
        DEV_SDK_LOG("SendRequest failed! ret = %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    if (ReadAuthDeviceInfo(reply, info) != DEV_CENTER_OK) {
        DEV_SDK_LOG("read node info failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    return DEV_CENTER_OK;
}

int32_t PublishDeviceDiscovery(const char *pkgName, const PublishInfo &publishInfo,
                               const ISoftbusPublishCallback *callback)
{
    if (pkgName == NULL) {
        DEV_SDK_LOG("pkgName is invalid");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!s_isInit) {
        InitDeviceManagerClient(pkgName);
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(pkgName)) {
        DEV_SDK_LOG("write pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.WriteInt32((int32_t)getpid())) {
        DEV_SDK_LOG("write pid failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.WriteInt32(publishInfo.publishId) || !data.WriteInt32(publishInfo.mode) ||
        !data.WriteInt32(publishInfo.medium) || !data.WriteInt32(publishInfo.freq) ||
        !data.WriteCString(publishInfo.capability)) {
        DEV_SDK_LOG("write publish common publishInfo failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.WriteUint32(publishInfo.dataLen)) {
        DEV_SDK_LOG("write capabilityData length failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (publishInfo.dataLen != 0 && !data.WriteCString((const char *)publishInfo.capabilityData)) {
        DEV_SDK_LOG("write capabilityData failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.WriteBool(publishInfo.ranging)) {
        DEV_SDK_LOG("write ranging failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t ret = SendRequest(SERVER_PUBLISH_DEVICE_DISCOVERY, ResultOption::TF_SYNC, data, reply);
    if (ret != DEV_CENTER_OK) {
        DEV_SDK_LOG("SendRequest failed! ret = %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    if (!reply.ReadInt32(ret)) {
        DEV_SDK_LOG("read ret failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (ret == DEV_CENTER_OK) {
        s_publishCb = callback;
    }
    return ret;
}

int32_t UnPublishDeviceDiscovery(const char *pkgName, int32_t publishId)
{
    if (pkgName == NULL) {
        DEV_SDK_LOG("pkgName is invalid");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!s_isInit) {
        InitDeviceManagerClient(pkgName);
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(pkgName)) {
        DEV_SDK_LOG("write pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.WriteInt32(publishId)) {
        DEV_SDK_LOG("write publishId failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t ret = SendRequest(SERVER_UNPUBLISH_DEVICE_DISCOVERY, ResultOption::TF_SYNC, data, reply);
    if (ret != DEV_CENTER_OK) {
        DEV_SDK_LOG("SendRequest failed! ret = %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    if (!reply.ReadInt32(ret)) {
        DEV_SDK_LOG("read ret failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (ret == DEV_CENTER_OK) {
        s_publishCb = NULL;
    }
    return ret;
}

int32_t StartDeviceDiscovery(const char *pkgName, const SubscribeInfo &subscribeInfo,
                             const ISoftbusDiscoveryCallback *callback)
{
    if (pkgName == NULL) {
        DEV_SDK_LOG("pkgName is invalid");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!s_isInit) {
        InitDeviceManagerClient(pkgName);
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(pkgName)) {
        DEV_SDK_LOG("write pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.WriteInt32((int32_t)getpid())) {
        DEV_SDK_LOG("write pid failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.WriteInt32(subscribeInfo.subscribeId) || !data.WriteInt32(subscribeInfo.mode) ||
        !data.WriteInt32(subscribeInfo.medium) || !data.WriteInt32(subscribeInfo.freq)) {
        DEV_SDK_LOG("write publish common subscribeInfo failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.WriteBool(subscribeInfo.isSameAccount) || !data.WriteBool(subscribeInfo.isWakeRemote) ||
        !data.WriteCString(subscribeInfo.capability)) {
        DEV_SDK_LOG("write flag and capability failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.WriteUint32(subscribeInfo.dataLen)) {
        DEV_SDK_LOG("write capabilityData length failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (subscribeInfo.dataLen != 0 && !data.WriteCString((const char *)subscribeInfo.capabilityData)) {
        DEV_SDK_LOG("write capabilityData failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t ret = SendRequest(SERVER_START_DEVICE_DISCOVERY, ResultOption::TF_SYNC, data, reply);
    if (ret != DEV_CENTER_OK) {
        DEV_SDK_LOG("SendRequest failed! ret = %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    if (!reply.ReadInt32(ret)) {
        DEV_SDK_LOG("read ret failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (ret == DEV_CENTER_OK) {
        s_discoveryCb = callback;
    }
    return ret;
}

int32_t StopDeviceDiscovery(const char *pkgName, int32_t subscribeId)
{
    if (pkgName == NULL) {
        DEV_SDK_LOG("pkgName is invalid");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!s_isInit) {
        InitDeviceManagerClient(pkgName);
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(pkgName)) {
        DEV_SDK_LOG("write pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.WriteInt32(subscribeId)) {
        DEV_SDK_LOG("write subscribeId failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t ret = SendRequest(SERVER_STOP_DEVICE_DISCOVERY, ResultOption::TF_SYNC, data, reply);
    if (ret != DEV_CENTER_OK) {
        DEV_SDK_LOG("SendRequest failed! ret = %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    if (!reply.ReadInt32(ret)) {
        DEV_SDK_LOG("read ret failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (ret == DEV_CENTER_OK) {
        s_discoveryCb = NULL;
    }
    return ret;
}

int32_t AuthenticateDevice(const char *pkgName, const char *deviceId)
{
    if (pkgName == NULL) {
        DEV_SDK_LOG("pkgName is invalid");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!s_isInit) {
        InitDeviceManagerClient(pkgName);
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(pkgName)) {
        DEV_SDK_LOG("write pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.WriteCString(deviceId)) {
        DEV_SDK_LOG("write deviceId failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t ret = SendRequest(SERVER_AUTH_DEVICE, ResultOption::TF_SYNC, data, reply);
    if (ret != DEV_CENTER_OK) {
        DEV_SDK_LOG("SendRequest failed! ret = %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    if (!reply.ReadInt32(ret)) {
        DEV_SDK_LOG("read ret failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    return ret;
}

int32_t UnAuthenticateDevice(const char *pkgName, const char *networkId)
{
    if (pkgName == NULL) {
        DEV_SDK_LOG("pkgName is invalid");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!s_isInit) {
        InitDeviceManagerClient(pkgName);
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(pkgName)) {
        DEV_SDK_LOG("write pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.WriteCString(networkId)) {
        DEV_SDK_LOG("write networkId failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t ret = SendRequest(SERVER_UNAUTH_DEVICE, ResultOption::TF_SYNC, data, reply);
    if (ret != DEV_CENTER_OK) {
        DEV_SDK_LOG("SendRequest failed! ret = %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    if (!reply.ReadInt32(ret)) {
        DEV_SDK_LOG("read ret failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    return ret;
}

int32_t GetTrustedDeviceList(const char *pkgName, AuthDeviceInfo **deviceInfo, int *infoNum)
{
    if (pkgName == NULL) {
        DEV_SDK_LOG("pkgName is invalid");
        return ERR_DEV_CENTER_FAILED;
    }
    if (infoNum == NULL) {
        DEV_SDK_LOG("infoNum is invalid");
        return ERR_DEV_CENTER_FAILED;
    }
    *infoNum = 0;
    if (!s_isInit) {
        InitDeviceManagerClient(pkgName);
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(pkgName)) {
        DEV_SDK_LOG("write pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t ret = SendRequest(SERVER_GET_TRUSTED_DEVICE_LIST, ResultOption::TF_SYNC, data, reply);
    if (ret != DEV_CENTER_OK) {
        DEV_SDK_LOG("SendRequest failed! ret = %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t retInfoNum;
    if (!reply.ReadInt32(retInfoNum)) {
        DEV_SDK_LOG("read infoNum failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (retInfoNum > 0) {
        *infoNum = retInfoNum;
        int32_t infoLen = 0;
        if (!reply.ReadInt32(infoLen)) {
            DEV_SDK_LOG("read infoLen failed!");
            return ERR_DEV_CENTER_FAILED;
        }
        if (infoLen != retInfoNum * sizeof(AuthDeviceInfo)) {
            DEV_SDK_LOG("read info size failed!");
            return ERR_DEV_CENTER_FAILED;
        }
        void *retBuf = (void *)reply.ReadUnpadBuffer(infoLen);
        if (retBuf == NULL) {
            DEV_SDK_LOG("read node failed!");
            return ERR_DEV_CENTER_FAILED;
        }
        *deviceInfo = static_cast<AuthDeviceInfo *>(malloc(sizeof(AuthDeviceInfo) * (retInfoNum)));
        if (*deviceInfo == NULL) {
            DEV_SDK_LOG("malloc node failed!");
            return ERR_DEV_CENTER_FAILED;
        }
        AuthDeviceInfo *tmpInfo = static_cast<AuthDeviceInfo *>(retBuf);
        *deviceInfo = tmpInfo;
    }
    return DEV_CENTER_OK;
}

int32_t DestoryClient(const char *pkgName)
{
    if (pkgName == NULL) {
        DEV_SDK_LOG("pkgName is invalid");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!s_isInit) {
        InitDeviceManagerClient(pkgName);
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(pkgName)) {
        DEV_SDK_LOG("write pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.WriteInt32((int32_t)getpid())) {
        DEV_SDK_LOG("write pid failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t ret = SendRequest(SERVER_DESTORY_CLIENT, ResultOption::TF_SYNC, data, reply);
    if (ret != DEV_CENTER_OK) {
        DEV_SDK_LOG("SendRequest failed! ret = %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    if (!reply.ReadInt32(ret)) {
        DEV_SDK_LOG("read ret failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    s_deviceStateCb = NULL;
    s_discoveryCb = NULL;
    s_uiStateUpdateCb = NULL;
    s_publishCb = NULL;
    s_isInit = false;
    s_pkgName = NULL;

    return ret;
}