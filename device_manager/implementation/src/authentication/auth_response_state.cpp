/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "auth_response_state.h"

#include "auth_device_manager.h"
#include "constants.h"
#include "softbus_adapter_log.h"

int32_t AuthResponseState::Leave()
{
    return DEV_CENTER_OK;
}

int32_t AuthResponseState::SetAuthContext(std::shared_ptr<AuthResponseContext> context)
{
    context_ = std::move(context);
    return DEV_CENTER_OK;
}

std::shared_ptr<AuthResponseContext> AuthResponseState::GetAuthContext()
{
    return context_;
}

int32_t AuthResponseState::SetAuthManager(std::shared_ptr<AuthManager> authManager)
{
    authManager_ = std::move(authManager);
    return DEV_CENTER_OK;
}

int32_t AuthResponseState::TransitionTo(std::shared_ptr<AuthResponseState> state)
{
    DEV_SERV_LOG("AuthRequestState::TransitionTo");
    std::shared_ptr<AuthManager> stateAuthManager = authManager_.lock();
    if (stateAuthManager == nullptr) {
        DEV_SERV_LOG("AuthRequestState::authManager_ null");
        return ERR_FAILED;
    }
    state->SetAuthManager(stateAuthManager);
    stateAuthManager->SetAuthResponseState(state);
    state->SetAuthContext(context_);
    this->Leave();
    state->Enter();
    return DEV_CENTER_OK;
}

int32_t AuthResponseInitState::GetStateType()
{
    return AuthState::AUTH_RESPONSE_INIT;
}

int32_t AuthResponseInitState::Enter()
{
    DEV_SERV_LOG("AuthResponse::AuthResponseInitState Enter");
    return DEV_CENTER_OK;
}

int32_t AuthResponseNegotiateState::GetStateType()
{
    return AuthState::AUTH_RESPONSE_NEGOTIATE;
}

int32_t AuthResponseNegotiateState::Enter()
{
    std::shared_ptr<AuthManager> stateAuthManager = authManager_.lock();
    if (stateAuthManager == nullptr) {
        DEV_SERV_LOG("AuthRequestState::authManager_ null");
        return ERR_FAILED;
    }
    stateAuthManager->RespNegotiate(context_->sessionId);
    return DEV_CENTER_OK;
}

int32_t AuthResponseConfirmState::GetStateType()
{
    return AuthState::AUTH_RESPONSE_CONFIRM;
}

int32_t AuthResponseConfirmState::Enter()
{
    DEV_SERV_LOG("AuthResponse::AuthResponseConfirmState Enter");
    std::shared_ptr<AuthManager> stateAuthManager = authManager_.lock();
    if (stateAuthManager == nullptr) {
        DEV_SERV_LOG("AuthRequestState::authManager_ null");
        return ERR_FAILED;
    }
    stateAuthManager->ShowConfigDialog();
    return DEV_CENTER_OK;
}

int32_t AuthResponseGroupState::GetStateType()
{
    return AuthState::AUTH_RESPONSE_GROUP;
}

int32_t AuthResponseGroupState::Enter()
{
    DEV_SERV_LOG("AuthResponse::AuthResponseGroupState Enter");
    std::shared_ptr<AuthManager> stateAuthManager = authManager_.lock();
    if (stateAuthManager == nullptr) {
        DEV_SERV_LOG("AuthRequestState::authManager_ null");
        return ERR_FAILED;
    }
    stateAuthManager->CreateGroup();
    return DEV_CENTER_OK;
}

int32_t AuthResponseShowState::GetStateType()
{
    return AuthState::AUTH_RESPONSE_SHOW;
}

int32_t AuthResponseShowState::Enter()
{
    std::shared_ptr<AuthManager> stateAuthManager = authManager_.lock();
    if (stateAuthManager == nullptr) {
        DEV_SERV_LOG("AuthRequestState::authManager_ null");
        return ERR_FAILED;
    }
    stateAuthManager->ShowAuthInfoDialog();
    return DEV_CENTER_OK;
}

int32_t AuthResponseFinishState::GetStateType()
{
    return AuthState::AUTH_RESPONSE_FINISH;
}

int32_t AuthResponseFinishState::Enter()
{
    std::shared_ptr<AuthManager> stateAuthManager = authManager_.lock();
    if (stateAuthManager == nullptr) {
        DEV_SERV_LOG("AuthRequestState::authManager_ null");
        return ERR_FAILED;
    }
    stateAuthManager->AuthenticateFinish();
    return DEV_CENTER_OK;
}
