/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "device_manager_center.h"
#include "securec.h"
#include <string>
#include <algorithm>
#include <map>
#include "parameter.h"
#include "anonymous.h"
#include "auth_message_processor.h"

#define DISTRIBUTED_HARDWARE_DEVICEMANAGER_SA_ID 4802

static bool s_isInit = false;

static std::shared_ptr<AuthManager> s_authMgr = nullptr;
static SoftbusConnector *s_softbusConnector = nullptr;
static HiChainConnector *s_hiChainConnector = nullptr;
static std::map<std::string, const ISoftbusDiscoveryCallback *> s_discoveryCallbackMap = {};
static std::map<std::string, const ISoftbusPublishCallback *> s_publishCallbackMap = {};
static std::map<std::string, const ISoftbusStateCallback *> s_stateCallbackMap = {};
static const ISoftbusStateCallback *s_deviceStateCallback = nullptr;
static const IUIStateUpdateCallback *s_uiStateUpdateCb = nullptr;
static const IAuthResultCallback *s_authResultCb = nullptr;

const char *g_authParam =
    "{\"authType\": 1, \"appIcon\": '', \"appThumbnail\": '', \"extraInfo\": {\"targetPkgName\": 'ohos.samples.etsdevicemanager', \"appName\": 'eTSDeviceManger', \"appDescription\": 'eTSDeviceManger Ability', \"business\": '0'}}";

std::string GetDeviceIdByNetWorkId(const char *networkId)
{
    std::string udid;
    std::string udidHash;
    if (SoftbusConnector::GetUdidByNetworkId(networkId, udid) != DEV_CENTER_OK) {
        DEV_SERV_LOG("get udid failed!");
        return "";
    }
    udidHash = SoftbusConnector::GetDeviceUdidHashByUdid(udid);
    return udidHash;
}

static void OnDevCenterPublishResult(int publishId, PublishResult result)
{
    DEV_SERV_LOG("publishId: %d, result: %d.", publishId, result);
}

static IPublishCb s_devicePublishCb = {
    .OnPublishResult = OnDevCenterPublishResult,
};

int32_t ConvertNodeBasicInfoToAuthDevice(const NodeBasicInfo &nodeBasicInfo, AuthDeviceInfo &authDeviceInfo)
{
    (void)memset_s(&authDeviceInfo, sizeof(AuthDeviceInfo), 0, sizeof(AuthDeviceInfo));
    if (memcpy_s(authDeviceInfo.networkId, sizeof(authDeviceInfo.networkId), nodeBasicInfo.networkId,
                 std::min(sizeof(authDeviceInfo.networkId), sizeof(nodeBasicInfo.networkId))) != DEV_CENTER_OK) {
        DEV_SERV_LOG("copy networkId data failed.");
    }

    if (memcpy_s(authDeviceInfo.deviceName, sizeof(authDeviceInfo.deviceName), nodeBasicInfo.deviceName,
                 std::min(sizeof(authDeviceInfo.deviceName), sizeof(nodeBasicInfo.deviceName))) != DEV_CENTER_OK) {
        DEV_SERV_LOG("copy deviceName data failed.");
    }
    authDeviceInfo.deviceTypeId = nodeBasicInfo.deviceTypeId;
    return DEV_CENTER_OK;
}

static void OnSoftBusDeviceOnline(NodeBasicInfo *info)
{
    DEV_SERV_LOG("begin.");
    if (info == nullptr) {
        DEV_SERV_LOG("NodeBasicInfo is nullptr.");
        return;
    }
    AuthDeviceInfo authDeviceInfo;
    ConvertNodeBasicInfoToAuthDevice(*info, authDeviceInfo);
    std::string deviceId = GetDeviceIdByNetWorkId(info->networkId);
    if (memcpy_s(authDeviceInfo.deviceId, sizeof(authDeviceInfo.deviceId), deviceId.c_str(),
                 std::min(sizeof(authDeviceInfo.deviceId), sizeof(deviceId.c_str()))) != DEV_CENTER_OK) {
        DEV_SERV_LOG("deviceId copy data failed.");
    }
    DEV_SERV_LOG("start handle device online event.");
    {
        if (s_deviceStateCallback != nullptr && s_deviceStateCallback->OnDeviceChanged != nullptr) {
            s_deviceStateCallback->OnDeviceChanged(AuthDeviceState::DEVICE_STATE_ONLINE, authDeviceInfo);
        }
    }

    DEV_SERV_LOG("device online, deviceId: %s.", GetAnonyString(authDeviceInfo.deviceId).c_str());
}

static void OnSoftbusDeviceOffline(NodeBasicInfo *info)
{
    DEV_SERV_LOG("begin.");
    if (info == nullptr) {
        DEV_SERV_LOG("NodeBasicInfo is nullptr.");
        return;
    }
    AuthDeviceInfo authDeviceInfo;
    ConvertNodeBasicInfoToAuthDevice(*info, authDeviceInfo);
    std::string deviceId = GetDeviceIdByNetWorkId(info->networkId);
    if (memcpy_s(authDeviceInfo.deviceId, sizeof(authDeviceInfo.deviceId), deviceId.c_str(),
                 std::min(sizeof(authDeviceInfo.deviceId), sizeof(deviceId.c_str()))) != DEV_CENTER_OK) {
        DEV_SERV_LOG("deviceId copy data failed.");
    }

    DEV_SERV_LOG("start handle device offline event.");

    if (s_deviceStateCallback != nullptr && s_deviceStateCallback->OnDeviceChanged != nullptr) {
        s_deviceStateCallback->OnDeviceChanged(AuthDeviceState::DEVICE_STATE_OFFLINE, authDeviceInfo);
    }

    DEV_SERV_LOG("device offline, deviceId: %s.", GetAnonyString(authDeviceInfo.deviceId).c_str());
}

static void OnSoftbusDeviceInfoChanged(NodeBasicInfoType type, NodeBasicInfo *info)
{
    DEV_SERV_LOG("begin.");
    if (info == nullptr) {
        DEV_SERV_LOG("NodeBasicInfo is nullptr.");
        return;
    }
    if (type == NodeBasicInfoType::TYPE_DEVICE_NAME || type == NodeBasicInfoType::TYPE_NETWORK_INFO) {
        DEV_SERV_LOG("DeviceInfo %d change.", type);
        AuthDeviceInfo authDeviceInfo;
        int32_t networkType = -1;
        if (type == NodeBasicInfoType::TYPE_NETWORK_INFO) {
            if (GetNodeKeyInfo(DEV_CENTER_PKG_NAME, info->networkId, NodeDeviceInfoKey::NODE_KEY_NETWORK_TYPE,
                               reinterpret_cast<uint8_t *>(&networkType), LNN_COMMON_LEN) != DEV_CENTER_OK) {
                DEV_SERV_LOG("GetNodeKeyInfo networkType failed.");
                return;
            }
            DEV_SERV_LOG("NetworkType %d.", networkType);
        }
        ConvertNodeBasicInfoToAuthDevice(*info, authDeviceInfo);
        authDeviceInfo.networkType = networkType;
        std::string deviceId = GetDeviceIdByNetWorkId(info->networkId);
        if (memcpy_s(authDeviceInfo.deviceId, sizeof(authDeviceInfo.deviceId), deviceId.c_str(),
                     std::min(sizeof(authDeviceInfo.deviceId), sizeof(deviceId.c_str()))) != DEV_CENTER_OK) {
            DEV_SERV_LOG("deviceId copy data failed.");
        }
        DEV_SERV_LOG("start handle device name change event.");

        if (s_deviceStateCallback != nullptr && s_deviceStateCallback->OnDeviceChanged != nullptr) {
            s_deviceStateCallback->OnDeviceChanged(AuthDeviceState::DEVICE_INFO_CHANGED, authDeviceInfo);
        }
        return;
    }
}

static INodeStateCb s_softbusNodeStateCb = {.events = EVENT_NODE_STATE_ONLINE | EVENT_NODE_STATE_OFFLINE |
                                                      EVENT_NODE_STATE_INFO_CHANGED,
                                            .onNodeOnline = OnSoftBusDeviceOnline,
                                            .onNodeOffline = OnSoftbusDeviceOffline,
                                            .onNodeBasicInfoChanged = OnSoftbusDeviceInfoChanged};

static int OnSessionOpened(int sessionId, int result)
{
    DEV_SERV_LOG("begin.");
    if (s_authMgr != nullptr) {
        return s_authMgr->OnSessionOpened(sessionId, result);
    }
    return ERR_DEV_CENTER_NOT_INIT;
}

static void OnSessionClosed(int sessionId)
{
    DEV_SERV_LOG("begin.");
    if (s_authMgr != nullptr) {
        s_authMgr->OnSessionClosed(sessionId);
    }
}

static void OnBytesReceived(int sessionId, const void *data, unsigned int dataLen)
{
    DEV_SERV_LOG("begin.");
    if (s_authMgr != nullptr) {
        if (s_authMgr->GetIsCryptoSupport()) {
            DEV_SERV_LOG("Start decryption.");
        }
        s_authMgr->OnDataReceived(sessionId, data, dataLen);
    }
}

static ISessionListener s_sessionListener = {.OnSessionOpened = OnSessionOpened,
                                             .OnSessionClosed = OnSessionClosed,
                                             .OnBytesReceived = OnBytesReceived,
                                             .OnMessageReceived = nullptr,
                                             .OnStreamReceived = nullptr};

static void OnAuthResult(const std::string &pkgName, const std::string &deviceId, const std::string &token,
                         int32_t status, int32_t reason)
{
    DEV_SERV_LOG("pkgName = %s, token = %s, status = %d, reason = %d", pkgName.c_str(), token.c_str(), status, reason);
    s_authResultCb->OnAuthResult(deviceId, token, status, reason);
}

static void OnVerifyAuthResult(const std::string &pkgName, const std::string &deviceId, int32_t resultCode,
                               const std::string &flag)
{
    DEV_SERV_LOG("pkgName = %s, resultCode = %d, flag = %s", pkgName.c_str(), resultCode, flag.c_str());
}

static int32_t SetUserOperation(int32_t action, const std::string &params)
{
    DEV_SERV_LOG("begin.");
    if (s_authMgr != nullptr) {
        return s_authMgr->OnUserOperation(action, params);
    }
    return ERR_DEV_CENTER_NOT_INIT;
}

static void OnUiCall(const std::string &pkgName, std::string &paramJson)
{
    DEV_SERV_LOG("option: %s", pkgName.c_str());
    if (s_uiStateUpdateCb == nullptr) {
        DEV_SERV_LOG("callback is nullptr!");
        return;
    }
    if (strcmp(pkgName.c_str(), UICALL_PKGNAME_SHOWCONFIGDIALOG) == 0) {
        // 收到认证请求，用户操作是否信任
        std::string remoteName = "未知设备";
        if (!paramJson.empty()) {
            nlohmann::json msgJsonObj = nlohmann::json::parse(paramJson, nullptr, false);
            if (msgJsonObj.is_discarded()) {
                DEV_SERV_LOG("recv jsonStr error");
            }
            if (!IsString(msgJsonObj, TAG_REQUESTER)) {
                DEV_SERV_LOG("err json string, first time");
            }
            remoteName = msgJsonObj[TAG_REQUESTER].get<std::string>();
        }
        int32_t ret = s_uiStateUpdateCb->OnUserOperation(remoteName.c_str());
        if (ret == 0) {
            SetUserOperation(USER_OPERATION_TYPE_ALLOW_DEV_CENTER_ALWAYS, paramJson);
        } else {
            SetUserOperation(USER_OPERATION_TYPE_CANCEL_AUTH, paramJson);
        }
    } else if (strcmp(pkgName.c_str(), UICALL_PKGNAME_UPDATESTATE) == 0) {
        // 更新认证UI状态显示
        int32_t msgType = 0;
        if (!paramJson.empty()) {
            nlohmann::json msgJsonObj = nlohmann::json::parse(paramJson, nullptr, false);
            if (msgJsonObj.is_discarded()) {
                DEV_SERV_LOG("recv jsonStr error");
            }
            if (!IsInt32(msgJsonObj, UI_STATE_MSG)) {
                DEV_SERV_LOG("err json string, first time");
            }
            msgType = msgJsonObj[UI_STATE_MSG].get<int32_t>();
        }
        DEV_SERV_LOG("updateState msgType: %d", msgType);
        s_uiStateUpdateCb->OnShowAuthState(msgType);
    } else if (strcmp(pkgName.c_str(), UICALL_PKGNAME_SHOWAUTHDIALOG) == 0) {
        // PIN码显示
        int32_t code = 0;
        if (!paramJson.empty()) {
            nlohmann::json msgJsonObj = nlohmann::json::parse(paramJson, nullptr, false);
            if (msgJsonObj.is_discarded()) {
                DEV_SERV_LOG("recv jsonStr error");
            }
            if (!IsInt32(msgJsonObj, PIN_CODE_KEY)) {
                DEV_SERV_LOG("err json string, first time");
            }
            code = msgJsonObj[PIN_CODE_KEY].get<int32_t>();
        }
        s_uiStateUpdateCb->OnShowAuthInfo(code);
    } else if (strcmp(pkgName.c_str(), UICALL_PKGNAME_SHOWSTARTAUTHDIALOG) == 0) {
        // 输入PIN码
        int32_t pinCode = s_uiStateUpdateCb->OnInputAuthInfo();
        if (pinCode > 0) {
            SetUserOperation(USER_OPERATION_TYPE_DONE_PINCODE_INPUT, std::to_string(pinCode));
        } else {
            SetUserOperation(USER_OPERATION_TYPE_CANCEL_PINCODE_INPUT, paramJson);
        }
    } else {
        // 取消弹窗显示
        s_uiStateUpdateCb->OnCancelDisplay();
    }
}

static int32_t OnVerifyAuthentication(std::string &authToken, const std::string &authParam)
{
    DEV_SERV_LOG("begin.");
    if (authParam.length() == 1) {
        if (authParam == std::string(EVENT_CONFIRM_CODE)) {
            return DEV_CENTER_OK;
        }
        DEV_SERV_LOG("Peer rejection");
        return ERR_DEV_CENTER_FAILED;
    }
    nlohmann::json authParamJson = nlohmann::json::parse(authParam, nullptr, false);
    if (authParamJson.is_discarded()) {
        DEV_SERV_LOG("DecodeRequestAuth jsonStr error");
        return ERR_DEV_CENTER_FAILED;
    }
    nlohmann::json authTokenJson = nlohmann::json::parse(authToken, nullptr, false);
    if (authTokenJson.is_discarded()) {
        DEV_SERV_LOG("DecodeRequestAuth jsonStr error");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!IsInt32(authTokenJson, PIN_CODE_KEY) || !IsString(authTokenJson, PIN_TOKEN) ||
        !IsInt32(authParamJson, PIN_CODE_KEY) || !IsString(authParamJson, PIN_TOKEN)) {
        DEV_SERV_LOG("err json string.");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t code = authTokenJson[PIN_CODE_KEY].get<int32_t>();
    std::string pinToken = authTokenJson[PIN_TOKEN].get<std::string>();
    int32_t inputPinCode = authParamJson[PIN_CODE_KEY].get<int32_t>();
    std::string inputPinToken = authParamJson[PIN_TOKEN].get<std::string>();
    if (code == inputPinCode && pinToken == inputPinToken) {
        return DEV_CENTER_OK;
    } else if (code != inputPinCode) {
        return ERR_DEV_CENTER_INPUT_PARA_INVALID;
    } else {
        return ERR_DEV_CENTER_FAILED;
    }
}

static IAuthManagerCallback s_authManagerCb = {.OnAuthResult = OnAuthResult,
                                               .OnVerifyAuthResult = OnVerifyAuthResult,
                                               .OnUiCall = OnUiCall,
                                               .OnVerifyAuthentication = OnVerifyAuthentication};

static void OnGroupCreated(int64_t requestId, const std::string &groupId)
{
    DEV_SERV_LOG("begin.");
    if (s_authMgr != nullptr) {
        s_authMgr->OnGroupCreated(requestId, groupId);
    }
}

static void OnMemberJoin(int64_t requestId, int32_t status)
{
    DEV_SERV_LOG("begin.");
    if (s_authMgr != nullptr) {
        s_authMgr->OnMemberJoin(requestId, status);
    }
}

static std::string GetConnectAddr(std::string deviceId)
{
    DEV_SERV_LOG("begin.");
    if (s_authMgr != nullptr) {
        return s_authMgr->GetConnectAddr(deviceId);
    }
    return "";
}

static int32_t GetPinCode()
{
    DEV_SERV_LOG("begin.");
    if (s_authMgr != nullptr) {
        return s_authMgr->GetPinCode();
    }
    return ERR_DEV_CENTER_NOT_START;
}

static IHiChainConnectorCallback s_hiChainConnectorCb = {.OnGroupCreated = OnGroupCreated,
                                                         .OnMemberJoin = OnMemberJoin,
                                                         .GetConnectAddr = GetConnectAddr,
                                                         .GetPinCode = GetPinCode};

static void OnGroupResult(int64_t requestId, int32_t action, const std::string &resultInfo)
{
    DEV_SERV_LOG("requestId = %lld, action = %d, resultInfo = %s", requestId, action, resultInfo.c_str());
}

static IGroupResCallback s_groupResCb = {.OnGroupResult = OnGroupResult};

static PublishInfo s_publishInfo = {.publishId = DISTRIBUTED_HARDWARE_DEVICEMANAGER_SA_ID,
                                    .mode = DISCOVER_MODE_ACTIVE,
                                    .medium = COAP,
                                    .freq = HIGH,
                                    .capability = DEV_CENTER_CAPABILITY_OSD,
                                    .ranging = false};

int32_t InitDeviceManagerCenter()
{
    if (s_isInit) {
        DEV_SERV_LOG("is already Initialize");
        return DEV_CENTER_OK;
    }
    DEV_SERV_LOG("Initialize");

    int32_t ret = CreateSessionServer(DEV_CENTER_PKG_NAME, DEV_CENTER_SESSION_NAME, &s_sessionListener, true);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("CreateSessionServer failed, ret: %d.", ret);
        return ERR_DEV_CENTER_INIT_FAILED;
    }

    ret = RegNodeDeviceStateCb(DEV_CENTER_PKG_NAME, &s_softbusNodeStateCb);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("RegNodeDeviceStateCb failed, ret: %d.", ret);
        return ERR_DEV_CENTER_INIT_FAILED;
    }

    ret = PublishLNN(DEV_CENTER_PKG_NAME, &s_publishInfo, &s_devicePublishCb);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("PublishLNN failed, ret: %d.", ret);
        return ERR_DEV_CENTER_INIT_FAILED;
    }

    if (s_softbusConnector == nullptr) {
        s_softbusConnector = new SoftbusConnector();
    }

    if (s_hiChainConnector == nullptr) {
        s_hiChainConnector = new HiChainConnector();
        s_hiChainConnector->RegisterHiChainCallback(&s_hiChainConnectorCb);
        s_hiChainConnector->RegisterHiChainGroupCallback(&s_groupResCb);
    }

    if (s_authMgr == nullptr) {
        s_authMgr = std::make_shared<AuthManager>();
        s_authMgr->RegisterAuthManagerCallback(&s_authManagerCb, s_softbusConnector, s_hiChainConnector);
    }
    s_isInit = true;
    DEV_SERV_LOG("init success.");
    return DEV_CENTER_OK;
}

int32_t DestoryDeviceManagerCenter()
{
    if (!s_isInit) {
        return ERR_DEV_CENTER_POINT_NULL;
    }
    if (s_softbusConnector != nullptr) {
        s_softbusConnector->UnRegisterSoftbusPublishCallback();
        s_softbusConnector->UnRegisterSoftbusDiscoveryCallback();
    }
    if (s_hiChainConnector != nullptr) {
        s_hiChainConnector->UnRegisterHiChainCallback();
        s_hiChainConnector->UnRegisterHiChainGroupCallback();
    }

    if (s_authMgr != nullptr) {
        s_authMgr->UnRegisterAuthManagerCallback();
    }
    DEV_SERV_LOG("begin.");
    int ret = RemoveSessionServer(DEV_CENTER_PKG_NAME, DEV_CENTER_SESSION_NAME);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("RemoveSessionServer failed, ret: %d.", ret);
        return ERR_FAILED;
    }
    ret = StopPublishLNN(DEV_CENTER_PKG_NAME, s_publishInfo.publishId);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("StopPublishLNN failed, ret: %d.", ret);
        return ERR_DEV_CENTER_INIT_FAILED;
    }
    return DEV_CENTER_OK;
}

int32_t RegisterPublishCallback(const ISoftbusPublishCallback *callback)
{
    if (s_softbusConnector != nullptr) {
        s_softbusConnector->RegisterSoftbusPublishCallback(callback);
        return DEV_CENTER_OK;
    }
    return ERR_DEV_CENTER_NOT_INIT;
}

int32_t UnRegisterPublishCallback()
{
    if (s_softbusConnector != nullptr) {
        s_softbusConnector->UnRegisterSoftbusPublishCallback();
        return DEV_CENTER_OK;
    }
    return ERR_DEV_CENTER_NOT_INIT;
}

int32_t RegisterDiscoverCallback(const ISoftbusDiscoveryCallback *callback)
{
    if (s_softbusConnector != nullptr) {
        s_softbusConnector->RegisterSoftbusDiscoveryCallback(callback);
        return DEV_CENTER_OK;
    }
    return ERR_DEV_CENTER_NOT_INIT;
}

int32_t UnRegisterDiscoverCallback(const std::string &pkgName)
{
    if (s_softbusConnector != nullptr) {
        s_softbusConnector->UnRegisterSoftbusDiscoveryCallback();
        return DEV_CENTER_OK;
    }
    return ERR_DEV_CENTER_NOT_INIT;
}

int32_t RegisterDeviceStateCallback(const ISoftbusStateCallback *callback)
{
    s_deviceStateCallback = callback;
    return DEV_CENTER_OK;
}

int32_t UnRegisterDeviceStateCallback()
{
    s_deviceStateCallback = nullptr;
    return DEV_CENTER_OK;
}

int32_t RegisterUIStateCallback(const IUIStateUpdateCallback *callback)
{
    s_uiStateUpdateCb = callback;
    return DEV_CENTER_OK;
}

int32_t UnRegisterUIStateCallback()
{
    s_uiStateUpdateCb = nullptr;
    return DEV_CENTER_OK;
}

int32_t RegisterAuthResultCallback(const IAuthResultCallback *callback)
{
    s_authResultCb = callback;
    return DEV_CENTER_OK;
}

int32_t UnRegisterAuthResultCallback()
{
    s_authResultCb = nullptr;
    return DEV_CENTER_OK;
}

int32_t GetLocalDeviceInfo(AuthDeviceInfo &info)
{
    DEV_SERV_LOG("begin.");
    NodeBasicInfo nodeBasicInfo;
    int32_t ret = GetLocalNodeDeviceInfo(DEV_CENTER_PKG_NAME, &nodeBasicInfo);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("GetLocalNodeDeviceInfo: failed, ret: %d.", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    ConvertNodeBasicInfoToAuthDevice(nodeBasicInfo, info);
    char localDeviceId[DEVICE_UUID_LENGTH] = {0};
    char udidHash[DEVICE_UUID_LENGTH] = {0};
    GetDevUdid(localDeviceId, DEVICE_UUID_LENGTH);
    GetUdidHash(localDeviceId, (unsigned char *)udidHash);

    if (memcpy_s(info.deviceId, DEV_CENTER_MAX_DEVICE_ID_LEN, udidHash, strlen(udidHash)) != 0) {
        DEV_SERV_LOG("get deviceId: %s failed", GetAnonyString(std::string(udidHash)).c_str());
    }
    DEV_SERV_LOG("end.");
    return DEV_CENTER_OK;
}

int32_t PublishDeviceDiscovery(const PublishInfo &publishInfo)
{
    DEV_SERV_LOG("begin.");
    if (s_softbusConnector != nullptr) {
        return s_softbusConnector->PublishDiscovery(publishInfo);
    }
    DEV_SERV_LOG("end.");
    return ERR_DEV_CENTER_NOT_INIT;
}

int32_t UnPublishDeviceDiscovery(int32_t publishId)
{
    DEV_SERV_LOG("begin.");
    UnRegisterPublishCallback();
    if (s_softbusConnector != nullptr) {
        return s_softbusConnector->UnPublishDiscovery(publishId);
    }
    DEV_SERV_LOG("end.");
    return ERR_DEV_CENTER_NOT_INIT;
}

int32_t StartDeviceDiscovery(const SubscribeInfo &subscribeInfo)
{
    DEV_SERV_LOG("begin.");
    if (s_softbusConnector != nullptr) {
        return s_softbusConnector->StartDiscovery(subscribeInfo);
    }
    DEV_SERV_LOG("end.");
    return ERR_DEV_CENTER_NOT_INIT;
}

int32_t StopDeviceDiscovery(uint16_t subscribeId)
{
    DEV_SERV_LOG("begin.");
    if (s_softbusConnector != nullptr) {
        return s_softbusConnector->StopDiscovery(subscribeId);
    }
    DEV_SERV_LOG("end.");
    return ERR_DEV_CENTER_NOT_INIT;
}

int32_t AuthenticateDevice(const std::string &deviceId)
{
    if (deviceId.empty()) {
        DEV_SERV_LOG("Invalid parameter");
        return ERR_DEV_CENTER_INPUT_PARA_INVALID;
    }
    if (s_authMgr != nullptr) {
        return s_authMgr->AuthenticateDevice(std::string(DEV_CENTER_PKG_NAME), DEV_CENTER_TYPE_PIN, deviceId,
                                             std::string(g_authParam));
    }
    return ERR_DEV_CENTER_NOT_INIT;
}

int32_t UnAuthenticateDevice(const std::string &networkId)
{
    if (networkId.empty()) {
        DEV_SERV_LOG("Invalid parameter");
        return ERR_DEV_CENTER_INPUT_PARA_INVALID;
    }
    if (s_authMgr != nullptr) {
        return s_authMgr->UnAuthenticateDevice(std::string(DEV_CENTER_PKG_NAME), networkId);
    }
    return ERR_DEV_CENTER_NOT_INIT;
}

int32_t GetTrustedDeviceList(AuthDeviceInfo **info, int *infoNum)
{
    int devCount = 0;
    NodeBasicInfo *nodeInfo = nullptr;
    int32_t ret = GetAllNodeDeviceInfo(DEV_CENTER_PKG_NAME, &nodeInfo, &devCount);
    *infoNum = devCount;
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("failed, ret: %d.", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    *info = static_cast<AuthDeviceInfo *>(malloc(sizeof(AuthDeviceInfo) * (devCount)));
    if (*info == nullptr) {
        FreeNodeInfo(nodeInfo);
        return ERR_DEV_CENTER_FAILED;
    }
    for (int32_t i = 0; i < devCount; i++) {
        NodeBasicInfo *nodeBasicInfo = nodeInfo + i;
        AuthDeviceInfo *deviceInfo = *info + i;
        ConvertNodeBasicInfoToAuthDevice(*nodeBasicInfo, *deviceInfo);
        std::string deviceId = GetDeviceIdByNetWorkId(nodeBasicInfo->networkId);
        if (memcpy_s((*deviceInfo).deviceId, sizeof((*deviceInfo).deviceId), deviceId.c_str(),
                     std::min(sizeof((*deviceInfo).deviceId), sizeof(deviceId.c_str()))) != DEV_CENTER_OK) {
            DEV_SERV_LOG("deviceId copy data failed.");
        }
    }

    FreeNodeInfo(nodeInfo);

    DEV_SERV_LOG("success, deviceCount: %d.", devCount);
    return ret;
}

int32_t GetDeviceInfo(const std::string &networkId, AuthDeviceInfo &info)
{
    int32_t nodeInfoCount = 0;
    NodeBasicInfo *nodeInfo = nullptr;
    int32_t ret = GetAllNodeDeviceInfo(DEV_CENTER_PKG_NAME, &nodeInfo, &nodeInfoCount);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("GetAllNodeDeviceInfo failed, ret: %d.", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    for (int32_t i = 0; i < nodeInfoCount; ++i) {
        NodeBasicInfo *nodeBasicInfo = nodeInfo + i;
        if (networkId == nodeBasicInfo->networkId) {
            DEV_SERV_LOG("name : %s.", nodeBasicInfo->deviceName);
            if (memcpy_s(info.deviceName, sizeof(info.deviceName), nodeBasicInfo->deviceName,
                         std::min(sizeof(info.deviceName), sizeof(nodeBasicInfo->deviceName))) != DEV_CENTER_OK) {
                DEV_SERV_LOG("deviceName copy deviceName data failed.");
            }
            info.deviceTypeId = nodeBasicInfo->deviceTypeId;
            break;
        }
    }
    FreeNodeInfo(nodeInfo);
    DEV_SERV_LOG("complete, deviceName : %s, deviceTypeId : %d.", info.deviceName, info.deviceTypeId);
    return ret;
}