/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hichain_connector.h"

#include <cstdlib>
#include <ctime>
#include <functional>
#include <securec.h>

#include "anonymous.h"
#include "constants.h"
#include "softbus_adapter_log.h"
#include "softbus_auth.h"
#include "nlohmann/json.hpp"
#include "parameter.h"
#include "unistd.h"

const int32_t PIN_CODE_NETWORK = 0;
const int32_t CREDENTIAL_NETWORK = 1;
const int32_t DELAY_TIME_MS = 10000;  // 10ms
const int32_t FIELD_EXPIRE_TIME_VALUE = 7;

constexpr const char *DEVICE_ID = "DEVICE_ID";
constexpr const char *FIELD_CREDENTIAL = "credential";
constexpr const char *ADD_HICHAIN_GROUP_SUCCESS = "ADD_HICHAIN_GROUP_SUCCESS";
constexpr const char *ADD_HICHAIN_GROUP_FAILED = "ADD_HICHAIN_GROUP_FAILED";
constexpr const char *DEV_CENTER_CREATE_GROUP_SUCCESS = "DEV_CENTER_CREATE_GROUP_SUCCESS";
constexpr const char *DEV_CENTER_CREATE_GROUP_FAILED = "DEV_CENTER_CREATE_GROUP_FAILED";
constexpr const char *ADD_HICHAIN_GROUP_SUCCESS_MSG = "dm add member to group success.";
constexpr const char *ADD_HICHAIN_GROUP_FAILED_MSG = "dm add member to group failed.";
constexpr const char *DEV_CENTER_CREATE_GROUP_SUCCESS_MSG = "dm create group success.";
constexpr const char *DEV_CENTER_CREATE_GROUP_FAILED_MSG = "dm create group failed.";

void from_json(const nlohmann::json &jsonObject, GroupInfo &groupInfo)
{
    if (jsonObject.find(FIELD_GROUP_NAME) != jsonObject.end() && jsonObject.at(FIELD_GROUP_NAME).is_string()) {
        groupInfo.groupName = jsonObject.at(FIELD_GROUP_NAME).get<std::string>();
    }

    if (jsonObject.find(FIELD_GROUP_ID) != jsonObject.end() && jsonObject.at(FIELD_GROUP_ID).is_string()) {
        groupInfo.groupId = jsonObject.at(FIELD_GROUP_ID).get<std::string>();
    }

    if (jsonObject.find(FIELD_GROUP_OWNER) != jsonObject.end() && jsonObject.at(FIELD_GROUP_OWNER).is_string()) {
        groupInfo.groupOwner = jsonObject.at(FIELD_GROUP_OWNER).get<std::string>();
    }

    if (jsonObject.find(FIELD_GROUP_TYPE) != jsonObject.end() && jsonObject.at(FIELD_GROUP_TYPE).is_number_integer()) {
        groupInfo.groupType = jsonObject.at(FIELD_GROUP_TYPE).get<int32_t>();
    }

    if (jsonObject.find(FIELD_GROUP_VISIBILITY) != jsonObject.end() &&
        jsonObject.at(FIELD_GROUP_VISIBILITY).is_number_integer()) {
        groupInfo.groupVisibility = jsonObject.at(FIELD_GROUP_VISIBILITY).get<int32_t>();
    }

    if (jsonObject.find(FIELD_USER_ID) != jsonObject.end() && jsonObject.at(FIELD_USER_ID).is_string()) {
        groupInfo.userId = jsonObject.at(FIELD_USER_ID).get<std::string>();
    }
}

const IHiChainConnectorCallback *HiChainConnector::hiChainConnectorCallback_ = nullptr;
const IGroupResCallback *HiChainConnector::hiChainResCallback_ = nullptr;
int32_t HiChainConnector::networkStyle_ = PIN_CODE_NETWORK;
bool g_createGroupFlag = false;
bool g_deleteGroupFlag = false;
bool g_groupIsRedundance = false;

HiChainConnector::HiChainConnector()
{
    DEV_SERV_LOG("[HICHAIN]HiChainConnector constructor");
    deviceAuthCallback_ = {.onTransmit = nullptr,
                           .onSessionKeyReturned = nullptr,
                           .onFinish = HiChainConnector::onFinish,
                           .onError = HiChainConnector::onError,
                           .onRequest = HiChainConnector::onRequest};
    int32_t ret = SdkRegAuthCallback(DEV_CENTER_PKG_NAME, DEV_CENTER_PKG_NAME, &deviceAuthCallback_);
    if (ret != HC_SUCCESS) {
        DEV_SERV_LOG("[HICHAIN]fail to register callback to hachain with ret:%d.", ret);
        return;
    }
    DEV_SERV_LOG("[HICHAIN]HiChainConnector constructor success.");
}

HiChainConnector::~HiChainConnector()
{
    DEV_SERV_LOG("[HICHAIN]HiChainConnector destructor.");
}

int32_t HiChainConnector::RegisterHiChainCallback(const IHiChainConnectorCallback *callback)
{
    hiChainConnectorCallback_ = callback;
    return DEV_CENTER_OK;
}

int32_t HiChainConnector::UnRegisterHiChainCallback()
{
    hiChainConnectorCallback_ = nullptr;
    return DEV_CENTER_OK;
}

int32_t HiChainConnector::CreateGroup(int64_t requestId, const std::string &groupName)
{
    networkStyle_ = PIN_CODE_NETWORK;
    GroupInfo groupInfo;
    if (IsGroupCreated(groupName, groupInfo)) {
        DeleteGroup(groupInfo.groupId);
    }
    DEV_SERV_LOG("[HICHAIN]CreateGroup requestId %ld", requestId);
    char localDeviceId[DEVICE_UUID_LENGTH] = {0};
    GetDevUdid(localDeviceId, DEVICE_UUID_LENGTH);
    std::string sLocalDeviceId = localDeviceId;
    nlohmann::json jsonObj;
    jsonObj[FIELD_GROUP_TYPE] = GROUP_TYPE_PEER_TO_PEER_GROUP;
    jsonObj[FIELD_DEVICE_ID] = sLocalDeviceId;
    jsonObj[FIELD_GROUP_NAME] = groupName;
    jsonObj[FIELD_USER_TYPE] = 0;
    jsonObj[FIELD_GROUP_VISIBILITY] = GROUP_VISIBILITY_PUBLIC;
    jsonObj[FIELD_EXPIRE_TIME] = FIELD_EXPIRE_TIME_VALUE;
    int32_t userId = DEFAULT_OS_ACCOUNT_ID;
    if (userId < 0) {
        DEV_SERV_LOG("[HICHAIN]get current process account user id failed");
        return ERR_FAILED;
    }

    int32_t ret = SdkCreateGroup(userId, requestId, DEV_CENTER_PKG_NAME, jsonObj.dump().c_str());
    if (ret != 0) {
        DEV_SERV_LOG("[HICHAIN]fail to create group with ret:%d, requestId:%ld.", ret, requestId);
        return ERR_DEV_CENTER_CREATE_GROUP_FAILED;
    }
    return DEV_CENTER_OK;
}

bool HiChainConnector::IsGroupCreated(std::string groupName, GroupInfo &groupInfo)
{
    nlohmann::json jsonObj;
    jsonObj[FIELD_GROUP_NAME] = groupName.c_str();
    std::string queryParams = jsonObj.dump();
    std::vector<GroupInfo> groupList;
    if (GetGroupInfo(queryParams, groupList)) {
        groupInfo = groupList[0];
        return true;
    }
    return false;
}

bool HiChainConnector::IsRedundanceGroup(const std::string &userId, int32_t authType, std::vector<GroupInfo> &groupList)
{
    nlohmann::json jsonObj;
    jsonObj[FIELD_GROUP_TYPE] = authType;
    std::string queryParams = jsonObj.dump();

    int32_t osAccountUserId = DEFAULT_OS_ACCOUNT_ID;
    if (osAccountUserId < 0) {
        DEV_SERV_LOG("[HICHAIN]get current process account user id failed");
        return ERR_FAILED;
    }
    if (!GetGroupInfo(osAccountUserId, queryParams, groupList)) {
        return false;
    }
    for (auto iter = groupList.begin(); iter != groupList.end(); iter++) {
        if (iter->userId != userId) {
            return true;
        }
    }
    return false;
}

bool HiChainConnector::GetGroupInfo(const std::string &queryParams, std::vector<GroupInfo> &groupList)
{
    char *groupVec = nullptr;
    uint32_t num = 0;
    int32_t userId = DEFAULT_OS_ACCOUNT_ID;
    if (userId < 0) {
        DEV_SERV_LOG("[HICHAIN]get current process account user id failed");
        return false;
    }
    int32_t ret = SdkGetGroupInfo(userId, DEV_CENTER_PKG_NAME, queryParams.c_str(), &groupVec, &num);
    if (ret != 0) {
        DEV_SERV_LOG("[HICHAIN]fail to get group info with ret:%d.", ret);
        return false;
    }
    if (groupVec == nullptr) {
        DEV_SERV_LOG("[HICHAIN]return groups info point is nullptr");
        return false;
    }
    if (num == 0) {
        DEV_SERV_LOG("[HICHAIN]return groups info number is zero.");
        return false;
    }
    DEV_SERV_LOG("[HICHAIN]GetGroupInfo group(%s), groupNum(%u)", groupVec, num);
    std::string relatedGroups = std::string(groupVec);
    SdkDestroyInfo(&groupVec);
    nlohmann::json jsonObject = nlohmann::json::parse(relatedGroups);
    if (jsonObject.is_discarded()) {
        DEV_SERV_LOG("[HICHAIN]returnGroups parse error");
        return false;
    }
    if (!jsonObject.is_array()) {
        DEV_SERV_LOG("[HICHAIN]json string is not array.");
        return false;
    }
    std::vector<GroupInfo> groupInfos = jsonObject.get<std::vector<GroupInfo>>();
    if (groupInfos.size() == 0) {
        DEV_SERV_LOG("[HICHAIN]GetGroupInfo group failed, groupInfos is empty.");
        return false;
    }
    groupList = groupInfos;
    return true;
}

int32_t HiChainConnector::GetGroupInfo(const int32_t userId, const std::string &queryParams,
                                       std::vector<GroupInfo> &groupList)
{
    char *groupVec = nullptr;
    uint32_t num = 0;
    int32_t ret = SdkGetGroupInfo(userId, DEV_CENTER_PKG_NAME, queryParams.c_str(), &groupVec, &num);
    if (ret != 0) {
        DEV_SERV_LOG("[HICHAIN]fail to get group info with ret:%d.", ret);
        return false;
    }
    if (groupVec == nullptr) {
        DEV_SERV_LOG("[HICHAIN]return groups info point is nullptr");
        return false;
    }
    if (num == 0) {
        DEV_SERV_LOG("[HICHAIN]return groups info number is zero.");
        return false;
    }
    DEV_SERV_LOG("[HICHAIN]GetGroupInfo group(%s), groupNum(%ud)", groupVec, num);
    std::string relatedGroups = std::string(groupVec);
    SdkDestroyInfo(&groupVec);
    nlohmann::json jsonObject = nlohmann::json::parse(relatedGroups);
    if (jsonObject.is_discarded()) {
        DEV_SERV_LOG("[HICHAIN]returnGroups parse error");
        return false;
    }
    if (!jsonObject.is_array()) {
        DEV_SERV_LOG("[HICHAIN]json string is not array.");
        return false;
    }
    std::vector<GroupInfo> groupInfos = jsonObject.get<std::vector<GroupInfo>>();
    if (groupInfos.size() == 0) {
        DEV_SERV_LOG("[HICHAIN]GetGroupInfo group failed, groupInfos is empty.");
        return false;
    }
    groupList = groupInfos;
    return true;
}

AuthForm HiChainConnector::GetGroupType(const std::string &deviceId)
{
    std::vector<GroupInfo> groupList;
    int32_t ret = GetRelatedGroups(deviceId, groupList);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("[HICHAIN]GetGroupType get related groups failed");
        return AuthForm::INVALID_TYPE;
    }

    if (groupList.size() == 0) {
        DEV_SERV_LOG("[HICHAIN]GetGroupType group list is empty");
        return AuthForm::INVALID_TYPE;
    }

    AuthFormPriority highestPriority = AuthFormPriority::PRIORITY_PEER_TO_PEER;
    for (auto it = groupList.begin(); it != groupList.end(); ++it) {
        if (g_authFormPriorityMap.count(it->groupType) == 0) {
            DEV_SERV_LOG("[HICHAIN]GetGroupType unsupported auth form");
            return AuthForm::INVALID_TYPE;
        }
        AuthFormPriority priority = g_authFormPriorityMap.at(it->groupType);
        if (priority > highestPriority) {
            highestPriority = priority;
        }
    }

    if (highestPriority == AuthFormPriority::PRIORITY_IDENTICAL_ACCOUNT) {
        return AuthForm::IDENTICAL_ACCOUNT;
    } else if (highestPriority == AuthFormPriority::PRIORITY_ACROSS_ACCOUNT) {
        return AuthForm::ACROSS_ACCOUNT;
    } else if (highestPriority == AuthFormPriority::PRIORITY_PEER_TO_PEER) {
        return AuthForm::PEER_TO_PEER;
    }

    return AuthForm::INVALID_TYPE;
}

int32_t HiChainConnector::AddMember(const std::string &deviceId, const std::string &connectInfo)
{
    DEV_SERV_LOG("[HICHAIN]AddMember start.");
    nlohmann::json jsonObject = nlohmann::json::parse(connectInfo, nullptr, false);
    if (jsonObject.is_discarded()) {
        DEV_SERV_LOG("[HICHAIN]DecodeRequestAuth jsonStr error");
        return ERR_FAILED;
    }
    if (!IsString(jsonObject, TAG_DEVICE_ID) || !IsInt32(jsonObject, PIN_CODE_KEY) ||
        !IsString(jsonObject, TAG_GROUP_ID) || !IsInt64(jsonObject, TAG_REQUEST_ID) ||
        !IsString(jsonObject, TAG_GROUP_NAME)) {
        DEV_SERV_LOG("[HICHAIN]err json string.");
        return ERR_FAILED;
    }
    char localDeviceId[DEVICE_UUID_LENGTH] = {0};
    GetDevUdid(localDeviceId, DEVICE_UUID_LENGTH);
    std::string connectInfomation = GetConnectPara(deviceId, jsonObject[TAG_DEVICE_ID].get<std::string>());

    int32_t pinCode = jsonObject[PIN_CODE_KEY].get<int32_t>();
    std::string groupId = jsonObject[TAG_GROUP_ID].get<std::string>();
    nlohmann::json jsonObj;
    jsonObj[FIELD_GROUP_ID] = groupId;
    jsonObj[FIELD_GROUP_TYPE] = GROUP_TYPE_PEER_TO_PEER_GROUP;
    jsonObj[FIELD_PIN_CODE] = std::to_string(pinCode).c_str();
    jsonObj[FIELD_IS_ADMIN] = false;
    jsonObj[FIELD_DEVICE_ID] = localDeviceId;
    jsonObj[FIELD_GROUP_NAME] = jsonObject[TAG_GROUP_NAME].get<std::string>();
    jsonObj[FIELD_CONNECT_PARAMS] = connectInfomation.c_str();
    std::string tmpStr = jsonObj.dump();
    int64_t requestId = jsonObject[TAG_REQUEST_ID].get<int64_t>();
    int32_t userId = DEFAULT_OS_ACCOUNT_ID;
    if (userId < 0) {
        DEV_SERV_LOG("[HICHAIN]get current process account user id failed");
        return ERR_FAILED;
    }
    int32_t ret = SdkAddMemberToGroup(userId, requestId, DEV_CENTER_PKG_NAME, tmpStr.c_str());
    if (ret != 0) {
        DEV_SERV_LOG("[HICHAIN]fail to add number to hichain group with ret:%d.", ret);
    }
    DEV_SERV_LOG("[HICHAIN]AddMember completed");
    return ret;
}

void HiChainConnector::onFinish(int64_t requestId, int operationCode, const char *returnData)
{
    std::string data = "";
    if (returnData != nullptr) {
        data = std::string(returnData);
    }
    DEV_SERV_LOG("[HICHAIN]onFinish reqId:%ld, operation:%d", requestId, operationCode);
    if (operationCode == GroupOperationCode::MEMBER_JOIN) {
        DEV_SERV_LOG("[HICHAIN]Add Member To Group success");
        if (hiChainConnectorCallback_ != nullptr) {
            hiChainConnectorCallback_->OnMemberJoin(requestId, DEV_CENTER_OK);
        }
    }
    if (operationCode == GroupOperationCode::GROUP_CREATE) {
        DEV_SERV_LOG("[HICHAIN]Create group success");
        if (networkStyle_ == CREDENTIAL_NETWORK) {
            if (hiChainResCallback_ != nullptr) {
                int32_t importAction = 0;
                hiChainResCallback_->OnGroupResult(requestId, importAction, data);
                g_createGroupFlag = true;
            }
        } else {
            if (hiChainConnectorCallback_ != nullptr) {
                hiChainConnectorCallback_->OnMemberJoin(requestId, DEV_CENTER_OK);
                hiChainConnectorCallback_->OnGroupCreated(requestId, data);
            }
        }
    }
    if (operationCode == GroupOperationCode::MEMBER_DELETE) {
        DEV_SERV_LOG("[HICHAIN]Delete Member from group success");
    }
    if (operationCode == GroupOperationCode::GROUP_DISBAND) {
        if (networkStyle_ == CREDENTIAL_NETWORK && hiChainResCallback_ != nullptr) {
            if (!g_groupIsRedundance) {
                int32_t deleteAction = 1;
                hiChainResCallback_->OnGroupResult(requestId, deleteAction, data);
            }
            g_deleteGroupFlag = true;
        }
        DEV_SERV_LOG("[HICHAIN]Disband group success");
    }
}

void HiChainConnector::onError(int64_t requestId, int operationCode, int errorCode, const char *errorReturn)
{
    std::string data = "";
    if (errorReturn != nullptr) {
        data = std::string(errorReturn);
    }
    DEV_SERV_LOG("[HICHAIN]onError reqId:%ld, operation:%d, errorCode:%d.", requestId, operationCode, errorCode);
    if (operationCode == GroupOperationCode::MEMBER_JOIN) {
        DEV_SERV_LOG("[HICHAIN]Add Member To Group failed");
        if (hiChainConnectorCallback_ != nullptr) {
            hiChainConnectorCallback_->OnMemberJoin(requestId, ERR_FAILED);
        }
    }
    if (operationCode == GroupOperationCode::GROUP_CREATE) {
        DEV_SERV_LOG("[HICHAIN]Create group failed");
        if (networkStyle_ == CREDENTIAL_NETWORK) {
            if (hiChainResCallback_ != nullptr) {
                int32_t importAction = 0;
                hiChainResCallback_->OnGroupResult(requestId, importAction, data);
                g_createGroupFlag = true;
            }
        } else {
            if (hiChainConnectorCallback_ != nullptr) {
                hiChainConnectorCallback_->OnGroupCreated(requestId, "{}");
            }
        }
    }
    if (operationCode == GroupOperationCode::MEMBER_DELETE) {
        DEV_SERV_LOG("[HICHAIN]Delete Member from group failed");
    }
    if (operationCode == GroupOperationCode::GROUP_DISBAND) {
        if (networkStyle_ == CREDENTIAL_NETWORK && hiChainResCallback_ != nullptr) {
            if (!g_groupIsRedundance) {
                int32_t deleteAction = 1;
                hiChainResCallback_->OnGroupResult(requestId, deleteAction, data);
            }
            g_deleteGroupFlag = true;
        }
        DEV_SERV_LOG("[HICHAIN]Disband group failed");
    }
}

char *HiChainConnector::onRequest(int64_t requestId, int operationCode, const char *reqParams)
{
    (void)requestId;
    (void)reqParams;
    if (operationCode != GroupOperationCode::MEMBER_JOIN) {
        DEV_SERV_LOG("[HICHAIN]onRequest operationCode %d", operationCode);
        return nullptr;
    }
    if (hiChainConnectorCallback_ == nullptr) {
        DEV_SERV_LOG("[HICHAIN]onRequest hiChainConnectorCallback_ is nullptr.");
        return nullptr;
    }
    nlohmann::json jsonObj;
    int32_t pinCode = hiChainConnectorCallback_->GetPinCode();
    if (pinCode == ERR_DEV_CENTER_NOT_START) {
        jsonObj[FIELD_CONFIRMATION] = REQUEST_REJECTED;
    } else {
        jsonObj[FIELD_CONFIRMATION] = REQUEST_ACCEPTED;
    }
    jsonObj[FIELD_PIN_CODE] = std::to_string(pinCode).c_str();
    char localDeviceId[DEVICE_UUID_LENGTH] = {0};
    GetDevUdid(localDeviceId, DEVICE_UUID_LENGTH);
    jsonObj[FIELD_DEVICE_ID] = localDeviceId;

    std::string jsonStr = jsonObj.dump();
    char *buffer = strdup(jsonStr.c_str());
    return buffer;
}

int64_t HiChainConnector::GenRequestId()
{
    return GenRandLongLong(MIN_REQUEST_ID, MAX_REQUEST_ID);
}

std::string HiChainConnector::GetConnectPara(std::string deviceId, std::string reqDeviceId)
{
    DEV_SERV_LOG("[HICHAIN]GetConnectPara get addrInfo");
    if (hiChainConnectorCallback_ == nullptr) {
        DEV_SERV_LOG("[HICHAIN]GetConnectPara hiChainConnectorCallback_ is nullptr.");
        return "";
    }
    std::string connectAddr = hiChainConnectorCallback_->GetConnectAddr(deviceId);
    nlohmann::json jsonObject = nlohmann::json::parse(connectAddr, nullptr, false);
    if (jsonObject.is_discarded()) {
        DEV_SERV_LOG("[HICHAIN]DecodeRequestAuth jsonStr error");
        return connectAddr;
    }
    jsonObject[DEVICE_ID] = reqDeviceId;

    return jsonObject.dump();
}

int32_t HiChainConnector::GetRelatedGroups(const std::string &deviceId, std::vector<GroupInfo> &groupList)
{
    DEV_SERV_LOG("[HICHAIN]GetRelatedGroups Start to get local related groups.");
    uint32_t groupNum = 0;
    char *returnGroups = nullptr;
    int32_t userId = DEFAULT_OS_ACCOUNT_ID;
    if (userId < 0) {
        DEV_SERV_LOG("[HICHAIN]get current process account user id failed");
        return ERR_FAILED;
    }
    int32_t ret = SdkGetRelatedGroups(userId, DEV_CENTER_PKG_NAME, deviceId.c_str(), &returnGroups, &groupNum);
    if (ret != 0) {
        DEV_SERV_LOG("[HICHAIN] fail to get related groups with ret:%d.", ret);
        return ERR_FAILED;
    }
    if (returnGroups == nullptr) {
        DEV_SERV_LOG("[HICHAIN] return related goups point is nullptr");
        return ERR_FAILED;
    }
    if (groupNum == 0) {
        DEV_SERV_LOG("[HICHAIN]return related goups number is zero.");
        return ERR_FAILED;
    }
    std::string relatedGroups = std::string(returnGroups);
    nlohmann::json jsonObject = nlohmann::json::parse(relatedGroups);
    if (jsonObject.is_discarded()) {
        DEV_SERV_LOG("[HICHAIN]returnGroups parse error");
        return ERR_FAILED;
    }
    if (!jsonObject.is_array()) {
        DEV_SERV_LOG("[HICHAIN]jsonObject is not an array.");
        return ERR_FAILED;
    }
    std::vector<GroupInfo> groupInfos = jsonObject.get<std::vector<GroupInfo>>();
    if (groupInfos.empty()) {
        DEV_SERV_LOG("[HICHAIN]GetRelatedGroups group failed, groupInfos is empty.");
        return ERR_FAILED;
    }
    groupList = groupInfos;
    return DEV_CENTER_OK;
}

int32_t HiChainConnector::GetSyncGroupList(std::vector<GroupInfo> &groupList, std::vector<std::string> &syncGroupList)
{
    if (groupList.empty()) {
        DEV_SERV_LOG("[HICHAIN]groupList is empty.");
        return ERR_FAILED;
    }
    for (auto group : groupList) {
        if (IsGroupInfoInvalid(group)) {
            continue;
        }
        syncGroupList.push_back(group.groupId);
    }
    return DEV_CENTER_OK;
}

bool HiChainConnector::IsDevicesInGroup(const std::string &hostDevice, const std::string &peerDevice)
{
    DEV_SERV_LOG("[HICHAIN]IsDevicesInGroup");
    std::vector<GroupInfo> hostGroupInfoList;
    GetRelatedGroups(hostDevice, hostGroupInfoList);
    std::vector<GroupInfo> peerGroupInfoList;
    GetRelatedGroups(peerDevice, peerGroupInfoList);
    for (const auto &hostGroupInfo : hostGroupInfoList) {
        for (const auto &peerGroupInfo : peerGroupInfoList) {
            if (hostGroupInfo.groupId == peerGroupInfo.groupId && hostGroupInfo.groupName == peerGroupInfo.groupName) {
                DEV_SERV_LOG("[HICHAIN]these are authenticated");
                return true;
            }
        }
    }
    return false;
}

bool HiChainConnector::IsGroupInfoInvalid(GroupInfo &group)
{
    if (group.groupType == GROUP_TYPE_IDENTICAL_ACCOUNT_GROUP || group.groupVisibility == GROUP_VISIBILITY_PUBLIC ||
        group.groupOwner != std::string(DEV_CENTER_PKG_NAME)) {
        return true;
    }
    return false;
}

int32_t HiChainConnector::SyncGroups(std::string deviceId, std::vector<std::string> &remoteGroupIdList)
{
    std::vector<GroupInfo> groupInfoList;
    GetRelatedGroups(deviceId, groupInfoList);
    for (auto &groupInfo : groupInfoList) {
        if (IsGroupInfoInvalid(groupInfo)) {
            continue;
        }
        auto iter = std::find(remoteGroupIdList.begin(), remoteGroupIdList.end(), groupInfo.groupId);
        if (iter == remoteGroupIdList.end()) {
            (void)DelMemberFromGroup(groupInfo.groupId, deviceId);
        }
    }
    return DEV_CENTER_OK;
}

int32_t HiChainConnector::DelMemberFromGroup(const std::string &groupId, const std::string &deviceId)
{
    int64_t requestId = GenRequestId();
    DEV_SERV_LOG("[HICHAIN]Start to delete member from group, requestId %ld, deviceId %s, groupId %s", requestId,
                 GetAnonyString(deviceId).c_str(), GetAnonyString(groupId).c_str());
    nlohmann::json jsonObj;
    jsonObj[FIELD_GROUP_ID] = groupId;
    jsonObj[FIELD_DELETE_ID] = deviceId;
    std::string deleteParams = jsonObj.dump();
    int32_t userId = DEFAULT_OS_ACCOUNT_ID;
    if (userId < 0) {
        DEV_SERV_LOG("[HICHAIN]get current process account user id failed");
        return ERR_FAILED;
    }
    int32_t ret = SdkDeleteMemberFromGroup(userId, requestId, DEV_CENTER_PKG_NAME, deleteParams.c_str());
    if (ret != 0) {
        DEV_SERV_LOG("[HICHAIN]fail to delete member from group with ret:%d.", ret);
        return ret;
    }
    return DEV_CENTER_OK;
}

int32_t HiChainConnector::DeleteGroup(std::string &groupId)
{
    int64_t requestId = GenRequestId();
    nlohmann::json jsonObj;
    jsonObj[FIELD_GROUP_ID] = groupId;
    std::string disbandParams = jsonObj.dump();
    int32_t userId = DEFAULT_OS_ACCOUNT_ID;
    if (userId < 0) {
        DEV_SERV_LOG("[HICHAIN]get current process account user id failed");
        return ERR_FAILED;
    }

    int32_t ret = SdkDeleteGroup(userId, requestId, DEV_CENTER_PKG_NAME, disbandParams.c_str());
    if (ret != 0) {
        DEV_SERV_LOG("[HICHAIN]fail to delete group with ret:%d.", ret);
        return ERR_FAILED;
    }
    return DEV_CENTER_OK;
}

int32_t HiChainConnector::DeleteGroup(const int32_t userId, std::string &groupId)
{
    int64_t requestId = GenRequestId();
    nlohmann::json jsonObj;
    jsonObj[FIELD_GROUP_ID] = groupId;
    std::string disbandParams = jsonObj.dump();
    int32_t ret = SdkDeleteGroup(userId, requestId, DEV_CENTER_PKG_NAME, disbandParams.c_str());
    if (ret != 0) {
        DEV_SERV_LOG("[HICHAIN]fail to delete group failed, ret: %d.", ret);
        return ERR_FAILED;
    }
    return DEV_CENTER_OK;
}

int32_t HiChainConnector::DeleteGroup(int64_t requestId_, const std::string &userId, const int32_t authType)
{
    networkStyle_ = CREDENTIAL_NETWORK;
    nlohmann::json jsonObj;
    jsonObj[FIELD_GROUP_TYPE] = authType;
    std::string queryParams = jsonObj.dump();
    std::vector<GroupInfo> groupList;
    if (!GetGroupInfo(queryParams, groupList)) {
        DEV_SERV_LOG("[HICHAIN]failed to get device join groups");
        return ERR_FAILED;
    }
    DEV_SERV_LOG("[HICHAIN]DeleteGroup groupList count = %d", groupList.size());
    bool userIsExist = false;
    std::string groupId = "";
    for (auto iter = groupList.begin(); iter != groupList.end(); iter++) {
        if (iter->userId == userId) {
            userIsExist = true;
            groupId = iter->groupId;
            break;
        }
    }
    if (!userIsExist) {
        DEV_SERV_LOG("[HICHAIN]input userId is exist in groupList!");
        return ERR_FAILED;
    }
    jsonObj[FIELD_GROUP_ID] = groupId;
    std::string disbandParams = jsonObj.dump();
    g_deleteGroupFlag = false;
    int32_t osAccountUserId = DEFAULT_OS_ACCOUNT_ID;
    if (osAccountUserId < 0) {
        DEV_SERV_LOG("[HICHAIN]get current process account user id failed");
        return ERR_FAILED;
    }
    int32_t ret = SdkDeleteGroup(osAccountUserId, requestId_, DEV_CENTER_PKG_NAME, disbandParams.c_str());
    if (ret != 0) {
        DEV_SERV_LOG("[HICHAIN]fail to delete hichain group with ret:%d.", ret);
        return ERR_FAILED;
    }
    int32_t nTickTimes = 0;
    while (!g_deleteGroupFlag) {
        usleep(DELAY_TIME_MS);
        if (++nTickTimes > SERVICE_INIT_TRY_MAX_NUM) {
            DEV_SERV_LOG("[HICHAIN]failed to delete group because timeout!");
            return ERR_FAILED;
        }
    }
    return DEV_CENTER_OK;
}

int32_t HiChainConnector::DeleteTimeOutGroup(const char *deviceId)
{
    DEV_SERV_LOG("[HICHAIN]DeleteTimeOutGroup start");
    int32_t userId = DEFAULT_OS_ACCOUNT_ID;
    if (userId < 0) {
        DEV_SERV_LOG("[HICHAIN]get current process account user id failed");
        return ERR_FAILED;
    }
    std::vector<GroupInfo> peerGroupInfoList;
    GetRelatedGroups(deviceId, peerGroupInfoList);
    char localDeviceId[DEVICE_UUID_LENGTH] = {0};
    GetDevUdid(localDeviceId, DEVICE_UUID_LENGTH);
    for (auto &group : peerGroupInfoList) {
        if (!(SdkIsDeviceInGroup(userId, DEV_CENTER_PKG_NAME, group.groupId.c_str(), localDeviceId))) {
            continue;
        }
        if ((!group.groupName.empty()) && (group.groupName[CHECK_DEV_CENTER_ALWAYS_POS] == DEV_CENTER_ALWAYS)) {
            DEV_SERV_LOG("[HICHAIN]DeleteTimeOutGroup always trusted group");
            continue;
        }
        if (group.groupType == GROUP_TYPE_PEER_TO_PEER_GROUP) {
            DeleteGroup(group.groupId);
        }
    }
    return DEV_CENTER_OK;
}

void HiChainConnector::DeleteRedundanceGroup(std::string &userId)
{
    int32_t nTickTimes = 0;
    g_deleteGroupFlag = false;
    DeleteGroup(userId);
    while (!g_deleteGroupFlag) {
        usleep(DELAY_TIME_MS);
        if (++nTickTimes > SERVICE_INIT_TRY_MAX_NUM) {
            DEV_SERV_LOG("[HICHAIN]failed to delete group because timeout!");
            return;
        }
    }
}

void HiChainConnector::DealRedundanceGroup(const std::string &userId, int32_t authType)
{
    g_groupIsRedundance = false;
    std::vector<GroupInfo> groupList;
    if (IsRedundanceGroup(userId, authType, groupList)) {
        DEV_SERV_LOG("[HICHAIN]CreateGroup IsRedundanceGroup");
        g_groupIsRedundance = true;
        for (auto iter = groupList.begin(); iter != groupList.end(); iter++) {
            if (iter->userId != userId) {
                DeleteRedundanceGroup(iter->userId);
            }
        }
        g_groupIsRedundance = false;
    }
}

int32_t HiChainConnector::CreateGroup(int64_t requestId, int32_t authType, const std::string &userId,
                                      nlohmann::json &jsonOutObj)
{
    DEV_SERV_LOG("[HICHAIN]CreateGroup start.");
    DealRedundanceGroup(userId, authType);
    networkStyle_ = CREDENTIAL_NETWORK;
    DEV_SERV_LOG("[HICHAIN]CreateGroup requestId %ld", requestId);
    char localDeviceId[DEVICE_UUID_LENGTH] = {0};
    GetDevUdid(localDeviceId, DEVICE_UUID_LENGTH);
    std::string sLocalDeviceId = localDeviceId;
    nlohmann::json jsonObj;
    jsonObj[FIELD_GROUP_TYPE] = authType;
    jsonObj[FIELD_USER_ID] = userId;
    jsonObj[FIELD_CREDENTIAL] = jsonOutObj;
    jsonObj[FIELD_DEVICE_ID] = sLocalDeviceId;
    jsonObj[FIELD_USER_TYPE] = 0;
    jsonObj[FIELD_GROUP_VISIBILITY] = GROUP_VISIBILITY_PUBLIC;
    jsonObj[FIELD_EXPIRE_TIME] = FIELD_EXPIRE_TIME_VALUE;
    g_createGroupFlag = false;
    int32_t osAccountUserId = DEFAULT_OS_ACCOUNT_ID;
    if (osAccountUserId < 0) {
        DEV_SERV_LOG("[HICHAIN]get current process account user id failed");
        return ERR_FAILED;
    }

    int32_t ret = SdkCreateGroup(osAccountUserId, requestId, DEV_CENTER_PKG_NAME, jsonObj.dump().c_str());
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("[HICHAIN]fail to create group with ret:%d, requestId:%ld.", ret, requestId);
        return ERR_DEV_CENTER_CREATE_GROUP_FAILED;
    }
    int32_t nTickTimes = 0;
    while (!g_createGroupFlag) {
        usleep(DELAY_TIME_MS);
        if (++nTickTimes > SERVICE_INIT_TRY_MAX_NUM) {
            DEV_SERV_LOG("[HICHAIN]failed to create group because timeout!");
            return ERR_FAILED;
        }
    }
    return DEV_CENTER_OK;
}

int32_t HiChainConnector::RegisterHiChainGroupCallback(const IGroupResCallback *callback)
{
    hiChainResCallback_ = callback;
    return DEV_CENTER_OK;
}

int32_t HiChainConnector::UnRegisterHiChainGroupCallback()
{
    hiChainResCallback_ = nullptr;
    return DEV_CENTER_OK;
}

int32_t HiChainConnector::getRegisterInfo(const std::string &queryParams, std::string &returnJsonStr)
{
    char *credentialInfo = nullptr;
    if (SdkGetRegisterInfo(queryParams.c_str(), &credentialInfo) != DEV_CENTER_OK) {
        DEV_SERV_LOG("[HICHAIN]fail to request hichain registerinfo.");
        return ERR_FAILED;
    }

    returnJsonStr = credentialInfo;
    SdkDestroyInfo(&credentialInfo);
    DEV_SERV_LOG("[HICHAIN]request hichain device registerinfo successfully.");
    return DEV_CENTER_OK;
}

int32_t HiChainConnector::GetGroupId(const std::string &userId, const int32_t groupType, std::string &groupId)
{
    nlohmann::json jsonObjGroup;
    jsonObjGroup[FIELD_GROUP_TYPE] = groupType;
    std::string queryParams = jsonObjGroup.dump();
    std::vector<GroupInfo> groupList;

    if (!GetGroupInfo(queryParams.c_str(), groupList)) {
        DEV_SERV_LOG("[HICHAIN]failed to get device join groups");
        return ERR_FAILED;
    }
    for (auto &groupinfo : groupList) {
        DEV_SERV_LOG("[HICHAIN]groupinfo.groupId:%s", groupinfo.groupId.c_str());
        if (groupinfo.userId == userId) {
            groupId = groupinfo.groupId;
            return DEV_CENTER_OK;
        }
    }
    return ERR_FAILED;
}

int32_t HiChainConnector::ParseRemoteCredential(const int32_t groupType, const std::string &userId,
                                                const nlohmann::json &jsonDeviceList, std::string &params,
                                                int32_t &osAccountUserId)
{
    if (userId.empty() || !jsonDeviceList.contains(FIELD_DEVICE_LIST)) {
        DEV_SERV_LOG("[HICHAIN]userId or deviceList is empty");
        return ERR_DEV_CENTER_INPUT_PARA_INVALID;
    }
    std::string groupId;
    if (GetGroupId(userId, groupType, groupId) != DEV_CENTER_OK) {
        DEV_SERV_LOG("[HICHAIN]failed to get groupid");
        return ERR_FAILED;
    }
    nlohmann::json jsonObj;
    jsonObj[FIELD_GROUP_ID] = groupId;
    jsonObj[FIELD_GROUP_TYPE] = groupType;
    jsonObj[FIELD_DEVICE_LIST] = jsonDeviceList[FIELD_DEVICE_LIST];
    params = jsonObj.dump();
    osAccountUserId = DEFAULT_OS_ACCOUNT_ID;
    if (osAccountUserId < 0) {
        DEV_SERV_LOG("[HICHAIN]get current process account user id failed");
        return ERR_FAILED;
    }
    return DEV_CENTER_OK;
}

int32_t HiChainConnector::addMultiMembers(const int32_t groupType, const std::string &userId,
                                          const nlohmann::json &jsonDeviceList)
{
    std::string addParams;
    int32_t osAccountUserId = 0;
    if (ParseRemoteCredential(groupType, userId, jsonDeviceList, addParams, osAccountUserId) != DEV_CENTER_OK) {
        DEV_SERV_LOG("[HICHAIN]ParseRemoteCredential failed!");
        return ERR_FAILED;
    }

    int32_t ret = SdkAddMultiMembersToGroup(osAccountUserId, DEV_CENTER_PKG_NAME, addParams.c_str());
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("[HICHAIN]fail to add member to hichain group with ret:%d.", ret);
        return ret;
    }
    return DEV_CENTER_OK;
}

int32_t HiChainConnector::deleteMultiMembers(const int32_t groupType, const std::string &userId,
                                             const nlohmann::json &jsonDeviceList)
{
    std::string deleteParams;
    int32_t osAccountUserId = 0;
    if (ParseRemoteCredential(groupType, userId, jsonDeviceList, deleteParams, osAccountUserId) != DEV_CENTER_OK) {
        DEV_SERV_LOG("[HICHAIN]ParseRemoteCredential failed!");
        return ERR_FAILED;
    }

    int32_t ret = SdkDelMultiMembersFromGroup(osAccountUserId, DEV_CENTER_PKG_NAME, deleteParams.c_str());
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("[HICHAIN]fail to delete member from hichain group with ret:%d.", ret);
        return ret;
    }
    return DEV_CENTER_OK;
}

std::vector<std::string> HiChainConnector::GetTrustedDevices(const std::string &localDeviceUdid)
{
    DEV_SERV_LOG("[HICHAIN]get localDeviceUdid: %s trusted devices.", GetAnonyString(localDeviceUdid).c_str());
    std::vector<GroupInfo> groups;
    int32_t ret = GetRelatedGroups(localDeviceUdid, groups);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("[HICHAIN]failed to get groupInfo, ret: %d", ret);
        return {};
    }

    int32_t userId = DEFAULT_OS_ACCOUNT_ID;
    if (userId < 0) {
        DEV_SERV_LOG("[HICHAIN]get current process account user id failed");
        return {};
    }
    std::vector<std::string> trustedDevices;
    for (const auto &group : groups) {
        char *devicesJson = nullptr;
        uint32_t devNum = 0;
        ret = SdkGetTrustedDevices(userId, DEV_CENTER_PKG_NAME, group.groupId.c_str(), &devicesJson, &devNum);
        if (ret != 0 || devicesJson == nullptr) {
            DEV_SERV_LOG("[HICHAIN]failed to get trusted devicesJson, ret: %d", ret);
            return {};
        }
        GetTrustedDevicesUdid(devicesJson, trustedDevices);
        SdkDestroyInfo(&devicesJson);
    }
    return trustedDevices;
}

int32_t HiChainConnector::GetTrustedDevicesUdid(const char *jsonStr, std::vector<std::string> &udidList)
{
    nlohmann::json jsonObject = nlohmann::json::parse(jsonStr, nullptr, false);
    if (jsonObject.is_discarded()) {
        DEV_SERV_LOG("[HICHAIN]credentialInfo string not a json type.");
        return ERR_FAILED;
    }
    for (nlohmann::json::iterator it1 = jsonObject.begin(); it1 != jsonObject.end(); it1++) {
        if (!IsString((*it1), FIELD_AUTH_ID)) {
            continue;
        }
        std::string udid = (*it1)[FIELD_AUTH_ID];
        udidList.push_back(udid);
    }
    return DEV_CENTER_OK;
}