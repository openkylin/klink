/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "softbus_connector.h"

#include <securec.h>
#include <unistd.h>
#include "softbus_error_code.h"
#include "softbus_adapter_log.h"
#include "anonymous.h"
#include "parameter.h"

#define DEV_CENTER_MAX_DEVICE_ID_LEN (96)
#define DEV_CENTER_MAX_DEVICE_NAME_LEN (128)

const int32_t SOFTBUS_SUBSCRIBE_ID_MASK = 0x0000FFFF;
const int32_t SOFTBUS_DISCOVER_DEVICE_INFO_MAX_SIZE = 100;
const int32_t SOFTBUS_TRUSTDEVICE_UUIDHASH_INFO_MAX_SIZE = 100;

constexpr const char *WIFI_IP = "WIFI_IP";
constexpr const char *WIFI_PORT = "WIFI_PORT";
constexpr const char *BR_MAC = "BR_MAC";
constexpr const char *BLE_MAC = "BLE_MAC";
constexpr const char *ETH_IP = "ETH_IP";
constexpr const char *ETH_PORT = "ETH_PORT";

SoftbusConnector::PulishStatus SoftbusConnector::publishStatus = SoftbusConnector::STATUS_UNKNOWN;
std::map<std::string, std::shared_ptr<DeviceInfo>> SoftbusConnector::discoveryDeviceInfoMap_ = {};
const ISoftbusDiscoveryCallback *SoftbusConnector::discoveryCallback_ = nullptr;
const ISoftbusPublishCallback *SoftbusConnector::publishCallback_ = nullptr;
std::queue<std::string> SoftbusConnector::discoveryDeviceIdQueue_ = {};
std::unordered_map<std::string, std::string> SoftbusConnector::deviceUdidMap_ = {};
std::mutex SoftbusConnector::discoveryCallbackMutex_;
std::mutex SoftbusConnector::discoveryDeviceInfoMutex_;
std::mutex SoftbusConnector::stateCallbackMutex_;
std::mutex SoftbusConnector::deviceUdidLocks_;

IPublishCb SoftbusConnector::softbusPublishCallback_ = {
    .OnPublishResult = SoftbusConnector::OnSoftbusPublishResult,
};
IRefreshCallback SoftbusConnector::softbusDiscoveryCallback_ = {
    .OnDeviceFound = SoftbusConnector::OnSoftbusDeviceFound,
    .OnDiscoverResult = SoftbusConnector::OnSoftbusDiscoveryResult,
};

SoftbusConnector::SoftbusConnector()
{
    DEV_SERV_LOG("[SOFTBUS]SoftbusConnector constructor.");
}

SoftbusConnector::~SoftbusConnector()
{
    DEV_SERV_LOG("[SOFTBUS]SoftbusConnector destructor.");
}

int32_t SoftbusConnector::RegisterSoftbusDiscoveryCallback(const ISoftbusDiscoveryCallback *callback)
{
    discoveryCallback_ = callback;
    return DEV_CENTER_OK;
}

int32_t SoftbusConnector::UnRegisterSoftbusDiscoveryCallback()
{
    discoveryCallback_ = nullptr;
    return DEV_CENTER_OK;
}

int32_t SoftbusConnector::RegisterSoftbusPublishCallback(const ISoftbusPublishCallback *callback)
{
    publishCallback_ = callback;
    return DEV_CENTER_OK;
}

int32_t SoftbusConnector::UnRegisterSoftbusPublishCallback()
{
    publishCallback_ = nullptr;
    return DEV_CENTER_OK;
}

int32_t SoftbusConnector::PublishDiscovery(const PublishInfo &publishInfo)
{
    DEV_SERV_LOG("[SOFTBUS]start, publishId: %d, mode: 0x%x, ranging: %d.", publishInfo.publishId, publishInfo.mode,
                 publishInfo.ranging);
    int32_t ret = ::PublishLNN(DEV_CENTER_PKG_NAME, &publishInfo, &softbusPublishCallback_);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("[SOFTBUS]failed, ret %d.", ret);
        return ERR_DEV_CENTER_PUBLISH_FAILED;
    }
    return ret;
}

int32_t SoftbusConnector::UnPublishDiscovery(int32_t publishId)
{
    DEV_SERV_LOG("[SOFTBUS]begin, publishId: %d.", publishId);
    int32_t ret = ::StopPublishLNN(DEV_CENTER_PKG_NAME, publishId);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("[SOFTBUS]failed with ret: %d.", ret);
        return ERR_DEV_CENTER_PUBLISH_FAILED;
    }
    return ret;
}

int32_t SoftbusConnector::StartDiscovery(const SubscribeInfo &subscribeInfo)
{
    SubscribeInfo subInfo;
    (void)memset_s(&subInfo, sizeof(SubscribeInfo), 0, sizeof(SubscribeInfo));
    subInfo.subscribeId = subscribeInfo.subscribeId;
    subInfo.mode = static_cast<DiscoverMode>(subscribeInfo.mode);
    subInfo.medium = static_cast<ExchangeMedium>(subscribeInfo.medium);
    subInfo.freq = static_cast<ExchangeFreq>(subscribeInfo.freq);
    subInfo.isSameAccount = subscribeInfo.isSameAccount;
    subInfo.isWakeRemote = subscribeInfo.isWakeRemote;
    subInfo.capability = subscribeInfo.capability;
    DEV_SERV_LOG("[SOFTBUS]begin, subscribeId: %d, mode: 0x%x, medium: %d.", subInfo.subscribeId, subInfo.mode,
                 subInfo.medium);
    int32_t ret = ::RefreshLNN(DEV_CENTER_PKG_NAME, &subInfo, &softbusDiscoveryCallback_);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("[SOFTBUS]failed, ret: %d.", ret);
        return ERR_DEV_CENTER_DISCOVERY_FAILED;
    }
    return ret;
}

int32_t SoftbusConnector::StopDiscovery(uint16_t subscribeId)
{
    DEV_SERV_LOG("[SOFTBUS]begin, subscribeId: %d.", (int32_t)subscribeId);
    int32_t ret = ::StopRefreshLNN(DEV_CENTER_PKG_NAME, subscribeId);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("[SOFTBUS]failed, ret: %d.", ret);
        return ERR_DEV_CENTER_DISCOVERY_FAILED;
    }
    return ret;
}

void SoftbusConnector::JoinLnn(const std::string &deviceId)
{
    std::string connectAddr;
    DEV_SERV_LOG("[SOFTBUS]start, deviceId: %s.", GetAnonyString(deviceId).c_str());
    ConnectionAddr *addrInfo = GetConnectAddr(deviceId, connectAddr);
    if (addrInfo == nullptr) {
        DEV_SERV_LOG("[SOFTBUS]addrInfo is nullptr.");
        return;
    }
    int32_t ret = ::JoinLNN(DEV_CENTER_PKG_NAME, addrInfo, OnSoftbusJoinLNNResult);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("[SOFTBUS]failed, ret: %d.", ret);
    }
    return;
}

int32_t SoftbusConnector::GetUdidByNetworkId(const char *networkId, std::string &udid)
{
    DEV_SERV_LOG("[SOFTBUS]start, networkId: %s.", GetAnonyString(std::string(networkId)).c_str());
    uint8_t tmpUdid[UDID_BUF_LEN] = {0};
    int32_t ret =
        GetNodeKeyInfo(DEV_CENTER_PKG_NAME, networkId, NodeDeviceInfoKey::NODE_KEY_UDID, tmpUdid, sizeof(tmpUdid));
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("[SOFTBUS]GetNodeKeyInfo failed, ret: %d.", ret);
        return ERR_FAILED;
    }
    udid = reinterpret_cast<char *>(tmpUdid);
    return ret;
}

int32_t SoftbusConnector::GetUuidByNetworkId(const char *networkId, std::string &uuid)
{
    DEV_SERV_LOG("[SOFTBUS]start, networkId: %s.", GetAnonyString(std::string(networkId)).c_str());
    uint8_t tmpUuid[UUID_BUF_LEN] = {0};
    int32_t ret =
        GetNodeKeyInfo(DEV_CENTER_PKG_NAME, networkId, NodeDeviceInfoKey::NODE_KEY_UUID, tmpUuid, sizeof(tmpUuid));
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("[SOFTBUS]GetNodeKeyInfo failed, ret: %d.", ret);
        return ERR_FAILED;
    }
    uuid = reinterpret_cast<char *>(tmpUuid);
    return ret;
}

bool SoftbusConnector::HaveDeviceInMap(std::string deviceId)
{
    auto iter = discoveryDeviceInfoMap_.find(deviceId);
    if (iter == discoveryDeviceInfoMap_.end()) {
        DEV_SERV_LOG("[SOFTBUS]deviceInfo not found by deviceId: %s.", GetAnonyString(deviceId).c_str());
        return false;
    }
    return true;
}

int32_t SoftbusConnector::GetConnectionIpAddress(const std::string &deviceId, std::string &ipAddress)
{
    DeviceInfo *deviceInfo = nullptr;
    {
        auto iter = discoveryDeviceInfoMap_.find(deviceId);
        if (iter == discoveryDeviceInfoMap_.end()) {
            DEV_SERV_LOG("[SOFTBUS]deviceInfo not found by deviceId: %s.", GetAnonyString(deviceId).c_str());
            return ERR_FAILED;
        }
        deviceInfo = iter->second.get();
    }

    if (deviceInfo->addrNum <= 0 || deviceInfo->addrNum >= CONNECTION_ADDR_MAX) {
        DEV_SERV_LOG("[SOFTBUS]deviceInfo address num not valid, addrNum: %d.", deviceInfo->addrNum);
        return ERR_FAILED;
    }
    for (uint32_t i = 0; i < deviceInfo->addrNum; ++i) {
        // currently, only support CONNECT_ADDR_WLAN
        if (deviceInfo->addr[i].type != ConnectionAddrType::CONNECTION_ADDR_WLAN &&
            deviceInfo->addr[i].type != ConnectionAddrType::CONNECTION_ADDR_ETH) {
            continue;
        }
        ipAddress = deviceInfo->addr[i].info.ip.ip;
        DEV_SERV_LOG("[SOFTBUS]get ip ok.");
        return DEV_CENTER_OK;
    }
    DEV_SERV_LOG("[SOFTBUS]failed to get ipAddress for deviceId: %s.", GetAnonyString(deviceId).c_str());
    return ERR_FAILED;
}

void SoftbusConnector::ConvertDeviceInfoToDmDevice(const DeviceInfo &deviceInfo, AuthDeviceInfo &authDeviceInfo)
{
    (void)memset_s(&authDeviceInfo, sizeof(AuthDeviceInfo), 0, sizeof(AuthDeviceInfo));
    if (memcpy_s(authDeviceInfo.deviceId, sizeof(authDeviceInfo.deviceId), deviceInfo.devId,
                 std::min(sizeof(authDeviceInfo.deviceId), sizeof(deviceInfo.devId))) != DEV_CENTER_OK) {
        DEV_SERV_LOG("[SOFTBUS]copy deviceId data failed.");
    }

    if (memcpy_s(authDeviceInfo.deviceName, sizeof(authDeviceInfo.deviceName), deviceInfo.devName,
                 std::min(sizeof(authDeviceInfo.deviceName), sizeof(deviceInfo.devName))) != DEV_CENTER_OK) {
        DEV_SERV_LOG("[SOFTBUS]copy deviceName data failed.");
    }
    authDeviceInfo.deviceTypeId = deviceInfo.devType;
    authDeviceInfo.range = deviceInfo.range;

    for (int i = 0; i < deviceInfo.addrNum; i++) {
        if (deviceInfo.addr[i].type == 0) {
            // 0: WLAN
            if (memcpy_s(authDeviceInfo.peerAddr, sizeof(authDeviceInfo.peerAddr), deviceInfo.addr[i].info.ip.ip,
                         std::min(sizeof(authDeviceInfo.peerAddr), sizeof(deviceInfo.addr[i].info.ip.ip))) !=
                DEV_CENTER_OK) {
                DEV_SERV_LOG("[SOFTBUS]copy peerAddr data failed.");
            }
        }
    }
}

ConnectionAddr *SoftbusConnector::GetConnectAddrByType(DeviceInfo *deviceInfo, ConnectionAddrType type)
{
    if (deviceInfo == nullptr) {
        return nullptr;
    }
    for (uint32_t i = 0; i < deviceInfo->addrNum; ++i) {
        if (deviceInfo->addr[i].type == type) {
            return &deviceInfo->addr[i];
        }
    }
    return nullptr;
}

ConnectionAddr *SoftbusConnector::GetConnectAddr(const std::string &deviceId, std::string &connectAddr)
{
    DeviceInfo *deviceInfo = nullptr;
    {
        auto iter = discoveryDeviceInfoMap_.find(deviceId);
        if (iter == discoveryDeviceInfoMap_.end()) {
            DEV_SERV_LOG("[SOFTBUS]deviceInfo not found by deviceId: %s.", GetAnonyString(deviceId).c_str());
            return nullptr;
        }
        deviceInfo = iter->second.get();
    }
    if (deviceInfo->addrNum <= 0 || deviceInfo->addrNum >= CONNECTION_ADDR_MAX) {
        DEV_SERV_LOG("[SOFTBUS]deviceInfo addrNum not valid, addrNum: %d.", deviceInfo->addrNum);
        return nullptr;
    }
    nlohmann::json jsonPara;
    ConnectionAddr *addr = GetConnectAddrByType(deviceInfo, ConnectionAddrType::CONNECTION_ADDR_ETH);
    if (addr != nullptr) {
        DEV_SERV_LOG("[SOFTBUS]get ETH ConnectionAddr for deviceId: %s.", GetAnonyString(deviceId).c_str());
        jsonPara[ETH_IP] = addr->info.ip.ip;
        jsonPara[ETH_PORT] = addr->info.ip.port;
        connectAddr = jsonPara.dump();
        return addr;
    }
    addr = GetConnectAddrByType(deviceInfo, ConnectionAddrType::CONNECTION_ADDR_WLAN);
    if (addr != nullptr) {
        jsonPara[WIFI_IP] = addr->info.ip.ip;
        jsonPara[WIFI_PORT] = addr->info.ip.port;
        DEV_SERV_LOG("[SOFTBUS]get WLAN ConnectionAddr for deviceId: %s.", GetAnonyString(deviceId).c_str());
        connectAddr = jsonPara.dump();
        return addr;
    }
    addr = GetConnectAddrByType(deviceInfo, ConnectionAddrType::CONNECTION_ADDR_BR);
    if (addr != nullptr) {
        jsonPara[BR_MAC] = addr->info.br.brMac;
        DEV_SERV_LOG("[SOFTBUS]get BR ConnectionAddr for deviceId: %s.", GetAnonyString(deviceId).c_str());
        connectAddr = jsonPara.dump();
        return addr;
    }
    addr = GetConnectAddrByType(deviceInfo, ConnectionAddrType::CONNECTION_ADDR_BLE);
    if (addr != nullptr) {
        jsonPara[BLE_MAC] = addr->info.ble.bleMac;
        connectAddr = jsonPara.dump();
        return addr;
    }
    DEV_SERV_LOG("[SOFTBUS]failed to get ConnectionAddr for deviceId: %s.", GetAnonyString(deviceId).c_str());
    return nullptr;
}

void SoftbusConnector::OnSoftbusPublishResult(int32_t publishId, PublishResult result)
{
    DEV_SERV_LOG("[SOFTBUS]Callback In, publishId: %d, result: %d.", publishId, result);
    publishCallback_->OnPublishResult(publishId, result);
}

void SoftbusConnector::OnSoftbusJoinLNNResult(ConnectionAddr *addr, const char *networkId, int32_t result)
{
    (void)addr;
    (void)networkId;
    DEV_SERV_LOG("[SOFTBUS]OnSoftbusJoinLNNResult, result: %d.", result);
}

void SoftbusConnector::OnSoftbusDeviceFound(const DeviceInfo *device)
{
    if (device == nullptr) {
        DEV_SERV_LOG("[SOFTBUS]device is null.");
        return;
    }
    std::string deviceId = device->devId;
    DEV_SERV_LOG("[SOFTBUS]notify found device: %s found, range: %d, isOnline: %d.", GetAnonyString(deviceId).c_str(),
                 device->range, device->isOnline);
    if (!device->isOnline) {
        std::shared_ptr<DeviceInfo> infoPtr = std::make_shared<DeviceInfo>();
        DeviceInfo *srcInfo = infoPtr.get();
        int32_t ret = memcpy_s(srcInfo, sizeof(DeviceInfo), device, sizeof(DeviceInfo));
        if (ret != DEV_CENTER_OK) {
            DEV_SERV_LOG("[SOFTBUS]save discovery device info failed, ret: %d.", ret);
            return;
        }
        {
            if (discoveryDeviceInfoMap_.find(deviceId) == discoveryDeviceInfoMap_.end()) {
                discoveryDeviceIdQueue_.emplace(deviceId);
            }
            discoveryDeviceInfoMap_[deviceId] = infoPtr;

            // Remove the earliest element when reached the max size
            if (discoveryDeviceIdQueue_.size() == SOFTBUS_DISCOVER_DEVICE_INFO_MAX_SIZE) {
                discoveryDeviceInfoMap_.erase(discoveryDeviceIdQueue_.front());
                discoveryDeviceIdQueue_.pop();
            }
        }
    }

    AuthDeviceInfo authDeviceInfo;
    ConvertDeviceInfoToDmDevice(*device, authDeviceInfo);

    if (device->isOnline) {
        int devCount = 0;
        NodeBasicInfo *nodeInfo = nullptr;
        int32_t ret = GetAllNodeDeviceInfo(DEV_CENTER_PKG_NAME, &nodeInfo, &devCount);
        if (ret != DEV_CENTER_OK) {
            DEV_SERV_LOG("GetAllNodeDeviceInfo failed, ret: %d.", ret);
        }

        for (int32_t i = 0; i < devCount; i++) {
            NodeBasicInfo *nodeBasicInfo = nodeInfo + i;
            std::string udid;
            std::string udidHash;
            if (GetUdidByNetworkId(nodeBasicInfo->networkId, udid) != DEV_CENTER_OK) {
                continue;
            }
            udidHash = GetDeviceUdidHashByUdid(udid);
            if (udidHash.empty()) {
                continue;
            }
            if (strcmp(authDeviceInfo.deviceId, udidHash.c_str()) == 0) {
                if (memcpy_s(authDeviceInfo.networkId, sizeof(authDeviceInfo.networkId), nodeBasicInfo->networkId,
                             std::min(sizeof(authDeviceInfo.networkId), sizeof(nodeBasicInfo->networkId))) !=
                    DEV_CENTER_OK) {
                    DEV_SERV_LOG("copy networkId data failed.");
                }
            }
        }
        FreeNodeInfo(nodeInfo);
    }

    discoveryCallback_->OnDeviceFound(authDeviceInfo, device->isOnline);
}

void SoftbusConnector::OnSoftbusDiscoveryResult(int subscribeId, RefreshResult result)
{
    uint16_t originId = static_cast<uint16_t>((static_cast<uint32_t>(subscribeId)) & SOFTBUS_SUBSCRIBE_ID_MASK);

    if (result == REFRESH_LNN_SUCCESS) {
        DEV_SERV_LOG("[SOFTBUS]start to discovery device successfully with subscribeId: %d, result: %d.", subscribeId,
                     result);
        discoveryCallback_->OnDiscoverySuccess(originId);
    } else {
        DEV_SERV_LOG("[SOFTBUS]fail to discovery device with subscribeId: %d, result: %d.", subscribeId, result);
        discoveryCallback_->OnDiscoveryFailed(originId, result);
    }
}

std::string SoftbusConnector::GetDeviceUdidByUdidHash(const std::string &udidHash)
{
    std::lock_guard<std::mutex> lock(deviceUdidLocks_);
    for (auto &iter : deviceUdidMap_) {
        if (iter.second == udidHash) {
            return iter.first;
        }
    }
    DEV_SERV_LOG("[SOFTBUS]fail to GetUdidByUdidHash, udidHash: %s", GetAnonyString(udidHash).c_str());
    return udidHash;
}

std::string SoftbusConnector::GetDeviceUdidHashByUdid(const std::string &udid)
{
    {
        std::lock_guard<std::mutex> lock(deviceUdidLocks_);
        auto iter = deviceUdidMap_.find(udid);
        if (iter != deviceUdidMap_.end()) {
            return deviceUdidMap_[udid];
        }
    }

    char udidHash[DEV_CENTER_MAX_DEVICE_ID_LEN] = {0};
    if (GetUdidHash(udid, (uint8_t *)udidHash) != DEV_CENTER_OK) {
        DEV_SERV_LOG("[SOFTBUS]get udidhash by udid: %s failed.", GetAnonyString(udid).c_str());
        return "";
    }
    DEV_SERV_LOG("[SOFTBUS]get udidhash: %s by udid: %s.", GetAnonyString(udidHash).c_str(),
                 GetAnonyString(udid).c_str());
    std::lock_guard<std::mutex> lock(deviceUdidLocks_);
    deviceUdidMap_[udid] = udidHash;
    return udidHash;
}

void SoftbusConnector::EraseUdidFromMap(const std::string &udid)
{
    std::lock_guard<std::mutex> lock(deviceUdidLocks_);
    auto iter = deviceUdidMap_.find(udid);
    if (iter == deviceUdidMap_.end()) {
        return;
    }
    size_t mapSize = deviceUdidMap_.size();
    if (mapSize >= SOFTBUS_TRUSTDEVICE_UUIDHASH_INFO_MAX_SIZE) {
        deviceUdidMap_.erase(udid);
    }
}

std::string SoftbusConnector::GetLocalDeviceName()
{
    NodeBasicInfo nodeBasicInfo;
    int32_t ret = GetLocalNodeDeviceInfo(DEV_CENTER_PKG_NAME, &nodeBasicInfo);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("[SOFTBUS]GetLocalNodeDeviceInfo failed, ret: %d.", ret);
        return "";
    }
    return nodeBasicInfo.deviceName;
}

int32_t SoftbusConnector::GetLocalDeviceTypeId()
{
    NodeBasicInfo nodeBasicInfo;
    int32_t ret = GetLocalNodeDeviceInfo(DEV_CENTER_PKG_NAME, &nodeBasicInfo);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("[SOFTBUS]GetLocalNodeDeviceInfo failed, ret: %d.", ret);
        return 0;
    }
    return nodeBasicInfo.deviceTypeId;
}
