/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "constants.h"
#include "softbus_adapter_log.h"
#include "timer_connector.h"

#include <pthread.h>
#include <thread>

constexpr const char *TIMER_RUNNING = "TimerRunning";

Timer::Timer(std::string name, int32_t time, TimerCallback callback)
    : timerName_(name), expire_(steadyClock::now()), state_(true), timeOut_(time), callback_(callback){};

AuthTimer::AuthTimer()
{
}

AuthTimer::~AuthTimer()
{
    DEV_SERV_LOG("AuthTimer destructor");
    DeleteAll();
    if (timerState_) {
        std::unique_lock<std::mutex> locker(timerStateMutex_);
        stopTimerCondition_.wait(locker, [this] { return static_cast<bool>(!timerState_); });
    }
}

int32_t AuthTimer::StartTimer(std::string name, int32_t timeOut, TimerCallback callback)
{
    DEV_SERV_LOG("Start %s", name.c_str());
    if (name.empty() || timeOut <= MIN_TIME_OUT || timeOut > MAX_TIME_OUT || callback == nullptr) {
        DEV_SERV_LOG("AuthTimer StartTimer input value invalid");
        return ERR_DEV_CENTER_INPUT_PARA_INVALID;
    }

    std::shared_ptr<Timer> timer = std::make_shared<Timer>(name, timeOut, callback);
    {
        std::lock_guard<std::mutex> locker(timerMutex_);
        timerQueue_.push(timer);
        timerVec_.emplace_back(timer);
    }

    if (timerState_) {
        DEV_SERV_LOG("is running");
        return DEV_CENTER_OK;
    }

    TimerRunning();
    {
        std::unique_lock<std::mutex> locker(timerStateMutex_);
        runTimerCondition_.wait(locker, [this] { return static_cast<bool>(timerState_); });
    }
    return DEV_CENTER_OK;
}

int32_t AuthTimer::DeleteTimer(std::string timerName)
{
    if (timerName.empty()) {
        DEV_SERV_LOG("timer is null");
        return ERR_DEV_CENTER_INPUT_PARA_INVALID;
    }

    DEV_SERV_LOG("name %s", timerName.c_str());
    std::lock_guard<std::mutex> locker(timerMutex_);
    for (auto iter : timerVec_) {
        if (iter->timerName_ == timerName) {
            iter->state_ = false;
        }
    }
    return DEV_CENTER_OK;
}

int32_t AuthTimer::DeleteAll()
{
    DEV_SERV_LOG("start");
    std::lock_guard<std::mutex> locker(timerMutex_);
    for (auto iter : timerVec_) {
        DEV_SERV_LOG("timer.name = %s ", iter->timerName_.c_str());
        iter->state_ = false;
    }
    return DEV_CENTER_OK;
}

int32_t AuthTimer::TimerRunning()
{
    int32_t ret = pthread_setname_np(pthread_self(), TIMER_RUNNING);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("setname failed.");
    }
    std::thread([this]() {
        {
            timerState_ = true;
            std::unique_lock<std::mutex> locker(timerStateMutex_);
            runTimerCondition_.notify_one();
        }
        while (!timerQueue_.empty() && timerState_) {
            std::this_thread::sleep_for(std::chrono::milliseconds(DELAY_TICK_MILLSECONDS));
            while (std::chrono::duration_cast<timerDuration>(steadyClock::now() - timerQueue_.top()->expire_).count() /
                           MILLISECOND_TO_SECOND >=
                       timerQueue_.top()->timeOut_ ||
                   !timerQueue_.top()->state_) {
                std::string name = timerQueue_.top()->timerName_;
                DEV_SERV_LOG("timer.name = %s", name.c_str());
                if (timerQueue_.top()->state_) {
                    timerQueue_.top()->callback_(name);
                }

                std::lock_guard<std::mutex> locker(timerMutex_);
                timerQueue_.pop();
                DeleteVector(name);
                if (timerQueue_.empty()) {
                    break;
                }
            }
        }
        {
            timerState_ = false;
            std::unique_lock<std::mutex> locker(timerStateMutex_);
            stopTimerCondition_.notify_one();
        }
    }).detach();
    return DEV_CENTER_OK;
}

void AuthTimer::DeleteVector(std::string name)
{
    for (auto iter = timerVec_.begin(); iter != timerVec_.end(); ++iter) {
        if ((*iter)->timerName_ == name) {
            timerVec_.erase(iter);
            break;
        }
    }
}