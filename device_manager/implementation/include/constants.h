/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <stdio.h>
#include <string>
#include "comm_type.h"

#define DEV_SERV_LOG(fmt, args...) printf("[DeviceMgrService][%s:%u]" fmt "\n", __func__, __LINE__, ##args);

enum UiAction {
    USER_OPERATION_TYPE_ALLOW_AUTH = 0,
    USER_OPERATION_TYPE_CANCEL_AUTH = 1,
    USER_OPERATION_TYPE_DEV_CENTER_CONFIRM_TIMEOUT = 2,
    USER_OPERATION_TYPE_CANCEL_PINCODE_DISPLAY = 3,
    USER_OPERATION_TYPE_CANCEL_PINCODE_INPUT = 4,
    USER_OPERATION_TYPE_DONE_PINCODE_INPUT = 5,
    USER_OPERATION_TYPE_ALLOW_DEV_CENTER_ALWAYS = 6,
};

constexpr const char *TAG_GROUP_ID = "groupId";
constexpr const char *TAG_GROUP_NAME = "GROUPNAME";
constexpr const char *TAG_REQUEST_ID = "REQUESTID";
constexpr const char *TAG_DEVICE_ID = "DEVICEID";
constexpr const char *TAG_DEV_CENTER_TYPE = "AUTHTYPE";
constexpr const char *TAG_CRYPTO_SUPPORT = "CRYPTOSUPPORT";
constexpr const char *TAG_VER = "ITF_VER";
constexpr const char *TAG_MSG_TYPE = "MSG_TYPE";
constexpr const char *DEV_CENTER_ITF_VER = "1.1";
constexpr const char *DEV_CENTER_PKG_NAME = "ohos.distributedhardware.devicemanager";
constexpr const char *DEV_CENTER_SESSION_NAME = "ohos.distributedhardware.devicemanager.resident";
const static char *DEV_CENTER_CAPABILITY_OSD = "osdCapability";

// Auth
constexpr const char *DEV_CENTER_TYPE = "authType";
constexpr const char *APP_OPERATION = "appOperation";
constexpr const char *CUSTOM_DESCRIPTION = "customDescription";
constexpr const char *TOKEN = "token";
constexpr const char *PIN_TOKEN = "pinToken";
constexpr const char *PIN_CODE_KEY = "pinCode";
constexpr int32_t CHECK_DEV_CENTER_ALWAYS_POS = 0;
constexpr const char DEV_CENTER_ALWAYS = '1';
constexpr const char DEV_CENTER_ONCE = '0';

// HiChain
constexpr int32_t DEV_CENTER_TYPE_PIN = 1;
constexpr int32_t SERVICE_INIT_TRY_MAX_NUM = 200;
constexpr int32_t DEVICE_UUID_LENGTH = 65;
constexpr int32_t GROUP_TYPE_INVALID_GROUP = -1;
constexpr int32_t GROUP_TYPE_IDENTICAL_ACCOUNT_GROUP = 1;
constexpr int32_t GROUP_TYPE_PEER_TO_PEER_GROUP = 256;
constexpr int32_t GROUP_TYPE_ACROSS_ACCOUNT_GROUP = 1282;
constexpr int64_t MIN_REQUEST_ID = 1000000000;
constexpr int64_t MAX_REQUEST_ID = 9999999999;

// ACE
constexpr const char *EVENT_CONFIRM = "EVENT_CONFIRM";
constexpr const char *EVENT_CANCEL = "EVENT_CANCEL";
constexpr const char *EVENT_INIT = "EVENT_INIT";
constexpr const char *EVENT_CONFIRM_CODE = "0";
constexpr const char *EVENT_CANCEL_CODE = "1";
constexpr const char *EVENT_INIT_CODE = "2";

constexpr const char *UICALL_PKGNAME_UPDATESTATE = "updateState";
constexpr const char *UICALL_PKGNAME_CANCELDISPLAY = "CancelDisplay";
constexpr const char *UICALL_PKGNAME_SHOWCONFIGDIALOG = "ShowConfigDialog";
constexpr const char *UICALL_PKGNAME_SHOWAUTHDIALOG = "ShowAuthInfoDialog";
constexpr const char *UICALL_PKGNAME_SHOWSTARTAUTHDIALOG = "ShowStartAuthDialog";

#endif  // CONSTANTS_H