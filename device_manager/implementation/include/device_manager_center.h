/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DEV_CENTER_MANAGER_H
#define DEV_CENTER_MANAGER_H

#include "device_manager_listenter.h"
#include "softbus_connector.h"
#include "hichain_connector.h"
#include "auth_device_manager.h"

constexpr const uint16_t MAX_SUBSCRIBE_ID = 65535;

// 初始化devicemanager
int32_t InitDeviceManagerCenter();
// 释放操作
int32_t DestoryDeviceManagerCenter();

// 注册发布回调
int32_t RegisterPublishCallback(const ISoftbusPublishCallback *callback);
// 取消发布回调注册
int32_t UnRegisterPublishCallback();
// 注册发现回调
int32_t RegisterDiscoverCallback(const ISoftbusDiscoveryCallback *callback);
// 取消发现回调注册
int32_t UnRegisterDiscoverCallback();
// 注册设备监听回调
int32_t RegisterDeviceStateCallback(const ISoftbusStateCallback *callback);
// 取消设备监听回调注册
int32_t UnRegisterDeviceStateCallback();
// 注册UI更新回调
int32_t RegisterUIStateCallback(const IUIStateUpdateCallback *callback);
// 取消UI更新回调注册
int32_t UnRegisterUIStateCallback();
// 注册认证结果回调
int32_t RegisterAuthResultCallback(const IAuthResultCallback *callback);
// 取消认证结果回调注册
int32_t UnRegisterAuthResultCallback();
// 获取本机信息
int32_t GetLocalDeviceInfo(AuthDeviceInfo &info);
// 发布设备发现服务
int32_t PublishDeviceDiscovery(const PublishInfo &publishInfo);
// 取消发布设备发现服务
int32_t UnPublishDeviceDiscovery(int32_t publishId);
// 开始发现
int32_t StartDeviceDiscovery(const SubscribeInfo &subscribeInfo);
// 停止发现
int32_t StopDeviceDiscovery(uint16_t subscribeId);
// 开始认证
int32_t AuthenticateDevice(const std::string &deviceId);
// 删除认证设备
int32_t UnAuthenticateDevice(const std::string &networkId);
// 获取可信列表 存在内存申请，需要释放
int32_t GetTrustedDeviceList(AuthDeviceInfo **info, int *infoNum);
// 获取设备信息
int32_t GetDeviceInfo(const std::string &networkId, AuthDeviceInfo &info);

#endif  // DEV_CENTER_MANAGER_H
