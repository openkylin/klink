#ifndef SOFTBUS_CONNECTOR_H
#define SOFTBUS_CONNECTOR_H

#include <map>
#include <unordered_map>
#include <memory>
#include <mutex>
#include <queue>
#include <string>
#include <vector>
#include <algorithm>

#include "softbus_bus_center.h"
#include "constants.h"
#include "device_manager_listenter.h"

class SoftbusConnector
{
public:
    static void OnSoftbusPublishResult(int32_t publishId, PublishResult result);

    static void OnSoftbusDeviceFound(const DeviceInfo *device);

    static void OnSoftbusDiscoveryResult(int subscribeId, RefreshResult result);

    static void OnSoftbusJoinLNNResult(ConnectionAddr *addr, const char *networkId, int32_t result);

    static void OnParameterChgCallback(const char *key, const char *value, void *context);

    static int32_t GetConnectionIpAddress(const std::string &deviceId, std::string &ipAddress);

    static ConnectionAddr *GetConnectAddr(const std::string &deviceId, std::string &connectAddr);

    static int32_t GetUdidByNetworkId(const char *networkId, std::string &udid);

    static int32_t GetUuidByNetworkId(const char *networkId, std::string &uuid);

    static std::string GetDeviceUdidByUdidHash(const std::string &udidHash);

    static std::string GetDeviceUdidHashByUdid(const std::string &udid);

    static void JoinLnn(const std::string &deviceId);

public:
    SoftbusConnector();
    ~SoftbusConnector();
    int32_t RegisterSoftbusDiscoveryCallback(const ISoftbusDiscoveryCallback *callback);
    int32_t UnRegisterSoftbusDiscoveryCallback();
    int32_t RegisterSoftbusPublishCallback(const ISoftbusPublishCallback *callback);
    int32_t UnRegisterSoftbusPublishCallback();
    int32_t PublishDiscovery(const PublishInfo &publishInfo);
    int32_t UnPublishDiscovery(int32_t publishId);
    int32_t StartDiscovery(const SubscribeInfo &subscribeInfo);
    int32_t StopDiscovery(uint16_t subscribeId);
    bool HaveDeviceInMap(std::string deviceId);
    void EraseUdidFromMap(const std::string &udid);
    std::string GetLocalDeviceName();
    int32_t GetLocalDeviceTypeId();

private:
    int32_t Init();
    static void ConvertDeviceInfoToDmDevice(const DeviceInfo &deviceInfo, AuthDeviceInfo &authDeviceInfo);
    static ConnectionAddr *GetConnectAddrByType(DeviceInfo *deviceInfo, ConnectionAddrType type);

private:
    enum PulishStatus {
        STATUS_UNKNOWN = 0,
        ALLOW_BE_DISCOVERY = 1,
        NOT_ALLOW_BE_DISCOVERY = 2,
    };
    static const ISoftbusDiscoveryCallback *discoveryCallback_;
    static const ISoftbusPublishCallback *publishCallback_;
    static PulishStatus publishStatus;
    static IRefreshCallback softbusDiscoveryCallback_;
    static IRefreshCallback softbusDiscoveryByIdCallback_;
    static IPublishCb softbusPublishCallback_;
    static std::map<std::string, std::shared_ptr<DeviceInfo>> discoveryDeviceInfoMap_;
    static std::queue<std::string> discoveryDeviceIdQueue_;
    static std::unordered_map<std::string, std::string> deviceUdidMap_;
    static std::mutex discoveryCallbackMutex_;
    static std::mutex discoveryDeviceInfoMutex_;
    static std::mutex stateCallbackMutex_;
    static std::mutex deviceUdidLocks_;
};
#endif  // SOFTBUS_CONNECTOR_H
