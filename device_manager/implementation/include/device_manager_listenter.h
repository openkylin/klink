/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DEVICE_MANAGER_LISTENTER
#define DEVICE_MANAGER_LISTENTER

#include <map>
#include <string>
#include "constants.h"
#include "softbus_bus_center.h"

typedef struct {
    void (*OnAuthResult)(const std::string &pkgName, const std::string &deviceId, const std::string &token,
                         int32_t status, int32_t reason);
    void (*OnVerifyAuthResult)(const std::string &pkgName, const std::string &deviceId, int32_t resultCode,
                               const std::string &flag);
    void (*OnUiCall)(const std::string &pkgName, std::string &paramJson);
    int32_t (*OnVerifyAuthentication)(std::string &authToken, const std::string &authParam);
} IAuthManagerCallback;

typedef struct {
    void (*OnAuthResult)(const std::string &deviceId, const std::string &token, int32_t status, int32_t reason);
} IAuthResultCallback;

typedef struct {
    void (*OnGroupCreated)(int64_t requestId, const std::string &groupId);
    void (*OnMemberJoin)(int64_t requestId, int32_t status);
    std::string (*GetConnectAddr)(std::string deviceId);
    int32_t (*GetPinCode)();
} IHiChainConnectorCallback;

typedef struct {
    void (*OnGroupResult)(int64_t requestId, int32_t action, const std::string &resultInfo);
} IGroupResCallback;

#endif  // DEVICE_MANAGER_LISTENTER
