/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AUTH_MANAGER_H
#define AUTH_MANAGER_H

#include <map>
#include <string>
#include <vector>

#include "auth_request_state.h"
#include "auth_response_state.h"
#include "constants.h"
#include "hichain_connector.h"
#include "softbus_connector.h"
#include "softbus_adapter_log.h"
#include "device_manager_listenter.h"
#include "timer_connector.h"
#include "inner_session.h"
#include "session.h"

typedef enum AuthState {
    AUTH_REQUEST_INIT = 1,
    AUTH_REQUEST_NEGOTIATE,
    AUTH_REQUEST_NEGOTIATE_DONE,
    AUTH_REQUEST_REPLY,
    AUTH_REQUEST_JOIN,
    AUTH_REQUEST_NETWORK,
    AUTH_REQUEST_FINISH,
    AUTH_RESPONSE_INIT = 20,
    AUTH_RESPONSE_NEGOTIATE,
    AUTH_RESPONSE_CONFIRM,
    AUTH_RESPONSE_GROUP,
    AUTH_RESPONSE_SHOW,
    AUTH_RESPONSE_FINISH,
} AuthState;

enum MsgType : int32_t {
    MSG_TYPE_UNKNOWN = 0,
    MSG_TYPE_NEGOTIATE = 80,
    MSG_TYPE_RESP_NEGOTIATE = 90,
    MSG_TYPE_REQ_AUTH = 100,
    MSG_TYPE_INVITE_AUTH_INFO = 102,
    MSG_TYPE_REQ_AUTH_TERMINATE = 104,
    MSG_TYPE_RESP_AUTH = 200,
    MSG_TYPE_JOIN_AUTH_INFO = 201,
    MSG_TYPE_RESP_AUTH_TERMINATE = 205,
    MSG_TYPE_CHANNEL_CLOSED = 300,
    MSG_TYPE_SYNC_GROUP = 400,
    MSG_TYPE_AUTH_BY_PIN = 500,
};

typedef struct AuthRequestContext {
    int32_t authType;
    std::string localDeviceId;
    std::string localDeviceName;
    int32_t localDeviceTypeId;
    std::string deviceId;
    std::string deviceName;
    std::string deviceTypeId;
    int32_t sessionId;
    int32_t groupVisibility;
    bool cryptoSupport;
    std::string cryptoName;
    std::string cryptoVer;
    std::string hostPkgName;
    std::string targetPkgName;
    std::string appOperation;
    std::string appDesc;
    std::string appName;
    std::string customDesc;
    std::string appThumbnail;
    std::string token;
    int32_t reason;
    std::vector<std::string> syncGroupList;
} AuthRequestContext;

typedef struct AuthResponseContext {
    int32_t authType;
    std::string deviceId;
    std::string localDeviceId;
    std::string deviceName;
    int32_t deviceTypeId;
    int32_t msgType;
    int32_t sessionId;
    bool cryptoSupport;
    bool isIdenticalAccount;
    std::string cryptoName;
    std::string cryptoVer;
    int32_t reply;
    std::string networkId;
    std::string groupId;
    std::string groupName;
    std::string hostPkgName;
    std::string targetPkgName;
    std::string appOperation;
    std::string appDesc;
    std::string customDesc;
    std::string appIcon;
    std::string appThumbnail;
    std::string token;
    std::string authToken;
    int32_t pageId;
    int64_t requestId;
    int32_t code;
    int32_t state;
    std::vector<std::string> syncGroupList;
} AuthResponseContext;

class AuthMessageProcessor;

class AuthManager final : public std::enable_shared_from_this<AuthManager>
{
public:
    AuthManager();
    ~AuthManager();

    int32_t RegisterAuthManagerCallback(const IAuthManagerCallback *callback, SoftbusConnector *softbusConnector,
                                        HiChainConnector *hiChainConnector);
    int32_t UnRegisterAuthManagerCallback();

    int32_t AuthenticateDevice(const std::string &pkgName, int32_t authType, const std::string &deviceId,
                               const std::string &extra);

    int32_t UnAuthenticateDevice(const std::string &pkgName, const std::string &networkId);

    int32_t UnBindDevice(const std::string &pkgName, const std::string &udidHash);

    int32_t VerifyAuthentication(const std::string &authParam);

    int OnSessionOpened(int sessionId, int result);

    void OnSessionClosed(int sessionId);

    void OnDataReceived(int sessionId, const void *data, unsigned int dataLen);

    void OnGroupCreated(int64_t requestId, const std::string &groupId);

    void OnMemberJoin(int64_t requestId, int32_t status);

    int32_t EstablishAuthChannel(const std::string &deviceId);

    void StartNegotiate(const int32_t &sessionId);

    void RespNegotiate(const int32_t &sessionId);

    void SendAuthRequest(const int32_t &sessionId);

    int32_t StartAuthProcess(const int32_t &action);

    void StartRespAuthProcess();

    int32_t CreateGroup();

    int32_t AddMember(int32_t pinCode);

    std::string GetConnectAddr(std::string deviceId);

    int32_t JoinNetwork();

    void AuthenticateFinish();

    bool GetIsCryptoSupport();

    int32_t SetAuthRequestState(std::shared_ptr<AuthRequestState> authRequestState);

    int32_t SetAuthResponseState(std::shared_ptr<AuthResponseState> authResponseState);

    int32_t GetPinCode();

    std::string GenerateGroupName();

    void HandleAuthenticateTimeout(std::string name);

    void CancelDisplay();

    int32_t GeneratePincode();

    void ShowConfigDialog();

    void ShowAuthInfoDialog();

    void ShowStartAuthDialog();

    int32_t OnUserOperation(int32_t action, const std::string &params);

    int32_t SetPageId(int32_t pageId);

    int32_t SetReasonAndFinish(int32_t reason, int32_t state);

    bool IsIdenticalAccount();

private:
    void UpdateUiState(const UiStateMsg msg);
    int32_t CheckAuthParamVaild(const std::string &pkgName, int32_t authType, const std::string &deviceId,
                                const std::string &extra);
    void ProcessSourceMsg();
    void ProcessSinkMsg();

private:
    SoftbusConnector *softbusConnector_ = nullptr;
    HiChainConnector *hiChainConnector_ = nullptr;
    const IAuthManagerCallback *authManagerListener_ = nullptr;
    std::shared_ptr<AuthRequestState> authRequestState_ = nullptr;
    std::shared_ptr<AuthResponseState> authResponseState_ = nullptr;
    std::shared_ptr<AuthRequestContext> authRequestContext_;
    std::shared_ptr<AuthResponseContext> authResponseContext_;
    std::shared_ptr<AuthMessageProcessor> authMessageProcessor_;
    std::shared_ptr<AuthTimer> timer_;
    bool isCryptoSupport_ = false;
    bool isFinishOfLocal_ = true;
    int32_t authTimes_ = 0;
    int32_t action_ = USER_OPERATION_TYPE_CANCEL_AUTH;
    bool isAddingMember_ = false;
    bool isAuth_ = false;  // 收到认证请求
};
#endif  // AUTH_MANAGER_H
