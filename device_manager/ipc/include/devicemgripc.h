#ifndef DEVICEMGRIPC_IPC
#define DEVICEMGRIPC_IPC

/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>
#include "parcel.h"

enum ResultOption {
    TF_SYNC = 0x00,
    TF_ASYNC = 0x01,
};

typedef struct {
    void (*OnRequest)(int32_t code, OHOS::Parcel &data, OHOS::Parcel &reply);
} DeviceIpcRequest;

bool InitIpc(bool isServer, DeviceIpcRequest *request);

int SendRequest(int32_t code, ResultOption option, OHOS::Parcel &data, OHOS::Parcel &reply, int32_t pid = -1);
#endif  // DEVICEMGRIPC_IPC