/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "devicemgripc.h"

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <thread>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/socket.h>
#include <sys/un.h>

#define DEV_IPC_LOG(fmt, args...) printf("[DeviceMgripc][%s:%u]" fmt "\n", __func__, __LINE__, ##args);

static key_t s_send_shm_key = 0x777F53;
static key_t s_rec_shm_key = 0x888F00;
const int IPC_SHM_FLAG = IPC_CREAT | 0666;
const size_t DATA_SIZE = 0x20000;
const char *DEV_IPC_SERVER_ADDR = "/tmp/ipc.devicemgr.server.socket";
const char *DEV_IPC_CLIENT_ADDR = "/tmp/ipc.devicemgr.client.socket";

struct DevIpcShmData {
    size_t inputSz;
    size_t outputSz;
    char inputData[DATA_SIZE];
    char outputData[DATA_SIZE];
    volatile bool needWait = false;
    uint32_t requestCode;
    int32_t pid = -1;
};

static bool s_isInit = false;
static bool s_isServer = true;
static DeviceIpcRequest *s_ipcRequestPtr = nullptr;

static DevIpcShmData *OpenShm(key_t shmKey)
{
    int shmFd = shmget(shmKey, sizeof(DevIpcShmData), IPC_SHM_FLAG);
    if (shmFd < 0) {
        DEV_IPC_LOG("Get shm failed");
        return nullptr;
    }
    void *shmPtr = shmat(shmFd, 0, 0);
    if (shmPtr == (void *)-1) {
        DEV_IPC_LOG("Map shm failed");
        return nullptr;
    }
    return (DevIpcShmData *)shmPtr;
}

static bool ShmInit(key_t shmKey)
{
    DevIpcShmData *shmPtr = OpenShm(shmKey);
    if (shmPtr == nullptr) {
        DEV_IPC_LOG("Create shm with key=0x%x", shmKey);
        return false;
    }
    shmPtr->needWait = false;
    shmdt((void *)shmPtr);
    return true;
}

void ProcessHandle()
{
    do {
        DevIpcShmData *shmPtr = OpenShm(s_rec_shm_key);
        if (shmPtr == nullptr) {
            DEV_IPC_LOG("Open shm error: %s", strerror(errno));
            return;
        }
        while (!shmPtr->needWait) {
            usleep(10);
        }
        if (!s_isServer && (shmPtr->pid != -1) && (shmPtr->pid != getpid())) {
            shmdt((void *)shmPtr);
            return;
        }
        OHOS::Parcel data;
        OHOS::Parcel reply;
        data.WriteUnpadBuffer(shmPtr->inputData, shmPtr->inputSz);

        s_ipcRequestPtr->OnRequest(shmPtr->requestCode, data, reply);

        shmPtr->outputSz = reply.GetDataSize();
        memcpy(shmPtr->outputData, (void *)reply.GetData(), shmPtr->outputSz);

        shmPtr->needWait = false;
        shmdt((void *)shmPtr);
    } while (true);
}

bool InitIpc(bool isServer, DeviceIpcRequest *request)
{
    if (s_isInit) {
        DEV_IPC_LOG("DeviceMgr Ipc is already Initialize");
        return true;
    }
    if (request == nullptr) {
        DEV_IPC_LOG("Invalid request ptr");
        return false;
    }

    if (isServer && (!ShmInit(s_send_shm_key) || !ShmInit(s_rec_shm_key))) {
        DEV_IPC_LOG("Shm inti failed");
        return false;
    }

    if (isServer) {
        std::swap(s_send_shm_key, s_rec_shm_key);
    }
    s_isServer = isServer;
    s_ipcRequestPtr = request;

    s_isInit = true;

    std::thread new_thread(std::bind(&ProcessHandle));
    new_thread.detach();

    return true;
}

int SendRequest(int32_t code, ResultOption option, OHOS::Parcel &data, OHOS::Parcel &reply, int32_t pid)
{
    if (!s_isInit) {
        DEV_IPC_LOG("DeviceMgr Ipc not init!");
        return -1;
    }

    DevIpcShmData *shmPtr = nullptr;

    shmPtr = OpenShm(s_send_shm_key);
    if (shmPtr == nullptr) {
        DEV_IPC_LOG("Open shm error: %s", strerror(errno));
        return -1;
    }

    // waiting previous ipc
    while (shmPtr->needWait)
        ;

    shmPtr->requestCode = code;
    shmPtr->pid = pid;
    shmPtr->inputSz = data.GetDataSize();
    memcpy(shmPtr->inputData, (void *)data.GetData(), shmPtr->inputSz);
    shmPtr->needWait = true;

    if (option == ResultOption::TF_ASYNC) {
        shmdt((void *)shmPtr);
        return 0;
    }

    while (shmPtr->needWait)
        ;

    reply.WriteUnpadBuffer(shmPtr->outputData, shmPtr->outputSz);
    shmdt((void *)shmPtr);
    return 0;
}