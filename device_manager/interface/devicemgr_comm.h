/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DEVICEMGR_COMM
#define DEVICEMGR_COMM

#include <stdint.h>
#include "softbus_common.h"

enum {
    DEV_CENTER_OK = 0,
    ERR_FAILED = -20000,
    ERR_DEV_CENTER_TIME_OUT = -20001,
    ERR_DEV_CENTER_NOT_INIT = -20002,
    ERR_DEV_CENTER_INIT_FAILED = -20004,
    ERR_DEV_CENTER_POINT_NULL = -20005,
    ERR_DEV_CENTER_INPUT_PARA_INVALID = -20006,
    ERR_DEV_CENTER_DISCOVERY_FAILED = -20009,
    ERR_DEV_CENTER_DISCOVERY_REPEATED = -20017,
    ERR_DEV_CENTER_UNSUPPORTED_DEV_CENTER_TYPE = -20018,
    ERR_DEV_CENTER_BUSINESS_BUSY = -20019,
    ERR_DEV_CENTER_OPEN_SESSION_FAILED = -20020,
    ERR_DEV_CENTER_PEER_REJECT = -20021,
    ERR_DEV_CENTER_REJECT = -20022,
    ERR_DEV_CENTER_FAILED = -20023,
    ERR_DEV_CENTER_NOT_START = -20024,
    ERR_DEV_CENTER_MESSAGE_INCOMPLETE = -20025,
    ERR_DEV_CENTER_CREATE_GROUP_FAILED = -20026,
    ERR_DEV_CENTER_IPC_READ_FAILED = -20027,
    ERR_DEV_CENTER_ENCRYPT_FAILED = -20028,
    ERR_DEV_CENTER_PUBLISH_FAILED = -20029,
    ERR_DEV_CENTER_PUBLISH_REPEATED = -20030,
    ERR_DEV_CENTER_STOP_DISCOVERY = -20031,
};

#define DEV_CENTER_MAX_DEVICE_ID_LEN (96)
#define DEV_CENTER_MAX_DEVICE_NAME_LEN (128)

typedef enum AuthDeviceState {
    /**
     * Device status is unknown.
     */
    DEVICE_STATE_UNKNOWN = -1,
    /**
     * Device online action, which indicates the device is physically online.
     */
    DEVICE_STATE_ONLINE = 0,
    /**
     * Device ready action, which indicates the information between devices has
     * been synchronized in the Distributed Data Service (DDS) module,
     * and the device is ready for running distributed services.
     */
    DEVICE_INFO_READY = 1,
    /**
     * Device offline action, which Indicates the device is physically offline.
     */
    DEVICE_STATE_OFFLINE = 2,
    /**
     * Device change action, which Indicates the device is physically change.
     */
    DEVICE_INFO_CHANGED = 3,
} AuthDeviceState;

enum UiStateMsg : int32_t {
    MSG_PIN_CODE_ERROR = 0,
    MSG_PIN_CODE_SUCCESS,
    MSG_CANCEL_PIN_CODE_SHOW,
    MSG_CANCEL_PIN_CODE_INPUT,
    MSG_DOING_AUTH,
};

typedef enum AuthForm {
    /**
     * Device Auth invalid.
     */
    INVALID_TYPE = -1,
    /**
     * Peer To Peer Device auth.
     */
    PEER_TO_PEER = 0,
    /**
     * Identical Account Device auth.
     */
    IDENTICAL_ACCOUNT = 1,
    /**
     * Across Account Device auth.
     */
    ACROSS_ACCOUNT = 2,
} AuthForm;

typedef enum AuthDeviceType {
    /**
     * Indicates an unknown device type.
     */
    DEVICE_TYPE_UNKNOWN = 0x00,
    /**
     * Indicates a smart camera.
     */
    DEVICE_TYPE_WIFI_CAMERA = 0x08,
    /**
     * Indicates a smart speaker.
     */
    DEVICE_TYPE_AUDIO = 0x0A,
    /**
     * Indicates a smart pc.
     */
    DEVICE_TYPE_PC = 0x0C,
    /**
     * Indicates a smart phone.
     */
    DEVICE_TYPE_PHONE = 0x0E,
    /**
     * Indicates a smart pad.
     */
    DEVICE_TYPE_PAD = 0x11,
    /**
     * Indicates a smart watch.
     */
    DEVICE_TYPE_WATCH = 0x6D,
    /**
     * Indicates a car.
     */
    DEVICE_TYPE_CAR = 0x83,
    /**
     * Indicates a smart TV.
     */
    DEVICE_TYPE_TV = 0x9C,
    /**
     * Indicates smart display
     */
    DEVICE_TYPE_SMART_DISPLAY = 0xA02,
} AuthDeviceType;

typedef struct AuthDeviceInfo {
    /**
     * Device Id of the device.
     */
    char deviceId[DEV_CENTER_MAX_DEVICE_ID_LEN];
    /**
     * Device name of the device.
     */
    char deviceName[DEV_CENTER_MAX_DEVICE_NAME_LEN];
    /**
     * Device type of the device.
     */
    uint16_t deviceTypeId;
    /**
     * NetworkId of the device.
     */
    char networkId[DEV_CENTER_MAX_DEVICE_ID_LEN];
    /**
     * The distance of discovered device, in centimeter(cm).
     */
    int32_t range;
    /**
     * NetworkType of the device.
     */
    int32_t networkType;
    /**
     * Device authentication form.
     */
    AuthForm authForm;

    /**
     * IP address in string format. It can be an IPv4 address written in dotted decimal notation
     * or an IPv6 address written in hexadecimal colon-separated notation.
     */
    char peerAddr[IP_STR_MAX_LEN];
} AuthDeviceInfo;

typedef struct {
    void (*OnShowAuthState)(int32_t msgType);
    void (*OnShowAuthInfo)(int32_t code);
    int32_t (*OnUserOperation)(const char *remoteName);
    void (*OnCancelDisplay)();
    int32_t (*OnInputAuthInfo)();
} IUIStateUpdateCallback;

typedef struct {
    void (*OnDeviceFound)(const AuthDeviceInfo &info, bool isOnline);
    void (*OnDiscoverySuccess)(int32_t subscribeId);
    void (*OnDiscoveryFailed)(int32_t subscribeId, int32_t failedReason);
} ISoftbusDiscoveryCallback;

typedef struct {
    void (*OnPublishResult)(int32_t publishId, int32_t publishResult);
} ISoftbusPublishCallback;

typedef struct {
    void (*OnDeviceChanged)(AuthDeviceState state, const AuthDeviceInfo &info);
} ISoftbusStateCallback;

#endif  // DEVICEMGR_COMM