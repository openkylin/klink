/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DEVICEMGR_SDK
#define DEVICEMGR_SDK

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "devicemgr_comm.h"

#ifdef __cplusplus
extern "C" {
#endif

// 注册设备监听回调
int32_t RegisterDeviceStateCallback(const char *pkgName, const ISoftbusStateCallback *callback);
// 取消设备监听回调注册
int32_t UnRegisterDeviceStateCallback(const char *pkgName);
// 注册UI更新回调
int32_t RegisterUIStateCallback(const char *pkgName, const IUIStateUpdateCallback *callback);
// 取消UI更新回调注册
int32_t UnRegisterUIStateCallback(const char *pkgName);
// 获取本机信息
int32_t GetLocalDeviceInfo(const char *pkgName, AuthDeviceInfo &info);
// 发布设备发现服务
int32_t PublishDeviceDiscovery(const char *pkgName, const PublishInfo &publishInfo,
                               const ISoftbusPublishCallback *callback);
// 取消发布设备发现服务
int32_t UnPublishDeviceDiscovery(const char *pkgName, int32_t publishId);
// 开始发现
int32_t StartDeviceDiscovery(const char *pkgName, const SubscribeInfo &subscribeInfo,
                             const ISoftbusDiscoveryCallback *callback);
// 停止发现
int32_t StopDeviceDiscovery(const char *pkgName, int32_t subscribeId);
// 开始认证
int32_t AuthenticateDevice(const char *pkgName, const char *deviceId);
// 删除认证设备
int32_t UnAuthenticateDevice(const char *pkgName, const char *networkId);
// 获取可信列表
int32_t GetTrustedDeviceList(const char *pkgName, AuthDeviceInfo **deviceInfo, int *infoNum);
// 取消客户端在服务侧的注册
int32_t DestoryClient(const char *pkgName);

#ifdef __cplusplus
}
#endif
#endif  // DEVICEMGR_SDK