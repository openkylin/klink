/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "devicemanagerservice.h"

#include <map>
#include <vector>
#include "securec.h"
#include "devicemgripc.h"
#include "anonymous.h"
#include "device_manager_center.h"
#include "device_manager_listenter.h"

static bool s_isAuthRuning = false;
static std::string s_authPkgName = "";
static std::map<std::string, int32_t> s_uiRegisterMap = {};
static std::map<std::string, int32_t> s_deviceStatePidMap = {};
static std::map<std::string, int32_t> s_publishPidMap = {};
static std::map<std::string, int32_t> s_discoveryPidMap = {};

using DeviceMgrServiceFunc = int32_t (*)(OHOS::Parcel &data, OHOS::Parcel &reply);
static std::map<uint32_t, DeviceMgrServiceFunc> s_memberFuncMap;

static int32_t RegisterDeviceStateCallbackInner(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    const char *pkgName = data.ReadCString();
    if (pkgName == nullptr) {
        DEV_SERV_LOG("read pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t pid;
    if (!data.ReadInt32(pid)) {
        DEV_SERV_LOG("read pid failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    s_deviceStatePidMap[pkgName] = pid;

    int32_t retReply = DEV_CENTER_OK;
    if (!reply.WriteInt32(retReply)) {
        DEV_SERV_LOG("write reply failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    return DEV_CENTER_OK;
}

static int32_t UnRegisterDeviceStateCallbackInner(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    const char *pkgName = data.ReadCString();
    if (pkgName == nullptr) {
        DEV_SERV_LOG("read pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (s_deviceStatePidMap.find(pkgName) == s_deviceStatePidMap.end()) {
        DEV_SERV_LOG("pkgName not found!");
        return ERR_DEV_CENTER_FAILED;
    }
    s_deviceStatePidMap.erase(pkgName);

    int32_t retReply = DEV_CENTER_OK;
    if (!reply.WriteInt32(retReply)) {
        DEV_SERV_LOG("write reply failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    return DEV_CENTER_OK;
}

static int32_t RegisterUIStateCallbackInner(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    const char *pkgName = data.ReadCString();
    if (pkgName == nullptr) {
        DEV_SERV_LOG("read pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t pid;
    if (!data.ReadInt32(pid)) {
        DEV_SERV_LOG("read pid failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!s_isAuthRuning) {
        s_authPkgName = pkgName;
    }
    s_uiRegisterMap[pkgName] = pid;

    int32_t retReply = DEV_CENTER_OK;
    if (!reply.WriteInt32(retReply)) {
        DEV_SERV_LOG("write reply failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    return DEV_CENTER_OK;
}

static int32_t UnRegisterUIStateCallbackInner(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    const char *pkgName = data.ReadCString();
    if (pkgName == nullptr) {
        DEV_SERV_LOG("read pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    s_uiRegisterMap.erase(pkgName);
    s_authPkgName = "";

    int32_t retReply = DEV_CENTER_OK;
    if (!reply.WriteInt32(retReply)) {
        DEV_SERV_LOG("write reply failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    return DEV_CENTER_OK;
}

static int32_t DestoryClient(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    const char *pkgName = data.ReadCString();
    if (pkgName == nullptr) {
        DEV_SERV_LOG("read pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t pid;
    if (!data.ReadInt32(pid)) {
        DEV_SERV_LOG("read pid failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    auto iter = s_uiRegisterMap.find(pkgName);
    if (iter != s_uiRegisterMap.end()) {
        if (iter->second == pid) {
            s_uiRegisterMap.erase(pkgName);
            s_authPkgName = "";
        }
    }
    iter = s_deviceStatePidMap.find(pkgName);
    if (iter != s_deviceStatePidMap.end()) {
        if (iter->second == pid) {
            s_deviceStatePidMap.erase(pkgName);
        }
    }
    iter = s_publishPidMap.find(pkgName);
    if (iter != s_publishPidMap.end()) {
        if (iter->second == pid) {
            s_publishPidMap.erase(pkgName);
        }
    }
    iter = s_discoveryPidMap.find(pkgName);
    if (iter != s_discoveryPidMap.end()) {
        if (iter->second == pid) {
            s_discoveryPidMap.erase(pkgName);
        }
    }

    int32_t retReply = DEV_CENTER_OK;
    if (!reply.WriteInt32(retReply)) {
        DEV_SERV_LOG("write reply failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    return DEV_CENTER_OK;
}

static int32_t GetLocalDeviceInfoInner(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    const char *pkgName = data.ReadCString();
    if (pkgName == nullptr) {
        DEV_SERV_LOG("read pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    AuthDeviceInfo info;
    if (GetLocalDeviceInfo(info) != DEV_CENTER_OK) {
        DEV_SERV_LOG("get local device info failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (WriteAuthDeviceInfo(reply, info) != DEV_CENTER_OK) {
        return ERR_DEV_CENTER_FAILED;
    }

    return DEV_CENTER_OK;
}

static int32_t PublishDeviceDiscoveryInner(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    const char *pkgName = data.ReadCString();
    if (pkgName == nullptr) {
        DEV_SERV_LOG("read pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t pid;
    if (!data.ReadInt32(pid)) {
        DEV_SERV_LOG("read pid failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    PublishInfo info;
    int32_t mode, medium, freq;
    (void)memset_s(&info, sizeof(PublishInfo), 0, sizeof(PublishInfo));
    if (!data.ReadInt32(info.publishId) || !data.ReadInt32(mode) || !data.ReadInt32(medium) || !data.ReadInt32(freq)) {
        DEV_SERV_LOG("read common publish info failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    info.mode = (DiscoverMode)mode;
    info.medium = (ExchangeMedium)medium;
    info.freq = (ExchangeFreq)freq;
    info.capability = data.ReadCString();
    if (info.capability == nullptr) {
        DEV_SERV_LOG("read capability failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.ReadUint32(info.dataLen)) {
        DEV_SERV_LOG("read dataLen failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (info.dataLen > 0 && info.dataLen < MAX_CAPABILITYDATA_LEN) {
        info.capabilityData = (unsigned char *)data.ReadCString();
        if (info.capabilityData == nullptr) {
            DEV_SERV_LOG("read capabilityData failed!");
            return ERR_DEV_CENTER_FAILED;
        }
    } else {
        info.capabilityData = nullptr;
        info.dataLen = 0;
    }
    if (!data.ReadBool(info.ranging)) {
        DEV_SERV_LOG("read ranging failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t retReply = PublishDeviceDiscovery(info);
    if (retReply == DEV_CENTER_OK) {
        s_publishPidMap[pkgName] = pid;
    }
    if (!reply.WriteInt32(retReply)) {
        DEV_SERV_LOG("write reply failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    return DEV_CENTER_OK;
}

static int32_t UnPublishDeviceDiscoveryInner(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    const char *pkgName = data.ReadCString();
    if (pkgName == nullptr) {
        DEV_SERV_LOG("read pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (s_publishPidMap.find(pkgName) == s_publishPidMap.end()) {
        DEV_SERV_LOG("pkgName not exist!");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t publishId;
    if (!data.ReadInt32(publishId)) {
        DEV_SERV_LOG("read publishId failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    s_publishPidMap.erase(pkgName);

    int32_t retReply = UnPublishDeviceDiscovery(publishId);
    if (!reply.WriteInt32(retReply)) {
        DEV_SERV_LOG("write reply failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    return DEV_CENTER_OK;
}

static int32_t StartDeviceDiscoveryInner(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    const char *pkgName = data.ReadCString();
    if (pkgName == nullptr) {
        DEV_SERV_LOG("read pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t pid;
    if (!data.ReadInt32(pid)) {
        DEV_SERV_LOG("read pid failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    SubscribeInfo info;
    int32_t mode, medium, freq;
    (void)memset_s(&info, sizeof(SubscribeInfo), 0, sizeof(SubscribeInfo));
    if (!data.ReadInt32(info.subscribeId) || !data.ReadInt32(mode) || !data.ReadInt32(medium) ||
        !data.ReadInt32(freq)) {
        DEV_SERV_LOG("read common subscribe info failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    info.mode = (DiscoverMode)mode;
    info.medium = (ExchangeMedium)medium;
    info.freq = (ExchangeFreq)freq;
    if (!data.ReadBool(info.isSameAccount) || !data.ReadBool(info.isWakeRemote)) {
        DEV_SERV_LOG("read subscribe info flag failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    info.capability = data.ReadCString();
    if (info.capability == nullptr) {
        DEV_SERV_LOG("read capability failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.ReadUint32(info.dataLen)) {
        DEV_SERV_LOG("read dataLen failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (info.dataLen > 0 && info.dataLen < MAX_CAPABILITYDATA_LEN) {
        info.capabilityData = (unsigned char *)data.ReadCString();
        if (info.capabilityData == nullptr) {
            DEV_SERV_LOG("read capabilityData failed!");
            return ERR_DEV_CENTER_FAILED;
        }
    } else {
        info.capabilityData = nullptr;
        info.dataLen = 0;
    }
    int32_t retReply = StartDeviceDiscovery(info);
    if (retReply == DEV_CENTER_OK) {
        s_discoveryPidMap[pkgName] = pid;
    }
    if (!reply.WriteInt32(retReply)) {
        DEV_SERV_LOG("write reply failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    return DEV_CENTER_OK;
}

static int32_t StopDeviceDiscoveryInner(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    const char *pkgName = data.ReadCString();
    if (pkgName == nullptr) {
        DEV_SERV_LOG("read pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (s_discoveryPidMap.find(pkgName) == s_discoveryPidMap.end()) {
        DEV_SERV_LOG("pkgName not exist!");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t subscribeId;
    if (!data.ReadInt32(subscribeId)) {
        DEV_SERV_LOG("read subscribeId failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t retReply = StopDeviceDiscovery(subscribeId);
    if (!reply.WriteInt32(retReply)) {
        DEV_SERV_LOG("write reply failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    return DEV_CENTER_OK;
}

static int32_t AuthenticateDeviceInner(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    if (s_isAuthRuning) {
        DEV_SERV_LOG("AuthenticateDevice is runing!");
        return ERR_DEV_CENTER_FAILED;
    }
    const char *pkgName = data.ReadCString();
    if (pkgName == nullptr) {
        DEV_SERV_LOG("read pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (s_uiRegisterMap.find(pkgName) == s_uiRegisterMap.end()) {
        DEV_SERV_LOG("ui register pkgName not found!");
        return ERR_DEV_CENTER_FAILED;
    }
    s_isAuthRuning = true;

    const char *deviceId = data.ReadCString();
    if (deviceId == nullptr) {
        DEV_SERV_LOG("read deviceId failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t retReply = AuthenticateDevice(std::string(deviceId));
    if (!reply.WriteInt32(retReply)) {
        DEV_SERV_LOG("write reply failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    return DEV_CENTER_OK;
}

static int32_t UnAuthenticateDeviceInner(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    const char *pkgName = data.ReadCString();
    if (pkgName == nullptr) {
        DEV_SERV_LOG("read pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    const char *networkId = data.ReadCString();
    if (networkId == nullptr) {
        DEV_SERV_LOG("read deviceId failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t retReply = UnAuthenticateDevice(std::string(networkId));
    if (!reply.WriteInt32(retReply)) {
        DEV_SERV_LOG("write reply failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    return DEV_CENTER_OK;
}

static int32_t GetTrustedDeviceListInner(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    const char *pkgName = data.ReadCString();
    if (pkgName == nullptr) {
        DEV_SERV_LOG("read pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    AuthDeviceInfo *info = nullptr;
    int32_t infoNum;
    if (GetTrustedDeviceList(&info, &infoNum) != DEV_CENTER_OK) {
        DEV_SERV_LOG("GetTrustedDeviceList failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (infoNum < 0 || (infoNum > 0 && info == nullptr)) {
        DEV_SERV_LOG("node info is invalid! info Num: %d", infoNum);
        return ERR_DEV_CENTER_FAILED;
    }
    if (!reply.WriteInt32(infoNum)) {
        DEV_SERV_LOG("write infoNum failed!");
        free(info);
        return ERR_DEV_CENTER_FAILED;
    }
    if (infoNum > 0) {
        int32_t infoLen = sizeof(AuthDeviceInfo);
        infoLen = infoLen * infoNum;
        if (!reply.WriteInt32(infoLen)) {
            DEV_SERV_LOG("write node size failed!");
            return ERR_DEV_CENTER_FAILED;
        }
        if (!reply.WriteUnpadBuffer((void *)info, infoLen)) {
            DEV_SERV_LOG("write node info failed!");
            return ERR_DEV_CENTER_FAILED;
        }
    }

    return DEV_CENTER_OK;
}

static int32_t GetDeviceInfoInner(OHOS::Parcel &data, OHOS::Parcel &reply)
{
    int32_t retReply = DEV_CENTER_OK;
    if (!reply.WriteInt32(retReply)) {
        DEV_SERV_LOG("write reply failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    return DEV_CENTER_OK;
}

static void InitMemberServerFuncMap()
{
    s_memberFuncMap[SERVER_REG_STATE_CB] = &RegisterDeviceStateCallbackInner;
    s_memberFuncMap[SERVER_UNREG_STATE_CB] = &UnRegisterDeviceStateCallbackInner;
    s_memberFuncMap[SERVER_REG_UI_CB] = &RegisterUIStateCallbackInner;
    s_memberFuncMap[SERVER_UNREG_UI_CB] = &UnRegisterUIStateCallbackInner;
    s_memberFuncMap[SERVER_DESTORY_CLIENT] = &DestoryClient;
    s_memberFuncMap[SERVER_GET_LOCAL_DEVICE_INFO] = &GetLocalDeviceInfoInner;
    s_memberFuncMap[SERVER_PUBLISH_DEVICE_DISCOVERY] = &PublishDeviceDiscoveryInner;
    s_memberFuncMap[SERVER_UNPUBLISH_DEVICE_DISCOVERY] = &UnPublishDeviceDiscoveryInner;
    s_memberFuncMap[SERVER_START_DEVICE_DISCOVERY] = &StartDeviceDiscoveryInner;
    s_memberFuncMap[SERVER_STOP_DEVICE_DISCOVERY] = &StopDeviceDiscoveryInner;
    s_memberFuncMap[SERVER_AUTH_DEVICE] = &AuthenticateDeviceInner;
    s_memberFuncMap[SERVER_UNAUTH_DEVICE] = &UnAuthenticateDeviceInner;
    s_memberFuncMap[SERVER_GET_TRUSTED_DEVICE_LIST] = &GetTrustedDeviceListInner;
    s_memberFuncMap[SERVER_GET_DEVICE_INFO] = &GetDeviceInfoInner;
}

static void OnRequest(int32_t code, OHOS::Parcel &data, OHOS::Parcel &reply)
{
    DEV_SERV_LOG("Server, code = %d", code);
    if (s_memberFuncMap.find(code) != s_memberFuncMap.end()) {
        int32_t retReply = s_memberFuncMap[code](data, reply);
        if ((retReply != DEV_CENTER_OK) && !reply.WriteInt32(retReply)) {
            DEV_SERV_LOG("Request:%d write reply failed!", code);
            return;
        }
    }

    return;
}

static DeviceIpcRequest s_ipcRequest = {.OnRequest = OnRequest};

static void OnPublishResult(int32_t publishId, int32_t publishResult)
{
    for (auto &iter : s_publishPidMap) {
        OHOS::Parcel data;
        OHOS::Parcel reply;
        std::string pkgName = iter.first;
        int32_t pid = iter.second;
        if (!data.WriteCString(pkgName.c_str())) {
            DEV_SERV_LOG("write pkgName failed!");
            return;
        }
        if (!data.WriteInt32(publishId)) {
            DEV_SERV_LOG("write publishId failed!");
            return;
        }
        if (!data.WriteInt32(publishResult)) {
            DEV_SERV_LOG("write publishResult failed!");
            return;
        }
        int32_t ret = SendRequest(CLIENT_ON_PUBLISH_RESULT, ResultOption::TF_ASYNC, data, reply, pid);
        if (ret != DEV_CENTER_OK) {
            DEV_SERV_LOG("SendRequest failed! ret = %d", ret);
        }
    }

    return;
}

static ISoftbusPublishCallback s_publishCallback = {.OnPublishResult = OnPublishResult};

static void OnDeviceFound(const AuthDeviceInfo &info, bool isOnline)
{
    for (auto &iter : s_discoveryPidMap) {
        OHOS::Parcel data;
        OHOS::Parcel reply;
        std::string pkgName = iter.first;
        int32_t pid = iter.second;
        if (!data.WriteCString(pkgName.c_str())) {
            DEV_SERV_LOG("write pkgName failed!");
            return;
        }
        if (!data.WriteBool(isOnline)) {
            DEV_SERV_LOG("write Online status failed!");
            return;
        }
        if (WriteAuthDeviceInfo(data, info) != DEV_CENTER_OK) {
            DEV_SERV_LOG("write info failed!");
            return;
        }
        int32_t ret = SendRequest(CLIENT_ON_DEVICE_FOUND, ResultOption::TF_ASYNC, data, reply, pid);
        if (ret != DEV_CENTER_OK) {
            DEV_SERV_LOG("SendRequest failed! ret = %d", ret);
        }
    }
    return;
}

static void OnDiscoverySuccess(int32_t subscribeId)
{
    for (auto &iter : s_discoveryPidMap) {
        OHOS::Parcel data;
        OHOS::Parcel reply;
        std::string pkgName = iter.first;
        int32_t pid = iter.second;
        if (!data.WriteCString(pkgName.c_str())) {
            DEV_SERV_LOG("write pkgName failed!");
            return;
        }
        if (!data.WriteInt32(subscribeId)) {
            DEV_SERV_LOG("write subscribeId failed!");
            return;
        }
        int32_t ret = SendRequest(CLIENT_ON_DISCOVERY_SUCC, ResultOption::TF_ASYNC, data, reply, pid);
        if (ret != DEV_CENTER_OK) {
            DEV_SERV_LOG("SendRequest failed! ret = %d", ret);
        }
    }
    return;
}

static void OnDiscoveryFailed(int32_t subscribeId, int32_t failedReason)
{
    for (auto &iter : s_discoveryPidMap) {
        OHOS::Parcel data;
        OHOS::Parcel reply;
        std::string pkgName = iter.first;
        int32_t pid = iter.second;
        if (!data.WriteCString(pkgName.c_str())) {
            DEV_SERV_LOG("write pkgName failed!");
            return;
        }
        if (!data.WriteInt32(subscribeId)) {
            DEV_SERV_LOG("write subscribeId failed!");
            return;
        }
        if (!data.WriteInt32(failedReason)) {
            DEV_SERV_LOG("write failedReason failed!");
            return;
        }
        int32_t ret = SendRequest(CLIENT_ON_DISCOVERY_FAIL, ResultOption::TF_ASYNC, data, reply, pid);
        if (ret != DEV_CENTER_OK) {
            DEV_SERV_LOG("SendRequest failed! ret = %d", ret);
        }
    }
    return;
}

static ISoftbusDiscoveryCallback s_discoveryCallback = {
    .OnDeviceFound = OnDeviceFound, .OnDiscoverySuccess = OnDiscoverySuccess, .OnDiscoveryFailed = OnDiscoveryFailed};

static void OnDeviceChanged(AuthDeviceState state, const AuthDeviceInfo &info)
{
    for (auto &iter : s_deviceStatePidMap) {
        OHOS::Parcel data;
        OHOS::Parcel reply;
        std::string pkgName = iter.first;
        int32_t pid = iter.second;
        if (!data.WriteCString(pkgName.c_str())) {
            DEV_SERV_LOG("write pkgName failed!");
            return;
        }
        if (!data.WriteInt32((int32_t)state)) {
            DEV_SERV_LOG("write state failed!");
            return;
        }
        if (WriteAuthDeviceInfo(data, info) != DEV_CENTER_OK) {
            DEV_SERV_LOG("write info failed!");
            return;
        }

        int32_t ret = SendRequest(CLIENT_ON_DEVICE_CHANGED, ResultOption::TF_ASYNC, data, reply, pid);
        if (ret != DEV_CENTER_OK) {
            DEV_SERV_LOG("SendRequest failed! ret = %d", ret);
        }
    }
    return;
}

static ISoftbusStateCallback s_stateCallback = {.OnDeviceChanged = OnDeviceChanged};

static int32_t GetUiCallPid()
{
    auto iter = s_uiRegisterMap.find(s_authPkgName);
    if (iter == s_uiRegisterMap.end()) {
        DEV_SERV_LOG("Uistate callback is nullpter");
        return ERR_DEV_CENTER_FAILED;
    }

    return iter->second;
}

static void OnShowAuthState(int32_t msgType)
{
    int32_t pid = GetUiCallPid();
    if (pid <= 0) {
        DEV_SERV_LOG("GetUiCallPid failed");
        return;
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(s_authPkgName.c_str())) {
        DEV_SERV_LOG("write pkgName failed!");
        return;
    }
    if (!data.WriteInt32(msgType)) {
        DEV_SERV_LOG("write msgType failed!");
        return;
    }

    int32_t ret = SendRequest(CLIENT_ON_SHOW_AUTH_STATE, ResultOption::TF_ASYNC, data, reply, pid);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("SendRequest failed! ret = %d", ret);
    }

    return;
}

static void OnShowAuthInfo(int32_t code)
{
    int32_t pid = GetUiCallPid();
    if (pid <= 0) {
        DEV_SERV_LOG("GetUiCallPid failed");
        return;
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(s_authPkgName.c_str())) {
        DEV_SERV_LOG("write pkgName failed!");
        return;
    }
    if (!data.WriteInt32(code)) {
        DEV_SERV_LOG("write code failed!");
        return;
    }

    int32_t ret = SendRequest(CLIENT_ON_SHOW_AUTH_INFO, ResultOption::TF_ASYNC, data, reply, pid);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("SendRequest failed! ret = %d", ret);
    }

    return;
}

static int32_t OnUserOperation(const char *remoteName)
{
    int32_t pid = GetUiCallPid();
    if (pid <= 0) {
        DEV_SERV_LOG("GetUiCallPid failed");
        return ERR_DEV_CENTER_FAILED;
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(s_authPkgName.c_str())) {
        DEV_SERV_LOG("write pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }
    if (!data.WriteCString(remoteName)) {
        DEV_SERV_LOG("write remoteName failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t ret = SendRequest(CLIENT_ON_USER_OPERATION, ResultOption::TF_SYNC, data, reply, pid);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("SendRequest failed! ret = %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    if (!reply.ReadInt32(ret)) {
        DEV_SERV_LOG("read ret failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    return ret;
}

static void OnCancelDisplay()
{
    int32_t pid = GetUiCallPid();
    if (pid <= 0) {
        DEV_SERV_LOG("GetUiCallPid failed");
        return;
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(s_authPkgName.c_str())) {
        DEV_SERV_LOG("write pkgName failed!");
        return;
    }

    int32_t ret = SendRequest(CLIENT_ON_CANCEL_DISPLAY, ResultOption::TF_ASYNC, data, reply, pid);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("SendRequest failed! ret = %d", ret);
    }

    return;
}

static int32_t OnInputAuthInfo()
{
    int32_t pid = GetUiCallPid();
    if (pid <= 0) {
        DEV_SERV_LOG("GetUiCallPid failed");
        return ERR_DEV_CENTER_FAILED;
    }
    OHOS::Parcel data;
    OHOS::Parcel reply;
    if (!data.WriteCString(s_authPkgName.c_str())) {
        DEV_SERV_LOG("write pkgName failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    int32_t ret = SendRequest(CLIENT_ON_INPUT_AUTH_INFO, ResultOption::TF_SYNC, data, reply, pid);
    if (ret != DEV_CENTER_OK) {
        DEV_SERV_LOG("SendRequest failed! ret = %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    if (!reply.ReadInt32(ret)) {
        DEV_SERV_LOG("read ret failed!");
        return ERR_DEV_CENTER_FAILED;
    }

    return ret;
}

static IUIStateUpdateCallback s_uiStateCallback = {.OnShowAuthState = OnShowAuthState,
                                                   .OnShowAuthInfo = OnShowAuthInfo,
                                                   .OnUserOperation = OnUserOperation,
                                                   .OnCancelDisplay = OnCancelDisplay,
                                                   .OnInputAuthInfo = OnInputAuthInfo};

static void OnAuthResult(const std::string &deviceId, const std::string &token, int32_t status, int32_t reason)
{
    s_isAuthRuning = false;
    return;
}

static IAuthResultCallback s_authResultCallback = {.OnAuthResult = OnAuthResult};

void InitDeviceManagerService()
{
    DEV_SERV_LOG("init device manager service begin");
    InitMemberServerFuncMap();
    InitDeviceManagerCenter();
    RegisterPublishCallback(&s_publishCallback);
    RegisterDiscoverCallback(&s_discoveryCallback);
    RegisterDeviceStateCallback(&s_stateCallback);
    RegisterUIStateCallback(&s_uiStateCallback);
    RegisterAuthResultCallback(&s_authResultCallback);
    InitIpc(true, &s_ipcRequest);
    DEV_SERV_LOG("init device manager service end");
}