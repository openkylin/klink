/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_MSG
#define APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_MSG

#include <inttypes.h>

#define DCONN_MAX_MSG_LEN 2048

#define DCONN_AUTH_HEADER_LEN 16
#define DCONN_AUTH_BODY_LEN 2032       // DCONN_MAX_MSG_LEN - DCONN_AUTH_HEADER_LEN
#define DCONN_DEVICE_ID_BODY_LEN 2044  // DCONN_MAX_MSG_LEN - sizeof(DConnMsgCommonHeader)
#define DCONN_MAX_KEY_LEN 32

enum DConnMsgType {
    DCONN_MSG_TYPE_AUTH = 0,
    DCONN_MSG_TYPE_DATA = 1,
    DCONN_MSG_TYPE_DEVICE_ID = 2,
    DCONN_MSG_TYPE_NOTIFY = 3,
};

typedef struct {
    uint64_t requestId;  // Hichain Request ID
} DConnAuthParam;

typedef struct {
    uint8_t *key;     // 密钥
    uint32_t keyLen;  // 密钥长度
} DConnDataParam;

typedef struct {
    const uint8_t *data;
    uint32_t dataLen;
} DConnMsgIn;

typedef struct {
    uint8_t *msg;
    uint32_t msgLen;
} DConnMsgOut;

/**
 * @brief 编码数据
 *
 * @param type 数据类型 DConnMsgType
 * @param data 明文数据
 * @param dataLen 明文数据长度
 * @param encParam 编码参数，需要与 type 对应
 * @param msg 编码后的字节流数据，调用者释放
 * @param msgLen 编码后的字节流数据长度
 * @return uint32_t 错误码
 */
uint32_t DConnEncMsg(uint16_t type, const void *encParam, const DConnMsgIn *in, DConnMsgOut *out);

/**
 * @brief 解码数据
 *
 * @param type 数据类型 DConnMsgType
 * @param msg 编码后的数据，需要与 type 对应
 * @param decParam 解码参数，需要与 type 对应
 * @param data 明文数据，调用者释放
 * @param dataLen 明文数据长度
 * @return uint32_t 错误码
 */
uint32_t DConnDecMsg(uint16_t type, void *decParam, const DConnMsgIn *in, DConnMsgOut *out);

/**
 * @brief 从编码的消息中获取消息类型
 *
 * @param data 编码的消息
 * @param dataLen 编码的消息长度
 * @param type 消息类型
 * @return uint32_t 错误码
 */
uint32_t DConnGetMsgType(const uint8_t *data, uint32_t dataLen, uint16_t *type);

#endif  // APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_MSG
