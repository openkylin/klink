/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DCONN_SEND_H
#define DCONN_SEND_H

#include <stdint.h>

/**
 * @brief 将数据发送给对等端
 *
 * @param idx 对等端索引
 * @param data 数据
 * @param dataLen 数据长度
 * @return uint32_t 错误码
 */
uint32_t SendDataToPeer(int32_t idx, uint8_t *data, uint32_t dataLen);

#endif  // DCONN_SEND_H