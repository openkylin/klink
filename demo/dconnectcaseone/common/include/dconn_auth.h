/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_AUTH
#define APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_AUTH

#include <stdint.h>

#define HC_CREATE_GROUP_REQUEST_ID 0  // 创建群组 request ID

/**
 * @brief Hichain processData proxy
 *
 * @param idx 索引
 * @param requestId 请求ID
 * @param data 数据
 * @param dataLen 数据长度
 * @return int32_t 错误码
 */
uint32_t HcProcessData(int32_t idx, int64_t requestId, const uint8_t *data, uint32_t dataLen);

/**
 * @brief Hichain 服务端群组初始化。
 *
 * @return int32_t 初始化结果
 */
uint32_t InitHcService(void);

/**
 * @brief 服务端创建 Hichain 群组
 *
 * @return int32_t 创建结果
 */
uint32_t CreateHcGroup(void);

/**
 * @brief 处理处于接收设备ID状态的对等端
 *
 * @param idx 对等端索引
 * @param data 设备ID报文（未校验）
 * @param dataLen 设备ID报文长度
 */
void HandleExpectDeviceIdStat(int32_t idx, const uint8_t *data, uint32_t dataLen);

#endif  // APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_AUTH