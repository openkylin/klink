/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_THREAD
#define APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_THREAD

#include <stdint.h>
#include <stdbool.h>

/**
 * @brief 套件线程状态。用于正确地结束线程，释放资源
 *
 */
enum DConnThrStat {
    THR_STAT_DISABLE = 0,  // 去激活态
    THR_STAT_ENABLE,       // 激活态
    THR_STAT_EXIT,         // 退出态
};

/**
 * @brief 使能线程
 *
 * @return uint32_t 错误码
 */
uint32_t EnableTransferThr(void *(*func)(void *));

/**
 * @brief 检查线程是否为指定状态
 *
 * @param stat 指定的线程状态
 * @return true
 * @return false
 */
bool IsThreadInStat(uint32_t stat);

/**
 * @brief 设置线程状态
 *
 * @param stat 线程状态
 */
void SetThreadStat(uint32_t stat);

/**
 * @brief 通知线程退出
 *
 */
void ExitTransferThr(void);

/**
 * @brief 等待线程退出
 *
 */
void WaitTransferThrDisabled(void);

#endif  // APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_THREAD
