/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DCONN_CJSON_H
#define DCONN_CJSON_H

#include <stdbool.h>
#include <stdint.h>

#include "cJSON.h"

const char *DConnGetStringFromCJson(const cJSON *jsonObj, const char *key);
int32_t DConnGetInt64FromJson(const cJSON *jsonObj, const char *key, int64_t *value);

void DConnAddIntToJson(cJSON *jsonObj, const char *key, int value);
void DConnAddInt64StringToJson(cJSON *jsonObj, const char *key, int64_t value);
void DConnAddStringToJson(cJSON *jsonObj, const char *key, const char *value);
void DConnAddBoolToJson(cJSON *jsonObj, const char *key, bool value);

#endif  // DCONN_CJSON_H