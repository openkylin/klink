/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DCONN_GROUP_MANAGER_H
#define DCONN_GROUP_MANAGER_H

#include <stdint.h>

/**
 * @brief 设备认证服务初始化
 *
 * @return int32_t 初始化结果
 */
uint32_t DConnInitDeviceAuthService(void);

/**
 * @brief 服务端创建 设备认证 群组
 *
 * @return int32_t 创建结果
 */
uint32_t CreateDeviceAuthGroup(void);

/**
 * @brief 邀请对端设备加入群组。
 *
 * @param idx 对端索引
 * @return uint32_t 错误码
 */
uint32_t InvitePeerIntoGroup(int32_t idx);

/**
 * @brief 处理 Add Member 自定义通道传输的数据
 *
 * @param requestId Request ID
 * @param data 解码后的数据
 * @param dataLen 解码后的数据长度
 * @return uint32_t 错误码
 */
uint32_t ProcessAddMemberData(int64_t requestId, const uint8_t *data, uint32_t dataLen);

#endif  // DCONN_GROUP_MANAGER_H