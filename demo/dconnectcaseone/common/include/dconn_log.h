/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_LOG
#define APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_LOG

#include <stdint.h>
#include <stdio.h>

typedef enum {
    DCONN_LOG_LEVEL_DEBUG = 0,
    DCONN_LOG_LEVEL_INFO,
    DCONN_LOG_LEVEL_WARN,
    DCONN_LOG_LEVEL_ERROR
} DConnLogLevel;

#ifdef OHOS_DEBUG
#    define DCONN_DEBUG(fmt, ...) (DConnLogPrint(DCONN_LOG_LEVEL_DEBUG, __FUNCTION__, fmt, ##__VA_ARGS__))
#else
#    define DCONN_DEBUG(fmt, ...) printf(fmt "\n", ##__VA_ARGS__)
#endif
#define DCONN_INFO(fmt, ...) (DConnLogPrint(DCONN_LOG_LEVEL_INFO, __FUNCTION__, fmt, ##__VA_ARGS__))
#define DCONN_WARN(fmt, ...) (DConnLogPrint(DCONN_LOG_LEVEL_WARN, __FUNCTION__, fmt, ##__VA_ARGS__))
#define DCONN_ERROR(fmt, ...) (DConnLogPrint(DCONN_LOG_LEVEL_ERROR, __FUNCTION__, fmt, ##__VA_ARGS__))

#define IPV4_ANONYMOUS_PLACEHOLDER ".*.*.%"
#define IPV6_ANONYMOUS_PLACEHOLDER ":*:*:*:*:*:*:%"

#define IS_IPV4_ADDRESS(ipAddr) (strstr(ipAddr, ":") == NULL && strstr(ipAddr, ".") != NULL)

void DumpBuf(uint8_t *buf, uint32_t len);
void DConnLogPrint(DConnLogLevel level, const char *funName, const char *fmt, ...);
uint32_t DconnIpTransToInt(const char *ipAddr);

#endif  // APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_LOG
