/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_PEER
#define APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_PEER

#define DCONN_SERVICE_PORT 8888
#define DCONN_UDID_LEN 64

enum PeerAuthStat {
    PEER_AUTH_STAT_UNAUTHORIZED = 0b00,
    PEER_AUTH_EXPECT_DEVICE_ID = 0b0011,  // 等待发送设备 ID
    PEER_AUTH_ADD_MEMBER = 0b0001,        // 添加设备进群组
    PEER_AUTH_AUTH_DEVICE = 0b0010,       // 授权设备
    PEER_AUTH_STAT_AUTHORIZED = 0b0100,   // 设备认证完成
    PEER_AUTH_STAT_INVALID = 0b1000,      // 无效
};

/**
 * @brief 对等端角色
 *
 */
enum PeerRole {
    PEER_ROLE_SERVICE = 0,
    PEER_ROLE_CLIENT = 1,
};

#define PEER_AUTH_STAT_END_MASK 0b1100  // 设备认证结束

#endif  // APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_PEER
