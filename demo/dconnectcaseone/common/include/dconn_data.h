/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_DATA
#define APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_DATA

#include <stdbool.h>
#include <stdint.h>
#include <sys/select.h>
#include <stdint.h>

#include "dconncaseone_interface.h"

/**
 * @brief 初始化套件数据模块。可重入，不会重复初始化。
 *
 * @return true 初始化成功
 * @return false 初始化失败
 */
uint32_t InitDConnData(void);

/**
 * @brief 释放全局资源
 *
 */
void DeInitDconnData(void);

/**
 * @brief 检查当前的对等端列表中，是否已经存在输入的 IP 地址
 *
 * @param ipAddr IP 地址
 * @return true 存在
 * @return false 不存在
 */
bool ContainsIpAddr(char *ipAddr);

/**
 * @brief 在系统控制块中获取一个新的对等端数据
 *
 * @param role 对等端角色 PeerRole
 * @return int32_t 获取成功，返回对等端数据索引。获取失败，返回 -1
 */
int32_t GetNewPeerDataIdx(uint32_t role);

/**
 * @brief 释放一个对等数据。包括关闭 socket
 *
 * @param idx 该对等数据的索引
 */
void ReleasePeerDataIdx(int32_t idx);

/**
 * @brief 获取对等端角色
 *
 * @param idx 对等端索引
 * @return uint32_t 角色 PeerRole
 */
uint32_t GetPeerRole(int32_t idx);

/**
 * @brief 根据 peer data 索引获取socket
 *
 * @param idx peer data 索引
 * @return int* socket fd 指针
 */
int *GetSocketByPeerDataIdx(int32_t idx);

/**
 * @brief 设置对等端 socket
 *
 * @param idx 对等端索引
 * @param socket 对等端 socket
 */
void SetSocketByPeerDataIdx(int32_t idx, int socket);

/**
 * @brief 根据 peer data 索引获取认证状态
 *
 * @param idx peer data 索引
 * @return uint32_t 认证状态 PeerAuthStat
 */
uint32_t GetAuthStatByPeerDataIdx(int32_t idx);

/**
 * @brief Set the Auth Stat By Peer Data Idx object
 *
 * @param idx peer data 索引
 * @param sta 认证状态 PeerAuthStat
 */
void SetAuthStatByPeerDataIdx(int32_t idx, uint32_t sta);

/**
 * @brief Get the Request Id By Peer Data Idx object
 *
 * @param idx 索引
 * @return int64_t 请求ID
 */
int64_t GetRequestIdByPeerDataIdx(int32_t idx);

/**
 * @brief Get the Idx By Request Id object
 *
 * @param requestId 请求ID
 * @return int32_t 索引
 */
int32_t GetIdxByRequestId(int64_t requestId);

/**
 * @brief Set the Session Key By Peer Idx object
 *
 * @param idx 索引
 * @param sessionKey 密钥
 * @param sessionKeyLen 密钥长度
 */
void SetSessionKeyByPeerIdx(int32_t idx, const uint8_t *sessionKey, uint32_t sessionKeyLen);

/**
 * @brief Get the Idx By Device Id object
 *
 * @param deviceId 设备ID
 * @return uint32_t 索引
 */
int32_t GetIdxByDeviceId(const char *deviceId);

/**
 * @brief Get the Session Key By Peer Idx object
 *
 * @param idx 索引
 * @param sessionKey 密钥
 * @param keyLength 密钥长度
 */
void GetSessionKeyByPeerIdx(int32_t idx, uint8_t **sessionKey, uint32_t *keyLength);

/**
 * @brief Set the Request Id By Peer Data Idx object
 *
 * @param idx 索引
 * @param requestId 请求ID
 */
void SetRequestIdByPeerDataIdx(int32_t idx, int64_t requestId);

/**
 * @brief Get the Device Id By Peer idx
 *
 * @param idx 对等端索引
 * @return NULL 未找到
 */
char *GetDeviceIdByPeerIdx(int32_t idx);

/**
 * @brief Set the Device Id For Idx object
 *
 * @param idx 对等端索引
 * @param deviceId 设备ID
 */
void SetDeviceIdForIdx(int32_t idx, const char *deviceId);

/**
 * @brief Get the UDID By Peer idx
 *
 * @param idx 对等端索引
 * @return NULL 未找到
 */
const char *GetUdidByPeerIdx(int32_t idx);

/**
 * @brief Set the UDID For Idx object
 *
 * @param idx 对等端索引
 * @param udid UDID
 */
void SetUdidForIdx(int32_t idx, const char *udid);

/**
 * @brief Get the Peer Data Idx By Udid object
 *
 * @param udid UDID
 * @return int32_t 对等端索引
 */
int32_t GetPeerDataIdxByUdid(const char *udid);

/**
 * @brief Get the Peer Fd By Device Id object
 *
 * @param deviceId 设备 ID
 * @param fd 对等端 FD
 * @return true 找到
 * @return false 未找到
 */
bool GetPeerFdByDeviceId(const char *deviceId, int *fd, bool authorized);

/**
 * @brief 获取服务器 socket 指针
 *
 * @return int 服务器 socket
 */
int GetServerSocket(void);

/**
 * @brief 设置服务器 socket 指针
 *
 * @param fd 服务器 socket
 */
void SetServerSocket(int fd);

/**
 * @brief 获取服务端 socket 地址族
 *
 * @return int* AF_INET/AF_INET6
 */
int *GetServerSocketAddrFamily(void);

/**
 * @brief 获取 IOT SDK回调函数
 *
 * @return CallbackParam* 回调函数指针
 */
CallbackParam *GetIotSdkCallback(void);

/**
 * @brief 初始化 select 列表。仅放入认证成功的对等端 socket
 *
 * @param readSet 读 fd 列表
 * @param exceptSet 异常 fd 列表
 * @return int 最大 fd 值。若没有任何 fd 被设置，则返回 -1
 */
int InitSelectFdSet(fd_set *readSet, fd_set *exceptSet);

/**
 * @brief Peer idx Handler
 *
 */
typedef void (*PeerIdxHandler)(int32_t fd);

/**
 * @brief Server socket Handler
 *
 */
typedef void (*ServerSocketHandler)(void);

/**
 * @brief 处理 fd set
 *
 * @param fdSet The fd set obj
 * @param handler The single peer idx handler
 * @param serverHandler The server socket Handler. Leave NULL for unused.
 */
void HandleFdSet(const fd_set *fdSet, PeerIdxHandler handler, ServerSocketHandler serverHandler);

/**
 * @brief 检查IP地址是否在client端记录中
 *
 * @param ipAddr IP 地址
 * @return DC_SUCCESS 存在
 * @return DC_ERROR   不存在
 */
uint32_t HasPeerIpAddr(uint32_t ipAddr);

/**
 * @brief Set the IP address For Idx object
 *
 * @param idx 对等端索引
 * @param ipAddr IP 地址
 */
void SetIpAddrByIdx(int32_t idx, uint32_t ipAddr);

/**
 * @brief 检查IP地址是否在server端记录中
 *
 * @return DC_SUCCESS 存在
 * @return DC_ERROR   不存在
 */
uint32_t HasServerIpAddr(void);

#endif  // APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_DATA
