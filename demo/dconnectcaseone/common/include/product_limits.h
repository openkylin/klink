/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUE_PRODUCT_LIMITS
#define APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUE_PRODUCT_LIMITS

#define MAX_DEVICES_NUM 100      // 客户端最多允许连接 100 目标设备
#define SESSION_KEY_LEN 32       // session key 最大长度
#define HICHAIN_AUTH_TIMEOUT 10  // Hichain 认证超时时间

#endif  // APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUE_PRODUCT_LIMITS