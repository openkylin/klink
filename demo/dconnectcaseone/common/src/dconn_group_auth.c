/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dconn_group_auth.h"

#include <stdlib.h>
#include <securec.h>
#include <stdbool.h>
#include <inttypes.h>

#include "cJSON.h"

#include "dconn_group_auth.h"
#include "dconn_log.h"
#include "dconn_msg.h"
#include "dconn_peer.h"
#include "dconn_data.h"
#include "dconn_cjson.h"
#include "device_auth.h"
#include "dconn_data_impl.h"
#include "dconn_auth_defines.h"
#include "dconncaseone_interface.h"
#include "dconn_group_common.h"

static bool OnGaTransmit(int64_t requestId, const uint8_t *data, uint32_t dataLen);
static void OnGaSessionKeyReturned(int64_t requestId, const uint8_t *sessionKey, uint32_t sessionKeyLen);
static void OnGaFinish(int64_t requestId, int operationCode, const char *returnData);
static void OnGaError(int64_t requestId, int operationCode, int errorCode, const char *errorReturn);
static char *OnGaRequest(int64_t authReqId, int authForm, const char *reqParams);

static const DeviceAuthCallback g_groupAuthCallback = {
    .onTransmit = OnGaTransmit,
    .onSessionKeyReturned = OnGaSessionKeyReturned,
    .onFinish = OnGaFinish,
    .onError = OnGaError,
    .onRequest = OnGaRequest,
};

static bool OnGaTransmit(int64_t requestId, const uint8_t *data, uint32_t dataLen)
{
    DCONN_DEBUG("requestID: %" PRId64 ", datalen: %" PRId32, requestId, dataLen);
    return OnCommonTransmit(requestId, data, dataLen);
}

static void OnGaSessionKeyReturned(int64_t requestId, const uint8_t *sessionKey, uint32_t sessionKeyLen)
{
    int32_t idx = GetIdxByRequestId(requestId);
    if (idx < 0) {
        DCONN_ERROR("GetIdxByRequestId failed by request ID: %" PRId64, requestId);
        return;
    }

    SetSessionKeyByPeerIdx(idx, sessionKey, sessionKeyLen);

    uint32_t role = GetPeerRole(idx);
    if (role == PEER_ROLE_SERVICE) {
        return;
    }

    char message[] = DCONN_SESSION_KEY_RETURNED_MESSAGE;
    uint32_t ret = SendNotifyMessage(idx, message, sizeof(message));
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Service send session key returned message failed for idx: %" PRId32 " ret: %" PRIu32, idx, ret);
        SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
    } else {
        DCONN_INFO("Service send session key returned message succeed for idx: %" PRId32, idx);
        SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_AUTHORIZED);
    }
}

static void OnGaFinish(int64_t requestId, int operationCode, const char *returnData)
{
    DCONN_DEBUG("Ignored requestID: %" PRId64, requestId);
    (void)requestId;
    (void)operationCode;
    (void)returnData;
}

static void OnGaError(int64_t requestId, int operationCode, int errorCode, const char *errorReturn)
{
    DCONN_INFO("AuthOnError requestID: %lld, operationCode: %d, errorCode: %d, errorReturn: %s", requestId,
               operationCode, errorCode, errorReturn);

    int32_t idx = GetIdxByRequestId(requestId);
    if (idx < 0) {
        DCONN_INFO("auth error but peer idx not found by request ID: %lld", requestId);
        return;
    }

    SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
}

static char *OnGaRequest(int64_t authReqId, int authForm, const char *reqParams)
{
    (void)authForm;
    (void)reqParams;
    DCONN_DEBUG("authReqId: %lld, authForm: %d, reqParams: %s", authReqId, authForm, reqParams);
    int32_t idx = GetIdxByRequestId(authReqId);
    if (idx < 0) {
        DCONN_ERROR("Cannot find peer by request ID: %" PRId64, authReqId);
        return BuildRejectConfirmation();
    }

    const char *udid = GetUdidByPeerIdx(idx);
    cJSON *rspJson = cJSON_CreateObject();
    if (rspJson == NULL) {
        DCONN_ERROR("Failed to allocate rspJson memory.");
        return NULL;
    }
    DConnAddStringToJson(rspJson, FIELD_SERVICE_PKG_NAME, DCONN_AUTH_APPID);
    DConnAddStringToJson(rspJson, FIELD_PEER_CONN_DEVICE_ID, udid);
    DConnAddIntToJson(rspJson, FIELD_CONFIRMATION, REQUEST_ACCEPTED);

    char *rspStr = cJSON_PrintUnformatted(rspJson);
    if (rspStr == NULL) {
        DCONN_ERROR("Failed to pack rspJson to string.");
        cJSON_Delete(rspJson);
        return NULL;
    }
    cJSON_Delete(rspJson);
    DCONN_DEBUG("Response json: %s", rspStr);
    return rspStr;
}

char *CreateAuthParams(int64_t requestId, const char *peerUdid)
{
    cJSON *addParams = cJSON_CreateObject();
    if (addParams == NULL) {
        DCONN_ERROR("Failed to allocate addParams memory.");
        return NULL;
    }
    DConnAddStringToJson(addParams, FIELD_PEER_CONN_DEVICE_ID, peerUdid);
    DConnAddInt64StringToJson(addParams, FIELD_PEER_AUTH_ID, requestId);
    DConnAddStringToJson(addParams, FIELD_SERVICE_PKG_NAME, DCONN_AUTH_APPID);
    DConnAddBoolToJson(addParams, FIELD_IS_CLIENT, true);
    char *addParamsStr = cJSON_PrintUnformatted(addParams);
    if (addParamsStr == NULL) {
        DCONN_ERROR("Failed to pack addParams to string.");
        cJSON_Delete(addParams);
        return NULL;
    }
    DCONN_DEBUG("addParams: %s", addParamsStr);
    cJSON_Delete(addParams);
    return addParamsStr;
}

uint32_t DConnAuthDevice(int32_t idx)
{
    SetAuthStatByPeerDataIdx(idx, PEER_AUTH_AUTH_DEVICE);
    const GroupAuthManager *ga = GetGaInstance();
    if (ga == NULL) {
        DCONN_ERROR("GaInstance request failed");
        SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
        return DC_ERR_AUTH_FAILED;
    }

    int64_t requestId = GetRequestIdByPeerDataIdx(idx);
    const char *peerUdid = GetUdidByPeerIdx(idx);
    char *authParams = CreateAuthParams(requestId, peerUdid);

    int32_t ret = ga->authDevice(DEFAULT_OS_ACCOUNT, requestId, authParams, &g_groupAuthCallback);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Auth Device request failed: %" PRId32, ret);
        SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
        cJSON_free(authParams);
        return DC_ERR_AUTH_FAILED;
    }

    cJSON_free(authParams);
    return DC_SUCCESS;
}

uint32_t ProcessAuthDeviceData(int64_t requestId, const uint8_t *data, uint32_t dataLen)
{
    return GetGaInstance()->processData(requestId, data, dataLen, &g_groupAuthCallback);
}
