/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef APPLICATIONS_APP_DCONNECTCASEONE_COMMON_SRC_DCONN_PEER_IMPL
#define APPLICATIONS_APP_DCONNECTCASEONE_COMMON_SRC_DCONN_PEER_IMPL

#include <stdbool.h>
#include <stdint.h>
#include <time.h>

#include "product_limits.h"
#include "dconncaseone_interface.h"
#include "dconn_peer.h"

enum PeerDataStat {
    PEER_DATA_STAT_UNUSED = 0,  // 未使用
    PEER_DATA_STAT_USED = 1     // 已被使用
};

/**
 * @brief 对等端数据结构
 *
 */
typedef struct {
    uint32_t stat;      // PeerDataStat
    uint32_t role;      // PeerRole
    uint32_t authStat;  // PeerAuthStat
    time_t authStartTime;
    uint32_t sessionKeyLen;
    int socket;
    uint32_t ipAddr;
    int64_t requestId;
    uint8_t sessionKey[SESSION_KEY_LEN];
    char deviceId[DCONN_DEVICE_ID_LEN + 1];
    char udid[DCONN_UDID_LEN + 1];
} PeerData;

typedef PeerData PeerDataList[MAX_DEVICES_NUM];

/**
 * @brief 初始化对等端数据
 *
 * @param peerDataList 对等端数据指针，内存已申请好
 */
void InitPeerData(PeerDataList peerDataList);

/**
 * @brief 释放对等端数据
 *
 * @param peerDataList 对等端数据指针，此函数不会释放指针
 */
void DeInitPeerData(PeerData *const peerDataList);

#endif  // APPLICATIONS_APP_DCONNECTCASEONE_COMMON_SRC_DCONN_PEER_IMPL
