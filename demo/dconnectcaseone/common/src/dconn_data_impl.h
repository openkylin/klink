/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef APPLICATIONS_APP_DCONNECTCASEONE_COMMON_SRC_DCONN_DATA_IMPL
#define APPLICATIONS_APP_DCONNECTCASEONE_COMMON_SRC_DCONN_DATA_IMPL

#include "dconn_peer_impl.h"
#include "dconn_auth_impl.h"

typedef struct {
    int serverSocket;      // 服务器 socket
    int serverAddrFamily;  // 服务器 socket 绑定的 IP 地址类型 AF_INET/AF_INET6
} ServerData;

/**
 * @brief 套件根数据结构
 *
 */
typedef struct {
    ServerData serverData;         // 服务端绑定的 socket 数据
    PeerDataList peerDataList;     // 对等端数据
    DConnHichainData hichainData;  // hichain 数据
} DConnData;

/**
 * @brief 获取套件根数据结构
 *
 * @return DConnData* 套件跟数据结构指针。使用前务必初始化
 */
DConnData *GetDConnData(void);

#endif  // APPLICATIONS_APP_DCONNECTCASEONE_COMMON_SRC_DCONN_DATA_IMPL
