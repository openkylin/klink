/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dconn_group_manager.h"

#include <unistd.h>
#include <stdlib.h>
#include <securec.h>
#include <stdbool.h>
#include <inttypes.h>

#include "cJSON.h"

#include "dconn_group_manager.h"
#include "device_auth.h"
#include "ipc_service.h"
#include "dconn_log.h"
#include "dconn_msg.h"
#include "dconn_data.h"
#include "dconn_peer.h"
#include "dconn_cjson.h"
#include "dconn_data_impl.h"
#include "dconn_auth_impl.h"
#include "dconn_auth_defines.h"
#include "device_auth_defines.h"
#include "dconncaseone_interface.h"
#include "dconn_group_common.h"

#define DCONN_CREATE_GROUP_REQUEST_ID 256
#define DCONN_DEVICE_AUTH_P2P_GROUP_TYPE 256
#define DCONN_WAIT_GROUP_CREATE_THRESHOLD 10

static bool OnGmTransmit(int64_t requestId, const uint8_t *data, uint32_t dataLen);
static void OnGmSessionKeyReturned(int64_t requestId, const uint8_t *sessionKey, uint32_t sessionKeyLen);
static void OnGmFinish(int64_t requestId, int operationCode, const char *returnData);
static void OnGmError(int64_t requestId, int operationCode, int errorCode, const char *errorReturn);
static char *OnGmRequest(int64_t requestId, int operationCode, const char *reqParams);

static const DeviceAuthCallback g_groupManagerCallback = {
    .onTransmit = OnGmTransmit,
    .onSessionKeyReturned = OnGmSessionKeyReturned,
    .onFinish = OnGmFinish,
    .onError = OnGmError,
    .onRequest = OnGmRequest,
};

static bool OnGmTransmit(int64_t requestId, const uint8_t *data, uint32_t dataLen)
{
    DCONN_DEBUG("requestID: %" PRId64 ", datalen: %" PRId32, requestId, dataLen);
    return OnCommonTransmit(requestId, data, dataLen);
}

static void OnGmSessionKeyReturned(int64_t requestId, const uint8_t *sessionKey, uint32_t sessionKeyLen)
{
    DCONN_DEBUG("Ignored requestID: %" PRId64 ", sessionKeyLen: %" PRId32, requestId, sessionKeyLen);
    (void)requestId;
    (void)sessionKey;
    (void)sessionKeyLen;
}

static void OnGmGroupCreateFinished(const char *returnData)
{
    cJSON *jsonObj = cJSON_Parse(returnData);
    if (jsonObj == NULL) {
        DCONN_ERROR("Cannot parse json from returnData");
        GetDConnData()->hichainData.groupCreateStat = HC_GROUP_CREATE_STAT_FAILED;
        return;
    }

    const char *groupId = DConnGetStringFromCJson(jsonObj, "groupId");
    if (groupId == NULL) {
        DCONN_ERROR("Cannot get group ID from returnData");
        GetDConnData()->hichainData.groupCreateStat = HC_GROUP_CREATE_STAT_FAILED;
    } else {  // 保存创建的群组id
        errno_t err = strcpy_s(GetDConnData()->hichainData.groupId, HC_GROUP_ID_LEN + 1, groupId);
        if (err != EOK) {
            DCONN_ERROR("strcpy_s failed.");
            GetDConnData()->hichainData.groupCreateStat = HC_GROUP_CREATE_STAT_FAILED;
            cJSON_Delete(jsonObj);
            return;
        }
        DCONN_INFO("Group create succeed: %s", groupId);
        GetDConnData()->hichainData.groupCreateStat = HC_GROUP_CREATE_STAT_SUCCEED;
    }

    cJSON_Delete(jsonObj);
}

static void OnGmAddMemberFinished(int64_t requestId)
{
    int32_t idx = GetIdxByRequestId(requestId);
    if (idx < 0) {
        DCONN_ERROR("GetIdxByRequestId failed by request ID: %" PRId64, requestId);
        return;
    }

    uint32_t role = GetPeerRole(idx);
    if (role == PEER_ROLE_SERVICE) {
        return;
    }

    // Here we are service.
    // Service should set auth stat to PEER_AUTH_AUTH_DEVICE.
    // Client do that on start auth device.
    SetAuthStatByPeerDataIdx(idx, PEER_AUTH_AUTH_DEVICE);
    char message[] = DCONN_ADD_MEMBER_FINISHED_MESSAGE;
    uint32_t ret = SendNotifyMessage(idx, message, sizeof(message));
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Service send add member finished message failed for idx: %" PRId32 " ret: %" PRIu32, idx, ret);
        SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
    }

    DCONN_INFO("Service send add member finished message succeed for idx: %" PRId32, idx);
}

static void OnGmFinish(int64_t requestId, int operationCode, const char *returnData)
{
    (void)operationCode;
    (void)returnData;
    DCONN_DEBUG("requestId: %" PRId64 ", operationCode: %d, returnData: %s", requestId, operationCode, returnData);
    if (requestId == DCONN_CREATE_GROUP_REQUEST_ID) {
        OnGmGroupCreateFinished(returnData);
        return;
    }

    OnGmAddMemberFinished(requestId);
    return;
}

static void OnGmError(int64_t requestId, int operationCode, int errorCode, const char *errorReturn)
{
    (void)operationCode;
    (void)errorReturn;
    // 设备绑定失败，触发该回调函数
    DCONN_DEBUG("AuthOnError requestID: %" PRId64 ", operationCode: %d, errorCode: %d, errorReturn: %s", requestId,
                operationCode, errorCode, errorReturn);
    if (requestId == 0) {
        DCONN_ERROR("Group create failed: %d", errorCode);
        GetDConnData()->hichainData.groupCreateStat = HC_GROUP_CREATE_STAT_FAILED;
        return;
    }

    int32_t idx = GetIdxByRequestId(requestId);
    if (idx < 0) {
        DCONN_INFO("auth error but peer idx not found by request ID: %" PRId64, requestId);
        return;
    }

    SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
}

typedef struct {
    int32_t idx;
    char peerDeviceId[DCONN_DEVICE_ID_LEN + 1];
    char localDeviceId[DCONN_DEVICE_ID_LEN + 1];
    char authKey[DCONN_AUTH_KEY_LEN + 1];
} DConnValidReqParams;

static uint32_t CheckRequestId(int64_t requestId, DConnValidReqParams *validParam)
{
    int32_t idx = GetIdxByRequestId(requestId);
    if (idx < 0) {
        DCONN_ERROR("Cannot get peer idx by request ID: %" PRId64, requestId);
        return DC_ERR_INVALID_REQUEST_ID;
    }
    validParam->idx = idx;

    return DC_SUCCESS;
}

static uint32_t CheckReqParams(const char *reqParams, DConnValidReqParams *validParam)
{
    // cJSON *reqJson = cJSON_Parse(reqParams);
    // if (reqJson == NULL) {
    //     DCONN_ERROR("Cannot create json from reqParams");
    //     return DC_ERR_INVALID_JSON;
    // }

    // const char *peerDeviceId = DConnGetStringFromCJson(reqJson, "peerDeviceId");
    GetDeviceID getDeviceID = GetIotSdkCallback()->getDeviceID;
    if (getDeviceID == NULL) {
        DCONN_ERROR("Get device ID callback not register.");
        return DC_ERR_CB_NOT_REGISTER;
    }
    const char *peerDeviceId = getDeviceID();
    if (peerDeviceId == NULL || strlen(peerDeviceId) > DCONN_DEVICE_ID_LEN) {
        DCONN_ERROR("Peer device ID is invalid.");
        // cJSON_Delete(reqJson);
        return DC_ERR_INVALID_DEVICE_ID;
    }
    errno_t err = strcpy_s(validParam->peerDeviceId, DCONN_DEVICE_ID_LEN, peerDeviceId);
    if (err != EOK) {
        DCONN_ERROR("strcpy_s failed.");
        // cJSON_Delete(reqJson);
        return DC_ERROR;
    }

    // cJSON_Delete(reqJson);
    return DC_SUCCESS;
}

static uint32_t CheckSdkParams(DConnValidReqParams *validParam)
{
    GetDeviceID getDeviceID = GetIotSdkCallback()->getDeviceID;
    if (getDeviceID == NULL) {
        DCONN_ERROR("Get device ID callback not register.");
        return DC_ERR_CB_NOT_REGISTER;
    }
    const char *localDeviceId = getDeviceID();
    if (localDeviceId == NULL || strlen(localDeviceId) > DCONN_DEVICE_ID_LEN) {
        DCONN_ERROR("Local device ID is invalid.");
        return DC_ERR_INVALID_DEVICE_ID;
    }
    errno_t err = strcpy_s(validParam->localDeviceId, DCONN_DEVICE_ID_LEN, localDeviceId);
    if (err != EOK) {
        DCONN_ERROR("strcpy_s failed.");
        return DC_ERROR;
    }

    GetAuthKey getAuthKey = GetIotSdkCallback()->getAuthKey;
    if (getAuthKey == NULL) {
        DCONN_ERROR("Get auth key callback not register.");
        return DC_ERR_CB_NOT_REGISTER;
    }
    const char *pinCode = getAuthKey(validParam->peerDeviceId);
    if (pinCode == NULL || strlen(pinCode) > DCONN_AUTH_KEY_LEN) {
        DCONN_ERROR("Auth key is invalid.");
        return DC_ERR_INVALID_AUTH_KEY;
    }
    err = strcpy_s(validParam->authKey, DCONN_AUTH_KEY_LEN, pinCode);
    if (err != EOK) {
        DCONN_ERROR("strcpy_s failed.");
        return DC_ERROR;
    }

    return DC_SUCCESS;
}

static uint32_t CheckOnRequestParams(int64_t requestId, const char *reqParams, DConnValidReqParams *validParam)
{
    uint32_t ret = CheckRequestId(requestId, validParam);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Check request ID failed.");
        return ret;
    }

    ret = CheckReqParams(reqParams, validParam);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Check reqParams failed.");
        return ret;
    }

    ret = CheckSdkParams(validParam);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Check SDK failed.");
        return ret;
    }

    return ret;
}

static char *BuildAcceptedConfirmation(const DConnValidReqParams *validParams)
{
    cJSON *rspJson = cJSON_CreateObject();
    if (rspJson == NULL) {
        DCONN_ERROR("Failed to allocate rspJson memory.");
        return NULL;
    }
    DConnAddIntToJson(rspJson, FIELD_CONFIRMATION, REQUEST_ACCEPTED);
    DConnAddStringToJson(rspJson, FIELD_PIN_CODE, validParams->authKey);
    DConnAddStringToJson(rspJson, FIELD_DEVICE_ID, validParams->localDeviceId);
    DConnAddIntToJson(rspJson, FIELD_USER_TYPE, 0);
    DConnAddIntToJson(rspJson, FIELD_EXPIRE_TIME, -1);

    char *rspBuffer = cJSON_PrintUnformatted(rspJson);
    if (rspBuffer == NULL) {
        DCONN_ERROR("Failed to pack rspJson to string.");
        cJSON_Delete(rspJson);
        return NULL;
    }
    cJSON_Delete(rspJson);
    return rspBuffer;
}

static char *OnGmRequest(int64_t requestId, int operationCode, const char *reqParams)
{
    (void)operationCode;
    DCONN_DEBUG("AuthOnRequest requestId: %" PRId64 ", operationCode: %d, reqParams: %s", requestId, operationCode,
                reqParams);
    DConnValidReqParams validParams;
    uint32_t ret = CheckOnRequestParams(requestId, reqParams, &validParams);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Check on request params failed: %" PRIu32, ret);
        return BuildRejectConfirmation();
    }

    // 下发相关信息
    DCONN_INFO("AuthOnRequest of DeviceId: %s", validParams.peerDeviceId);
    SetDeviceIdForIdx(validParams.idx, validParams.peerDeviceId);

    return BuildAcceptedConfirmation(&validParams);
}

uint32_t DConnInitDeviceAuthService(void)
{
    static bool hichainInited = false;
    if (hichainInited) {
        DCONN_DEBUG("Hichain already initialized.");
        return DC_SUCCESS;
    }

    // 设置设备认证服务
    int32_t ret = InitDeviceAuthService();
    if (ret != HC_SUCCESS) {
        DCONN_ERROR("init hc service failed: %d", ret);
        return DC_ERR_AUTH_INIT_FAILED;
    }

    // 初始化 gm 实例
    if (GetGmInstance() == NULL) {
        DCONN_ERROR("GetGmInstance failed");
        return DC_ERR_AUTH_INIT_FAILED;
    }

    // 设置回调函数
    int32_t result = GetGmInstance()->regCallback(DCONN_AUTH_APPID, &g_groupManagerCallback);
    if (result != HC_SUCCESS) {
        DCONN_ERROR("gm regCallback failed: %" PRIu32, result);
        return DC_ERR_REG_HICHAIN_CB;
    }

    hichainInited = true;
    return DC_SUCCESS;
}

static bool IsGmGroupExist(void)
{
    cJSON *queryParams = cJSON_CreateObject();
    if (queryParams == NULL) {
        DCONN_ERROR("Failed to allocate queryParams memory.");
        return false;
    }
    DConnAddStringToJson(queryParams, FIELD_GROUP_NAME, DCONN_AUTH_GROUP_NAME);
    char *queryParamsStr = cJSON_PrintUnformatted(queryParams);
    if (queryParamsStr == NULL) {
        DCONN_ERROR("Failed to pack queryParams to string.");
        cJSON_Delete(queryParams);
        return false;
    }

    uint32_t groupNum = 0;
    char *returnGroupVec = NULL;
    GetGmInstance()->getGroupInfo(DEFAULT_OS_ACCOUNT, DCONN_AUTH_APPID, queryParamsStr, &returnGroupVec, &groupNum);

    cJSON_Delete(queryParams);
    cJSON_free(queryParamsStr);
    if (groupNum == 0) {
        DCONN_INFO("group not exist");
        cJSON_free(returnGroupVec);
        return false;
    }

    DCONN_INFO("group info: groupNum: %" PRId32 ", returnGroupVec: %s", groupNum, returnGroupVec);
    cJSON *groupVecJson = cJSON_Parse(returnGroupVec);
    if (groupVecJson == NULL) {
        DCONN_ERROR("Cannot parse json from returnGroupVec.");
        cJSON_free(returnGroupVec);
        return false;
    }
    cJSON *groupVecJson0 = cJSON_GetArrayItem(groupVecJson, 0);
    if (groupVecJson0 == NULL) {
        DCONN_ERROR("Failed to get groupInfo.");
        cJSON_Delete(groupVecJson);
        cJSON_free(returnGroupVec);
        return false;
    }
    const char *groupId = DConnGetStringFromCJson(groupVecJson0, "groupId");
    if (strcpy_s(GetDConnData()->hichainData.groupId, HC_GROUP_ID_LEN + 1, groupId) != EOK) {
        DCONN_ERROR("strcpy_s failed.");
        cJSON_Delete(groupVecJson);
        cJSON_free(returnGroupVec);
        return false;
    }
    GetDConnData()->hichainData.groupCreateStat = HC_GROUP_CREATE_STAT_SUCCEED;

    cJSON_Delete(groupVecJson);
    cJSON_free(returnGroupVec);
    return true;
}

int32_t CreateGmGroup(void)
{
    GetDeviceID getDeviceID = GetIotSdkCallback()->getDeviceID;
    if (getDeviceID == NULL) {
        DCONN_ERROR("Get device ID callback not register.");
        return DC_ERR_CB_NOT_REGISTER;
    }
    const char *localDeviceId = getDeviceID();
    if (localDeviceId == NULL || strlen(localDeviceId) > DCONN_DEVICE_ID_LEN) {
        DCONN_INFO("Invalid device ID from IOT SDK.");
        return DC_ERR_INVALID_DEVICE_ID;
    }

    DCONN_INFO("Get device ID from IOT SDK: %s", localDeviceId);
    cJSON *createParam = cJSON_CreateObject();
    if (createParam == NULL) {
        DCONN_ERROR("Failed to allocate createParam memory.");
        return DC_ERR_ALLOC_MEMORY;
    }
    DConnAddIntToJson(createParam, FIELD_GROUP_TYPE, DCONN_DEVICE_AUTH_P2P_GROUP_TYPE);
    DConnAddStringToJson(createParam, FIELD_DEVICE_ID, localDeviceId);
    DConnAddStringToJson(createParam, FIELD_GROUP_NAME, DCONN_AUTH_GROUP_NAME);
    DConnAddIntToJson(createParam, FIELD_USER_TYPE, 0);
    DConnAddIntToJson(createParam, FIELD_GROUP_VISIBILITY, -1);
    DConnAddIntToJson(createParam, FIELD_EXPIRE_TIME, -1);

    char *createParamStr = cJSON_PrintUnformatted(createParam);
    if (createParamStr == NULL) {
        DCONN_ERROR("Failed to pack createParam to string.");
        cJSON_Delete(createParam);
        return DC_ERR_INVALID_JSON;
    }
    int32_t result = GetGmInstance()->createGroup(DEFAULT_OS_ACCOUNT, DCONN_CREATE_GROUP_REQUEST_ID, DCONN_AUTH_APPID,
                                                  createParamStr);
    cJSON_Delete(createParam);
    cJSON_free(createParamStr);

    return result;
}

uint32_t CreateDeviceAuthGroup(void)
{
    if (GetDConnData()->hichainData.groupCreateStat == HC_GROUP_CREATE_STAT_SUCCEED) {
        DCONN_INFO("Hichain group already created in this session");
        return DC_SUCCESS;
    }

    if (IsGmGroupExist()) {
        // Hichain 有数据库能力，设备再启动时之前创建的群组仍然存在
        DCONN_INFO("Hichain group already exists.");
        return DC_SUCCESS;
    }

    int32_t result = CreateGmGroup();
    if (result != HC_SUCCESS) {
        DCONN_ERROR("create group request failed: %" PRId32, result);
        return DC_ERR_CREATE_GROUP;
    }
    // 等待群组创建成功回调
    uint32_t count = 0;
    while (count < DCONN_WAIT_GROUP_CREATE_THRESHOLD &&
           GetDConnData()->hichainData.groupCreateStat == HC_GROUP_CREATE_STAT_UNINIT) {
        DCONN_DEBUG("waiting for hc create group: %" PRId32, GetDConnData()->hichainData.groupCreateStat);
        sleep(1);
        count++;
    }

    if (GetDConnData()->hichainData.groupCreateStat != HC_GROUP_CREATE_STAT_SUCCEED) {
        DCONN_ERROR("create hc group callback failed. count = %" PRIu32, count);
        return DC_ERR_CREATE_GROUP;
    }

    DCONN_DEBUG("Hichain group create finished: %" PRId32, GetDConnData()->hichainData.groupCreateStat);
    return DC_SUCCESS;
}

uint32_t InvitePeerIntoGroup(int32_t idx)
{
    GetAuthKey getAuthKey = GetIotSdkCallback()->getAuthKey;
    if (getAuthKey == NULL) {
        DCONN_ERROR("Get auth key callback not register.");
        SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
        return DC_ERR_CB_NOT_REGISTER;
    }
    const char *pinCode = getAuthKey(GetDeviceIdByPeerIdx(idx));
    if (pinCode == NULL || strlen(pinCode) > DCONN_AUTH_KEY_LEN) {
        DCONN_ERROR("Invalid auth key.");
        SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
        return DC_ERR_INVALID_AUTH_KEY;
    }

    // 1. 构造参数
    cJSON *addParams = cJSON_CreateObject();
    if (addParams == NULL) {
        DCONN_ERROR("Failed to allocate addParams memory.");
        return DC_ERR_ALLOC_MEMORY;
    }
    DConnAddStringToJson(addParams, FIELD_GROUP_ID, GetDConnData()->hichainData.groupId);
    DConnAddIntToJson(addParams, FIELD_GROUP_TYPE, DCONN_DEVICE_AUTH_P2P_GROUP_TYPE);  // 点对点群组类型
    DConnAddStringToJson(addParams, FIELD_PIN_CODE, pinCode);
    DConnAddBoolToJson(addParams, FIELD_IS_ADMIN, true);
    char *addParamsStr = cJSON_PrintUnformatted(addParams);
    if (addParamsStr == NULL) {
        DCONN_ERROR("Failed to pack addParams to string.");
        cJSON_Delete(addParams);
        return DC_ERR_INVALID_JSON;
    }

    // 2. 异步邀请加入
    int32_t ret = GetGmInstance()->addMemberToGroup(DEFAULT_OS_ACCOUNT, GetRequestIdByPeerDataIdx(idx),
                                                    DCONN_AUTH_APPID, addParamsStr);

    // 3. 返回结果，释放资源
    cJSON_Delete(addParams);
    cJSON_free(addParamsStr);
    if (ret != HC_SUCCESS) {
        DCONN_INFO("Invite peer [%" PRId32 "] to group failed: %" PRId32, idx, ret);
        SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
        return DC_ERR_ADD_MEMBER;
    }

    DCONN_INFO("addMemberToGroup sucess");
    return DC_SUCCESS;
}

uint32_t ProcessAddMemberData(int64_t requestId, const uint8_t *data, uint32_t dataLen)
{
    return GetGmInstance()->processData(requestId, data, dataLen);
}
