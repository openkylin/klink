/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dconn_thread.h"

#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <pthread.h>
#include <securec.h>

#include "dconn_data_impl.h"
#include "dconn_log.h"
#include "dconn_sched_yield.h"

static uint32_t g_transferThreadStat = THR_STAT_DISABLE;
static pthread_t g_tid = 0;

uint32_t EnableTransferThr(void *(*func)(void *))
{
    g_transferThreadStat = THR_STAT_ENABLE;
    int ret = pthread_create(&g_tid, NULL, func, NULL);
    if (ret < 0) {
        DCONN_ERROR("Cannot create thread: %d", ret);
        return DC_ERR_CREATE_THREAD;
    }

    DCONN_INFO("Thread created: %lu", g_tid);
    return DC_SUCCESS;
}

bool IsThreadInStat(uint32_t stat)
{
    if (g_transferThreadStat == stat) {
        return true;
    }
    return false;
}

void SetThreadStat(uint32_t stat)
{
    g_transferThreadStat = stat;
}

void ExitTransferThr(void)
{
    g_transferThreadStat = THR_STAT_EXIT;
}

void WaitTransferThrDisabled(void)
{
    if (g_tid == 0) {
        DCONN_ERROR("pthread_t is NULL");
        return;
    }
    int ret = pthread_join(g_tid, NULL);
    DCONN_INFO("Thread exit is finished, ret:%d", ret);
}