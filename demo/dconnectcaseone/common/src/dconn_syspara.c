/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dconn_syspara.h"

#include <string.h>
#include <inttypes.h>
#include <securec.h>

#include "parameter.h"

#include "dconn_peer.h"
#include "dconn_log.h"
#include "dconn_data.h"
#include "dconncaseone_interface.h"

uint32_t DConnGetUdid(char *udid)
{
    errno_t err = memset_s(udid, DCONN_UDID_LEN + 1, 0, DCONN_UDID_LEN + 1);
    if (err != EOK) {
        DCONN_ERROR("memset_s failed.");
        return DC_ERROR;
    }
    int ret = GetDevUdid(udid, DCONN_UDID_LEN + 1);
    if (ret != 0) {
        DCONN_ERROR("Cannot get dev udid: %d", ret);
        return DC_ERR_CANNOT_GET_UDID;
    }

    DCONN_DEBUG("Get UDID succeed.");
    return DC_SUCCESS;
}

uint32_t DConnGetDeviceUdid(DConnDeviceUdId *udid)
{
    GetDeviceID getDeviceId = GetIotSdkCallback()->getDeviceID;
    if (getDeviceId == NULL) {
        DCONN_ERROR("Get device ID callback not register.");
        return DC_ERR_CB_NOT_REGISTER;
    }

    udid->deviceId = getDeviceId();
    if (udid->deviceId == NULL || strlen(udid->deviceId) > DCONN_DEVICE_ID_LEN) {
        DCONN_ERROR("Get device ID failed from IOT SDK.");
        return DC_ERR_INVALID_DEVICE_ID;
    }

    uint32_t ret = DConnGetUdid(udid->udid);
    if (ret != 0) {
        DCONN_ERROR("Cannot get dev udid: %" PRIu32, ret);
        return ret;
    }
    return DC_SUCCESS;
}
