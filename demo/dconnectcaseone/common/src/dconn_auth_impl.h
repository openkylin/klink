/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DCONN_AUTH_IMPL_H
#define DCONN_AUTH_IMPL_H

#include <stdint.h>

#define HC_GROUP_ID_LEN 64

enum HcGroupCreateStat {
    HC_GROUP_CREATE_STAT_UNINIT = 0,   // 未创建
    HC_GROUP_CREATE_STAT_FAILED = 1,   // 创建失败
    HC_GROUP_CREATE_STAT_SUCCEED = 2,  // 创建成功
};

/**
 * @brief Hichain 认证数据
 *
 */
typedef struct {
    char groupId[HC_GROUP_ID_LEN + 1];
    int32_t groupCreateStat;  // 群组创建状态 HcGroupCreateStat
} DConnHichainData;

/**
 * @brief 初始化 Hichain 数据
 *
 * @param data Hichain 数据
 */
void InitHichainData(DConnHichainData *data);

#endif