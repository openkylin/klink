/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DCONN_HANDLER_H
#define DCONN_HANDLER_H

#include <stdint.h>

/**
 * @brief 处理接收到 Device ID 时的消息
 *
 * @param idx 对等端索引。一定不是 -1
 * @param data 收到的数据。一定不是空指针
 * @param dataLen 收到的数据长度
 * @return uint32_t 错误码
 */
uint32_t DConnOnDeviceIdReceived(int32_t idx, const uint8_t *data, uint32_t dataLen);

/**
 * @brief 处理对等端接入
 *
 */
void DConnHandleClientAccess(void);

/**
 * @brief 处理收到设备绑定过程，Group Manager自定义通道收到的消息
 *
 * @param idx 对等端索引。一定不是 -1
 * @param data 收到的数据。一定不是空指针
 * @param dataLen 收到的数据长度
 * @return uint32_t 错误码
 */
uint32_t DConnOnGmTransmitReceived(int32_t idx, const uint8_t *data, uint32_t dataLen);

/**
 * @brief 处理收到设备绑定过程，Group Auth自定义通道收到的消息
 *
 * @param idx 对等端索引。一定不是 -1
 * @param data 收到的数据。一定不是空指针
 * @param dataLen 收到的数据长度
 * @return uint32_t 错误码
 */
uint32_t DConnOnGaTransmitReceived(int32_t idx, const uint8_t *data, uint32_t dataLen);

/**
 * @brief 处理收到的业务数据
 *
 * @param idx 对等端索引。一定不是 -1
 * @param data 收到的数据。一定不是空指针
 * @param dataLen 收到的数据长度
 * @return uint32_t 错误码
 */
uint32_t DConnOnBizDataReceived(int32_t idx, const uint8_t *data, uint32_t dataLen);

/**
 * @brief 处理接收到通知消息
 *
 * @param idx 对等端索引。一定不是 -1
 * @param data 收到的数据。一定不是空指针
 * @param dataLen 收到的数据长度
 * @return uint32_t 错误码
 */
uint32_t DConnOnNotifyMessageReceived(int32_t idx, const uint8_t *data, uint32_t dataLen);

#endif  // DCONN_HANDLER_H