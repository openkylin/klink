/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dconn_handler.h"

#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "cJSON.h"

#include "dconn_data.h"
#include "dconn_peer.h"
#include "dconn_log.h"
#include "dconn_mbedtls_gcm.h"
#include "dconn_send.h"
#include "dconn_msg.h"
#include "dconn_group_manager.h"
#include "dconn_cjson.h"
#include "dconn_syspara.h"
#include "dconn_group_auth.h"
#include "dconn_group_common.h"

static uint32_t SavePeerDeviceId(int32_t idx, const uint8_t *data, uint32_t dataLen, bool shoudSaveRequestId)
{
    DConnMsgIn in = {
        .data = data,
        .dataLen = dataLen,
    };
    DConnMsgOut out;
    uint32_t ret = DConnDecMsg(DCONN_MSG_TYPE_DEVICE_ID, NULL, &in, &out);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Cannot decode device ID message for idx: %" PRId32 " ret: %" PRIu32, idx, ret);
        return ret;
    }

    cJSON *payload = cJSON_Parse((const char *)out.msg);
    free(out.msg);
    if (payload == NULL) {
        DCONN_ERROR("Cannot parse device ID message for idx: %" PRId32 " ret: %" PRIu32, idx, ret);
        cJSON_Delete(payload);
        return ret;
    }

    const char *deviceId = DConnGetStringFromCJson(payload, "deviceId");
    if (deviceId == NULL) {
        DCONN_ERROR("Cannot get device ID from message for idx: %" PRId32 " ret: %" PRIu32, idx, ret);
        cJSON_Delete(payload);
        return ret;
    }
    SetDeviceIdForIdx(idx, deviceId);

    const char *udid = DConnGetStringFromCJson(payload, "udid");
    if (udid == NULL) {
        DCONN_ERROR("Cannot get UDID from message for idx: %" PRId32 " ret: %" PRIu32, idx, ret);
        cJSON_Delete(payload);
        return ret;
    }
    SetUdidForIdx(idx, udid);

    if (shoudSaveRequestId) {
        int64_t requestId;
        int32_t result = DConnGetInt64FromJson(payload, "requestId", &requestId);
        if (result != DC_SUCCESS) {
            DCONN_ERROR("Cannot get request ID from message for idx: %" PRId32 " ret: %" PRId32, idx, result);
            cJSON_Delete(payload);
            return result;
        }

        SetRequestIdByPeerDataIdx(idx, requestId);
    }

    cJSON_Delete(payload);
    return DC_SUCCESS;
}

static uint32_t BuildServiceDeviceIdPayload(int32_t idx, char **payload)
{
    DConnDeviceUdId deviceUdId;
    uint32_t ret = DConnGetDeviceUdid(&deviceUdId);
    if (ret != 0) {
        DCONN_ERROR("Cannot get dev udid: %" PRIu32, ret);
        return ret;
    }

    int64_t requestId;
    ret = DConnGenerateRandom((uint8_t *)&requestId, sizeof(int64_t));
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Cannot generate request ID for idx: %" PRId32 " ret: %" PRIu32, idx, ret);
        return ret;
    }

    cJSON *payloadJson = cJSON_CreateObject();
    if (payloadJson == NULL) {
        DCONN_ERROR("Failed to allocate payloadJson memory.");
        return DC_ERR_ALLOC_MEMORY;
    }
    SetRequestIdByPeerDataIdx(idx, requestId);
    DConnAddStringToJson(payloadJson, "deviceId", deviceUdId.deviceId);
    DConnAddStringToJson(payloadJson, "udid", deviceUdId.udid);
    DConnAddInt64StringToJson(payloadJson, "requestId", requestId);
    *payload = cJSON_PrintUnformatted(payloadJson);
    if (*payload == NULL) {
        DCONN_ERROR("Failed to pack payloadJson to string.");
        cJSON_Delete(payloadJson);
        return DC_ERR_INVALID_JSON;
    }
    cJSON_Delete(payloadJson);
    DCONN_DEBUG("Service send device ID to client payload.");
    return DC_SUCCESS;
}

static uint32_t DoSendDeviceIdWithRequestIdToPeer(int32_t idx, char *payload)
{
    DConnMsgIn in = {
        .data = (const uint8_t *)payload,
        .dataLen = strlen(payload) + 1,
    };
    DConnMsgOut out;
    uint32_t ret = DConnEncMsg(DCONN_MSG_TYPE_DEVICE_ID, NULL, &in, &out);
    cJSON_free(payload);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Cannot encode device ID message for idx: %" PRId32 " ret: %" PRIu32, idx, ret);
        return ret;
    }

    SetAuthStatByPeerDataIdx(idx, PEER_AUTH_ADD_MEMBER);
    ret = SendDataToPeer(idx, out.msg, out.msgLen);
    free(out.msg);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Cannot send device ID message for idx: %" PRId32 " ret: %" PRIu32, idx, ret);
        return ret;
    }

    return DC_SUCCESS;
}

static uint32_t SendDeviceIdWithRequestIdToPeer(int32_t idx)
{
    char *payload;
    uint32_t ret = BuildServiceDeviceIdPayload(idx, &payload);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Build service device ID param failed for idx: %" PRId32 " ret: %" PRIu32, idx, ret);
        return ret;
    }

    return DoSendDeviceIdWithRequestIdToPeer(idx, payload);
}

static uint32_t ServiceOnDeviceIdReceived(int32_t idx, const uint8_t *data, uint32_t dataLen)
{
    uint32_t ret = SavePeerDeviceId(idx, data, dataLen, false);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Save device ID failed for idx: %" PRId32 " ret: %" PRIu32, idx, ret);
        return ret;
    }

    ret = SendDeviceIdWithRequestIdToPeer(idx);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Send device ID to peer failed for idx: %" PRId32 " ret: %" PRIu32, idx, ret);
        return ret;
    }

    SetAuthStatByPeerDataIdx(idx, PEER_AUTH_ADD_MEMBER);
    return DC_SUCCESS;
}

static uint32_t ClientOnDeviceIdReceived(int32_t idx, const uint8_t *data, uint32_t dataLen)
{
    uint32_t ret = SavePeerDeviceId(idx, data, dataLen, true);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Save device ID failed for idx: %" PRId32 " ret: %" PRIu32, idx, ret);
        return ret;
    }

    SetAuthStatByPeerDataIdx(idx, PEER_AUTH_ADD_MEMBER);
    ret = InvitePeerIntoGroup(idx);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Invite member to group failed for idx: %" PRId32 " ret: %" PRIu32, idx, ret);
        return ret;
    }

    return DC_SUCCESS;
}

uint32_t DConnOnDeviceIdReceived(int32_t idx, const uint8_t *data, uint32_t dataLen)
{
    // Peer is client, thus local is service. Otherwise.
    return GetPeerRole(idx) == PEER_ROLE_CLIENT ? ServiceOnDeviceIdReceived(idx, data, dataLen)
                                                : ClientOnDeviceIdReceived(idx, data, dataLen);
}

static uint32_t AcceptPeerSocket(int *peerSocket, uint32_t *addr)
{
    // 接受传入的连接
    DCONN_DEBUG("waiting for connection");
    struct sockaddr_in peerAddr;
    socklen_t peerSocketLen = sizeof(peerAddr);

    *peerSocket = accept(GetServerSocket(), (struct sockaddr *)&peerAddr, &peerSocketLen);
    if (*peerSocket < 0) {
        DCONN_ERROR("Accepted an invalid peer: %d", *peerSocket);
        return DC_ERR_CONNECTION_REFUSED;
    }

    *addr = peerAddr.sin_addr.s_addr;
    return DC_SUCCESS;
}

static uint32_t CheckAddressValid(uint32_t addr)
{
    struct in_addr inAddr = {
        .s_addr = addr,
    };
    const char *addrStr = inet_ntoa(inAddr);
    if (addrStr == NULL) {
        DCONN_ERROR("Cannot parse the client address.");
        return DC_ERR_CONNECTION_REFUSED;
    }

    IsValidIP isValidIp = GetIotSdkCallback()->isValidIP;
    if (isValidIp == NULL) {
        DCONN_ERROR("IsValidIP callback not registered.");
        return DC_ERR_CB_NOT_REGISTER;
    }

    uint32_t ipValue = DconnIpTransToInt(addrStr);
    unsigned char *ipAddress = (unsigned char *)&ipValue;
    if (!isValidIp(addrStr)) {
        DCONN_ERROR("Invalid address: %" IPV4_ANONYMOUS_PLACEHOLDER, ipAddress[3], ipAddress[0]);
        return DC_ERR_CONNECTION_REFUSED;
    }

    DCONN_INFO("Client connection accepted: %" IPV4_ANONYMOUS_PLACEHOLDER, ipAddress[3], ipAddress[0]);
    return DC_SUCCESS;
}

void DConnHandleClientAccess(void)
{
    int peerSocket;
    uint32_t peerAddr;
    uint32_t ret = AcceptPeerSocket(&peerSocket, &peerAddr);
    if (ret != DC_SUCCESS) {
        return;
    }

    ret = CheckAddressValid(peerAddr);
    if (ret != DC_SUCCESS) {
        close(peerSocket);
        DCONN_ERROR("Check address failed: %" PRIu32, ret);
        return;
    }

    int32_t idx = GetNewPeerDataIdx(PEER_ROLE_CLIENT);
    if (idx < 0) {
        DCONN_ERROR("Max peer num exceed. Client access refused.");
        close(peerSocket);
        return;
    }

    SetSocketByPeerDataIdx(idx, peerSocket);
    SetAuthStatByPeerDataIdx(idx, PEER_AUTH_EXPECT_DEVICE_ID);
    return;
}

uint32_t DConnOnGmTransmitReceived(int32_t idx, const uint8_t *data, uint32_t dataLen)
{
    DConnMsgIn in = {
        .data = data,
        .dataLen = dataLen,
    };
    DConnAuthParam param;
    DConnMsgOut out;
    uint32_t ret = DConnDecMsg(DCONN_MSG_TYPE_AUTH, &param, &in, &out);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Decode auth message for idx: %" PRId32 " failed: %" PRIu32, idx, ret);
        SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
        return ret;
    }

    ret = ProcessAddMemberData(param.requestId, out.msg, out.msgLen);
    free(out.msg);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Process device auth data for idx: %" PRId32 " failed: %" PRIu32, idx, ret);
        SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
        return ret;
    }

    return DC_SUCCESS;
}

uint32_t DConnOnGaTransmitReceived(int32_t idx, const uint8_t *data, uint32_t dataLen)
{
    DConnMsgIn in = {
        .data = data,
        .dataLen = dataLen,
    };
    DConnMsgOut out;
    DConnAuthParam param;
    uint32_t ret = DConnDecMsg(DCONN_MSG_TYPE_AUTH, &param, &in, &out);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Decode auth message for idx: %" PRId32 " failed: %" PRIu32, idx, ret);
        SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
        return ret;
    }

    ret = ProcessAuthDeviceData(param.requestId, out.msg, out.msgLen);
    free(out.msg);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Process device auth data for idx: %" PRId32 " failed: %" PRIu32, idx, ret);
        SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
        return ret;
    }

    return DC_SUCCESS;
}

uint32_t DConnOnBizDataReceived(int32_t idx, const uint8_t *data, uint32_t dataLen)
{
    uint8_t *sessionKey;
    uint32_t keyLength;
    GetSessionKeyByPeerIdx(idx, &sessionKey, &keyLength);
    if (keyLength == 0) {
        DCONN_ERROR("Cannot find session key for idx %" PRId32, idx);
        return DC_ERR_DEVICE_NOT_AUTHORIZED;
    }

    const char *deviceId = GetDeviceIdByPeerIdx(idx);
    if (deviceId == NULL) {
        DCONN_ERROR("Cannot find device ID for idx %" PRId32, idx);
        return DC_ERR_DEVICE_NOT_AUTHORIZED;
    }

    DConnMsgIn in = {
        .data = data,
        .dataLen = dataLen,
    };
    DConnDataParam param = {
        .key = sessionKey,
        .keyLen = keyLength,
    };
    DConnMsgOut out;
    uint32_t ret = DConnDecMsg(DCONN_MSG_TYPE_DATA, &param, &in, &out);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Cannot decode data for idx: %" PRId32 " , ret: %" PRIu32, idx, ret);
        return ret;
    }

    ReceiveDataCallback callback = GetIotSdkCallback()->onReceiveDataCb;
    if (callback == NULL) {
        DCONN_ERROR("On receive data callback not registered.");
        return DC_ERR_CB_NOT_REGISTER;
    }

    DCONN_DEBUG("Before call on receive data callback.");
    callback(deviceId, (const char *)out.msg, out.msgLen);
    DCONN_DEBUG("After call on receive data callback.");
    free(out.msg);
    return DC_SUCCESS;
}

uint32_t DConnOnNotifyMessageReceived(int32_t idx, const uint8_t *data, uint32_t dataLen)
{
    (void)dataLen;
    DConnMsgIn in = {
        .data = data,
        .dataLen = dataLen,
    };
    DConnMsgOut out;
    uint32_t ret = DConnDecMsg(DCONN_MSG_TYPE_NOTIFY, NULL, &in, &out);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Cannot decode notify message for idx: %" PRId32 " ret: %" PRIu32, idx, ret);
        SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
        return ret;
    }

    if (strncmp((const char *)out.msg, DCONN_SESSION_KEY_RETURNED_MESSAGE,
                sizeof(DCONN_SESSION_KEY_RETURNED_MESSAGE)) == 0) {
        if (GetAuthStatByPeerDataIdx(idx) == PEER_AUTH_AUTH_DEVICE) {
            SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_AUTHORIZED);
            free(out.msg);
            return DC_SUCCESS;
        }
    }

    if (strncmp((const char *)out.msg, DCONN_ADD_MEMBER_FINISHED_MESSAGE, sizeof(DCONN_ADD_MEMBER_FINISHED_MESSAGE)) ==
        0) {
        if (GetAuthStatByPeerDataIdx(idx) == PEER_AUTH_ADD_MEMBER) {
            DConnAuthDevice(idx);
            free(out.msg);
            return DC_SUCCESS;
        }
    }

    free(out.msg);
    SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
    return DC_ERR_INVALID_NOTIFY_MESSAGE;
}
