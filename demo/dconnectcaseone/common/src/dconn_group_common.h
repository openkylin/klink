/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DCONN_GROUP_COMMON_H
#define DCONN_GROUP_COMMON_H

#include <stdint.h>
#include <stdbool.h>

#define DCONN_SESSION_KEY_RETURNED_MESSAGE "Service at session key returned stat"
#define DCONN_ADD_MEMBER_FINISHED_MESSAGE "Service at add member finished stat"

bool OnCommonTransmit(int64_t requestId, const uint8_t *data, uint32_t dataLen);
char *BuildRejectConfirmation(void);
uint32_t SendNotifyMessage(int32_t idx, const char *message, uint32_t messageLen);

#endif  // DCONN_GROUP_COMMON_H