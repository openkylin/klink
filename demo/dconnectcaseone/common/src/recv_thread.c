/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "recv_thread.h"

#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <securec.h>
#include <unistd.h>

#include "dconn_thread.h"
#include "dconn_data.h"
#include "dconn_log.h"
#include "dconn_msg.h"
#include "dconn_peer.h"
#include "dconn_handler.h"
#include "dconn_sched_yield.h"

#define DCONN_ANY_CONDITION UINT32_MAX

typedef uint32_t (*DConnRecvFunc)(int32_t idx, const uint8_t *data, uint32_t dataLen);

typedef struct {
    uint32_t msgType;
    uint32_t peerAuthStat;
    uint32_t peerRole;
    DConnRecvFunc handler;
} DconnRecvHandler;

static const DconnRecvHandler RECV_HANDLES[] = {
    {
        .msgType = DCONN_MSG_TYPE_DEVICE_ID,
        .peerAuthStat = PEER_AUTH_EXPECT_DEVICE_ID,
        .peerRole = DCONN_ANY_CONDITION,
        .handler = DConnOnDeviceIdReceived,
    },
    {
        .msgType = DCONN_MSG_TYPE_AUTH,
        .peerAuthStat = PEER_AUTH_ADD_MEMBER,
        .peerRole = DCONN_ANY_CONDITION,
        .handler = DConnOnGmTransmitReceived,
    },
    {
        .msgType = DCONN_MSG_TYPE_AUTH,
        .peerAuthStat = PEER_AUTH_AUTH_DEVICE,
        .peerRole = DCONN_ANY_CONDITION,
        .handler = DConnOnGaTransmitReceived,
    },
    {
        .msgType = DCONN_MSG_TYPE_DATA,
        .peerAuthStat = PEER_AUTH_STAT_AUTHORIZED,
        .peerRole = DCONN_ANY_CONDITION,
        .handler = DConnOnBizDataReceived,
    },
    {
        .msgType = DCONN_MSG_TYPE_NOTIFY,
        .peerAuthStat = DCONN_ANY_CONDITION,
        .peerRole = PEER_ROLE_SERVICE,
        .handler = DConnOnNotifyMessageReceived,
    },
};

static DConnRecvFunc GetHandler(uint16_t type, uint32_t stat, uint32_t role)
{
    for (size_t i = 0; i < (sizeof(RECV_HANDLES) / sizeof(DconnRecvHandler)); i++) {
        if (RECV_HANDLES[i].msgType != DCONN_ANY_CONDITION && type != RECV_HANDLES[i].msgType) {
            continue;
        }

        if (RECV_HANDLES[i].peerAuthStat != DCONN_ANY_CONDITION && stat != RECV_HANDLES[i].peerAuthStat) {
            continue;
        }

        if (RECV_HANDLES[i].peerRole != DCONN_ANY_CONDITION && role != RECV_HANDLES[i].peerRole) {
            continue;
        }

        return RECV_HANDLES[i].handler;
    }

    return NULL;
}

static void RecvData(int32_t idx)
{
    int *fd = GetSocketByPeerDataIdx(idx);
    uint8_t *data = malloc(DCONN_MAX_MSG_LEN);
    if (data == NULL) {
        DCONN_ERROR("Cannot alloc memory!");
        return;
    }

    errno_t err = memset_s(data, DCONN_MAX_MSG_LEN, 0, DCONN_MAX_MSG_LEN);
    if (err != EOK) {
        DCONN_ERROR("memset_s failed.");
        SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
        free(data);
        return;
    }
    int dataLen = recv(*fd, data, DCONN_MAX_MSG_LEN, 0);
    if (dataLen <= 0) {
        // DCONN_ERROR("Peer close the socket: %" PRId32, idx);
        SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
        free(data);
        return;
    }

    uint16_t type;
    uint32_t ret = DConnGetMsgType(data, dataLen, &type);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Cannot get message type: %" PRIu32, ret);
        SetAuthStatByPeerDataIdx(idx, PEER_AUTH_STAT_INVALID);
        free(data);
        return;
    }

    uint32_t stat = GetAuthStatByPeerDataIdx(idx);
    uint32_t role = GetPeerRole(idx);
    DConnRecvFunc handler = GetHandler(type, stat, role);
    if (handler == NULL) {
        DCONN_ERROR("Cannot find a handler for idx: %" PRId32 " type: %" PRIu32 " stat: %" PRIu32 " role: %" PRIu32,
                    idx, type, stat, role);
        free(data);
        return;
    }

    DCONN_DEBUG("Before handler for idx: %" PRId32 " type: %" PRIu32 " stat: %" PRIu32 " role: %" PRIu32, idx, type,
                stat, role);
    ret = handler(idx, data, dataLen);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Handle received message to idx: %" PRId32 " failed: %" PRIu32, idx, ret);
    }

    free(data);
    return;
}

void *RecvThread(void *param)
{
    DCONN_DEBUG("Recv data thread begin");
    (void)param;
    fd_set readSet;
    fd_set exceptfds;
    struct timeval timeout = {
        .tv_sec = 1,
        .tv_usec = 0,
    };  // 必须设置超时，否则新加入的对等端可能不会被监听到

    while (IsThreadInStat(THR_STAT_ENABLE)) {
        int maxFd = InitSelectFdSet(&readSet, &exceptfds);
        if (maxFd < 0) {
            DConnSchedYield();
            continue;
        }

        int ret = select(maxFd + 1, &readSet, NULL, &exceptfds, &timeout);
        if (ret > 0) {
            // DCONN_DEBUG("Select succeed. total number of bits: %d", ret);
            HandleFdSet(&readSet, RecvData, DConnHandleClientAccess);
        } else if (ret == 0) {
            // DCONN_DEBUG("Select timeout");
            timeout.tv_sec = 1;
            timeout.tv_usec = 0;
        } else {
            DCONN_ERROR("Select error: %d", errno);
            HandleFdSet(&exceptfds, ReleasePeerDataIdx, NULL);
        }
    }

    // 告知线程已退出
    SetThreadStat(THR_STAT_DISABLE);
    DCONN_DEBUG("Recv data thread end");
    return NULL;
}
