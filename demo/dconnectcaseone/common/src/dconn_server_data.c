/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dconn_server_data.h"

#include <unistd.h>

#include "dconn_log.h"

void InitServerData(ServerData *data)
{
    DCONN_DEBUG("init server data begin");
    data->serverSocket = -1;
    DCONN_DEBUG("init server data end");
}

void DeInitServerData(ServerData *data)
{
    if (data->serverSocket != -1) {
        close(data->serverSocket);
        data->serverSocket = -1;
    }
}