/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dconn_cjson.h"

#include <stdlib.h>
#include <inttypes.h>

#include "securec.h"

#include "dconncaseone_interface.h"

#define DECIMAL 10

const char *DConnGetStringFromCJson(const cJSON *jsonObj, const char *key)
{
    if (jsonObj == NULL || key == NULL) {
        return NULL;
    }

    cJSON *jsonObjTmp = cJSON_GetObjectItemCaseSensitive(jsonObj, key);
    if (jsonObjTmp != NULL && cJSON_IsString(jsonObjTmp)) {
        return cJSON_GetStringValue(jsonObjTmp);
    }

    return NULL;
}

static int64_t DConnStringToInt64(const char *cp)
{
    if (cp == NULL) {
        return 0;
    }

    return strtoll(cp, NULL, DECIMAL);
}

int32_t DConnGetInt64FromJson(const cJSON *jsonObj, const char *key, int64_t *value)
{
    const char *str = DConnGetStringFromCJson(jsonObj, key);
    if (str == NULL) {
        return DC_ERR_INVALID_JSON;
    }

    *value = DConnStringToInt64(str);
    return DC_SUCCESS;
}

static void DConnCreateJson(cJSON *jsonObj, const char *key, cJSON *tmp)
{
    if (cJSON_ReplaceItemInObjectCaseSensitive(jsonObj, key, tmp) == false) {
        cJSON_Delete(tmp);
        return;
    }
}

void DConnAddIntToJson(cJSON *jsonObj, const char *key, int value)
{
    if (jsonObj == NULL || key == NULL) {
        return;
    }

    cJSON *objInJson = cJSON_GetObjectItemCaseSensitive(jsonObj, key);
    if (objInJson == NULL) {
        if (cJSON_AddNumberToObject(jsonObj, key, value) == NULL) {
            return;
        }
    } else {
        cJSON *tmp = cJSON_CreateNumber(value);
        if (tmp == NULL) {
            return;
        }
        DConnCreateJson(jsonObj, key, tmp);
    }

    return;
}

void DConnAddInt64StringToJson(cJSON *jsonObj, const char *key, int64_t value)
{
    char buffer[65] = {0};
    if (sprintf_s(buffer, sizeof(buffer), "%" PRId64, value) <= 0) {
        return;
    }

    DConnAddStringToJson(jsonObj, key, buffer);
    return;
}

void DConnAddStringToJson(cJSON *jsonObj, const char *key, const char *value)
{
    if (jsonObj == NULL || key == NULL || value == NULL) {
        return;
    }

    cJSON *objInJson = cJSON_GetObjectItemCaseSensitive(jsonObj, key);
    if (objInJson == NULL) {
        if (cJSON_AddStringToObject(jsonObj, key, value) == NULL) {
            return;
        }
    } else {
        cJSON *tmp = cJSON_CreateString(value);
        if (tmp == NULL) {
            return;
        }
        DConnCreateJson(jsonObj, key, tmp);
    }

    return;
}

void DConnAddBoolToJson(cJSON *jsonObj, const char *key, bool value)
{
    if (jsonObj == NULL || key == NULL) {
        return;
    }

    cJSON *objInJson = cJSON_GetObjectItemCaseSensitive(jsonObj, key);
    if (objInJson == NULL) {
        if (cJSON_AddBoolToObject(jsonObj, key, value) == NULL) {
            return;
        }
    } else {
        cJSON *tmp = cJSON_CreateBool(value);
        if (tmp == NULL) {
            return;
        }
        DConnCreateJson(jsonObj, key, tmp);
    }

    return;
}
