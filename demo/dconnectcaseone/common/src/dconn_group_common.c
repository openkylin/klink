/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dconn_group_common.h"

#include <stdlib.h>

#include "cJSON.h"
#include "device_auth.h"

#include "dconn_log.h"
#include "dconn_msg.h"
#include "dconncaseone_interface.h"
#include "dconn_send.h"
#include "dconn_cjson.h"
#include "dconn_data.h"

bool OnCommonTransmit(int64_t requestId, const uint8_t *data, uint32_t dataLen)
{
    int32_t idx = GetIdxByRequestId(requestId);
    if (idx < 0) {
        DCONN_ERROR("will not send hc data: requestId: %" PRId64 ", idx: %" PRId32, requestId, idx);
        return false;
    }

    DConnAuthParam param = {
        .requestId = requestId,
    };
    DConnMsgIn in = {
        .data = data,
        .dataLen = dataLen,
    };
    DConnMsgOut out;
    uint32_t ret = DConnEncMsg(DCONN_MSG_TYPE_AUTH, &param, &in, &out);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Encode failed. Data will not send: %" PRIu32, ret);
        return false;
    }

    ret = SendDataToPeer(idx, out.msg, out.msgLen);
    free(out.msg);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Data trasmit failed: %" PRIu32, ret);
        return false;
    }

    return true;
}

char *BuildRejectConfirmation(void)
{
    cJSON *rspJson = cJSON_CreateObject();
    if (rspJson == NULL) {
        DCONN_ERROR("Failed to allocate rspJson memory.");
        return NULL;
    }
    DConnAddIntToJson(rspJson, FIELD_CONFIRMATION, REQUEST_REJECTED);
    char *rspStr = cJSON_PrintUnformatted(rspJson);
    if (rspStr == NULL) {
        DCONN_ERROR("Failed to pack rspJson to string.");
        cJSON_Delete(rspJson);
        return NULL;
    }
    cJSON_Delete(rspJson);

    return rspStr;
}

uint32_t SendNotifyMessage(int32_t idx, const char *message, uint32_t messageLen)
{
    DConnMsgIn in = {
        .data = (const uint8_t *)message,
        .dataLen = messageLen,
    };
    DConnMsgOut out;
    uint32_t ret = DConnEncMsg(DCONN_MSG_TYPE_NOTIFY, NULL, &in, &out);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Encode notify message failed: %" PRIu32, ret);
        return ret;
    }

    ret = SendDataToPeer(idx, out.msg, out.msgLen);
    free(out.msg);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Send notify message failed: %" PRIu32, ret);
        return ret;
    }

    return DC_SUCCESS;
}
