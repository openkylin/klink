/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dconn_msg.h"

#include <stdlib.h>
#include <string.h>
#include <securec.h>
#include <netinet/in.h>

#include "product_limits.h"
#include "dconn_log.h"
#include "dconncaseone_interface.h"
#include "dconn_mbedtls_gcm.h"

#define DCONN_HW_LEN 2
#define DCONN_DWORD_BITS 32

#define DCONN_PACK_STRUCT __attribute__((packed))

static uint32_t g_additionMessage = 0;  // 用于AES-GCM模式加密的额外信息，每次自增防止重放攻击

typedef struct {
    uint8_t hw[DCONN_HW_LEN];  // 固定字符 HW
    uint16_t type;             // 消息类型 DConnMsgType
} DCONN_PACK_STRUCT DConnMsgCommonHeader;

typedef struct {
    DConnMsgCommonHeader commonHeader;
    uint32_t len;        // 可变长数据长度
    uint64_t requestId;  // Hichain Request ID
} DCONN_PACK_STRUCT DConnAuthMsg;

typedef struct {
    DConnMsgCommonHeader commonHeader;
    uint32_t len;                // 可变长数据长度
    uint8_t iv[DCONN_IV_LEN];    // 初始化向量
    uint8_t add[DCONN_ADD_LEN];  // 额外信息
    uint8_t tag[DCONN_TAG_LEN];  // 验证码
} DCONN_PACK_STRUCT DConnDataMsg;

static uint64_t HostToNetworkLongLong(uint64_t hostLongLong)
{
    return (((uint64_t)htonl(hostLongLong)) << DCONN_DWORD_BITS) + htonl(hostLongLong >> DCONN_DWORD_BITS);
}

static uint64_t NetworkToHostLongLong(uint64_t netLongLong)
{
    return (((uint64_t)ntohl(netLongLong)) << DCONN_DWORD_BITS) + ntohl(netLongLong >> DCONN_DWORD_BITS);
}

static uint32_t DConnEncAuthMsg(const DConnAuthParam *encParam, const DConnMsgIn *in, DConnMsgOut *out)
{
    if (in->dataLen > DCONN_AUTH_BODY_LEN) {
        DCONN_ERROR("Auth body len exceed.");
        return DC_ERR_HICHAIN_AUTH_MSG_EXCEED;
    }

    out->msg = malloc(sizeof(DConnAuthMsg) + in->dataLen);
    if (out->msg == NULL) {
        DCONN_ERROR("Cannot allocate memory.");
        return DC_ERR_ALLOC_MEMORY;
    }

    if (in->dataLen > DCONN_AUTH_BODY_LEN) {
        DCONN_ERROR("Auth body len exceed.");
        free(out->msg);
        out->msg = NULL;
        return DC_ERR_HICHAIN_AUTH_MSG_EXCEED;
    }

    DConnAuthMsg authMsgHeader;
    errno_t err = memcpy_s(authMsgHeader.commonHeader.hw, DCONN_HW_LEN, "HW", DCONN_HW_LEN);
    if (err != EOK) {
        DCONN_ERROR("memcpy_s failed.");
        free(out->msg);
        out->msg = NULL;
        return DC_ERROR;
    }
    authMsgHeader.commonHeader.type = htons(DCONN_MSG_TYPE_AUTH);
    authMsgHeader.len = htonl(in->dataLen);
    authMsgHeader.requestId = HostToNetworkLongLong(encParam->requestId);

    err = memcpy_s(out->msg, sizeof(DConnAuthMsg), &authMsgHeader, sizeof(DConnAuthMsg));
    if (err != EOK) {
        DCONN_ERROR("memcpy_s failed.");
        free(out->msg);
        out->msg = NULL;
        return DC_ERROR;
    }
    err = memcpy_s(out->msg + sizeof(DConnAuthMsg), DCONN_AUTH_BODY_LEN, in->data, in->dataLen);
    if (err != EOK) {
        DCONN_ERROR("memcpy_s failed.");
        free(out->msg);
        out->msg = NULL;
        return DC_ERROR;
    }
    out->msgLen = sizeof(DConnAuthMsg) + in->dataLen;
    return DC_SUCCESS;
}

static uint32_t DConnEncDataMsg(const DConnDataParam *encParam, const DConnMsgIn *in, DConnMsgOut *out)
{
    out->msgLen = sizeof(DConnDataMsg) + in->dataLen;
    out->msg = malloc(out->msgLen);
    if (out->msg == NULL) {
        return DC_ERR_ALLOC_MEMORY;
    }

    DConnDataMsg dataMsgHeader;
    errno_t err = memcpy_s(dataMsgHeader.commonHeader.hw, DCONN_HW_LEN, "HW", DCONN_HW_LEN);
    if (err != EOK) {
        DCONN_ERROR("memcpy_s failed.");
        free(out->msg);
        out->msg = NULL;
        return DC_ERROR;
    }
    dataMsgHeader.commonHeader.type = htons(DCONN_MSG_TYPE_DATA);
    dataMsgHeader.len = htonl(in->dataLen);

    DConnGenerateRandom(dataMsgHeader.iv, DCONN_IV_LEN);
    err = memcpy_s(dataMsgHeader.add, DCONN_ADD_LEN, &g_additionMessage, DCONN_ADD_LEN);
    if (err != EOK) {
        DCONN_ERROR("memcpy_s failed.");
        free(out->msg);
        out->msg = NULL;
        return DC_ERROR;
    }
    g_additionMessage++;

    DConnGcmAlgIn algIn = {
        .data = in->data,
        .dataLen = in->dataLen,
        .key = encParam->key,
        .keyLen = encParam->keyLen,
        .iv = dataMsgHeader.iv,
        .add = dataMsgHeader.add,
    };
    uint32_t ret = DConnEncrypt(&algIn, (out->msg + sizeof(DConnDataMsg)), dataMsgHeader.tag);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Encrypt failed: %" PRIu32, ret);
        free(out->msg);
        out->msg = NULL;
        return ret;
    }

    err = memcpy_s(out->msg, sizeof(DConnDataMsg), &dataMsgHeader, sizeof(DConnDataMsg));
    if (err != EOK) {
        DCONN_ERROR("memcpy_s failed.");
        free(out->msg);
        out->msg = NULL;
        return DC_ERROR;
    }
    return ret;
}

static uint32_t DConnEncRawMsg(uint16_t type, const DConnMsgIn *in, DConnMsgOut *out)
{
    if (in->dataLen > DCONN_DEVICE_ID_BODY_LEN) {
        DCONN_ERROR("Raw message body len exceed.");
        return DC_ERR_HICHAIN_AUTH_MSG_EXCEED;
    }

    out->msg = malloc(sizeof(DConnMsgCommonHeader) + in->dataLen);
    if (out->msg == NULL) {
        DCONN_ERROR("Cannot allocate memory.");
        return DC_ERR_ALLOC_MEMORY;
    }

    DConnMsgCommonHeader header;
    errno_t err = memcpy_s(header.hw, DCONN_HW_LEN, "HW", DCONN_HW_LEN);
    if (err != EOK) {
        DCONN_ERROR("memcpy_s failed.");
        free(out->msg);
        out->msg = NULL;
        return DC_ERROR;
    }
    header.type = htons(type);

    err = memcpy_s(out->msg, sizeof(DConnMsgCommonHeader), &header, sizeof(DConnMsgCommonHeader));
    if (err != EOK) {
        DCONN_ERROR("memcpy_s failed.");
        free(out->msg);
        out->msg = NULL;
        return DC_ERROR;
    }
    err = memcpy_s(out->msg + sizeof(DConnMsgCommonHeader), in->dataLen, in->data, in->dataLen);
    if (err != EOK) {
        DCONN_ERROR("memcpy_s failed.");
        free(out->msg);
        out->msg = NULL;
        return DC_ERROR;
    }
    out->msgLen = sizeof(DConnMsgCommonHeader) + in->dataLen;
    return DC_SUCCESS;
}

uint32_t DConnEncMsg(uint16_t type, const void *encParam, const DConnMsgIn *in, DConnMsgOut *out)
{
    if (type == DCONN_MSG_TYPE_AUTH) {
        return DConnEncAuthMsg((const DConnAuthParam *)encParam, in, out);
    } else if (type == DCONN_MSG_TYPE_DATA) {
        return DConnEncDataMsg((const DConnDataParam *)encParam, in, out);
    } else if (type == DCONN_MSG_TYPE_DEVICE_ID) {
        return DConnEncRawMsg(DCONN_MSG_TYPE_DEVICE_ID, in, out);
    } else if (type == DCONN_MSG_TYPE_NOTIFY) {
        return DConnEncRawMsg(DCONN_MSG_TYPE_NOTIFY, in, out);
    }

    return DC_ERR_BRANCH;
}

static uint32_t DConnDecAuthMsg(DConnAuthParam *decParam, const DConnMsgIn *in, DConnMsgOut *out)
{
    DConnAuthMsg authMsgHeader;
    errno_t err = memcpy_s(&authMsgHeader, sizeof(DConnAuthMsg), in->data, sizeof(DConnAuthMsg));
    if (err != EOK) {
        DCONN_ERROR("memcpy_s failed.");
        return DC_ERROR;
    }

    if (memcmp(authMsgHeader.commonHeader.hw, "HW", DCONN_HW_LEN) != 0) {
        DCONN_ERROR("Invalid message header HW [%c%c]", *authMsgHeader.commonHeader.hw,
                    *(authMsgHeader.commonHeader.hw + 1));
        return DC_ERR_HEADER_INVALID;
    }

    uint16_t type = ntohs(authMsgHeader.commonHeader.type);
    if (type != DCONN_MSG_TYPE_AUTH) {
        DCONN_ERROR("Invalid message header type [%2" PRIu16 "]", type);
        return DC_ERR_HEADER_INVALID;
    }

    uint32_t len = ntohl(authMsgHeader.len);
    if (len > DCONN_AUTH_BODY_LEN) {
        DCONN_ERROR("Invalid message header len [%" PRIu32 "]", len);
        return DC_ERR_HEADER_INVALID;
    }

    if (len + sizeof(DConnAuthMsg) > (in->dataLen)) {
        DCONN_ERROR("Invalid message len [%" PRIu32 "]", (in->dataLen));
        return DC_ERR_HEADER_INVALID;
    }

    DCONN_DEBUG("All header check passed.");
    decParam->requestId = NetworkToHostLongLong(authMsgHeader.requestId);
    out->msg = malloc(len);
    if (out->msg == NULL) {
        DCONN_ERROR("Cannot allocate memory.");
        return DC_ERR_ALLOC_MEMORY;
    }

    err = memcpy_s(out->msg, DCONN_AUTH_BODY_LEN, in->data + sizeof(DConnAuthMsg), len);
    if (err != EOK) {
        DCONN_ERROR("memcpy_s failed.");
        free(out->msg);
        out->msg = NULL;
        return DC_ERROR;
    }
    out->msgLen = len;
    return DC_SUCCESS;
}

static uint32_t DConnCheckDataBeforeDecode(const DConnDataMsg *dataMsgHeader, const DConnMsgIn *in)
{
    if (memcmp(dataMsgHeader->commonHeader.hw, "HW", DCONN_HW_LEN) != 0) {
        DCONN_ERROR("Invalid message header HW [%c%c]", *dataMsgHeader->commonHeader.hw,
                    *(dataMsgHeader->commonHeader.hw + 1));
        return DC_ERR_HEADER_INVALID;
    }

    uint16_t type = ntohs(dataMsgHeader->commonHeader.type);
    if (type != DCONN_MSG_TYPE_DATA) {
        DCONN_ERROR("Invalid message header type [%2" PRIu16 "]", type);
        return DC_ERR_HEADER_INVALID;
    }

    uint32_t len = ntohl(dataMsgHeader->len);
    if (len > DCONN_DATA_BODY_LEN) {
        DCONN_ERROR("Invalid message header len [%" PRIu32 "]", len);
        return DC_ERR_HEADER_INVALID;
    }

    if (len + sizeof(DConnDataMsg) > in->dataLen) {
        DCONN_ERROR("Invalid message len [%" PRIu32 "]", in->dataLen);
        return DC_ERR_HEADER_INVALID;
    }

    return DC_SUCCESS;
}

static uint32_t DConnDecDataMsg(DConnDataParam *decParam, const DConnMsgIn *in, DConnMsgOut *out)
{
    DConnDataMsg dataMsgHeader;
    errno_t err = memcpy_s(&dataMsgHeader, sizeof(DConnDataMsg), in->data, sizeof(DConnDataMsg));
    if (err != EOK) {
        DCONN_ERROR("memcpy_s failed.");
        return DC_ERROR;
    }

    uint32_t ret = DConnCheckDataBeforeDecode(&dataMsgHeader, in);
    if (ret != DC_SUCCESS) {
        return ret;
    }

    out->msg = malloc(ntohl(dataMsgHeader.len));
    if (out->msg == NULL) {
        DCONN_ERROR("Cannot allocate memory.");
        return DC_ERR_ALLOC_MEMORY;
    }

    out->msgLen = ntohl(dataMsgHeader.len);
    DConnGcmAlgIn algIn = {
        .data = ((in->data) + sizeof(DConnDataMsg)),
        .dataLen = ntohl(dataMsgHeader.len),
        .key = decParam->key,
        .keyLen = decParam->keyLen,
        .iv = dataMsgHeader.iv,
        .add = dataMsgHeader.add,
    };
    ret = DConnDecrypt(&algIn, dataMsgHeader.tag, out->msg);
    if (ret != DC_SUCCESS) {
        free(out->msg);
        out->msg = NULL;
        DCONN_ERROR("Decrypt failed: %" PRIu32, ret);
        return ret;
    }

    return DC_SUCCESS;
}

static uint32_t DConnDecRawMsg(uint16_t type, const DConnMsgIn *in, DConnMsgOut *out)
{
    DConnMsgCommonHeader header;
    errno_t err = memcpy_s(&header, sizeof(DConnMsgCommonHeader), in->data, sizeof(DConnMsgCommonHeader));
    if (err != EOK) {
        DCONN_ERROR("memcpy_s failed.");
        return DC_ERROR;
    }

    if (memcmp(header.hw, "HW", DCONN_HW_LEN) != 0) {
        DCONN_ERROR("Invalid message header HW [%c%c]", *header.hw, *(header.hw + 1));
        return DC_ERR_HEADER_INVALID;
    }

    if (ntohs(header.type) != type) {
        DCONN_ERROR("Invalid message header type [%2" PRIu16 "]", type);
        return DC_ERR_HEADER_INVALID;
    }

    DCONN_DEBUG("All header check passed.");
    uint32_t len = in->dataLen - sizeof(DConnMsgCommonHeader);
    out->msg = malloc(len);
    if (out->msg == NULL) {
        DCONN_ERROR("Cannot allocate memory.");
        return DC_ERR_ALLOC_MEMORY;
    }

    err = memcpy_s(out->msg, len, in->data + sizeof(DConnMsgCommonHeader), len);
    if (err != EOK) {
        DCONN_ERROR("memcpy_s failed.");
        free(out->msg);
        out->msg = NULL;
        return DC_ERROR;
    }
    out->msgLen = len;
    return DC_SUCCESS;
}

uint32_t DConnDecMsg(uint16_t type, void *decParam, const DConnMsgIn *in, DConnMsgOut *out)
{
    if (type == DCONN_MSG_TYPE_AUTH) {
        return DConnDecAuthMsg((DConnAuthParam *)decParam, in, out);
    } else if (type == DCONN_MSG_TYPE_DATA) {
        return DConnDecDataMsg((DConnDataParam *)decParam, in, out);
    } else if (type == DCONN_MSG_TYPE_DEVICE_ID) {
        return DConnDecRawMsg(DCONN_MSG_TYPE_DEVICE_ID, in, out);
    } else if (type == DCONN_MSG_TYPE_NOTIFY) {
        return DConnDecRawMsg(DCONN_MSG_TYPE_NOTIFY, in, out);
    }

    return DC_ERR_BRANCH;
}

uint32_t DConnGetMsgType(const uint8_t *data, uint32_t dataLen, uint16_t *type)
{
    if (dataLen < sizeof(DConnMsgCommonHeader)) {
        DCONN_ERROR("Invalid message header. Data has invalid length.");
        return DC_ERR_HEADER_INVALID;
    }

    DConnMsgCommonHeader header;
    errno_t err = memcpy_s(&header, sizeof(DConnMsgCommonHeader), data, sizeof(DConnMsgCommonHeader));
    if (err != EOK) {
        DCONN_ERROR("memcpy_s failed.");
        return DC_ERROR;
    }
    *type = ntohs(header.type);
    return DC_SUCCESS;
}
