/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_MBEDTLS_GCM
#define APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_MBEDTLS_GCM

#include <stdbool.h>
#include <inttypes.h>

#include "product_limits.h"

#define DCONN_IV_LEN 12
#define DCONN_ADD_LEN 4
#define DCONN_TAG_LEN 16

/**
 * @brief 生成指定长度的随机数
 *
 * @param random 随机数
 * @param len 长度
 * @return uint32_t 错误码
 */
uint32_t DConnGenerateRandom(uint8_t *random, uint32_t len);

typedef struct {
    const uint8_t *data;
    uint32_t dataLen;
    const uint8_t *key;
    uint32_t keyLen;
    const uint8_t *iv;
    const uint8_t *add;
} DConnGcmAlgIn;

/**
 * @brief AES-GCM 模式加密数据
 *
 * @param plainText 明文
 * @param plainTextLen 明文长度
 * @param key 密钥
 * @param keyLen 密钥长度
 * @param iv 初始化向量
 * @param add 额外信息
 * @param cryptedText 密文。密文长度与明文一致。
 * @param tag 验证码
 * @return uint32_t 错误码
 */
uint32_t DConnEncrypt(const DConnGcmAlgIn *in, uint8_t *cryptedText, uint8_t *tag);

/**
 * @brief AES-GCM 模式解密数据
 *
 * @param cryptedText 密文
 * @param cryptedTextLen 密文长度
 * @param key 密钥
 * @param keyLen 密钥长度
 * @param iv 初始化向量
 * @param add 额外信息
 * @param tag 验证码
 * @param plainText 明文。明文长度与密文一致。
 * @return uint32_t 错误码
 */
uint32_t DConnDecrypt(const DConnGcmAlgIn *in, const uint8_t *tag, uint8_t *plainText);

#endif  // APPLICATIONS_APP_DCONNECTCASEONE_COMMON_INCLUDE_DCONN_MBEDTLS_GSM
