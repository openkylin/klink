/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dconn_send.h"

#include <inttypes.h>
#include <errno.h>
#include <sys/socket.h>

#include "dconn_data.h"
#include "dconn_log.h"
#include "dconn_peer.h"

uint32_t SendDataToPeer(int32_t idx, uint8_t *data, uint32_t dataLen)
{
    int *fd = GetSocketByPeerDataIdx(idx);
    if (*fd < 0) {
        DCONN_ERROR("Invalid socket for idx: %" PRId32, idx);
        return DC_ERR_CONNECTION_REFUSED;
    }

    uint32_t stat = GetAuthStatByPeerDataIdx(idx);
    if (stat == PEER_AUTH_STAT_INVALID) {
        DCONN_ERROR("Invalid auth stat for idx: %" PRId32, idx);
        return DC_ERR_CONNECTION_REFUSED;
    }

    SendDataCallback sendDataCallback = NULL;
    const char *targetDeviceId = NULL;
    if (stat == PEER_AUTH_STAT_AUTHORIZED) {
        sendDataCallback = GetIotSdkCallback()->sendDataResultCb;
        if (sendDataCallback == NULL) {
            DCONN_ERROR("Send data callback not registerd. Data will not send.");
            return DC_ERR_CB_NOT_REGISTER;
        }

        targetDeviceId = GetDeviceIdByPeerIdx(idx);
        if (targetDeviceId == NULL) {
            DCONN_ERROR("Cannot find device ID for idx: %" PRId32, idx);
            return DC_ERR_DEVICE_ID_NOT_FOUND;
        }
    }

    // Return success but callback failed below.
    DumpBuf(data, dataLen);
    ssize_t ret = send(*fd, data, (size_t)dataLen, 0);
    if (ret < 0) {
        DCONN_ERROR("Send data failed: %d", errno);
        if (sendDataCallback != NULL && targetDeviceId != NULL) {
            sendDataCallback(targetDeviceId, DC_ERR_SEND_FAILED);
        }
    } else {
        if (sendDataCallback != NULL && targetDeviceId != NULL) {
            sendDataCallback(targetDeviceId, DC_SUCCESS);
        }
    }

    return DC_SUCCESS;
}
