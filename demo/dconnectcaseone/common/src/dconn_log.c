/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dconn_log.h"

#include <arpa/inet.h>
#include "securec.h"
#ifdef __LITEOS__
#    include "log.h"
#else
#    include "hilog/log.h"
#endif

#define HEXDECIMAL 16
#define LOG_PRINT_MAX_LEN 1024

#ifdef __LITEOS__
#    define DCONN_HILOG_TYPE HILOG_MODULE_APP
#else
#    define DCONN_HILOG_TYPE LOG_APP
#endif

void DumpBuf(uint8_t *buf, uint32_t len)
{
#ifdef OHOS_DEBUG
    uint32_t i;
    printf("buf:");
    for (i = 0; i < len; i++) {
        printf("%s%02X%s", i % HEXDECIMAL == 0 ? "\r\n\t" : " ", buf[i], i == len - 1 ? "\r\n" : "");
    }
#else
    (void)buf;
    (void)len;
#endif
}

#define DCONN_LOG_DEBUG(buf) HILOG_DEBUG(LOG_DEBUG, "%s", buf)
#define DCONN_LOG_INFO(buf) HILOG_INFO(LOG_INFO, "%s", buf)
#define DCONN_LOG_WARN(buf) HILOG_INFO(LOG_WARN, "%s", buf)
#define DCONN_LOG_ERROR(buf) HILOG_INFO(LOG_ERROR, "%s", buf)

static void DConnOutPrint(const char *buf, DConnLogLevel level)
{
#ifdef OHOS_DEBUG
    switch (level) {
        case DCONN_LOG_LEVEL_DEBUG:
            printf("[D][DCONN]: %s\n", buf);
            break;
        case DCONN_LOG_LEVEL_INFO:
            printf("[I][DCONN]: %s\n", buf);
            break;
        case DCONN_LOG_LEVEL_WARN:
            printf("[W][DCONN]: %s\n", buf);
            break;
        case DCONN_LOG_LEVEL_ERROR:
            printf("[E][DCONN]: %s\n", buf);
            break;
        default:
            break;
    }
    return;
#else
    switch (level) {
        case DCONN_LOG_LEVEL_DEBUG:
            DCONN_LOG_DEBUG(buf);
            break;
        case DCONN_LOG_LEVEL_INFO:
            DCONN_LOG_INFO(buf);
            break;
        case DCONN_LOG_LEVEL_WARN:
            DCONN_LOG_WARN(buf);
            break;
        case DCONN_LOG_LEVEL_ERROR:
            DCONN_LOG_ERROR(buf);
            break;
        default:
            break;
    }
#endif
}

void DConnLogPrint(DConnLogLevel level, const char *funName, const char *fmt, ...)
{
    size_t ulPos = 0;
    char *outStr = malloc(LOG_PRINT_MAX_LEN);
    if (outStr == NULL) {
        return;
    }
    int32_t ret = sprintf_s(outStr, LOG_PRINT_MAX_LEN, "%s: ", funName);
    if (ret < 0) {
        free(outStr);
        return;
    }
    ulPos = strlen(outStr);
    va_list arg;
    va_start(arg, fmt);
    ret = vsprintf_s(&outStr[ulPos], LOG_PRINT_MAX_LEN - ulPos, fmt, arg);
    va_end(arg);
    if (ret < 0) {
        free(outStr);
        return;
    }
    DConnOutPrint(outStr, level);
    free(outStr);
}

uint32_t DconnIpTransToInt(const char *ipAddr)
{
    if (ipAddr == NULL || strstr(ipAddr, ".") == NULL) {
        printf("Invalid IP address, convert to int failed.\n");
        return 0;
    }
    uint32_t ipValue = inet_addr(ipAddr);
    return ntohl(ipValue);
}
