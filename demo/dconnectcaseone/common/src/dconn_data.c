/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <pthread.h>
#include <securec.h>
#include <arpa/inet.h>

#include "dconn_data.h"
#include "dconn_peer.h"
#include "dconn_log.h"
#include "dconn_data_impl.h"
#include "dconn_server_data.h"
#include "dconn_msg.h"

#define DCONN_COUNT_THRESHOLD 20
#define DCONN_SHIFT_32_BITS 32

static DConnData *g_dconnData = NULL;
static pthread_mutex_t g_initMutex = PTHREAD_MUTEX_INITIALIZER;  // 初始化互斥量，避免并发初始化
static pthread_mutex_t g_peerDataMutex = PTHREAD_MUTEX_INITIALIZER;
static CallbackParam g_iotSdkCallbackParam;  // IOT SDK 回调函数

uint32_t InitDConnData(void)
{
    DCONN_DEBUG("Init data begin");
    pthread_mutex_lock(&g_initMutex);
    if (g_dconnData != NULL) {
        pthread_mutex_unlock(&g_initMutex);
        DCONN_DEBUG("Data already inited");
        return DC_SUCCESS;
    }

    g_dconnData = (DConnData *)malloc(sizeof(DConnData));
    if (g_dconnData == NULL) {
        DCONN_ERROR("Malloc for g_dconnData failed!");
        pthread_mutex_unlock(&g_initMutex);
        return DC_ERR_ALLOC_MEMORY;
    }

    // 设置初始值
    InitPeerData(g_dconnData->peerDataList);
    InitServerData(&g_dconnData->serverData);
    InitHichainData(&g_dconnData->hichainData);

    pthread_mutex_unlock(&g_initMutex);
    DCONN_DEBUG("init data end");
    return DC_SUCCESS;
}

void DeInitDconnData(void)
{
    if (g_dconnData == NULL) {
        return;
    }

    // 释放资源时，需与初始化资源顺序相反
    DeInitServerData(&g_dconnData->serverData);
    DeInitPeerData(g_dconnData->peerDataList);

    // 释放全局数据
    free(g_dconnData);
    g_dconnData = NULL;
}

DConnData *GetDConnData(void)
{
    return g_dconnData;
}

int32_t GetNewPeerDataIdx(uint32_t role)
{
    pthread_mutex_lock(&g_peerDataMutex);
    for (uint32_t i = 0; i < MAX_DEVICES_NUM; i++) {
        if (g_dconnData->peerDataList[i].stat == PEER_DATA_STAT_USED) {
            continue;
        }

        // 原则上获取时初始化，释放时不做额外的初始化操作
        errno_t err = memset_s(&g_dconnData->peerDataList[i], sizeof(PeerData), 0, sizeof(PeerData));
        if (err != EOK) {
            DCONN_ERROR("memset_s failed.");
            pthread_mutex_unlock(&g_peerDataMutex);
            return -1;
        }
        g_dconnData->peerDataList[i].socket = -1;
        g_dconnData->peerDataList[i].authStat = PEER_AUTH_STAT_UNAUTHORIZED;
        g_dconnData->peerDataList[i].role = role;
        g_dconnData->peerDataList[i].authStartTime = time(NULL);
        g_dconnData->peerDataList[i].stat = PEER_DATA_STAT_USED;
        pthread_mutex_unlock(&g_peerDataMutex);
        return i;
    }

    pthread_mutex_unlock(&g_peerDataMutex);
    return -1;
}

void ReleasePeerDataIdx(int32_t idx)
{
    pthread_mutex_lock(&g_peerDataMutex);
    g_dconnData->peerDataList[idx].stat = PEER_DATA_STAT_UNUSED;
    close(g_dconnData->peerDataList[idx].socket);
    errno_t err = memset_s(g_dconnData->peerDataList[idx].sessionKey, SESSION_KEY_LEN, 0, SESSION_KEY_LEN);
    if (err != EOK) {
        DCONN_ERROR("memset_s failed.");
        pthread_mutex_unlock(&g_peerDataMutex);
        return;
    }
    DCONN_INFO("Peer data released: %" PRId32, idx);
    pthread_mutex_unlock(&g_peerDataMutex);
}

uint32_t GetPeerRole(int32_t idx)
{
    return g_dconnData->peerDataList[idx].role;
}

void SetSocketByPeerDataIdx(int32_t idx, int socket)
{
    g_dconnData->peerDataList[idx].socket = socket;
}

int *GetSocketByPeerDataIdx(int32_t idx)
{
    return &g_dconnData->peerDataList[idx].socket;
}

int64_t GetRequestIdByPeerDataIdx(int32_t idx)
{
    return g_dconnData->peerDataList[idx].requestId;
}

void SetAuthStatByPeerDataIdx(int32_t idx, uint32_t sta)
{
    DCONN_DEBUG("Peer idx: %" PRId32 " auth stat changed to: %" PRIu32, idx, sta);
    g_dconnData->peerDataList[idx].authStat = sta;
}

void SetRequestIdByPeerDataIdx(int32_t idx, int64_t requestId)
{
    g_dconnData->peerDataList[idx].requestId = requestId;
}

uint32_t GetAuthStatByPeerDataIdx(int32_t idx)
{
    return g_dconnData->peerDataList[idx].authStat;
}

int32_t GetIdxByRequestId(int64_t requestId)
{
    for (uint32_t i = 0; i < MAX_DEVICES_NUM; i++) {
        if (g_dconnData->peerDataList[i].stat == PEER_DATA_STAT_UNUSED) {
            continue;
        }

        if (g_dconnData->peerDataList[i].requestId != requestId) {
            continue;
        }

        return i;
    }

    return -1;
}

uint32_t GetAuthStatByPeerDataRequestId(int64_t requestId)
{
    for (uint32_t i = 0; i < MAX_DEVICES_NUM; i++) {
        if (g_dconnData->peerDataList[i].requestId != requestId) {
            continue;
        } else {
            return i;
        }
    }

    return -1;
}

char *GetDeviceIdByPeerIdx(int32_t idx)
{
    if (g_dconnData->peerDataList[idx].stat != PEER_DATA_STAT_USED) {
        return NULL;
    }

    return g_dconnData->peerDataList[idx].deviceId;
}

void SetDeviceIdForIdx(int32_t idx, const char *deviceId)
{
    errno_t ret = strcpy_s(g_dconnData->peerDataList[idx].deviceId, DCONN_DEVICE_ID_LEN + 1, deviceId);
    if (ret != EOK) {
        DCONN_ERROR("strcpy_s failed: %d", ret);
    }
}

const char *GetUdidByPeerIdx(int32_t idx)
{
    if (g_dconnData->peerDataList[idx].stat != PEER_DATA_STAT_USED) {
        return NULL;
    }

    return g_dconnData->peerDataList[idx].udid;
}

void SetUdidForIdx(int32_t idx, const char *udid)
{
    errno_t ret = strcpy_s(g_dconnData->peerDataList[idx].udid, DCONN_UDID_LEN + 1, udid);
    if (ret != EOK) {
        DCONN_ERROR("strcpy_s failed: %d", ret);
    }
}

int32_t GetPeerDataIdxByUdid(const char *udid)
{
    for (uint32_t i = 0; i < MAX_DEVICES_NUM; i++) {
        if (g_dconnData->peerDataList[i].stat != PEER_DATA_STAT_USED) {
            continue;
        }
        if (strcmp(g_dconnData->peerDataList[i].udid, udid) != 0) {
            continue;
        }

        return i;
    }

    return -1;
}

bool GetPeerFdByDeviceId(const char *deviceId, int *fd, bool authorized)
{
    for (uint32_t i = 0; i < MAX_DEVICES_NUM; i++) {
        if (strcmp(deviceId, g_dconnData->peerDataList[i].deviceId) != 0) {
            continue;
        }

        if (g_dconnData->peerDataList[i].stat != PEER_DATA_STAT_USED) {
            DCONN_ERROR("invalid peer");
            return false;
        }

        if (authorized && g_dconnData->peerDataList[i].authStat != PEER_AUTH_STAT_AUTHORIZED) {
            DCONN_ERROR("unauthorized peer");
            return false;
        }

        *fd = g_dconnData->peerDataList[i].socket;
        return true;
    }

    DCONN_ERROR("peer match device ID not found");
    return false;
}

int GetServerSocket(void)
{
    return g_dconnData->serverData.serverSocket;
}

void SetServerSocket(int fd)
{
    g_dconnData->serverData.serverSocket = fd;
}

int *GetServerSocketAddrFamily(void)
{
    return &g_dconnData->serverData.serverAddrFamily;
}

CallbackParam *GetIotSdkCallback(void)
{
    return &g_iotSdkCallbackParam;
}

void SetSessionKeyByPeerIdx(int32_t idx, const uint8_t *sessionKey, uint32_t sessionKeyLen)
{
    errno_t err = memcpy_s(g_dconnData->peerDataList[idx].sessionKey, SESSION_KEY_LEN, sessionKey, sessionKeyLen);
    if (err != EOK) {
        DCONN_ERROR("memcpy_s failed.");
        return;
    }
    g_dconnData->peerDataList[idx].sessionKeyLen = sessionKeyLen;
}

void GetSessionKeyByPeerIdx(int32_t idx, uint8_t **sessionKey, uint32_t *keyLength)
{
    if (g_dconnData->peerDataList[idx].stat == PEER_DATA_STAT_UNUSED ||
        g_dconnData->peerDataList[idx].authStat != PEER_AUTH_STAT_AUTHORIZED) {
        *keyLength = 0;
        return;
    }

    *sessionKey = g_dconnData->peerDataList[idx].sessionKey;
    *keyLength = g_dconnData->peerDataList[idx].sessionKeyLen;
}

int32_t GetIdxByDeviceId(const char *deviceId)
{
    for (uint32_t i = 0; i < MAX_DEVICES_NUM; i++) {
        if (g_dconnData->peerDataList[i].stat != PEER_DATA_STAT_USED) {
            continue;
        }
        if (strcmp(g_dconnData->peerDataList[i].deviceId, deviceId) != 0) {
            continue;
        }

        return i;
    }

    return -1;
}

int InitSelectFdSet(fd_set *readSet, fd_set *exceptSet)
{
    int maxFd = -1;
    time_t now = time(NULL);
    FD_ZERO(readSet);
    FD_ZERO(exceptSet);

    for (uint32_t i = 0; i < MAX_DEVICES_NUM; i++) {
        if (g_dconnData->peerDataList[i].stat != PEER_DATA_STAT_USED) {
            continue;
        }

        if ((g_dconnData->peerDataList[i].role == PEER_ROLE_CLIENT) &&
            ((g_dconnData->peerDataList[i].authStat == PEER_AUTH_STAT_INVALID) ||
             ((g_dconnData->peerDataList[i].authStat != PEER_AUTH_STAT_AUTHORIZED) &&
              (now - g_dconnData->peerDataList[i].authStartTime > HICHAIN_AUTH_TIMEOUT)))) {
            // Unlike client, server has no timer to release client peers.
            // We should release auth failed or timeout peers here.
            DCONN_ERROR("Client peer auth time exceed: %" PRIu32, i);
            ReleasePeerDataIdx(i);
            continue;
        }

        int fd = g_dconnData->peerDataList[i].socket;
        if (fd == -1) {
            continue;
        }
        FD_SET(fd, readSet);
        FD_SET(fd, exceptSet);
        maxFd = (maxFd > fd) ? maxFd : fd;
    }

    if (g_dconnData->serverData.serverSocket != -1) {
        FD_SET(g_dconnData->serverData.serverSocket, readSet);
        FD_SET(g_dconnData->serverData.serverSocket, exceptSet);
        maxFd = (maxFd > g_dconnData->serverData.serverSocket) ? maxFd : g_dconnData->serverData.serverSocket;
    }

    return maxFd;
}

void HandleFdSet(const fd_set *fdSet, PeerIdxHandler handler, ServerSocketHandler serverHandler)
{
    if (serverHandler != NULL && g_dconnData->serverData.serverSocket != -1 &&
        FD_ISSET(g_dconnData->serverData.serverSocket, fdSet)) {
        serverHandler();
    }

    for (uint32_t i = 0; i < MAX_DEVICES_NUM; i++) {
        if (g_dconnData->peerDataList[i].stat != PEER_DATA_STAT_USED) {
            continue;
        }

        int fd = g_dconnData->peerDataList[i].socket;
        if (fd == -1 || !FD_ISSET(fd, fdSet)) {
            continue;
        }

        handler(i);
    }
}

uint32_t HasPeerIpAddr(uint32_t ipAddr)
{
    for (uint32_t i = 0; i < MAX_DEVICES_NUM; i++) {
        if (g_dconnData->peerDataList[i].stat == PEER_DATA_STAT_UNUSED) {
            continue;
        }
        if (g_dconnData->peerDataList[i].ipAddr == ipAddr) {
            return DC_SUCCESS;
        }
    }

    return DC_ERROR;
}

void SetIpAddrByIdx(int32_t idx, uint32_t ipAddr)
{
    g_dconnData->peerDataList[idx].ipAddr = ipAddr;
}

uint32_t HasServerIpAddr(void)
{
    if (g_dconnData->serverData.serverSocket == -1) {
        return DC_ERROR;
    }

    return DC_SUCCESS;
}
