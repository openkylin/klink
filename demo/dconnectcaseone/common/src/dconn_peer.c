/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <unistd.h>

#include "dconn_peer_impl.h"
#include "dconn_peer.h"
#include "dconn_log.h"

void InitPeerData(PeerDataList peerDataList)
{
    DCONN_DEBUG("init peer data begin");
    for (int i = 0; i < MAX_DEVICES_NUM; i++) {
        peerDataList[i].stat = PEER_DATA_STAT_UNUSED;  // 设置为未使用
        peerDataList[i].authStat = PEER_AUTH_STAT_UNAUTHORIZED;
        peerDataList[i].requestId = 0;
        peerDataList[i].authStartTime = 0;
    }
    DCONN_DEBUG("init peer data end");
}

void DeInitPeerData(PeerData *const peerDataList)
{
    for (int i = 0; i < MAX_DEVICES_NUM; i++) {
        if (peerDataList[i].stat == PEER_DATA_STAT_UNUSED) {
            continue;
        }

        if (peerDataList[i].socket != -1) {
            close(peerDataList[i].socket);
            peerDataList[i].socket = -1;
        }
    }
}