/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dconn_mbedtls_gcm.h"

#include <securec.h>

#include "mbedtls/gcm.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"

#include "dconn_log.h"
#include "dconncaseone_interface.h"

#define BITS_IN_BYTE 8
#define DCONN_GEN_RANDOM_THRESHOLD 200

static const uint8_t DCONN_RAMDOM_SEED_CUSTOM[] = {'D', 'C', 'O', 'N', 'N'};

static uint32_t DConnCtrDrbgSeed(mbedtls_ctr_drbg_context *ctrDrbg, mbedtls_entropy_context *entropy)
{
    mbedtls_ctr_drbg_init(ctrDrbg);
    mbedtls_entropy_init(entropy);

    int ret = mbedtls_ctr_drbg_seed(ctrDrbg, mbedtls_entropy_func, entropy, DCONN_RAMDOM_SEED_CUSTOM,
                                    sizeof(DCONN_RAMDOM_SEED_CUSTOM));
    if (ret != 0) {
        DCONN_ERROR("Ctr drbg seed failed! mbedtls ret = %d", ret);
        mbedtls_ctr_drbg_free(ctrDrbg);
        mbedtls_entropy_free(entropy);
        return DC_ERR_MBEDTLS_SEED;
    }

    return DC_SUCCESS;
}

uint32_t DConnGenerateRandom(uint8_t *random, uint32_t len)
{
    mbedtls_entropy_context entropy;
    mbedtls_ctr_drbg_context ctrDrbg;
    uint32_t ret = DConnCtrDrbgSeed(&ctrDrbg, &entropy);
    if (ret != DC_SUCCESS) {
        return ret;
    }

    uint32_t count = 0;
    int result = 0;
    do {
        result = mbedtls_ctr_drbg_random(&ctrDrbg, random, len);
        if (result != 0) {
            DCONN_ERROR("Mbedtls random failed! mbedtls ret = %d", result);
            errno_t err = memset_s(random, len, 0, len);
            if (err != EOK) {
                DCONN_ERROR("memset_s failed.");
            }
            count++;
        }
    } while (result != 0 && count < DCONN_GEN_RANDOM_THRESHOLD);

    if (count >= DCONN_GEN_RANDOM_THRESHOLD) {
        DCONN_ERROR("Mbedtls random failed count exceed: %" PRIu32, count);
        mbedtls_ctr_drbg_free(&ctrDrbg);
        mbedtls_entropy_free(&entropy);
        return DC_ERR_MBEDTLS_GEN_RANDOM;
    }

    mbedtls_ctr_drbg_free(&ctrDrbg);
    mbedtls_entropy_free(&entropy);
    return DC_SUCCESS;
}

uint32_t DConnEncrypt(const DConnGcmAlgIn *in, uint8_t *cryptedText, uint8_t *tag)
{
    mbedtls_gcm_context ctx;
    mbedtls_gcm_init(&ctx);

    int ret = mbedtls_gcm_setkey(&ctx, MBEDTLS_CIPHER_ID_AES, in->key, in->keyLen * BITS_IN_BYTE);
    if (ret != 0) {
        DCONN_ERROR("mbedtls_gcm_setkey failed: %d", ret);
        mbedtls_gcm_free(&ctx);
        return DC_ERR_MBEDTLS_SETKEY;
    }

    ret = mbedtls_gcm_crypt_and_tag(&ctx, MBEDTLS_GCM_ENCRYPT, in->dataLen, in->iv, DCONN_IV_LEN, in->add,
                                    DCONN_ADD_LEN, in->data, cryptedText, DCONN_TAG_LEN, tag);
    if (ret != 0) {
        DCONN_ERROR("mbedtls_gcm_crypt_and_tag failed: %d", ret);
        mbedtls_gcm_free(&ctx);
        return DC_ERR_MBEDTLS_ENCRYPT;
    }

    mbedtls_gcm_free(&ctx);
    return DC_SUCCESS;
}

uint32_t DConnDecrypt(const DConnGcmAlgIn *in, const uint8_t *tag, uint8_t *plainText)
{
    mbedtls_gcm_context ctx;
    mbedtls_gcm_init(&ctx);
    int ret = mbedtls_gcm_setkey(&ctx, MBEDTLS_CIPHER_ID_AES, in->key, in->keyLen * BITS_IN_BYTE);
    if (ret != 0) {
        DCONN_ERROR("mbedtls_gcm_setkey failed: %d", ret);
        mbedtls_gcm_free(&ctx);
        return DC_ERR_MBEDTLS_SETKEY;
    }

    ret = mbedtls_gcm_auth_decrypt(&ctx, in->dataLen, in->iv, DCONN_IV_LEN, in->add, DCONN_ADD_LEN, tag, DCONN_TAG_LEN,
                                   in->data, plainText);
    if (ret != 0) {
        DCONN_ERROR("mbedtls_gcm_auth_decrypt failed: %d", ret);
        mbedtls_gcm_free(&ctx);
        return DC_ERR_MBEDTLS_DECRYPT;
    }

    mbedtls_gcm_free(&ctx);
    return DC_SUCCESS;
}
