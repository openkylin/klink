/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <securec.h>

#include "dconncaseone_interface.h"
#include "dconn_data.h"
#include "service.h"
#include "client.h"
#include "dconn_thread.h"
#include "dconn_log.h"
#include "dconn_msg.h"
#include "dconn_group_manager.h"
#include "recv_thread.h"
#include "dconn_send.h"

// 大特性版本号
#define MAJOR_VERSION "1"
// 芯片版本号
#ifdef __MIPSEL__
#    define CHIP_VERSION "1"
#elif __ARMEL__
#    define CHIP_VERSION "2"
#endif
// 特性版本号
#define FEATURE_VERSION "0"
// 小版本号
// 0~199开发版本号
// 200~299 外部联调版本号
// 300 以上，对外商用发布版本号
#define MINOR_VERSION "0"

uint32_t InitDConnCaseOne(uint32_t type, const char *ipAry, char *errorIp)
{
    if (ipAry == NULL || errorIp == NULL) {
        DCONN_ERROR("Address array or errorIp pointer is null.");
        return DC_ERR_NULLPTR;
    }

    // 初始化数据
    uint32_t ret = InitDConnData();
    if (ret != DC_SUCCESS) {
        return ret;
    }

    // 使能数据传输线程
    ret = EnableTransferThr(RecvThread);
    if (ret != DC_SUCCESS) {
        return ret;
    }

    // 初始化设备认证服务
    ret = DConnInitDeviceAuthService();
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Init device auth service failed: %" PRIu32, ret);
        return ret;
    }

    if (type == INIT_SERVICE) {
        ret = DConnInitService(ipAry);
        if (ret != DC_SUCCESS) {
            // 服务端初始化失败，IP肯定只有一个
            errno_t err = strcpy_s(errorIp, DCONN_IP_ADDR_LEN + 1, ipAry);
            if (err != EOK) {
                DCONN_ERROR("strcpy_s failed.");
            }
        }
    } else if (type == INIT_CLIENT) {
        ret = DConnInitClient(ipAry, errorIp);
    } else {
        ret = DC_ERR_INVALID_INIT_TYPE;
    }

    return ret;
}

void RegisterCallback(const CallbackParam *callback)
{
    if (callback == NULL) {
        return;
    }

    errno_t err = memcpy_s(GetIotSdkCallback(), sizeof(CallbackParam), callback, sizeof(CallbackParam));
    if (err != EOK) {
        DCONN_ERROR("memcpy_s failed.");
        return;
    }
}

uint32_t DConnSendData(const char *targetDeviceId, const char *data, uint32_t dataLen)
{
    if (targetDeviceId == NULL || data == NULL) {
        DCONN_ERROR("targetDeviceId or data pointer is null.");
        return DC_ERR_NULLPTR;
    }

    if (dataLen > DCONN_DATA_BODY_LEN) {
        DCONN_ERROR("Send data len exceed: %" PRIu32, dataLen);
        return DC_ERR_SEND_DATA_EXCEED;
    }

    int32_t idx = GetIdxByDeviceId(targetDeviceId);
    if (idx < 0) {
        DCONN_ERROR("Device ID not found: %s", targetDeviceId);
        return DC_ERR_DEVICE_ID_NOT_FOUND;
    }

    DConnDataParam param;
    GetSessionKeyByPeerIdx(idx, &param.key, &param.keyLen);
    if (param.keyLen == 0) {
        DCONN_ERROR("Device not authorized: %s", targetDeviceId);
        return DC_ERR_DEVICE_NOT_AUTHORIZED;
    }

    DConnMsgIn in = {
        .data = (const uint8_t *)data,
        .dataLen = dataLen,
    };
    DConnMsgOut out;
    uint32_t ret = DConnEncMsg(DCONN_MSG_TYPE_DATA, &param, &in, &out);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Encode message failed");
        return ret;
    }

    ret = SendDataToPeer(idx, out.msg, out.msgLen);
    free(out.msg);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Send data to peer idx: %" PRId32 " failed. Data will not send: %" PRIu32, idx, ret);
        return ret;
    }

    return DC_SUCCESS;
}

void UnRegisterCallback(void)
{
    if (GetIotSdkCallback() == NULL) {
        return;
    }

    errno_t err = memset_s(GetIotSdkCallback(), sizeof(CallbackParam), 0, sizeof(CallbackParam));
    if (err != EOK) {
        DCONN_ERROR("memset_s failed.");
        return;
    }
}

void CloseDConnCaseOne(void)
{
    // 通知数据传输线程退出
    (void)ExitTransferThr();

    // 等待所有线程退出
    WaitTransferThrDisabled();

    // 释放全局数据
    DeInitDconnData();
}

const char *GetDConnVersion(void)
{
    return MAJOR_VERSION "."
                         "1.0"
                         "." FEATURE_VERSION "." MINOR_VERSION;
}
