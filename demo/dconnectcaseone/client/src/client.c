/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "client.h"

#include <stdbool.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <time.h>
#include <securec.h>

#include "cJSON.h"

#include "dconn_syspara.h"
#include "dconn_data.h"
#include "dconn_peer.h"
#include "dconncaseone_interface.h"
#include "product_limits.h"
#include "dconn_log.h"
#include "dconn_cjson.h"
#include "dconn_group_manager.h"
#include "dconn_send.h"
#include "dconn_msg.h"
#include "dconn_sched_yield.h"

/**
 * @brief 滑动窗口算法。从IP地址列表字符串中获取下一个 IP 地址。若中间存在错误格式的 IP 地址，则会导致后续的 IP
 * 地址都无法解析。
 *
 * @param ipAry IP 地址列表，以逗号分隔
 * @param idx 字符索引。正常情况下传入时总指向下一个IP地址的开始字符，返回后指向获取到的 IP 地址最后一个字符的下一个字符
 * @param ipAddr 获取到的 IP 地址
 * @return uint32_t 错误码
 */
static uint32_t GetNextIpAddr(const char *ipAry, uint32_t *idx, char *ipAddr, bool *hasNextIpAddr)
{
    errno_t err = memset_s(ipAddr, DCONN_IP_ADDR_LEN + 1, 0, DCONN_IP_ADDR_LEN + 1);
    if (err != EOK) {
        DCONN_ERROR("memset_s failed.");
        return DC_ERROR;
    }
    uint32_t ipAddrIdx = 0;
    while ((*idx) != strlen(ipAry) + 1) {
        char chr = ipAry[*idx];
        if (ipAddrIdx > DCONN_IP_ADDR_LEN + 1) {
            return DC_ERR_IP_FORMAT;
        }

        if (chr == ',' || chr == '\0') {
            // IP 地址需以 "," 或 "\0" 结束
            ipAddr[ipAddrIdx] = '\0';
            (*idx)++;
            *hasNextIpAddr = (chr != '\0');  // 最后一个 IP 地址
            return DC_SUCCESS;
        }

        ipAddr[ipAddrIdx] = chr;
        (*idx)++;
        ipAddrIdx++;
    }

    return DC_ERR_IP_FORMAT;
}

static uint32_t IpAddrToInt(int addressFamily, const char *ipAddr, uint32_t *addr)
{
    uint32_t ipValue = DconnIpTransToInt(ipAddr);
    unsigned char *ipAddress = (unsigned char *)&ipValue;
    if (addressFamily == AF_INET) {
        if (inet_pton(AF_INET, ipAddr, (void *)addr) == 0) {
            DCONN_ERROR("Invalid IPv4 address: %" IPV4_ANONYMOUS_PLACEHOLDER, ipAddress[3], ipAddress[0]);
            return DC_ERR_IP_FORMAT;
        }
    } else {
        if (inet_pton(AF_INET6, ipAddr, (void *)addr) == 0) {
            DCONN_ERROR("Invalid IPv6 address: %" IPV6_ANONYMOUS_PLACEHOLDER, ipAddress[0], ipAddress[0]);
            return DC_ERR_IP_FORMAT;
        }
    }
    return DC_SUCCESS;
}

static uint32_t ConnectServer(int addressFamily, const char *ipAddr, uint32_t addr, int *fd)
{
    (void)ipAddr;
    uint32_t ipValue = DconnIpTransToInt(ipAddr);
    unsigned char *ipAddress = (unsigned char *)&ipValue;
    (void)ipAddress;
    DCONN_DEBUG("Connect server begin: %" IPV4_ANONYMOUS_PLACEHOLDER, ipAddress[3], ipAddress[0]);

    *fd = socket(addressFamily, SOCK_STREAM, 0);
    if (*fd == -1) {
        DCONN_ERROR("Client socket init error: %d", errno);
        return DC_ERR_IP_FORMAT;
    }

    struct sockaddr_in servAddr;
    struct sockaddr_in6 servAddr6;
    int result = 0;
    errno_t err = memset_s(&servAddr, sizeof(servAddr), 0, sizeof(servAddr));
    if (err != EOK) {
        DCONN_ERROR("memset_s failed.");
        return DC_ERROR;
    }
    if (addressFamily == AF_INET) {
        servAddr.sin_family = AF_INET;
        servAddr.sin_addr.s_addr = addr;
        servAddr.sin_port = htons(DCONN_SERVICE_PORT);
        result = connect(*fd, (struct sockaddr *)&servAddr, sizeof(struct sockaddr_in));
    } else {
        servAddr6.sin6_family = AF_INET6;
        servAddr.sin_addr.s_addr = addr;
        servAddr6.sin6_port = htons(DCONN_SERVICE_PORT);
        result = connect(*fd, (struct sockaddr *)&servAddr6, sizeof(struct sockaddr_in6));
    }

    if (result == -1) {
        // 若服务端 accept 得到的 IP 地址不合法，则会直接断开连接
        DCONN_ERROR("Client connect to server error: %d", errno);
        return DC_ERR_CONNECTION_REFUSED;
    }

    DCONN_DEBUG("Server connected: %" IPV4_ANONYMOUS_PLACEHOLDER, ipAddress[3], ipAddress[0]);
    return DC_SUCCESS;
}

static uint32_t BuildDeviceIdMessage(char **deviceIdMessage)
{
    DConnDeviceUdId deviceUdId;
    uint32_t ret = DConnGetDeviceUdid(&deviceUdId);
    if (ret != 0) {
        DCONN_ERROR("Cannot get dev udid: %" PRIu32, ret);
        return ret;
    }

    cJSON *payloadJson = cJSON_CreateObject();
    if (payloadJson == NULL) {
        DCONN_ERROR("Failed to allocate payloadJson memory.");
        return DC_ERR_ALLOC_MEMORY;
    }
    DConnAddStringToJson(payloadJson, "deviceId", deviceUdId.deviceId);
    DConnAddStringToJson(payloadJson, "udid", deviceUdId.udid);
    *deviceIdMessage = cJSON_PrintUnformatted(payloadJson);
    if (*deviceIdMessage == NULL) {
        DCONN_ERROR("Failed to pack payloadJson to string.");
        cJSON_Delete(payloadJson);
        return DC_ERR_INVALID_JSON;
    }
    cJSON_Delete(payloadJson);

    DCONN_DEBUG("Build deviceIdMessage succeed.");
    return DC_SUCCESS;
}

static uint32_t ClientSendDeviceId(int32_t peerIdx)
{
    char *deviceIdMessage;
    uint32_t ret = BuildDeviceIdMessage(&deviceIdMessage);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Build device ID message failed: %" PRIu32, ret);
        return ret;
    }

    DConnMsgIn in = {
        .data = (const uint8_t *)deviceIdMessage,
        .dataLen = strlen(deviceIdMessage),
    };
    DConnMsgOut out;
    ret = DConnEncMsg(DCONN_MSG_TYPE_DEVICE_ID, NULL, &in, &out);
    cJSON_free(deviceIdMessage);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Encode device ID message failed: %" PRIu32, ret);
        return ret;
    }

    ret = SendDataToPeer(peerIdx, out.msg, out.msgLen);
    free(out.msg);
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Send device ID message failed: %" PRIu32, ret);
        return ret;
    }

    return DC_SUCCESS;
}

static bool GetAuthProcess(int32_t peerIdx, uint32_t authStat)
{
    DConnDataParam param;
    GetSessionKeyByPeerIdx(peerIdx, &param.key, &param.keyLen);
    bool isAuthFinished = authStat & PEER_AUTH_STAT_END_MASK;

    return !isAuthFinished || (param.keyLen == 0);
}

static uint32_t WaitForAuthStat(int32_t peerIdx)
{
    time_t start = time(NULL);
    DCONN_DEBUG("Client auth start at: %lld", start);
    bool inAuthProcess = true;
    bool isTimeout = false;
    uint32_t stat;

    do {
        stat = GetAuthStatByPeerDataIdx(peerIdx);
        inAuthProcess = GetAuthProcess(peerIdx, stat);
        isTimeout = (time(NULL) - start) > HICHAIN_AUTH_TIMEOUT;
        DConnSchedYield();
    } while (inAuthProcess && !isTimeout);

    DCONN_DEBUG("Client auth wait end: isAuthProcess: %s, isTimeout: %s", inAuthProcess ? "true" : "false",
                isTimeout ? "true" : "false");
    return stat;
}

static uint32_t InitOneClient(int addressFamily, const char *ipAddr, uint32_t addr)
{
    int32_t peerIdx = GetNewPeerDataIdx(PEER_ROLE_SERVICE);
    if (peerIdx == -1) {
        return DC_ERR_PEER_NUM_EXCEED;
    }

    int *socket = GetSocketByPeerDataIdx(peerIdx);
    uint32_t ret = ConnectServer(addressFamily, ipAddr, addr, socket);
    if (ret != DC_SUCCESS) {
        ReleasePeerDataIdx(peerIdx);
        return ret;
    }

    SetAuthStatByPeerDataIdx(peerIdx, PEER_AUTH_EXPECT_DEVICE_ID);
    ret = ClientSendDeviceId(peerIdx);
    if (ret != DC_SUCCESS) {
        ReleasePeerDataIdx(peerIdx);
        return ret;
    }

    uint32_t stat = WaitForAuthStat(peerIdx);
    if (stat != PEER_AUTH_STAT_AUTHORIZED) {
        // Async processes should set the peer auth stat to let client release peer data.
        ReleasePeerDataIdx(peerIdx);
        uint32_t ipValue = DconnIpTransToInt(ipAddr);
        unsigned char *ipAddress = (unsigned char *)&ipValue;
        DCONN_ERROR("Client init failed: %" IPV4_ANONYMOUS_PLACEHOLDER ", in stat: %" PRIu32, ipAddress[3],
                    ipAddress[0], stat);
        return DC_ERR_AUTH_FAILED;
    }

    SetIpAddrByIdx(peerIdx, addr);
    return DC_SUCCESS;
}

uint32_t DConnInitClient(const char *ipAry, char *errorIp)
{
    uint32_t ret = CreateDeviceAuthGroup();
    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Create device auth group failed: %" PRIu32, ret);
        return ret;
    }

    char ipAddr[DCONN_IP_ADDR_LEN + 1] = {0};
    uint32_t ipIdx = 0;
    uint32_t addr = 0;
    int addressFamily = AF_INET;
    bool hasNextIpAddr = false;
    do {
        ret = GetNextIpAddr(ipAry, &ipIdx, ipAddr, &hasNextIpAddr);
        if (ret != DC_SUCCESS) {
            DCONN_ERROR("Next IP address [%" PRId32 "] parse failed: %" PRIu32, ipIdx, ret);
            break;
        }

        uint32_t ipValue = DconnIpTransToInt(ipAddr);
        unsigned char *ipAddress = (unsigned char *)&ipValue;
        if (!IS_IPV4_ADDRESS(ipAddr)) {
            addressFamily = AF_INET6;
        }
        ret = IpAddrToInt(addressFamily, ipAddr, &addr);
        if (ret != DC_SUCCESS) {
            DCONN_ERROR("IP address [%" IPV4_ANONYMOUS_PLACEHOLDER "] Converting failed: %" PRIu32, ipAddress[3],
                        ipAddress[0], ret);
            break;
        }

        ret = HasPeerIpAddr(addr);
        if (ret == DC_SUCCESS) {
            continue;
        }

        DCONN_DEBUG("Init next client: %" IPV4_ANONYMOUS_PLACEHOLDER, ipAddress[3], ipAddress[0]);
        ret = InitOneClient(addressFamily, ipAddr, addr);
        if (ret != DC_SUCCESS) {
            DCONN_ERROR("Init one client [%" PRId32 "] failed: %" PRIu32, ipIdx, ret);
            break;
        }

        DCONN_INFO("Client [%" PRId32 "] init succeed.", ipIdx);
    } while (hasNextIpAddr);

    if (ret != DC_SUCCESS) {
        DCONN_ERROR("Client init failed: %" PRIu32, ret);
        if (strcpy_s(errorIp, DCONN_IP_ADDR_LEN + 1, ipAddr) != EOK) {
            DCONN_ERROR("strcpy_s failed.");
        }
        return ret;
    }

    DCONN_INFO("All client init succeed");
    return DC_SUCCESS;
}
