/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef APPLICATIONS_APP_DCONNECTCASEONE_CLIENT_INCLUE_CLIENT
#define APPLICATIONS_APP_DCONNECTCASEONE_CLIENT_INCLUE_CLIENT

#include <stdint.h>

/**
 * @brief 初始化客户端。按 ipAry 列表顺序初始化，遇到第一个失败的则停止。
 *
 * @param ipAry IP地址列表，以半角逗号","分隔
 * @param errorIp 第一个出错的 IP 地址
 * @return uint32_t 错误码
 */
uint32_t DConnInitClient(const char *ipAry, char *errorIp);

#endif  // APPLICATIONS_APP_DCONNECTCASEONE_CLIENT_INCLUE_CLIENT