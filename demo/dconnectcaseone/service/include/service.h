/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef APPLICATIONS_APP_DCONNECTCASEONE_SERVICE_INCLUE_SERVICE
#define APPLICATIONS_APP_DCONNECTCASEONE_SERVICE_INCLUE_SERVICE

#include <stdint.h>

/**
 * @brief 初始化服务端
 *
 * @details 服务端初始化到启动监听阶段即会返回。
 *          第一次初始化服务端后会创建线程接收客户端的认证，或处理已认证的客户端的消息。
 *          此方法可重入。重复调用时若与前一次的 IP 地址一致则不处理，不一致则会关闭前一次的连接，重新创建服务端。
 * @param ipAry 服务端 IP 地址。一个 IPv4 或 IPv6 地址字符串
 * @return int 初始化结果
 */
uint32_t DConnInitService(const char *ipAry);

#endif  // APPLICATIONS_APP_DCONNECTCASEONE_SERVICE_INCLUE_SERVICE