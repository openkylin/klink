/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "service.h"

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <securec.h>

#include "dconn_data.h"
#include "dconncaseone_interface.h"
#include "dconn_log.h"
#include "dconn_peer.h"

#define DCONN_LISTEN_QUEUE_LEN 10

static uint32_t BindSocket(int addressFamily, const char *ipAry, int *fd)
{
    struct sockaddr_in servAddr;
    struct sockaddr_in6 servAddr6;

    uint32_t ipValue = DconnIpTransToInt(ipAry);
    unsigned char *ipAddress = (unsigned char *)&ipValue;
    int result = -1;
    if (addressFamily == AF_INET) {
        errno_t err = memset_s(&servAddr, sizeof(servAddr), 0, sizeof(servAddr));
        if (err != EOK) {
            DCONN_ERROR("memset_s failed.");
            return DC_ERROR;
        }
        servAddr.sin_family = AF_INET;
        if (inet_pton(AF_INET, ipAry, (void *)&servAddr.sin_addr) == 0) {
            DCONN_ERROR("Invalid IPv4 address: %" IPV4_ANONYMOUS_PLACEHOLDER, ipAddress[3], ipAddress[0]);
            return DC_ERR_IP_FORMAT;
        }
        servAddr.sin_port = htons(DCONN_SERVICE_PORT);
        result = bind(*fd, (struct sockaddr *)&servAddr, sizeof(struct sockaddr_in));
    } else {
        servAddr6.sin6_family = AF_INET6;
        if (inet_pton(AF_INET6, ipAry, (void *)&servAddr6.sin6_addr) == 0) {
            DCONN_ERROR("Invalid IPv6 address: %" IPV6_ANONYMOUS_PLACEHOLDER, ipAddress[0], ipAddress[0]);
            return DC_ERR_IP_FORMAT;
        }
        servAddr6.sin6_port = htons(DCONN_SERVICE_PORT);
        result = bind(*fd, (struct sockaddr *)&servAddr6, sizeof(struct sockaddr_in6));
    }

    if (result < 0) {
        DCONN_ERROR("Socket bind error: %d", errno);
        return DC_ERR_BIND_PORT;
    }

    result = listen(*fd, DCONN_LISTEN_QUEUE_LEN);
    if (result < 0) {
        DCONN_ERROR("Socket listen error: %d", errno);
        return DC_ERR_CONNECTION_REFUSED;
    }

    DCONN_INFO("Socket bind succeed.");
    return DC_SUCCESS;
}

uint32_t DConnInitService(const char *ipAry)
{
    uint32_t ipValue = DconnIpTransToInt(ipAry);
    unsigned char *ipAddress = (unsigned char *)&ipValue;
    (void)ipAddress;
    DCONN_DEBUG("Init service at TCP: %" IPV4_ANONYMOUS_PLACEHOLDER ":%d", ipAddress[3], ipAddress[0],
                DCONN_SERVICE_PORT);

    int addressFamily = AF_INET;
    if (strstr(ipAry, ".") == NULL) {
        addressFamily = AF_INET6;
        DCONN_INFO("IPv6 address detected.");
    }
    *GetServerSocketAddrFamily() = addressFamily;

    uint32_t ret = HasServerIpAddr();
    if (ret == DC_SUCCESS) {
        DCONN_DEBUG("Ip address is already exists");
        return DC_SUCCESS;
    }

    int fd = socket(addressFamily, SOCK_STREAM, 0);
    if (fd == -1) {
        DCONN_ERROR("Socket init error: %d", errno);
        return DC_ERR_CONNECTION_REFUSED;
    }

    ret = BindSocket(addressFamily, ipAry, &fd);
    if (ret != DC_SUCCESS) {
        close(fd);
        return ret;
    }
    SetServerSocket(fd);

    return DC_SUCCESS;
}
