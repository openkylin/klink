/*
 * Copyright (C) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <securec.h>

#include "dconncaseone_interface.h"

static uint32_t g_initType;
static char g_deviceId[64] = {0};

void SendDataToDeviceCb(const char *deviceId, uint32_t result)
{
    printf("Data sent to [%s] result: %u\n", deviceId, result);
}

void ReceiveDataFromDeviceCb(const char *deviceId, const char *receiveData, uint32_t datelen)
{
    printf("Data received from device [%s]. Datalen: %u, Data: %s\n", deviceId, datelen, receiveData);
}

bool IsValidIPCb(const char *ip)
{
    printf("IsValidIPCb: %s\n", ip);
    return true;
}

bool IsValidDeviceIDCb(const char *deviceID)
{
    printf("IsValidDeviceIDCb: %s\n", deviceID);
    return true;
}

char *GetAuthKeyCb(const char *targetDeviceID)
{
    printf("GetAuthKeyCb: %s\n", targetDeviceID);
    static char authKey[] = "123456";
    return authKey;
}

char *GetDeviceIDCb(void)
{
    return g_deviceId;
}

void SendDataLoop()
{
    while (1) {
        int shouldClose;
        printf("Do you want to close dconnect case one? 1 for yes, 0 for no: \n");
        if (scanf_s("%d", &shouldClose) <= 0) {
            printf("Input parameter error.\n");
            continue;
        }
        if (shouldClose == 1) {
            printf("Closing...\n");
            CloseDConnCaseOne();
            printf("Closed.\n");
            return;
        }

        char targetDeviceId[64];
        char data[64];
        printf("Please input device ID you want to send: \n");
        if (scanf_s("%s", targetDeviceId, 64) <= 0) {
            printf("Input parameter error.\n");
            continue;
        }
        printf("Please input data you want to send: \n");
        if (scanf_s("%s", data, 64) <= 0) {
            printf("Input parameter error.\n");
            continue;
        }

        uint32_t datalen = strlen(data) + 1;
        uint32_t result = DConnSendData(targetDeviceId, data, datalen);
        printf("Send %u data result: %u\n", datalen, result);
    }
}

int main(void)
{
    CallbackParam param = {
        .sendDataResultCb = SendDataToDeviceCb,
        .onReceiveDataCb = ReceiveDataFromDeviceCb,
        .isValidIP = IsValidIPCb,
        .isValidDeviceID = IsValidDeviceIDCb,
        .getAuthKey = GetAuthKeyCb,
        .getDeviceID = GetDeviceIDCb,
    };
    RegisterCallback(&param);

    printf("Distributed Connect Case One Ver. %s \n", GetDConnVersion());
    printf("Please input init type. 0 for service, 1 for client: \n");
    char ipAry[64];
    char errorIp[64];

    if (scanf_s("%u", &g_initType) <= 0) {
        printf("Input parameter error.\n");
        return 0;
    }
    if (g_initType == INIT_SERVICE) {
        printf("init as service \n");
    } else if (g_initType == INIT_CLIENT) {
        printf("init as client \n");
    } else {
        return 0;
    }

    printf("Please input device ID: \n");
    if (scanf_s("%s", g_deviceId, 64) <= 0) {
        printf("Input parameter error.\n");
        return 0;
    }

    printf("Please input IP address: \n");
    if (scanf_s("%s", ipAry, 64) <= 0) {
        printf("Input parameter error.\n");
        return 0;
    }
    uint32_t ret = InitDConnCaseOne(g_initType, ipAry, errorIp);
    printf("Init ret: %u\n", ret);
    if (ret != DC_SUCCESS) {
        return 0;
    }

    SendDataLoop();
    return 0;
}