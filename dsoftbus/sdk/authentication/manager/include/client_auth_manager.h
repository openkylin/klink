/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CLIENT_AUTH_MANAGER_H
#define CLIENT_AUTH_MANAGER_H

#include "softbus_auth.h"
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

int AuthClientInit(void);
void AuthClientDeInit(void);

void AuthOnFinish(int64_t requestId, int operationCode, const char *returnData);
void AuthOnError(int64_t requestId, int operationCode, int errorCode, const char *errorReturn);
char *AuthOnRequest(int64_t requestId, int operationCode, const char *reqParams);

#ifdef __cplusplus
}
#endif
#endif