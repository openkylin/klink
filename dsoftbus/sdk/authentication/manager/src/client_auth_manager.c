/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "client_auth_manager.h"

#include <securec.h>
#include <string.h>
#include "auth_server_proxy.h"
#include "softbus_client_frame_manager.h"
#include "softbus_def.h"
#include "softbus_errcode.h"
#include "softbus_log.h"
#include "softbus_type_def.h"
#include "stdbool.h"

#define MAX_AUTH_APPID_LEN 128

static DeviceAuthCallback *g_deviceAuthCallback = NULL;

int AuthClientInit(void)
{
    if (AuthServerProxyInit() != SOFTBUS_OK) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "disc server proxy init failed.");
        return SOFTBUS_ERR;
    }
    SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_INFO, "Init success as auth");
    return SOFTBUS_OK;
}

void AuthClientDeInit(void)
{
    if (g_deviceAuthCallback == NULL) {
        return;
    }
    g_deviceAuthCallback = NULL;
    AuthServerProxyDeInit();
    SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_INFO, "DeInit success");
}

int32_t SdkRegAuthCallback(const char *packageName, const char *appId, DeviceAuthCallback *callback)
{
    if ((packageName == NULL) || (strlen(packageName) >= PKG_NAME_SIZE_MAX) || (appId == NULL) ||
        (strlen(appId) >= MAX_AUTH_APPID_LEN) || (callback == NULL)) {
        return SOFTBUS_INVALID_PARAM;
    }

    if (InitSoftBus(packageName, true) != SOFTBUS_OK) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "init softbus err");
        return SOFTBUS_DISCOVER_NOT_INIT;
    }
    if (CheckPackageName(packageName) != SOFTBUS_OK) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "check packageName failed");
        return SOFTBUS_INVALID_PARAM;
    }

    g_deviceAuthCallback = callback;
    int32_t ret = ServerIpcRegAuthInfo(packageName, appId);
    if (ret != SOFTBUS_OK) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "Server RegAuthCallback failed, ret = %d", ret);
        return ret;
    }

    return SOFTBUS_OK;
}

int32_t SdkCreateGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *createParams)
{
    int32_t ret = ServerIpcCreateGroup(osAccountId, requestId, appId, createParams);
    if (ret != SOFTBUS_OK) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "Server CreateGroup failed, ret = %d", ret);
        return ret;
    }

    return SOFTBUS_OK;
}

int32_t SdkGetRegisterInfo(const char *reqJsonStr, char **returnRegisterInfo)
{
    int32_t ret = ServerIpcGetRegisterInfo(reqJsonStr, returnRegisterInfo);
    if (ret != SOFTBUS_OK) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "Server GetRegisterInfo failed, ret = %d", ret);
        return ret;
    }

    return SOFTBUS_OK;
}

int32_t SdkDestroyInfo(char **returnInfo)
{
    int32_t ret = ServerIpcDestroyInfo(returnInfo);
    if (ret != SOFTBUS_OK) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "Server DestroyInfo failed, ret = %d", ret);
        return ret;
    }

    return SOFTBUS_OK;
}

int32_t SdkAddMultiMembersToGroup(int32_t osAccountId, const char *appId, const char *addParams)
{
    int32_t ret = ServerIpcAddMultiMembersToGroup(osAccountId, appId, addParams);
    if (ret != SOFTBUS_OK) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "Server AddMultiMembersToGroup failed, ret = %d", ret);
        return ret;
    }

    return SOFTBUS_OK;
}

int32_t SdkDelMultiMembersFromGroup(int32_t osAccountId, const char *appId, const char *deleteParams)
{
    int32_t ret = ServerIpcDelMultiMembersFromGroup(osAccountId, appId, deleteParams);
    if (ret != SOFTBUS_OK) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "Server DelMultiMembersFromGroup failed, ret = %d", ret);
        return ret;
    }

    return SOFTBUS_OK;
}

int32_t SdkGetTrustedDevices(int32_t osAccountId, const char *appId, const char *groupId, char **returnDevInfoVec,
                             uint32_t *deviceNum)
{
    int32_t ret = ServerIpcGetTrustedDevices(osAccountId, appId, groupId, returnDevInfoVec, deviceNum);
    if (ret != SOFTBUS_OK) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "Server GetTrustedDevices failed, ret = %d", ret);
        return ret;
    }

    return SOFTBUS_OK;
}

bool SdkIsDeviceInGroup(int32_t osAccountId, const char *appId, const char *groupId, const char *deviceId)
{
    return ServerIpcIsDeviceInGroup(osAccountId, appId, groupId, deviceId);
}

int32_t SdkUnRegAuthInfo(const char *appId)
{
    return ServerIpcUnRegAuthInfo(appId);
}

int32_t SdkDeleteGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *disbandParams)
{
    return ServerIpcDeleteGroup(osAccountId, requestId, appId, disbandParams);
}

int32_t SdkAddMemberToGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *addParams)
{
    return ServerIpcAddMemberToGroup(osAccountId, requestId, appId, addParams);
}

int32_t SdkDeleteMemberFromGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *deleteParams)
{
    return ServerIpcDeleteMemberFromGroup(osAccountId, requestId, appId, deleteParams);
}

int32_t SdkCheckAccessToGroup(int32_t osAccountId, const char *appId, const char *groupId)
{
    return ServerIpcCheckAccessToGroup(osAccountId, appId, groupId);
}

int32_t SdkGetPkInfoList(int32_t osAccountId, const char *appId, const char *queryParams, char **returnInfoList,
                         uint32_t *returnInfoNum)
{
    return ServerIpcGetPkInfoList(osAccountId, appId, queryParams, returnInfoList, returnInfoNum);
}

int32_t SdkGetGroupInfoById(int32_t osAccountId, const char *appId, const char *groupId, char **returnGroupInfo)
{
    return ServerIpcGetGroupInfoById(osAccountId, appId, groupId, returnGroupInfo);
}

int32_t SdkGetGroupInfo(int32_t osAccountId, const char *appId, const char *queryParams, char **returnGroupVec,
                        uint32_t *groupNum)
{
    return ServerIpcGetGroupInfo(osAccountId, appId, queryParams, returnGroupVec, groupNum);
}

int32_t SdkGetJoinedGroups(int32_t osAccountId, const char *appId, int groupType, char **returnGroupVec,
                           uint32_t *groupNum)
{
    return ServerIpcGetJoinedGroups(osAccountId, appId, groupType, returnGroupVec, groupNum);
}

int32_t SdkGetRelatedGroups(int32_t osAccountId, const char *appId, const char *peerDeviceId, char **returnGroupVec,
                            uint32_t *groupNum)
{
    return ServerIpcGetRelatedGroups(osAccountId, appId, peerDeviceId, returnGroupVec, groupNum);
}

int32_t SdkGetAuthDeviceInfoById(int32_t osAccountId, const char *appId, const char *deviceId, const char *groupId,
                                 char **returnDeviceInfo)
{
    return ServerIpcGetAuthDeviceInfoById(osAccountId, appId, deviceId, groupId, returnDeviceInfo);
}

void AuthOnFinish(int64_t requestId, int operationCode, const char *returnData)
{
    SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_INFO, "Sdk AuthOnFinish, requestId = %lld, operationCode = %d", requestId,
               operationCode);
    if (g_deviceAuthCallback != NULL && g_deviceAuthCallback->onFinish != NULL) {
        g_deviceAuthCallback->onFinish(requestId, operationCode, returnData);
    }
}

void AuthOnError(int64_t requestId, int operationCode, int errorCode, const char *errorReturn)
{
    SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_INFO,
               "Sdk AuthOnError, requestId = %lld, operationCode = %d, errorCode = %d", requestId, operationCode,
               errorCode);
    if (g_deviceAuthCallback != NULL && g_deviceAuthCallback->onError != NULL) {
        g_deviceAuthCallback->onError(requestId, operationCode, errorCode, errorReturn);
    }
}

char *AuthOnRequest(int64_t requestId, int operationCode, const char *reqParams)
{
    SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_INFO, "Sdk AuthOnRequest, requestId = %lld, operationCode = %d", requestId,
               operationCode);
    if (g_deviceAuthCallback != NULL && g_deviceAuthCallback->onRequest != NULL) {
        return g_deviceAuthCallback->onRequest(requestId, operationCode, reqParams);
    }
    return NULL;
}
