/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AUTH_SERVER_PROXY_H
#define AUTH_SERVER_PROXY_H

#include <stdint.h>

#include "softbus_auth.h"

#ifdef __cplusplus
#    if __cplusplus
extern "C" {
#    endif
#endif

int32_t AuthServerProxyInit(void);
void AuthServerProxyDeInit(void);

int32_t ServerIpcRegAuthInfo(const char *pkgName, const char *appId);
int32_t ServerIpcCreateGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *createParams);
int32_t ServerIpcGetRegisterInfo(const char *reqJsonStr, char **returnRegisterInfo);
int32_t ServerIpcDestroyInfo(char **returnInfo);
int32_t ServerIpcAddMultiMembersToGroup(int32_t osAccountId, const char *appId, const char *addParams);
int32_t ServerIpcDelMultiMembersFromGroup(int32_t osAccountId, const char *appId, const char *deleteParams);
int32_t ServerIpcGetTrustedDevices(int32_t osAccountId, const char *appId, const char *groupId, char **returnDevInfoVec,
                                   uint32_t *deviceNum);
bool ServerIpcIsDeviceInGroup(int32_t osAccountId, const char *appId, const char *groupId, const char *deviceId);
int32_t ServerIpcUnRegAuthInfo(const char *appId);
int32_t ServerIpcDeleteGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *disbandParams);
int32_t ServerIpcAddMemberToGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *addParams);
int32_t ServerIpcDeleteMemberFromGroup(int32_t osAccountId, int64_t requestId, const char *appId,
                                       const char *deleteParams);
int32_t ServerIpcCheckAccessToGroup(int32_t osAccountId, const char *appId, const char *groupId);
int32_t ServerIpcGetPkInfoList(int32_t osAccountId, const char *appId, const char *queryParams, char **returnInfoList,
                               uint32_t *returnInfoNum);
int32_t ServerIpcGetGroupInfoById(int32_t osAccountId, const char *appId, const char *groupId, char **returnGroupInfo);
int32_t ServerIpcGetGroupInfo(int32_t osAccountId, const char *appId, const char *queryParams, char **returnGroupVec,
                              uint32_t *groupNum);
int32_t ServerIpcGetJoinedGroups(int32_t osAccountId, const char *appId, int groupType, char **returnGroupVec,
                                 uint32_t *groupNum);
int32_t ServerIpcGetRelatedGroups(int32_t osAccountId, const char *appId, const char *peerDeviceId,
                                  char **returnGroupVec, uint32_t *groupNum);
int32_t ServerIpcGetAuthDeviceInfoById(int32_t osAccountId, const char *appId, const char *deviceId,
                                       const char *groupId, char **returnDeviceInfo);

#ifdef __cplusplus
#    if __cplusplus
}
#    endif /* __cplusplus */
#endif     /* __cplusplus */

#endif  // !AUTH_SERVER_PROXY_H
