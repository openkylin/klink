/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "auth_server_proxy.h"
#include "auth_server_proxy_standard.h"

#include <mutex>
#include "ipc_skeleton.h"
#include "iremote_broker.h"
#include "iremote_object.h"
#include "iremote_proxy.h"
#include "softbus_errcode.h"
#include "softbus_server_ipc_interface_code.h"
#include "softbus_log.h"

using namespace OHOS;

namespace {
sptr<AuthServerProxy> g_serverProxy = nullptr;
uint32_t g_getSystemAbilityId = 2;
const std::u16string SAMANAGER_INTERFACE_TOKEN = u"ohos.samgr.accessToken";
std::mutex g_mutex;
}  // namespace

static sptr<IRemoteObject> GetSystemAbility()
{
    MessageParcel data;

    if (!data.WriteInterfaceToken(SAMANAGER_INTERFACE_TOKEN)) {
        return nullptr;
    }

    data.WriteInt32(SOFTBUS_SERVER_SA_ID_INNER);
    MessageParcel reply;
    MessageOption option;
    sptr<IRemoteObject> samgr = IPCSkeleton::GetContextObject();
    int32_t err = samgr->SendRequest(g_getSystemAbilityId, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "Get GetSystemAbility failed!\n");
        return nullptr;
    }
    return reply.ReadRemoteObject();
}

int32_t AuthServerProxyInit(void)
{
    std::lock_guard<std::mutex> lock(g_mutex);
    sptr<IRemoteObject> object = GetSystemAbility();
    if (object == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "Get remote softbus object failed!\n");
        return SOFTBUS_ERR;
    }
    g_serverProxy = new (std::nothrow) AuthServerProxy(object);
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "Create bus center server proxy failed!\n");
        return SOFTBUS_SERVER_NOT_INIT;
    }
    return SOFTBUS_OK;
}

void AuthServerProxyDeInit(void)
{
    delete g_serverProxy;
    g_serverProxy = nullptr;
}

int32_t ServerIpcRegAuthInfo(const char *pkgName, const char *appId)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    pid_t pid = OHOS::IPCSkeleton::GetCallingPid();
    return g_serverProxy->RegAuthInfo(pkgName, appId, (int32_t)pid);
}

int32_t ServerIpcCreateGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *createParams)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    return g_serverProxy->CreateGroup(osAccountId, requestId, appId, createParams);
}

int32_t ServerIpcGetRegisterInfo(const char *reqJsonStr, char **returnRegisterInfo)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    return g_serverProxy->GetRegisterInfo(reqJsonStr, returnRegisterInfo);
}

int32_t ServerIpcDestroyInfo(char **returnInfo)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    return g_serverProxy->DestroyInfo(returnInfo);
}

int32_t ServerIpcAddMultiMembersToGroup(int32_t osAccountId, const char *appId, const char *addParams)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    return g_serverProxy->AddMultiMembersToGroup(osAccountId, appId, addParams);
}

int32_t ServerIpcDelMultiMembersFromGroup(int32_t osAccountId, const char *appId, const char *deleteParams)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    return g_serverProxy->DelMultiMembersFromGroup(osAccountId, appId, deleteParams);
}

int32_t ServerIpcGetTrustedDevices(int32_t osAccountId, const char *appId, const char *groupId, char **returnDevInfoVec,
                                   uint32_t *deviceNum)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    return g_serverProxy->GetTrustedDevices(osAccountId, appId, groupId, returnDevInfoVec, deviceNum);
}

bool ServerIpcIsDeviceInGroup(int32_t osAccountId, const char *appId, const char *groupId, const char *deviceId)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return false;
    }
    return g_serverProxy->IsDeviceInGroup(osAccountId, appId, groupId, deviceId);
}

int32_t ServerIpcUnRegAuthInfo(const char *appId)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    return g_serverProxy->UnRegAuthInfo(appId);
}

int32_t ServerIpcDeleteGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *disbandParams)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    return g_serverProxy->DeleteGroup(osAccountId, requestId, appId, disbandParams);
}

int32_t ServerIpcAddMemberToGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *addParams)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    return g_serverProxy->AddMemberToGroup(osAccountId, requestId, appId, addParams);
}

int32_t ServerIpcDeleteMemberFromGroup(int32_t osAccountId, int64_t requestId, const char *appId,
                                       const char *deleteParams)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    return g_serverProxy->DeleteMemberFromGroup(osAccountId, requestId, appId, deleteParams);
}

int32_t ServerIpcCheckAccessToGroup(int32_t osAccountId, const char *appId, const char *groupId)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    return g_serverProxy->CheckAccessToGroup(osAccountId, appId, groupId);
}

int32_t ServerIpcGetPkInfoList(int32_t osAccountId, const char *appId, const char *queryParams, char **returnInfoList,
                               uint32_t *returnInfoNum)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    return g_serverProxy->GetPkInfoList(osAccountId, appId, queryParams, returnInfoList, returnInfoNum);
}

int32_t ServerIpcGetGroupInfoById(int32_t osAccountId, const char *appId, const char *groupId, char **returnGroupInfo)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    return g_serverProxy->GetGroupInfoById(osAccountId, appId, groupId, returnGroupInfo);
}

int32_t ServerIpcGetGroupInfo(int32_t osAccountId, const char *appId, const char *queryParams, char **returnGroupVec,
                              uint32_t *groupNum)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    return g_serverProxy->GetGroupInfo(osAccountId, appId, queryParams, returnGroupVec, groupNum);
}

int32_t ServerIpcGetJoinedGroups(int32_t osAccountId, const char *appId, int groupType, char **returnGroupVec,
                                 uint32_t *groupNum)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    return g_serverProxy->GetJoinedGroups(osAccountId, appId, groupType, returnGroupVec, groupNum);
}

int32_t ServerIpcGetRelatedGroups(int32_t osAccountId, const char *appId, const char *peerDeviceId,
                                  char **returnGroupVec, uint32_t *groupNum)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    return g_serverProxy->GetRelatedGroups(osAccountId, appId, peerDeviceId, returnGroupVec, groupNum);
}

int32_t ServerIpcGetAuthDeviceInfoById(int32_t osAccountId, const char *appId, const char *deviceId,
                                       const char *groupId, char **returnDeviceInfo)
{
    if (g_serverProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "softbus server g_serverProxy is nullptr!\n");
        return SOFTBUS_ERR;
    }
    return g_serverProxy->GetAuthDeviceInfoById(osAccountId, appId, deviceId, groupId, returnDeviceInfo);
}
