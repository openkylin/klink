/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "auth_server_proxy_standard.h"

#include <securec.h>
#include "auth_server_proxy.h"
#include "discovery_service.h"
#include "ipc_skeleton.h"
#include "iremote_broker.h"
#include "iremote_object.h"
#include "iremote_proxy.h"
#include "message_parcel.h"
#include "softbus_adapter_mem.h"
#include "softbus_errcode.h"
#include "softbus_feature_config.h"
#include "softbus_server_ipc_interface_code.h"
#include "softbus_log.h"

namespace OHOS {
static uint32_t g_getSystemAbilityId = 2;
const std::u16string SAMANAGER_INTERFACE_TOKEN = u"ohos.samgr.accessToken";
static sptr<IRemoteObject> GetSystemAbility()
{
    MessageParcel data;

    if (!data.WriteInterfaceToken(SAMANAGER_INTERFACE_TOKEN)) {
        return nullptr;
    }

    data.WriteInt32(SOFTBUS_SERVER_SA_ID_INNER);
    MessageParcel reply;
    MessageOption option;
    sptr<IRemoteObject> samgr = IPCSkeleton::GetContextObject();
    int32_t err = samgr->SendRequest(g_getSystemAbilityId, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "Get GetSystemAbility failed!\n");
        return nullptr;
    }
    return reply.ReadRemoteObject();
}

int32_t AuthServerProxy::RegAuthInfo(const char *pkgName, const char *appId, int32_t pid)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "RegAuthInfo write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }
    data.WriteCString(pkgName);
    data.WriteCString(appId);
    data.WriteInt32(pid);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_REG_AUTHINFO, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "RegAuthInfo send request failed!");
        return SOFTBUS_ERR;
    }

    int32_t serverRet = 0;
    int32_t ret = reply.ReadInt32(serverRet);
    if (!ret) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "RegAuthInfo read serverRet failed!");
        return SOFTBUS_ERR;
    }
    return serverRet;
}

int32_t AuthServerProxy::CreateGroup(int32_t osAccountId, int64_t requestId, const char *appId,
                                     const char *createParams)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "CreateGroup write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }

    data.WriteInt32(osAccountId);
    data.WriteInt64(requestId);
    data.WriteCString(appId);
    data.WriteCString(createParams);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_CREATE_GROUP, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "CreateGroup send request failed!");
        return SOFTBUS_ERR;
    }

    int32_t serverRet = 0;
    int32_t ret = reply.ReadInt32(serverRet);
    if (!ret) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "CreateGroup read serverRet failed!");
        return SOFTBUS_ERR;
    }
    return serverRet;
}

int32_t AuthServerProxy::GetRegisterInfo(const char *reqJsonStr, char **returnRegisterInfo)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetRegisterInfo write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }

    data.WriteCString(reqJsonStr);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_GET_REGINFO, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetRegisterInfo send request failed!");
        return SOFTBUS_ERR;
    }
    const char *retData = reply.ReadCString();
    if (retData != nullptr) {
        *returnRegisterInfo = strdup(retData);
    }
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::DestroyInfo(char **returnInfo)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "DestroyInfo write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_DESTROY_INFO, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "DestroyInfo send request failed!");
        return SOFTBUS_ERR;
    }
    const char *retData = reply.ReadCString();
    if (retData != nullptr) {
        *returnInfo = strdup(retData);
    }
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::AddMultiMembersToGroup(int32_t osAccountId, const char *appId, const char *addParams)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "AddMultiMembersToGroup write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }
    data.WriteInt32(osAccountId);
    data.WriteCString(appId);
    data.WriteCString(addParams);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_ADD_MULTI_MEMBERS_GROUP, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "AddMultiMembersToGroup send request failed!");
        return SOFTBUS_ERR;
    }

    int32_t serverRet = 0;
    int32_t ret = reply.ReadInt32(serverRet);
    if (!ret) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "AddMultiMembersToGroup read serverRet failed!");
        return SOFTBUS_ERR;
    }
    return serverRet;
}

int32_t AuthServerProxy::DelMultiMembersFromGroup(int32_t osAccountId, const char *appId, const char *deleteParams)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "DelMultiMembersFromGroup write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }
    data.WriteInt32(osAccountId);
    data.WriteCString(appId);
    data.WriteCString(deleteParams);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_DEL_MULTI_MEMBERS_GROUP, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "DelMultiMembersFromGroup send request failed!");
        return SOFTBUS_ERR;
    }

    int32_t serverRet = 0;
    int32_t ret = reply.ReadInt32(serverRet);
    if (!ret) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "DelMultiMembersFromGroup read serverRet failed!");
        return SOFTBUS_ERR;
    }
    return serverRet;
}

int32_t AuthServerProxy::GetTrustedDevices(int32_t osAccountId, const char *appId, const char *groupId,
                                           char **returnDevInfoVec, uint32_t *deviceNum)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetTrustedDevices write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }
    data.WriteInt32(osAccountId);
    data.WriteCString(appId);
    data.WriteCString(groupId);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_GET_TRUSTED_DEVICES, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetTrustedDevices send request failed!");
        return SOFTBUS_ERR;
    }

    int32_t ret = 0;
    uint32_t devNum = 0;
    const char *retData = reply.ReadCString();
    if (retData != nullptr) {
        *returnDevInfoVec = strdup(retData);
    }
    if (*returnDevInfoVec == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetTrustedDevices read returnDevInfoVec failed!");
        return SOFTBUS_ERR;
    }
    ret = reply.ReadUint32(devNum);
    if (!ret) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetTrustedDevices read deviceNum failed!");
        return SOFTBUS_ERR;
    }
    *deviceNum = devNum;

    return SOFTBUS_OK;
}

bool AuthServerProxy::IsDeviceInGroup(int32_t osAccountId, const char *appId, const char *groupId, const char *deviceId)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return false;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "IsDeviceInGroup write InterfaceToken failed!");
        return false;
    }
    data.WriteInt32(osAccountId);
    data.WriteCString(appId);
    data.WriteCString(groupId);
    data.WriteCString(deviceId);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_IS_DEVICE_IN_GROUP, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "IsDeviceInGroup send request failed!");
        return false;
    }

    bool ret;
    ret = reply.ReadBool();

    return ret;
}

int32_t AuthServerProxy::UnRegAuthInfo(const char *appId)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "UnRegAuthInfo write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }
    data.WriteCString(appId);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_UNREG_AUTHINFO, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "UnRegAuthInfo send request failed!");
        return SOFTBUS_ERR;
    }

    int32_t serverRet = 0;
    int32_t ret = reply.ReadInt32(serverRet);
    if (!ret) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "UnRegAuthInfo read serverRet failed!");
        return SOFTBUS_ERR;
    }
    return serverRet;
}

int32_t AuthServerProxy::DeleteGroup(int32_t osAccountId, int64_t requestId, const char *appId,
                                     const char *disbandParams)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "DeleteGroup write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }
    data.WriteInt32(osAccountId);
    data.WriteInt64(requestId);
    data.WriteCString(appId);
    data.WriteCString(disbandParams);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_DELETE_GROUP, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "DeleteGroup send request failed!");
        return SOFTBUS_ERR;
    }

    int32_t serverRet = 0;
    int32_t ret = reply.ReadInt32(serverRet);
    if (!ret) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "DeleteGroup read serverRet failed!");
        return SOFTBUS_ERR;
    }
    return serverRet;
}

int32_t AuthServerProxy::AddMemberToGroup(int32_t osAccountId, int64_t requestId, const char *appId,
                                          const char *addParams)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "AddMemberToGroup write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }
    data.WriteInt32(osAccountId);
    data.WriteInt64(requestId);
    data.WriteCString(appId);
    data.WriteCString(addParams);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_ADD_MEMBERS_GROUP, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "AddMemberToGroup send request failed!");
        return SOFTBUS_ERR;
    }

    int32_t serverRet = 0;
    int32_t ret = reply.ReadInt32(serverRet);
    if (!ret) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "AddMemberToGroup read serverRet failed!");
        return SOFTBUS_ERR;
    }
    return serverRet;
}

int32_t AuthServerProxy::DeleteMemberFromGroup(int32_t osAccountId, int64_t requestId, const char *appId,
                                               const char *deleteParams)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "DeleteMemberFromGroup write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }
    data.WriteInt32(osAccountId);
    data.WriteInt64(requestId);
    data.WriteCString(appId);
    data.WriteCString(deleteParams);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_DEL_MEMBERS_GROUP, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "DeleteMemberFromGroup send request failed!");
        return SOFTBUS_ERR;
    }

    int32_t serverRet = 0;
    int32_t ret = reply.ReadInt32(serverRet);
    if (!ret) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "DeleteMemberFromGroup read serverRet failed!");
        return SOFTBUS_ERR;
    }
    return serverRet;
}

int32_t AuthServerProxy::CheckAccessToGroup(int32_t osAccountId, const char *appId, const char *groupId)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "CheckAccessToGroup write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }
    data.WriteInt32(osAccountId);
    data.WriteCString(appId);
    data.WriteCString(groupId);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_CHECK_ACCESS_GROUP, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "CheckAccessToGroup send request failed!");
        return SOFTBUS_ERR;
    }

    int32_t serverRet = 0;
    int32_t ret = reply.ReadInt32(serverRet);
    if (!ret) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "CheckAccessToGroup read serverRet failed!");
        return SOFTBUS_ERR;
    }
    return serverRet;
}

int32_t AuthServerProxy::GetPkInfoList(int32_t osAccountId, const char *appId, const char *queryParams,
                                       char **returnInfoList, uint32_t *returnInfoNum)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetPkInfoList write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }
    data.WriteInt32(osAccountId);
    data.WriteCString(appId);
    data.WriteCString(queryParams);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_GET_PKINFO_LIST, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetPkInfoList send request failed!");
        return SOFTBUS_ERR;
    }

    int32_t ret = 0;
    uint32_t infoNum = 0;
    const char *retData = reply.ReadCString();
    if (retData != nullptr) {
        *returnInfoList = strdup(retData);
    }
    ret = reply.ReadUint32(infoNum);
    if (!ret) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetPkInfoList read infoNum failed!");
        return SOFTBUS_ERR;
    }
    *returnInfoNum = infoNum;

    return SOFTBUS_OK;
}

int32_t AuthServerProxy::GetGroupInfoById(int32_t osAccountId, const char *appId, const char *groupId,
                                          char **returnGroupInfo)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetGroupInfoById write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }
    data.WriteInt32(osAccountId);
    data.WriteCString(appId);
    data.WriteCString(groupId);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_GET_GROUP_INFO_BY_ID, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetGroupInfoById send request failed!");
        return SOFTBUS_ERR;
    }

    const char *retData = reply.ReadCString();
    if (retData != nullptr) {
        *returnGroupInfo = strdup(retData);
    }

    return SOFTBUS_OK;
}

int32_t AuthServerProxy::GetGroupInfo(int32_t osAccountId, const char *appId, const char *queryParams,
                                      char **returnGroupVec, uint32_t *groupNum)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetGroupInfo write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }
    data.WriteInt32(osAccountId);
    data.WriteCString(appId);
    data.WriteCString(queryParams);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_GET_GROUP_INFO, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetGroupInfo send request failed!");
        return SOFTBUS_ERR;
    }

    int32_t ret = 0;
    uint32_t num = 0;
    const char *retData = reply.ReadCString();
    if (retData != nullptr) {
        *returnGroupVec = strdup(retData);
    }
    ret = reply.ReadUint32(num);
    if (!ret) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetGroupInfo read num failed!");
        return SOFTBUS_ERR;
    }
    *groupNum = num;

    return SOFTBUS_OK;
}

int32_t AuthServerProxy::GetJoinedGroups(int32_t osAccountId, const char *appId, int groupType, char **returnGroupVec,
                                         uint32_t *groupNum)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetJoinedGroups write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }
    data.WriteInt32(osAccountId);
    data.WriteCString(appId);
    data.WriteInt32(groupType);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_GET_JOINED_GROUPS, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetJoinedGroups send request failed!");
        return SOFTBUS_ERR;
    }

    int32_t ret = 0;
    uint32_t num = 0;
    const char *retData = reply.ReadCString();
    if (retData != nullptr) {
        *returnGroupVec = strdup(retData);
    }
    ret = reply.ReadUint32(num);
    if (!ret) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetJoinedGroups read num failed!");
        return SOFTBUS_ERR;
    }
    *groupNum = num;

    return SOFTBUS_OK;
}

int32_t AuthServerProxy::GetRelatedGroups(int32_t osAccountId, const char *appId, const char *peerDeviceId,
                                          char **returnGroupVec, uint32_t *groupNum)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetRelatedGroups write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }
    data.WriteInt32(osAccountId);
    data.WriteCString(appId);
    data.WriteCString(peerDeviceId);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_GET_RELATED_GROUPS, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetRelatedGroups send request failed!");
        return SOFTBUS_ERR;
    }

    int32_t ret = 0;
    uint32_t num = 0;
    const char *retData = reply.ReadCString();
    if (retData != nullptr) {
        *returnGroupVec = strdup(retData);
    }
    ret = reply.ReadUint32(num);
    if (!ret) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetRelatedGroups read num failed!");
        return SOFTBUS_ERR;
    }
    *groupNum = num;

    return SOFTBUS_OK;
}

int32_t AuthServerProxy::GetAuthDeviceInfoById(int32_t osAccountId, const char *appId, const char *deviceId,
                                               const char *groupId, char **returnDeviceInfo)
{
    sptr<IRemoteObject> remote = GetSystemAbility();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr!");
        return SOFTBUS_ERR;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetAuthDeviceInfoById write InterfaceToken failed!");
        return SOFTBUS_ERR;
    }
    data.WriteInt32(osAccountId);
    data.WriteCString(appId);
    data.WriteCString(deviceId);
    data.WriteCString(groupId);

    MessageParcel reply;
    MessageOption option;
    int32_t err = remote->SendRequest(SERVER_GET_DEVICE_INFO_BY_ID, data, reply, option);
    if (err != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "GetAuthDeviceInfoById send request failed!");
        return SOFTBUS_ERR;
    }

    const char *retData = reply.ReadCString();
    if (retData != nullptr) {
        *returnDeviceInfo = strdup(retData);
    }

    return SOFTBUS_OK;
}

int32_t AuthServerProxy::StartDiscovery(const char *pkgName, const SubscribeInfo *subInfo)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::StopDiscovery(const char *pkgName, int subscribeId)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::PublishService(const char *pkgName, const PublishInfo *pubInfo)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::UnPublishService(const char *pkgName, int publishId)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::SoftbusRegisterService(const char *clientPkgName, const sptr<IRemoteObject> &object,
                                                int32_t callingUid, int32_t callingPid)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::CreateSessionServer(const char *pkgName, const char *sessionName, int32_t callingUid,
                                             int32_t callingPid)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::RemoveSessionServer(const char *pkgName, const char *sessionName)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::OpenSession(const SessionParam *param, TransInfo *info)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::OpenAuthSession(const char *sessionName, const ConnectionAddr *addrInfo)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::NotifyAuthSuccess(int32_t channelId, int32_t channelType)
{
    (void)channelId;
    (void)channelType;
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::CloseChannel(int32_t channelId, int32_t channelType)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::SendMessage(int32_t channelId, int32_t channelType, const void *data, uint32_t len,
                                     int32_t msgType)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::QosReport(int32_t channelId, int32_t chanType, int32_t appType, int quality)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::StreamStats(int32_t channelId, int32_t channelType, const StreamSendStats *data)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::RippleStats(int32_t channelId, int32_t channelType, const TrafficStats *data)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::JoinLNN(const char *pkgName, void *addr, uint32_t addrTypeLen)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::JoinMetaNode(const char *pkgName, void *addr, CustomData *customData, uint32_t addrTypeLen)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::LeaveLNN(const char *pkgName, const char *networkId)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::LeaveMetaNode(const char *pkgName, const char *networkId)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::GetAllOnlineNodeInfo(const char *pkgName, void **info, uint32_t infoTypeLen, int *infoNum)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::GetLocalDeviceInfo(const char *pkgName, void *info, uint32_t infoTypeLen)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::GetNodeKeyInfo(const char *pkgName, const char *networkId, int key, unsigned char *buf,
                                        uint32_t len)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::SetNodeDataChangeFlag(const char *pkgName, const char *networkId, uint16_t dataChangeFlag)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::StartTimeSync(const char *pkgName, const char *targetNetworkId, int32_t accuracy,
                                       int32_t period)
{
    return SOFTBUS_OK;
}

int32_t AuthServerProxy::StopTimeSync(const char *pkgName, const char *targetNetworkId)
{
    return SOFTBUS_OK;
}

}  // namespace OHOS