set(trans_session_sdk_path "${dsoftbus_sdk_path}/transmission/session")

set(trans_session_sdk_src
    "${trans_session_sdk_path}/src/client_trans_session_callback.c"
    "${trans_session_sdk_path}/src/client_trans_session_manager.c"
    "${trans_session_sdk_path}/src/client_trans_message_service.c"
    "${trans_session_sdk_path}/src/client_trans_session_service.c"
    "${trans_session_sdk_path}/cpp/src/session_callback_mock.cpp"
    "${trans_session_sdk_path}/cpp/src/session_impl.cpp"
    "${trans_session_sdk_path}/cpp/src/session_mock.cpp"
    "${trans_session_sdk_path}/cpp/src/session_service_impl.cpp"
    "${dsoftbus_root_path}/core/transmission/broadcast/src/trans_spec_object_proxy.cpp"
    PARENT_SCOPE
)
set(trans_session_sdk_inc
    "${trans_session_sdk_path}/include"
    "${trans_session_sdk_path}/cpp/include"
    "${trans_session_sdk_path}/cpp/src"
    "${trans_session_sdk_path}/trans_channel/udp/file/include"
    PARENT_SCOPE
)