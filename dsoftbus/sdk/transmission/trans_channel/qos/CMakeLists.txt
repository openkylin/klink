set(trans_qos_sdk_inc
    "${trans_channel_sdk_path}/qos/include"
    PARENT_SCOPE
)
set(trans_qos_sdk_src
    "${trans_channel_sdk_path}/qos/src/client_qos_manager.c"
    PARENT_SCOPE
)