set(libsoftbus_stream_sdk_path "${dsoftbus_stream_sdk_path}/libsoftbus_stream")

set(libsoftbus_stream_inc
    "${libsoftbus_stream_sdk_path}/include"
    PARENT_SCOPE
)

set(libsoftbus_stream_src
    "${libsoftbus_stream_sdk_path}/raw_stream_data.cpp"
    "${libsoftbus_stream_sdk_path}/stream_common_data.cpp"
    "${libsoftbus_stream_sdk_path}/stream_depacketizer.cpp"
    "${libsoftbus_stream_sdk_path}/stream_manager.cpp"
    "${libsoftbus_stream_sdk_path}/stream_msg_manager.cpp"
    "${libsoftbus_stream_sdk_path}/stream_packetizer.cpp"
    "${libsoftbus_stream_sdk_path}/vtp_instance.cpp"
    "${libsoftbus_stream_sdk_path}/vtp_stream_socket.cpp"
    PARENT_SCOPE
)

set(libsoftbus_stream_lib
    FillpSo.open
    sec_shared
    PARENT_SCOPE
)