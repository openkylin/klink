/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SOFTBUS_AUTH_H
#define SOFTBUS_AUTH_H

#include <stdbool.h>
#include <stdint.h>

#include "device_auth.h"

#ifdef __cplusplus
extern "C" {
#endif

int32_t SdkRegAuthCallback(const char *packageName, const char *appId, DeviceAuthCallback *callback);
int32_t SdkCreateGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *createParams);
int32_t SdkGetRegisterInfo(const char *reqJsonStr, char **returnRegisterInfo);
int32_t SdkDestroyInfo(char **returnInfo);
int32_t SdkAddMultiMembersToGroup(int32_t osAccountId, const char *appId, const char *addParams);
int32_t SdkDelMultiMembersFromGroup(int32_t osAccountId, const char *appId, const char *deleteParams);
int32_t SdkGetTrustedDevices(int32_t osAccountId, const char *appId, const char *groupId, char **returnDevInfoVec,
                             uint32_t *deviceNum);
bool SdkIsDeviceInGroup(int32_t osAccountId, const char *appId, const char *groupId, const char *deviceId);
int32_t SdkUnRegAuthInfo(const char *appId);
int32_t SdkDeleteGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *disbandParams);
int32_t SdkAddMemberToGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *addParams);
int32_t SdkDeleteMemberFromGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *deleteParams);
int32_t SdkCheckAccessToGroup(int32_t osAccountId, const char *appId, const char *groupId);
int32_t SdkGetPkInfoList(int32_t osAccountId, const char *appId, const char *queryParams, char **returnInfoList,
                         uint32_t *returnInfoNum);
int32_t SdkGetGroupInfoById(int32_t osAccountId, const char *appId, const char *groupId, char **returnGroupInfo);
int32_t SdkGetGroupInfo(int32_t osAccountId, const char *appId, const char *queryParams, char **returnGroupVec,
                        uint32_t *groupNum);
int32_t SdkGetJoinedGroups(int32_t osAccountId, const char *appId, int groupType, char **returnGroupVec,
                           uint32_t *groupNum);
int32_t SdkGetRelatedGroups(int32_t osAccountId, const char *appId, const char *peerDeviceId, char **returnGroupVec,
                            uint32_t *groupNum);
int32_t SdkGetAuthDeviceInfoById(int32_t osAccountId, const char *appId, const char *deviceId, const char *groupId,
                                 char **returnDeviceInfo);

#ifdef __cplusplus
}
#endif
#endif