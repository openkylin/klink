/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef AUTH_HICHAIN_ADAPTER_H
#define AUTH_HICHAIN_ADAPTER_H

#include "device_auth.h"
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
#    if __cplusplus
extern "C" {
#    endif
#endif

typedef enum {
    AUTH_GROUP_ACCOUNT = 0x001,
    AUTH_GROUP_P2P = 0x100,
    AUTH_GROUP_MESH = 0x101,
    AUTH_GROUP_COMPATIBLE = 0x200,
} HichainGroup;

typedef enum {
    ID_TYPE_UNKNOWN = 0,
    ID_TYPE_DEVID,
    ID_TYPE_UID,
} TrustedRelationIdType;

int32_t RegChangeListener(const char *appId, DataChangeListener *listener);
int32_t UnregChangeListener(const char *appId);
int32_t AuthDevice(int64_t authReqId, const char *authParams, const DeviceAuthCallback *cb);
int32_t ProcessAuthData(int64_t authSeq, const uint8_t *data, uint32_t len, DeviceAuthCallback *cb);
bool CheckDeviceInGroupByType(const char *udid, const char *uuid, HichainGroup groupType);
void DestroyDeviceAuth(void);
bool IsPotentialTrustedDevice(TrustedRelationIdType idType, const char *deviceId, bool isPrecise);

int32_t RegDeviceAuthCallback(const char *appId, DeviceAuthCallback *cb);
int32_t UnRegDeviceAuthCallback(const char *appId);
int32_t CreateAuthGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *createParams);
int32_t DeleteAuthGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *disbandParams);
int32_t AddMemberToAuthGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *addParams);
int32_t DeleteMemberFromAuthGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *deleteParams);
int32_t GetRegisterAuthInfo(const char *reqJsonStr, char **returnRegisterInfo);
int32_t CheckAccessToAuthGroup(int32_t osAccountId, const char *appId, const char *groupId);
int32_t GetAuthPkInfoList(int32_t osAccountId, const char *appId, const char *queryParams, char **returnInfoList,
                          uint32_t *returnInfoNum);
int32_t GetAuthGroupInfoById(int32_t osAccountId, const char *appId, const char *groupId, char **returnGroupInfo);
int32_t GetAuthGroupInfo(int32_t osAccountId, const char *appId, const char *queryParams, char **returnGroupVec,
                         uint32_t *groupNum);
int32_t GetJoinedAuthGroups(int32_t osAccountId, const char *appId, int groupType, char **returnGroupVec,
                            uint32_t *groupNum);
int32_t GetRelatedAuthGroups(int32_t osAccountId, const char *appId, const char *peerDeviceId, char **returnGroupVec,
                             uint32_t *groupNum);
int32_t GetAuthDeviceInfoById(int32_t osAccountId, const char *appId, const char *deviceId, const char *groupId,
                              char **returnDeviceInfo);
int32_t DestroyAuthInfo(char **returnInfo);
int32_t AddMultiMembersToAuthGroup(int32_t osAccountId, const char *appId, const char *addParams);
int32_t DelMultiMembersFromAuthGroup(int32_t osAccountId, const char *appId, const char *deleteParams);
int32_t GetTrustedAuthDevices(int32_t osAccountId, const char *appId, const char *groupId, char **returnDevInfoVec,
                              uint32_t *deviceNum);
bool IsDeviceInAuthGroup(int32_t osAccountId, const char *appId, const char *groupId, const char *deviceId);

#ifdef __cplusplus
#    if __cplusplus
}
#    endif
#endif
#endif /* AUTH_HICHAIN_ADAPTER_H */