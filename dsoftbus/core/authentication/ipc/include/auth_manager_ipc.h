/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AUTH_MANAGER_IPC_H
#define AUTH_MANAGER_IPC_H

#include <stdbool.h>
#include <stdint.h>

#include "device_auth.h"

#ifdef __cplusplus
extern "C" {
#endif

int32_t IpcRegAuthInfo(const char *packageName, const char *appId, int32_t pid);
int32_t IpcUnRegAuthInfo(const char *appId);
int32_t IpcCreateGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *createParams);
int32_t IpcDeleteGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *disbandParams);
int32_t IpcAddMemberToGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *addParams);
int32_t IpcDeleteMemberFromGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *deleteParams);
int32_t IpcGetRegisterInfo(const char *reqJsonStr, char **returnRegisterInfo);
int32_t IpcCheckAccessToGroup(int32_t osAccountId, const char *appId, const char *groupId);
int32_t IpcGetPkInfoList(int32_t osAccountId, const char *appId, const char *queryParams, char **returnInfoList,
                         uint32_t *returnInfoNum);
int32_t IpcGetGroupInfoById(int32_t osAccountId, const char *appId, const char *groupId, char **returnGroupInfo);
int32_t IpcGetGroupInfo(int32_t osAccountId, const char *appId, const char *queryParams, char **returnGroupVec,
                        uint32_t *groupNum);
int32_t IpcGetJoinedGroups(int32_t osAccountId, const char *appId, int groupType, char **returnGroupVec,
                           uint32_t *groupNum);
int32_t IpcGetRelatedGroups(int32_t osAccountId, const char *appId, const char *peerDeviceId, char **returnGroupVec,
                            uint32_t *groupNum);
int32_t IpcGetAuthDeviceInfoById(int32_t osAccountId, const char *appId, const char *deviceId, const char *groupId,
                                 char **returnDeviceInfo);
int32_t IpcDestroyInfo(char **returnInfo);
int32_t IpcAddMultiMembersToGroup(int32_t osAccountId, const char *appId, const char *addParams);
int32_t IpcDelMultiMembersFromGroup(int32_t osAccountId, const char *appId, const char *deleteParams);
int32_t IpcGetTrustedDevices(int32_t osAccountId, const char *appId, const char *groupId, char **returnDevInfoVec,
                             uint32_t *deviceNum);
bool IpcIsDeviceInGroup(int32_t osAccountId, const char *appId, const char *groupId, const char *deviceId);

#ifdef __cplusplus
}
#endif
#endif /* AUTH_MANAGER_IPC_H */