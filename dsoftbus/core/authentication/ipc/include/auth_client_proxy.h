/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AUTH_CLIENT_PROXY_H
#define AUTH_CLIENT_PROXY_H

#include <stdint.h>

#ifdef __cplusplus
#    if __cplusplus
extern "C" {
#    endif
#endif

void RegisterPkgName(const char *pkgName, int32_t pid);
void UnRegisterPkgName();

void ClientOnAuthFinish(int64_t requestId, int operationCode, const char *returnData);
void ClientOnAuthError(int64_t requestId, int operationCode, int errorCode, const char *errorReturn);
char *ClientOnAuthRequest(int64_t requestId, int operationCode, const char *reqParams);

#ifdef __cplusplus
#    if __cplusplus
}
#    endif /* __cplusplus */
#endif     /* __cplusplus */

#endif
