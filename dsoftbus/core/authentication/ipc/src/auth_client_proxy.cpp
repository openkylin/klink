/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "auth_client_proxy.h"

#include "securec.h"
#include "auth_client_proxy_standard.h"
#include "softbus_client_info_manager.h"
#include "softbus_def.h"
#include "softbus_errcode.h"
#include "softbus_log.h"
#include "softbus_permission.h"

using namespace OHOS;

static char *g_pkgName = nullptr;
static int32_t g_clientPid = -1;

static sptr<AuthClientProxy> GetClientProxy()
{
    sptr<IRemoteObject> clientObject = SoftbusClientInfoManager::GetInstance().GetSoftbusClientProxy(g_pkgName);
    sptr<AuthClientProxy> clientProxy = new (std::nothrow) AuthClientProxy(clientObject);
    return clientProxy;
}

void RegisterPkgName(const char *pkgName, int32_t pid)
{
    SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_INFO, "auth client proxy RegisterPkgName %s!\n", pkgName);
    g_pkgName = strdup(pkgName);
    g_clientPid = pid;
}

void UnRegisterPkgName()
{
    free(g_pkgName);
    g_pkgName = nullptr;
    g_clientPid = -1;
}

NO_SANITIZE("cfi")
void ClientOnAuthFinish(int64_t requestId, int operationCode, const char *returnData)
{
    sptr<AuthClientProxy> clientProxy = GetClientProxy();
    if (clientProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "auth client proxy is nullptr!\n");
        return;
    }
    clientProxy->setCallingPid(g_clientPid);
    clientProxy->OnAuthFinish(requestId, operationCode, returnData);
}

NO_SANITIZE("cfi")
void ClientOnAuthError(int64_t requestId, int operationCode, int errorCode, const char *errorReturn)
{
    sptr<AuthClientProxy> clientProxy = GetClientProxy();
    if (clientProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "auth client proxy is nullptr!\n");
        return;
    }
    clientProxy->setCallingPid(g_clientPid);
    clientProxy->OnAuthError(requestId, operationCode, errorCode, errorReturn);
}

NO_SANITIZE("cfi")
char *ClientOnAuthRequest(int64_t requestId, int operationCode, const char *reqParams)
{
    sptr<AuthClientProxy> clientProxy = GetClientProxy();
    if (clientProxy == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "auth client proxy is nullptr!\n");
        return nullptr;
    }
    clientProxy->setCallingPid(g_clientPid);
    return clientProxy->OnAuthRequest(requestId, operationCode, reqParams);
}