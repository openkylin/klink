/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "auth_manager_ipc.h"

#include <cstring>
#include <mutex>
#include <securec.h>
#include <vector>

#include "auth_client_proxy.h"
#include "auth_hichain_adapter.h"
#include "softbus_def.h"
#include "softbus_errcode.h"
#include "softbus_log.h"
#include "softbus_permission.h"

static DeviceAuthCallback g_deviceAuthCb = {.onTransmit = nullptr,
                                            .onSessionKeyReturned = nullptr,
                                            .onFinish = ClientOnAuthFinish,
                                            .onError = ClientOnAuthError,
                                            .onRequest = ClientOnAuthRequest};

int32_t IpcRegAuthInfo(const char *packageName, const char *appId, int32_t pid)
{
    RegisterPkgName(packageName, pid);
    return RegDeviceAuthCallback(appId, &g_deviceAuthCb);
}

int32_t IpcUnRegAuthInfo(const char *appId)
{
    UnRegisterPkgName();
    return UnRegDeviceAuthCallback(appId);
}

int32_t IpcCreateGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *createParams)
{
    return CreateAuthGroup(osAccountId, requestId, appId, createParams);
}

int32_t IpcDeleteGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *disbandParams)
{
    return DeleteAuthGroup(osAccountId, requestId, appId, disbandParams);
}

int32_t IpcAddMemberToGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *addParams)
{
    return AddMemberToAuthGroup(osAccountId, requestId, appId, addParams);
}

int32_t IpcDeleteMemberFromGroup(int32_t osAccountId, int64_t requestId, const char *appId, const char *deleteParams)
{
    return DeleteMemberFromAuthGroup(osAccountId, requestId, appId, deleteParams);
}

int32_t IpcGetRegisterInfo(const char *reqJsonStr, char **returnRegisterInfo)
{
    return GetRegisterAuthInfo(reqJsonStr, returnRegisterInfo);
}

int32_t IpcCheckAccessToGroup(int32_t osAccountId, const char *appId, const char *groupId)
{
    return CheckAccessToAuthGroup(osAccountId, appId, groupId);
}

int32_t IpcGetPkInfoList(int32_t osAccountId, const char *appId, const char *queryParams, char **returnInfoList,
                         uint32_t *returnInfoNum)
{
    return GetAuthPkInfoList(osAccountId, appId, queryParams, returnInfoList, returnInfoNum);
}

int32_t IpcGetGroupInfoById(int32_t osAccountId, const char *appId, const char *groupId, char **returnGroupInfo)
{
    return GetAuthGroupInfoById(osAccountId, appId, groupId, returnGroupInfo);
}

int32_t IpcGetGroupInfo(int32_t osAccountId, const char *appId, const char *queryParams, char **returnGroupVec,
                        uint32_t *groupNum)
{
    return GetAuthGroupInfo(osAccountId, appId, queryParams, returnGroupVec, groupNum);
}

int32_t IpcGetJoinedGroups(int32_t osAccountId, const char *appId, int groupType, char **returnGroupVec,
                           uint32_t *groupNum)
{
    return GetJoinedAuthGroups(osAccountId, appId, groupType, returnGroupVec, groupNum);
}

int32_t IpcGetRelatedGroups(int32_t osAccountId, const char *appId, const char *peerDeviceId, char **returnGroupVec,
                            uint32_t *groupNum)
{
    return GetRelatedAuthGroups(osAccountId, appId, peerDeviceId, returnGroupVec, groupNum);
}

int32_t IpcGetAuthDeviceInfoById(int32_t osAccountId, const char *appId, const char *deviceId, const char *groupId,
                                 char **returnDeviceInfo)
{
    return GetAuthDeviceInfoById(osAccountId, appId, deviceId, groupId, returnDeviceInfo);
}

int32_t IpcDestroyInfo(char **returnInfo)
{
    return DestroyAuthInfo(returnInfo);
}

int32_t IpcAddMultiMembersToGroup(int32_t osAccountId, const char *appId, const char *addParams)
{
    return AddMultiMembersToAuthGroup(osAccountId, appId, addParams);
}

int32_t IpcDelMultiMembersFromGroup(int32_t osAccountId, const char *appId, const char *deleteParams)
{
    return DelMultiMembersFromAuthGroup(osAccountId, appId, deleteParams);
}

int32_t IpcGetTrustedDevices(int32_t osAccountId, const char *appId, const char *groupId, char **returnDevInfoVec,
                             uint32_t *deviceNum)
{
    return GetTrustedAuthDevices(osAccountId, appId, groupId, returnDevInfoVec, deviceNum);
}

bool IpcIsDeviceInGroup(int32_t osAccountId, const char *appId, const char *groupId, const char *deviceId)
{
    return IsDeviceInAuthGroup(osAccountId, appId, groupId, deviceId);
}