/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "auth_client_proxy_standard.h"

#include "message_parcel.h"
#include "softbus_errcode.h"
#include "softbus_server_ipc_interface_code.h"
#include "softbus_log.h"
#include "securec.h"

namespace OHOS {
int32_t AuthClientProxy::OnChannelOpened(const char *sessionName, const ChannelInfo *info)
{
    return SOFTBUS_OK;
}

int32_t AuthClientProxy::OnChannelOpenFailed(int32_t channelId, int32_t channelType, int32_t errCode)
{
    return SOFTBUS_OK;
}

int32_t AuthClientProxy::OnChannelLinkDown(const char *networkId, int32_t routeType)
{
    return SOFTBUS_OK;
}

int32_t AuthClientProxy::OnChannelClosed(int32_t channelId, int32_t channelType)
{
    return SOFTBUS_OK;
}

int32_t AuthClientProxy::OnChannelMsgReceived(int32_t channelId, int32_t channelType, const void *dataInfo,
                                              uint32_t len, int32_t type)
{
    return SOFTBUS_OK;
}

int32_t AuthClientProxy::OnChannelQosEvent(int32_t channelId, int32_t channelType, int32_t eventId, int32_t tvCount,
                                           const QosTv *tvList)
{
    return SOFTBUS_OK;
}

void AuthClientProxy::OnDeviceFound(const DeviceInfo *deviceInfo)
{
}

void AuthClientProxy::OnDiscoverFailed(int subscribeId, int failReason)
{
}

void AuthClientProxy::OnDiscoverySuccess(int subscribeId)
{
}

void AuthClientProxy::OnPublishSuccess(int publishId)
{
}

void AuthClientProxy::OnPublishFail(int publishId, int reason)
{
}

int32_t AuthClientProxy::OnJoinLNNResult(void *addr, uint32_t addrTypeLen, const char *networkId, int retCode)
{
    return SOFTBUS_OK;
}

int32_t AuthClientProxy::OnJoinMetaNodeResult(void *addr, uint32_t addrTypeLen, const char *networkId, int retCode)
{
    return SOFTBUS_OK;
}

int32_t AuthClientProxy::OnLeaveLNNResult(const char *networkId, int retCode)
{
    return SOFTBUS_OK;
}

int32_t AuthClientProxy::OnLeaveMetaNodeResult(const char *networkId, int retCode)
{
    return SOFTBUS_OK;
}

int32_t AuthClientProxy::OnNodeOnlineStateChanged(const char *pkgName, bool isOnline, void *info, uint32_t infoTypeLen)
{
    return SOFTBUS_OK;
}

int32_t AuthClientProxy::OnNodeBasicInfoChanged(const char *pkgName, void *info, uint32_t infoTypeLen, int32_t type)
{
    return SOFTBUS_OK;
}

int32_t AuthClientProxy::OnTimeSyncResult(const void *info, uint32_t infoTypeLen, int32_t retCode)
{
    return SOFTBUS_OK;
}

void AuthClientProxy::OnPublishLNNResult(int32_t publishId, int32_t reason)
{
}

void AuthClientProxy::OnRefreshLNNResult(int32_t refreshId, int32_t reason)
{
}

void AuthClientProxy::OnRefreshDeviceFound(const void *device, uint32_t deviceLen)
{
}

void AuthClientProxy::setCallingPid(int32_t pid)
{
    callingPid_ = pid;
}

void AuthClientProxy::OnAuthFinish(int64_t requestId, int operationCode, const char *returnData)
{
    sptr<IRemoteObject> remote = Remote();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr");
        return;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "write InterfaceToken failed!");
        return;
    }
    if (!data.WriteInt64(requestId)) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "write requestId failed");
        return;
    }
    if (!data.WriteInt32(operationCode)) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "write operationCode failed");
        return;
    }
    if (returnData != nullptr) {
        if (!data.WriteCString(returnData)) {
            SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "write returnData failed");
            return;
        }
    }

    MessageParcel reply;
    MessageOption option{MessageOption::TF_ASYNC};
    if (remote->SendRequest(CLIENT_ON_AUTH_FINISH, data, reply, option, callingPid_) != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "OnAuthFinish send request failed");
        return;
    }
    return;
}

void AuthClientProxy::OnAuthError(int64_t requestId, int operationCode, int errorCode, const char *errorReturn)
{
    sptr<IRemoteObject> remote = Remote();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr");
        return;
    }

    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "write InterfaceToken failed!");
        return;
    }
    if (!data.WriteInt64(requestId)) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "write requestId failed");
        return;
    }
    if (!data.WriteInt32(operationCode)) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "write operationCode failed");
        return;
    }
    if (!data.WriteInt32(errorCode)) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "write errorCode failed");
        return;
    }
    if (errorReturn != nullptr) {
        if (!data.WriteCString(errorReturn)) {
            SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "write errorReturn failed");
            return;
        }
    }

    MessageParcel reply;
    MessageOption option{MessageOption::TF_ASYNC};
    if (remote->SendRequest(CLIENT_ON_AUTH_ERROR, data, reply, option, callingPid_) != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "OnAuthError send request failed");
        return;
    }
    return;
}

char *AuthClientProxy::OnAuthRequest(int64_t requestId, int operationCode, const char *reqParams)
{
    sptr<IRemoteObject> remote = Remote();
    if (remote == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "remote is nullptr");
        return nullptr;
    }
    MessageParcel data;
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "write InterfaceToken failed!");
        return nullptr;
    }
    if (!data.WriteInt64(requestId)) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "write requestId failed");
        return nullptr;
    }
    if (!data.WriteInt32(operationCode)) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "write operationCode failed");
        return nullptr;
    }
    if (reqParams != nullptr) {
        if (!data.WriteCString(reqParams)) {
            SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "write reqParams failed");
            return nullptr;
        }
    }

    MessageParcel reply;
    MessageOption option;
    if (remote->SendRequest(CLIENT_ON_AUTH_REQUEST, data, reply, option, callingPid_) != 0) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "OnAuthRequest send request failed");
        return nullptr;
    }

    const char *replyData = reply.ReadCString();
    if (replyData == nullptr) {
        SoftBusLog(SOFTBUS_LOG_AUTH, SOFTBUS_LOG_ERROR, "read replyData failed");
        return nullptr;
    }
    char *retData = strdup(replyData);
    return retData;
}

}  // namespace OHOS