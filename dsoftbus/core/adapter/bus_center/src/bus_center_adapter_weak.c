/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#include <fcntl.h>
#include <securec.h>

#include "bus_center_adapter.h"
#include "softbus_adapter_log.h"
#include "softbus_errcode.h"
#include "parameter.h"

#define DEFAULT_DEVICE_NAME "UNKNOWN"
#define DEFAULR_DEVICE_NAME_LENGTH 128
#define UUID_BUF_LEN 128
#define DEVICE_TYPE_BUF_LEN 17

static int32_t GetUuid(char *uuid)
{
    return GetDevUdid(uuid, UUID_BUF_LEN);
}

int32_t __attribute__((weak)) GetCommonDevInfo(const CommonDeviceKey key, char *value, uint32_t len)
{
    if (value == NULL) {
        HILOG_ERROR(SOFTBUS_HILOG_ID, "fail: para error!");
        return SOFTBUS_INVALID_PARAM;
    }
    switch (key) {
        case COMM_DEVICE_KEY_DEVNAME: {
            char host[DEFAULR_DEVICE_NAME_LENGTH];
            gethostname(host, DEFAULR_DEVICE_NAME_LENGTH - 1);
            if (strncpy_s(value, len, host, strlen(host)) != EOK) {
                return SOFTBUS_ERR;
            }
            HILOG_INFO(SOFTBUS_HILOG_ID, "HOSTNAME = %s", value);
            break;
        }

        case COMM_DEVICE_KEY_UDID: {
            char uuid[UUID_BUF_LEN];
            if (GetUuid(uuid) != SOFTBUS_OK) {
                return SOFTBUS_ERR;
            } else {
                if (strncpy_s(value, len, uuid, strlen(uuid)) != EOK) {
                    return SOFTBUS_ERR;
                }
            }
            HILOG_INFO(SOFTBUS_HILOG_ID, "UUID = %s", value);
            break;
        }

        case COMM_DEVICE_KEY_DEVTYPE: {
            if (strncpy_s(value, len, DEFAULT_DEVICE_TYPE_STR, strlen(DEFAULT_DEVICE_TYPE_STR)) != EOK) {
                HILOG_ERROR(SOFTBUS_HILOG_ID, "fail: get device type error!");
                return SOFTBUS_ERR;
            }

            HILOG_INFO(SOFTBUS_HILOG_ID, "DEVICETYPE = %s", value);
            break;
        }
        default:
            break;
    }
    return SOFTBUS_OK;
}

int32_t GetWlanIpv4Addr(char *ip, uint32_t size)
{
    (void)ip;
    (void)size;
    return SOFTBUS_ERR;
}