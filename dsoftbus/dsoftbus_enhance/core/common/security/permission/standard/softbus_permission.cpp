/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "softbus_permission.h"

#include <sys/types.h>
#include <unistd.h>

#include "permission_entry.h"
#include "softbus_errcode.h"
#include "softbus_log.h"

NO_SANITIZE("cfi") int32_t TransPermissionInit(void)
{
    // todo
    return SOFTBUS_OK;
}

NO_SANITIZE("cfi") void TransPermissionDeinit(void)
{
    // todo
    return;
}

NO_SANITIZE("cfi") int32_t CalcPermType(pid_t callingUid, pid_t callingPid)
{
    // todo
    return NORMAL_APP;
}

NO_SANITIZE("cfi")
int32_t CheckTransPermission(pid_t callingUid, pid_t callingPid, const char *pkgName, const char *sessionName,
                             uint32_t actions)
{
    // todo
    return SOFTBUS_OK;
}

NO_SANITIZE("cfi") int32_t CheckTransSecLevel(const char *mySessionName, const char *peerSessionName)
{
    // todo
    return SOFTBUS_OK;
}

NO_SANITIZE("cfi") bool CheckDiscPermission(pid_t callingUid, const char *pkgName)
{
    // todo
    return true;
}

NO_SANITIZE("cfi") bool CheckBusCenterPermission(pid_t callingUid, const char *pkgName)
{
    // todo
    return true;
}

NO_SANITIZE("cfi") int32_t GrantTransPermission(int32_t callingUid, int32_t callingPid, const char *sessionName)
{
    // todo
    return SOFTBUS_OK;
}

NO_SANITIZE("cfi") int32_t RemoveTransPermission(const char *sessionName)
{
    // todo
    return SOFTBUS_OK;
}

NO_SANITIZE("cfi") int32_t CheckDynamicPermission(void)
{
    // todo
    return SOFTBUS_OK;
}
