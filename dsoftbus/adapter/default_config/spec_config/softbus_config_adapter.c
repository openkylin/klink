/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#include <arpa/inet.h>
#include <net/if.h>
#include <securec.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "softbus_adapter_log.h"
#include "softbus_config_adapter.h"
#include "softbus_def.h"

#define MAX_BYTES_LENGTH 4194304
#define MAX_IF_NAME_LEN 256
#define IF_COUNT_MAX 16
#define DELIMITER_OUTSIDE ","
#define DELIMITER_INSIDE ":"
#ifndef DEFAULT_NET_INF_NAME_FOR_LINUX
#    define LNN_WLAN_IF_NAME_PREFIX "wlan"
#else
#    define LNN_WLAN_IF_NAME_PREFIX "wl"
#endif  // !DEFAULT_NET_INF_NAME_FOR_LINUX

NO_SANITIZE("cfi") void SoftbusConfigAdapterInit(const ConfigSetProc *sets)
{
    int32_t val;
    val = MAX_BYTES_LENGTH;
    sets->SetConfig(SOFTBUS_INT_MAX_BYTES_LENGTH, (unsigned char *)&val, sizeof(val));
    val = 0x1;
    sets->SetConfig(SOFTBUS_INT_AUTH_ABILITY_COLLECTION, (unsigned char *)&val, sizeof(val));

    // set ifName
    LOG_INFO("start set ifName config");
    int32_t fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd < 0) {
        LOG_ERR("open socket failed");
        return;
    }
    struct ifreq req[IF_COUNT_MAX];
    struct ifconf conf = {
        .ifc_len = sizeof(req),
        .ifc_buf = (char *)&req,
    };
    int32_t ret = ioctl(fd, SIOCGIFCONF, (char *)&conf);
    if (ret < 0) {
        LOG_ERR("ioctl fail, errno = %d", errno);
        close(fd);
        return;
    }
    int32_t num = conf.ifc_len / sizeof(struct ifreq);
    LOG_INFO("network interface num = %d", num);
    char ifName[MAX_IF_NAME_LEN] = {0};
    for (int32_t i = 0; (i < num) && (i < IF_COUNT_MAX); i++) {
        if (strncmp(LNN_WLAN_IF_NAME_PREFIX, req[i].ifr_name, strlen(LNN_WLAN_IF_NAME_PREFIX)) != 0) {
            continue;
        }
        LOG_INFO("network interface name is %s", req[i].ifr_name);
        char tmp[MAX_IF_NAME_LEN] = {0};
        sprintf_s(tmp, MAX_IF_NAME_LEN, "%d:%s", i, req[i].ifr_name);
        strncat_s(ifName, MAX_IF_NAME_LEN, tmp, strlen(tmp));
        if (i != num - 1) {
            strncat_s(ifName, MAX_IF_NAME_LEN, DELIMITER_OUTSIDE, strlen(DELIMITER_OUTSIDE));
        }
    }
    close(fd);
    if (ifName[0] == '\0') {
        return;
    }
    sets->SetConfig(SOFTBUS_STR_LNN_NET_IF_NAME, (unsigned char *)&ifName, sizeof(ifName));
}