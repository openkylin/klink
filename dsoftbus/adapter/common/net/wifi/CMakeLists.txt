set(softbus_adapter_wifi_path "${softbus_adapter_common}/net/wifi")

set(adapter_wifi_inc "${softbus_adapter_wifi_path}/include" PARENT_SCOPE)

if (dsoftbus_feature_lnn_net)
    set(adapter_wifi_src "${softbus_adapter_wifi_path}/common/softbus_wifi_api_adapter.c" PARENT_SCOPE)
    set(adapter_wifi_lib wifi_sdk PARENT_SCOPE)
endif()
