set(softbus_adapter_hdi_path ${softbus_adapter_common}/net/hdi)

set(adapter_hdi_inc "${softbus_adapter_hdi_path}/include" PARENT_SCOPE)

if(dsoftbus_feature_lnn_net)
    set(adapter_hdi_src "${softbus_adapter_hdi_path}/common/softbus_adapter_wlan_extend.c" PARENT_SCOPE)
    set(adapter_hdi_lib wlan_proxy_1.1 PARENT_SCOPE)
endif()