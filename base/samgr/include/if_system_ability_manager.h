/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef INTERFACES_INNERKITS_SAMGR_INCLUDE_IF_SYSTEM_ABILITY_MANAGER_H
#define INTERFACES_INNERKITS_SAMGR_INCLUDE_IF_SYSTEM_ABILITY_MANAGER_H

#include <string>
#include <list>

#include "hilog/log.h"
#include "iremote_broker.h"
#include "iremote_object.h"
#include "iremote_proxy.h"
#include "samgr_ipc_interface_code.h"

namespace OHOS {
#define HILOGI(fmt, ...) printf("[Samgr]:" fmt "\n", ##__VA_ARGS__)
#define HILOGW(fmt, ...) printf("[Samgr]:" fmt "\n", ##__VA_ARGS__)
#define HILOGD(fmt, ...) printf("[Samgr]:" fmt "\n", ##__VA_ARGS__)
#define HILOGE(fmt, ...) printf("[Samgr]:" fmt "\n", ##__VA_ARGS__)

class ISystemAbilityManager : public IRemoteBroker
{
public:
    enum {
        SHEEFT_CRITICAL = 0,
        SHEEFT_HIGH,
        SHEEFT_NORMAL,
        SHEEFT_DEFAULT,
        SHEEFT_PROTO,
    };

    static const unsigned int DUMP_FLAG_PRIORITY_DEFAULT = 1 << SHEEFT_DEFAULT;

    /**
     * GetSystemAbility, Retrieve an existing ability, retrying and blocking for a few seconds if it doesn't exist.
     *
     * @param systemAbilityId, Need to obtain the said of sa.
     * @return nullptr indicates acquisition failure.
     */
    virtual sptr<IRemoteObject> GetSystemAbility(int32_t systemAbilityId) = 0;

    struct SAExtraProp {
        SAExtraProp() = default;
        SAExtraProp(bool isDistributed, unsigned int dumpFlags, const std::u16string &capability,
                    const std::u16string &permission)
        {
            this->isDistributed = isDistributed;
            this->dumpFlags = dumpFlags;
            this->capability = capability;
            this->permission = permission;
        }

        bool isDistributed = false;
        unsigned int dumpFlags = DUMP_FLAG_PRIORITY_DEFAULT;
        std::u16string capability;
        std::u16string permission;
    };

    /**
     * AddSystemAbility, add an ability to samgr.
     *
     * @param systemAbilityId, Need to add the said of sa.
     * @param ability, SA to be added.
     * @param extraProp, Additional parameters for sa, such as whether it is distributed.
     * @return ERR_OK indicates successful add.
     */
    virtual int32_t AddSystemAbility(int32_t systemAbilityId, const sptr<IRemoteObject> &ability,
                                     const SAExtraProp &extraProp = SAExtraProp(false, DUMP_FLAG_PRIORITY_DEFAULT, u"",
                                                                                u"")) = 0;

public:
    DECLARE_INTERFACE_DESCRIPTOR(u"OHOS.ISystemAbilityManager");

protected:
    static constexpr int32_t FIRST_SYS_ABILITY_ID = 0x00000001;
    static constexpr int32_t LAST_SYS_ABILITY_ID = 0x00ffffff;
    bool CheckInputSysAbilityId(int32_t sysAbilityId) const
    {
        if (sysAbilityId >= FIRST_SYS_ABILITY_ID && sysAbilityId <= LAST_SYS_ABILITY_ID) {
            return true;
        }
        return false;
    }
    static inline const std::u16string SAMANAGER_INTERFACE_TOKEN = u"ohos.samgr.accessToken";
};
}  // namespace OHOS

#endif  // !defined(INTERFACES_INNERKITS_SAMGR_INCLUDE_IF_SYSTEM_ABILITY_MANAGER_H )
