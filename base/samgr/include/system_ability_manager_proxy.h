/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef INTERFACES_INNERKITS_SAMGR_INCLUDE_SYSTEM_ABILITY_MANAGER_PROXY_H
#define INTERFACES_INNERKITS_SAMGR_INCLUDE_SYSTEM_ABILITY_MANAGER_PROXY_H

#include <string>
#include "if_system_ability_manager.h"

namespace OHOS {
class SystemAbilityManagerProxy : public IRemoteProxy<ISystemAbilityManager>
{
public:
    explicit SystemAbilityManagerProxy(const sptr<IRemoteObject> &impl) : IRemoteProxy<ISystemAbilityManager>(impl)
    {
    }
    ~SystemAbilityManagerProxy() = default;

    /**
     * GetSystemAbility, Retrieve an existing ability, retrying and blocking for a few seconds if it doesn't exist.
     *
     * @param systemAbilityId, Need to obtain the said of sa.
     * @return nullptr indicates acquisition failure.
     */
    sptr<IRemoteObject> GetSystemAbility(int32_t systemAbilityId) override;

    /**
     * AddSystemAbility, add an ability to samgr
     *
     * @param systemAbilityId, Need to add the said of sa.
     * @param ability, SA to be added.
     * @param extraProp, Additional parameters for sa, such as whether it is distributed.
     * @return ERR_OK indicates successful add.
     */
    int32_t AddSystemAbility(int32_t systemAbilityId, const sptr<IRemoteObject> &ability,
                             const SAExtraProp &extraProp) override;

private:
    int32_t MarshalSAExtraProp(const SAExtraProp &extraProp, MessageParcel &data) const;
    static inline BrokerDelegator<SystemAbilityManagerProxy> delegator_;
};
}  // namespace OHOS

#endif  // !defined(INTERFACES_INNERKITS_SAMGR_INCLUDE_SYSTEM_ABILITY_MANAGER_PROXY_H)
