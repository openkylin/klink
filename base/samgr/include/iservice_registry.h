/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef SERVICE_REGISTRY_INCLUDE_H
#define SERVICE_REGISTRY_INCLUDE_H

#include <mutex>
#include "ipc_types.h"
#include <iremote_broker.h>
#include "ipc_object_stub.h"
#include "iremote_proxy.h"
#include "system_ability_manager.h"

namespace OHOS {
class SystemAbilityManagerClient
{
public:
    /**
     * GetInstance, get SystemAbilityManagerClient instance.
     *
     * @return Get Single Instance Object.
     */
    static SystemAbilityManagerClient &GetInstance();

    /**
     * GetSystemAbilityManager, get system ability manager.
     *
     * @return Get systemAbilityManager_.
     */
    sptr<SystemAbilityManager> GetSystemAbilityManager();

private:
    SystemAbilityManagerClient() = default;
    ~SystemAbilityManagerClient() = default;

    sptr<SystemAbilityManager> systemAbilityManager_;
};
}  // namespace OHOS

#endif  // ZIPC_SERVICE_REGISTRY_INCLUDE_H