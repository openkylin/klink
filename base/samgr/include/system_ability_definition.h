/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef _SYSTEM_ABILITY_DEF_H
#define _SYSTEM_ABILITY_DEF_H

#ifdef __cplusplus
#    include <string>
#    include <map>

namespace OHOS {

enum {
    BUNDLE_MGR_SERVICE_SYS_ABILITY_ID = 401,
    SOFTBUS_SERVER_SA_ID = 4700,
    DEVICE_AUTH_SERVICE_ID = 4701,
};

static const std::map<int, std::string> saNameMap_ = {
    {BUNDLE_MGR_SERVICE_SYS_ABILITY_ID, "BundleMgr"},
    {SOFTBUS_SERVER_SA_ID, "DSoftbus"},
    {DEVICE_AUTH_SERVICE_ID, "DeviceAuthService"},
};

};  // namespace OHOS
#endif

#endif