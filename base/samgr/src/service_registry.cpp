/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#include "iservice_registry.h"

#include <unistd.h>

#include "errors.h"
#include "hilog/log.h"
#include "ipc_skeleton.h"
#include "ipc_types.h"
#include "iremote_broker.h"
#include "iremote_object.h"
#include "iremote_proxy.h"
#include "message_option.h"
#include "message_parcel.h"
#include "refbase.h"
#include "string_ex.h"

namespace OHOS {

namespace {
constexpr int32_t MASK = 0x100;
constexpr int32_t INTERFACE_TOKEN = 0;
constexpr int64_t SLEEP_TIME = 1;
constexpr int32_t RETRY_TIMES = 10;
}  // namespace

SystemAbilityManagerClient &SystemAbilityManagerClient::GetInstance()
{
    static auto instance = new SystemAbilityManagerClient();
    return *instance;
}

sptr<SystemAbilityManager> SystemAbilityManagerClient::GetSystemAbilityManager()
{
    return SystemAbilityManager::GetInstance();
}
}  // namespace OHOS
