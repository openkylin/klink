/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#include "system_ability_manager_proxy.h"

#include <unistd.h>
#include <vector>

#include "errors.h"
#include "ipc_types.h"
#include "iremote_object.h"
#include "message_option.h"
#include "message_parcel.h"
#include "refbase.h"
#include "string_ex.h"

using namespace std;
namespace OHOS {
namespace {
const int32_t RETRY_TIME_OUT_NUMBER = 10;
const int32_t SLEEP_INTERVAL_TIME = 100;
const int32_t SLEEP_ONE_MILLI_SECOND_TIME = 1000;
}  // namespace
sptr<IRemoteObject> SystemAbilityManagerProxy::GetSystemAbility(int32_t systemAbilityId)
{
    return nullptr;
}

int32_t SystemAbilityManagerProxy::AddSystemAbility(int32_t systemAbilityId, const sptr<IRemoteObject> &ability,
                                                    const SAExtraProp &extraProp)
{
    return ERR_OK;
}

int32_t SystemAbilityManagerProxy::MarshalSAExtraProp(const SAExtraProp &extraProp, MessageParcel &data) const
{
    if (!data.WriteBool(extraProp.isDistributed)) {
        HILOGW("MarshalSAExtraProp Write isDistributed failed!");
        return ERR_FLATTEN_OBJECT;
    }
    if (!data.WriteInt32(extraProp.dumpFlags)) {
        HILOGW("MarshalSAExtraProp Write dumpFlags failed!");
        return ERR_FLATTEN_OBJECT;
    }
    if (!data.WriteString16(extraProp.capability)) {
        HILOGW("MarshalSAExtraProp Write capability failed!");
        return ERR_FLATTEN_OBJECT;
    }
    if (!data.WriteString16(extraProp.permission)) {
        HILOGW("MarshalSAExtraProp Write defPermission failed!");
        return ERR_FLATTEN_OBJECT;
    }
    return ERR_OK;
}
}  // namespace OHOS
