/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#include "system_ability_manager.h"

#include <cinttypes>
#include <thread>
#include <unistd.h>

#include "accesstoken_kit.h"
#include "datetime_ex.h"
#include "directory_ex.h"
#include "errors.h"
#include "hitrace_meter.h"
#include "ipc_skeleton.h"
#include "parameter.h"
#include "string_ex.h"

#ifdef SUPPORT_DEVICE_MANAGER
#    include "device_manager.h"
using namespace OHOS::DistributedHardware;
#endif

using namespace std;

namespace OHOS {
namespace {
constexpr int32_t MAX_SA_FREQUENCY_COUNT = INT32_MAX - 1000000;
constexpr int32_t SHFIT_BIT = 32;
}  // namespace

std::mutex SystemAbilityManager::instanceLock;
sptr<SystemAbilityManager> SystemAbilityManager::instance;

SystemAbilityManager::SystemAbilityManager()
{
}

SystemAbilityManager::~SystemAbilityManager()
{
}

sptr<SystemAbilityManager> SystemAbilityManager::GetInstance()
{
    std::lock_guard<std::mutex> autoLock(instanceLock);
    if (instance == nullptr) {
        instance = new SystemAbilityManager;
    }
    return instance;
}

sptr<IRemoteObject> SystemAbilityManager::GetSystemAbility(int32_t systemAbilityId)
{
    HILOGD("%s called, systemAbilityId = %d", __func__, systemAbilityId);

    shared_lock<shared_mutex> readLock(abilityMapLock_);
    auto iter = abilityMap_.find(systemAbilityId);
    if (iter != abilityMap_.end()) {
        HILOGD("found service : %d.", systemAbilityId);
        return iter->second.remoteObj;
    }
    HILOGW("NOT found service : %d", systemAbilityId);
    return nullptr;
}

int32_t SystemAbilityManager::AddSystemAbility(int32_t systemAbilityId, const sptr<IRemoteObject> &ability,
                                               const SAExtraProp &extraProp)
{
    unique_lock<shared_mutex> writeLock(abilityMapLock_);
    auto saSize = abilityMap_.size();
    if (saSize >= MAX_SERVICES) {
        HILOGE("map size error, (Has been greater than %zu)", saSize);
        return ERR_INVALID_VALUE;
    }
    SAInfo saInfo;
    saInfo.remoteObj = ability;
    saInfo.isDistributed = extraProp.isDistributed;
    saInfo.capability = extraProp.capability;
    saInfo.permission = Str16ToStr8(extraProp.permission);
    abilityMap_[systemAbilityId] = std::move(saInfo);
    HILOGI("insert %d. size : %u", systemAbilityId, abilityMap_.size());
    return ERR_OK;
}

}  // namespace OHOS
