/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef HI_SYS_EVENT_H
#define HI_SYS_EVENT_H
#include "hisysevent_c.h"

#ifdef __cplusplus

#    include <iostream>
#    include <string>
#    include <sstream>
#    include <vector>

/*
 * Usage: define string macro "DOMAIN_MASKS" to disable one or more components.
 *     Method1: add macro in this header file, e.g.          #define DOMAIN_MASKS "AAFWK|APPEXECFWK|ACCOUNT"
 *     Method1: addd cflags in build.gn file, e.g.           -D DOMAIN_MASKS="AAFWK|APPEXECFWK|ACCOUNT"
 */
namespace OHOS {
namespace HiviewDFX {

class HiSysEvent
{
public:
    // system event domain list
    class Domain
    {
    public:
        static constexpr char DEVICE_AUTH[] = "DEVICE_AUTH";
    };

    enum EventType {
        FAULT = 1,      // system fault event
        STATISTIC = 2,  // system statistic event
        SECURITY = 3,   // system security event
        BEHAVIOR = 4    // system behavior event
    };
};

/**
 * @brief Macro interface for writing system event.
 * @param domain      event domain.
 * @param eventName   event name.
 * @param type        event type.
 * @return 0 means success,
 *     greater than 0 also means success but with some data ignored,
 *     less than 0 means failure.
 */
#    define HiSysEventWrite(domain, eventName, type, ...) ({})
}  // namespace HiviewDFX
}  // namespace OHOS

#endif  // __cplusplus
#endif  // HI_SYS_EVENT_H
