/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef HIVIEWDFX_HITRACECHAIN_C_H
#define HIVIEWDFX_HITRACECHAIN_C_H

#include <endian.h>
#include <stdarg.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct HiTraceIdStruct {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    uint64_t valid : 1;
    uint64_t ver : 3;
    uint64_t chainId : 60;

    uint64_t flags : 12;
    uint64_t spanId : 26;
    uint64_t parentSpanId : 26;
#elif __BYTE_ORDER == __BIG_ENDIAN
    uint64_t chainId : 60;
    uint64_t ver : 3;
    uint64_t valid : 1;

    uint64_t parentSpanId : 26;
    uint64_t spanId : 26;
    uint64_t flags : 12;
#else
#    error "ERROR: No BIG_LITTLE_ENDIAN defines."
#endif
} HiTraceIdStruct;

HiTraceIdStruct HiTraceChainBegin(const char *name, int flags);
void HiTraceChainEnd(const HiTraceIdStruct *pId);
void HiTraceChainSetId(const HiTraceIdStruct *pId);
HiTraceIdStruct HiTraceChainGetId(void);
void HiTraceChainClearId(void);
#ifdef __cplusplus
}
#endif

#endif  // HIVIEWDFX_HITRACECHAIN_C_H