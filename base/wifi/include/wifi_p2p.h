/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_C_P2P_H
#define OHOS_C_P2P_H

#include "wifi_error_code.h"
#include "wifi_p2p_config.h"

#ifdef __cplusplus
extern "C" {
#endif
typedef void (*P2pStateChangedCallback)(P2pState state);
typedef void (*P2pPersistentGroupsChangedCallback)(void);
typedef void (*P2pConnectionChangedCallback)(const WifiP2pLinkedInfo info);
typedef void (*P2pPeersChangedCallback)(WifiP2pDevice *devices, int len);

/**
 * @Description Get p2p enable status
 *
 * @param state - enable status
 * @return WifiErrorCode - operation result
 */
WifiErrorCode GetP2pEnableStatus(P2pState *state);
/**
 * @Description Get the Current Group object.
 *
 * @param groupInfo - the WifiP2pGroupInfo object
 * @return WifiErrorCode - operation result
 */
WifiErrorCode GetCurrentGroup(WifiP2pGroupInfo *groupInfo);
/**
 * @Description Remove a P2P Group.
 *
 * @return WifiErrorCode - operation result
 */
WifiErrorCode RemoveGroup();
/**
 * @Description register p2p state changed event
 *
 * @param callback - callback function
 * @return ErrCode - operation result
 */
WifiErrorCode RegisterP2pStateChangedCallback(const P2pStateChangedCallback callback);
/**
 * @Description register p2p persistent group change event
 *
 * @param callback - callback function
 * @return ErrCode - operation result
 */
WifiErrorCode RegisterP2pPersistentGroupsChangedCallback(const P2pPersistentGroupsChangedCallback callback);
/**
 * @Description register p2p connection change event
 *
 * @param callback - callback function
 * @return ErrCode - operation result
 */
WifiErrorCode RegisterP2pConnectionChangedCallback(const P2pConnectionChangedCallback callback);
/**
 * @Description register p2p peers change event
 *
 * @param callback - callback function
 * @return ErrCode - operation result
 */
WifiErrorCode RegisterP2pPeersChangedCallback(const P2pPeersChangedCallback callback);
/**
 * @Description Stop Wi-Fi P2P device search.
 *
 * @return WifiErrorCode - operation result
 */
WifiErrorCode StopDiscoverDevices();
#ifdef __cplusplus
}
#endif

#endif
