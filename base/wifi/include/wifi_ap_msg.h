/*
 * Copyright (C) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_WIFI_AP_MSG_H
#define OHOS_WIFI_AP_MSG_H
#include <cstdint>
#include <set>
#include <string>

namespace OHOS {
namespace Wifi {
#define AP_CHANNEL_DEFAULT 6
#define AP_CHANNEL_5G_DEFAULT 149
#define WIFI_BSSID_LENGTH 18
enum class ApState {
    AP_STATE_NONE = 0,
    AP_STATE_IDLE,
    AP_STATE_STARTING,
    AP_STATE_STARTED,
    AP_STATE_CLOSING,
    AP_STATE_CLOSED,
};
}  // namespace Wifi
}  // namespace OHOS
#endif