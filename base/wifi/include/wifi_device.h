/*
 * Copyright (C) 2020-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup wifiservice
 * @{
 *
 * @brief Provides functions for the Wi-Fi station and hotspot modes.
 *
 * You can use this module to enable and disable the Wi-Fi station or hotspot mode, connect to and disconnect from a
 * station or hotspot, query the station or hotspot status, and listen for events. \n
 *
 * @since 7
 */

/**
 * @file wifi_device.h
 *
 * @brief Provides capabilities to enable and disable the station mode, connect to and disconnect from a station,
 * query the station status, and listen for events.
 *
 * @since 7
 */

#ifndef WIFI_DEVICE_C_H
#    define WIFI_DEVICE_C_H
#    include "wifi_event.h"
#    include "station_info.h"
#    include "wifi_scan_info.h"
#    include "wifi_error_code.h"
#    include "wifi_linked_info.h"
#    include "wifi_device_config.h"

#    ifdef __cplusplus
extern "C" {
#    endif
/**
 * @brief Checks whether the station mode is enabled.
 *
 * @return Returns {@link WIFI_STA_ACTIVE} if the station mode is enabled; returns {@link WIFI_STA_NOT_ACTIVE}
 * otherwise.
 * @since 7
 */
int IsWifiActive(void);
/**
 * @brief Starts a Wi-Fi scan.
 *
 * @return Returns {@link WIFI_SUCCESS} if the Wi-Fi scan is started; returns an error code defined in
 * {@link WifiErrorCode} otherwise.
 * @since 7
 */
WifiErrorCode Scan(void);
/**
 * @brief Obtains an array of hotspots detected in a Wi-Fi scan.
 *
 * The array of hotspots can be obtained only after the Wi-Fi scan is complete. \n
 *
 * @param result Indicates the array of hotspots detected in a Wi-Fi scan. The array is requested and released by the
 * caller. The value must be greater than or equal to {@link WIFI_SCAN_HOTSPOT_LIMIT}.
 * @param size Indicates the size of the array.
 * @return Returns {@link WIFI_SUCCESS} if the array of hotspots detected in the Wi-Fi scan is obtained; returns an
 * error code defined in {@link WifiErrorCode} otherwise.
 * @since 7
 */
WifiErrorCode GetScanInfoList(WifiScanInfo *result, unsigned int *size);
/**
 * @brief Obtains all hotspot configurations.
 *
 * Hotspot configurations were added using {@link AddDeviceConfig}. \n
 *
 * @param result Indicates the array of all hotspot configurations. The array is requested and released by the caller.
 * The value must be greater than or equal to {@link WIFI_MAX_CONFIG_SIZE}.
 * @param size Indicates the size of the array.
 * @return Returns {@link WIFI_SUCCESS} if all hotspot configurations are obtained; returns an error code defined in
 * {@link WifiErrorCode} otherwise.
 * @since 7
 */
WifiErrorCode GetDeviceConfigs(WifiDeviceConfig *result, unsigned int *size);
/**
 * @brief Connect to a hotspot by config.
 *
 * @param config is device configuration to connect the Wi-Fi network.
 * @return Returns {@link WIFI_SUCCESS} if the hotspot is connected; returns an error code defined in
 * {@link WifiErrorCode} otherwise.
 * @since 7
 */
WifiErrorCode ConnectToDevice(const WifiDeviceConfig *config);
/**
 * @brief Disconnects this Wi-Fi connection.
 *
 * @return Returns {@link WIFI_SUCCESS} if this Wi-Fi connection is disconnected; returns an error code defined
 * in {@link WifiErrorCode} otherwise.
 * @since 7
 */
WifiErrorCode Disconnect(void);
/**
 * @brief Registers a callback for a specified Wi-Fi event.
 *
 * The registered callback will be invoked when the Wi-Fi event defined in {@link WifiEvent} occurs. \n
 *
 * @param event Indicates the event for which the callback is to be registered.
 * @return Returns {@link WIFI_SUCCESS} if the callback is registered successfully; returns an error code defined
 * in {@link WifiErrorCode} otherwise.
 * @since 7
 */
WifiErrorCode RegisterWifiEvent(WifiEvent *event);
/**
 * @brief Unregisters a callback previously registered for a specified Wi-Fi event.
 *
 * @param event Indicates the event for which the callback is to be unregistered.
 * @return Returns {@link WIFI_SUCCESS} if the callback is unregistered successfully; returns an error code defined
 * in {@link WifiErrorCode} otherwise.
 * @since 7
 */
WifiErrorCode UnRegisterWifiEvent(WifiEvent *event);
/**
 * @brief Obtains information about the connected hotspot.
 *
 * @param result Indicates the information about the connected hotspot.
 * @return Returns {@link WIFI_SUCCESS} if the information about the connected hotspot is obtained; returns an error
 * code defined in {@link WifiErrorCode} otherwise.
 * @since 7
 */
WifiErrorCode GetLinkedInfo(WifiLinkedInfo *result);
#    ifdef __cplusplus
}
#    endif

#endif  // WIFI_DEVICE_C_H
/** @} */
