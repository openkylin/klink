/*
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wifi_device.h"

int IsWifiActive(void)
{
    // todo
    return WIFI_STA_ACTIVE;
}

WifiErrorCode Scan(void)
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode GetScanInfoList(WifiScanInfo *result, unsigned int *size)
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode GetDeviceConfigs(WifiDeviceConfig *result, unsigned int *size)
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode ConnectToDevice(const WifiDeviceConfig *config)
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode Disconnect(void)
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode RegisterWifiEvent(WifiEvent *event)
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode UnRegisterWifiEvent(WifiEvent *event)
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode GetLinkedInfo(WifiLinkedInfo *result)
{
    // todo
    return WIFI_SUCCESS;
}