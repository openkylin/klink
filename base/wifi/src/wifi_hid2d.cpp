/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wifi_hid2d.h"

WifiErrorCode Hid2dGetChannelListFor5G(int *chanList, int len)
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode Hid2dSharedlinkIncrease(void)
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode Hid2dSharedlinkDecrease(void)
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode Hid2dCreateGroup(const int frequency, FreqType type)
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode Hid2dRemoveGcGroup(const char gcIfName[IF_NAME_LEN])
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode Hid2dGetRecommendChannel(const RecommendChannelRequest *request, RecommendChannelResponse *response)
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode Hid2dConnect(const Hid2dConnectConfig *config)
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode Hid2dRequestGcIp(const unsigned char gcMac[MAC_LEN], unsigned int ipAddr[IPV4_ARRAY_LEN])
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode Hid2dConfigIPAddr(const char ifName[IF_NAME_LEN], const IpAddrInfo *ipInfo)
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode Hid2dReleaseIPAddr(const char ifName[IF_NAME_LEN])
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode Hid2dGetSelfWifiCfgInfo(SelfCfgType cfgType, char cfgData[CFG_DATA_MAX_BYTES], int *getDatValidLen)
{
    // todo
    return WIFI_SUCCESS;
}

WifiErrorCode Hid2dSetPeerWifiCfgInfo(PeerCfgType cfgType, char cfgData[CFG_DATA_MAX_BYTES], int setDataValidLen)
{
    // todo
    return WIFI_SUCCESS;
}

int Hid2dIsWideBandwidthSupported(void)
{
    // todo
    return 1;
}