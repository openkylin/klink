/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#include "watchdog.h"

namespace OHOS {
namespace HiviewDFX {
Watchdog::Watchdog()
{
}

Watchdog::~Watchdog()
{
}
void Watchdog::RunOneShotTask(const std::string &name, Task &&task, uint64_t delay)
{
    // todo
    return;
}

void Watchdog::RunPeriodicalTask(const std::string &name, Task &&task, uint64_t interval, uint64_t delay)
{
    // todo
    return;
}

}  // end of namespace HiviewDFX
}  // end of namespace OHOS
