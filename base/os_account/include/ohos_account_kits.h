/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

/**
 * @addtogroup OhosAccount
 * @{
 *
 * @brief Provides ohos account management.
 *
 * Provides the capability to manage ohos accounts.
 *
 * @since 7.0
 * @version 7.0
 */

/**
 * @file ohos_account_kits.h
 *
 * @brief Declares ohos account interfaces.
 *
 * @since 7.0
 * @version 7.0
 */
#ifndef BASE_ACCOUNT_OHOS_ACCOUNT_KITS_H
#define BASE_ACCOUNT_OHOS_ACCOUNT_KITS_H
#include <string>
#include "account_info.h"
#include "nocopyable.h"

namespace OHOS {
namespace AccountSA {
/**
 * Interfaces for ohos account subsystem.
 */
class OhosAccountKits
{
public:
    virtual ~OhosAccountKits() = default;
    DISALLOW_COPY_AND_MOVE(OhosAccountKits);

    /**
     * Get instance of ohos account manager.
     *
     * @return Instance of ohos account manager.
     */
    static OhosAccountKits &GetInstance();

    /**
     * Query OHOS Account Info.
     *
     * @param VOID.
     * @return Return a pair of operation result and ohos account info.
     */
    std::pair<bool, OhosAccountInfo> QueryOhosAccountInfo();

protected:
    OhosAccountKits() = default;
};
}  // namespace AccountSA
}  // namespace OHOS

#endif  // BASE_ACCOUNT_OHOS_ACCOUNT_KITS_H