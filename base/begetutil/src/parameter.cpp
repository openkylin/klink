/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "parameter.h"
#include "securec.h"
#include <fcntl.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <uuid/uuid.h>

#define DEFAULT_UDID "ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00"
#define UDID_BUF_LEN 128
#define UDID_PATH DSOFTBUS_DATA_PATH "/service/el1/public"

#define PARAMETER_TAG "[Parameter]"

/*
 * 函数名称 checkMkdir
 * 函数功能 创建文件
 * 函数描述 传入一个文件所在的绝对路径 创建文件
 * 入    参 sPath 文件绝对路径
 * 返    回 SUCCESS return 0
 *          FAIL    return 非0
 */
int checkMkdir(char *sPath)
{
    int iRet = 0;            /* 函数返回值 */
    char sFilePath[256 + 1]; /* 创建文件路径 */
    char sPathTmp[256 + 1];  /* 临时文件路径 */
    char *pDir = NULL;

    struct stat stFileStat;

    memset(sFilePath, 0x00, sizeof(sFilePath));
    memset(sPathTmp, 0x00, sizeof(sPathTmp));
    memset(&stFileStat, 0x00, sizeof(stFileStat));

    memcpy(sFilePath, sPath, sizeof(sFilePath));

    pDir = strtok(sFilePath, "/");
    strcat(sPathTmp, "/");
    strcat(sPathTmp, pDir);
    strcat(sPathTmp, "/");

    memset(&stFileStat, 0x00, sizeof(stFileStat));
    stat(sPathTmp, &stFileStat);

    if (!S_ISDIR(stFileStat.st_mode)) {
        iRet = mkdir(sPathTmp, S_IWUSR | S_IRUSR | S_IXUSR);
        if (-1 == iRet) {
            printf("%s: mkdir path [%s] error [%ld]\n", PARAMETER_TAG, sPathTmp, iRet);
            return iRet;
        }
    }

    while (NULL != (pDir = strtok(NULL, "/"))) {
        strcat(sPathTmp, pDir);
        strcat(sPathTmp, "/");

        memset(&stFileStat, 0x00, sizeof(stFileStat));
        stat(sPathTmp, &stFileStat);

        if (!S_ISDIR(stFileStat.st_mode)) {
            iRet = mkdir(sPathTmp,
                         S_IWUSR | S_IRUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH | S_IXOTH);
            if (-1 == iRet) {
                printf("%s: mkdir path [%s] error [%ld]\n", PARAMETER_TAG, sPathTmp, iRet);
                return iRet;
            }
        }
    }

    return iRet;
}

int GetParameter(const char *key, const char *def, char *value, uint32_t len)
{
    // todo
    return 0;
}

const char *GetDeviceType(void)
{
    return DEFAULT_DEVICE_TYPE_STR;
}

int GetDevUdid(char *udid, int size)
{
    if (access(UDID_PATH, F_OK) == -1) {
        if (checkMkdir(UDID_PATH) != 0) {
            return -1;
        }
    }

    int32_t fd;
    char uuidFilePath[size];

    if (strncpy_s(uuidFilePath, size, UDID_PATH, strlen(UDID_PATH)) != EOK) {
        return -1;
    }
    if (strcat_s(uuidFilePath, size, "/uuid") != EOK) {
        return -1;
    }
    if (access(uuidFilePath, F_OK) == -1) {
        if ((fd = open(uuidFilePath, O_RDWR | O_CREAT)) > 0) {
            memset(udid, 0, size);
            uuid_t uuid_key;
            uuid_generate(uuid_key);
            uuid_unparse(uuid_key, udid);
            if (write(fd, udid, strlen(udid)) != strlen(udid)) {
                printf("%s fail: write udid file error! %s", PARAMETER_TAG, strerror(errno));
                close(fd);
                return -1;
            }
            close(fd);
            return 0;
        }
    } else {
        if ((fd = open(uuidFilePath, O_RDONLY)) > 0) {
            memset(udid, 0, size);
            int ret = read(fd, udid, size);
            if (ret < 0) {
                printf("%s fail: read udid file error! %s", PARAMETER_TAG, strerror(errno));
                close(fd);
                return -1;
            }
            close(fd);
            return 0;
        }
    }

    printf("%s fail: open udid file error! %s", PARAMETER_TAG, strerror(errno));
    return -1;
}
