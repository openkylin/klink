/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef OHOS_ABILITY_RUNTIME_DATA_ABILITY_OBSERVER_STUB_H
#define OHOS_ABILITY_RUNTIME_DATA_ABILITY_OBSERVER_STUB_H

#include <iremote_broker.h>
#include <iremote_stub.h>
#include <list>
#include "uri.h"
#include "message_parcel.h"

namespace OHOS {
namespace AAFwk {
struct ChangeInfo {
    enum ChangeType : uint32_t {
        INSERT = 0,
        DELETE,
        UPDATE,
        OTHER,
        INVAILD,
    };

    static bool Marshalling(const ChangeInfo &input, MessageParcel &data);
    static bool Unmarshalling(ChangeInfo &output, MessageParcel &data);

    ChangeType changeType_ = INVAILD;
    mutable std::list<Uri> uris_ = {};
    void *data_ = nullptr;
    uint32_t size_ = 0;
    static constexpr int LIST_MAX_COUNT = 3000;
};
class IDataAbilityObserver : public OHOS::IRemoteBroker
{
public:
    DECLARE_INTERFACE_DESCRIPTOR(u"ohos.aafwk.DataAbilityObserver");

    enum {
        TRANS_HEAD,
        DATA_ABILITY_OBSERVER_CHANGE = TRANS_HEAD,
        DATA_ABILITY_OBSERVER_CHANGE_EXT,
        DATA_ABILITY_OBSERVER_CHANGE_PREFERENCES,
        TRANS_BUTT,
    };

    /**
     * @brief Called back to notify that the data being observed has changed.
     *
     * @param uri Indicates the path of the data to operate.
     */
    virtual void OnChange() = 0;

    /**
     * @brief Called back to notify that the data being observed has changed.
     *
     * @param changeInfo Indicates the info of the data to operate.
     */
    virtual void OnChangeExt(const ChangeInfo &changeInfo)
    {
        return;
    }

    /**
     * @brief Called back to notify that the data being observed has changed.
     *
     * @param key Indicates the key that has changed.
     */
    virtual void OnChangePreferences(const std::string &key)
    {
        return;
    }
};

class DataAbilityObserverStub : public IRemoteStub<IDataAbilityObserver>
{
public:
    DataAbilityObserverStub();
    virtual ~DataAbilityObserverStub();
    int OnRemoteRequest(uint32_t code, MessageParcel &data, MessageParcel &reply, MessageOption &option) override;
};
}  // namespace AAFwk
}  // namespace OHOS
#endif  // OHOS_ABILITY_RUNTIME_DATA_ABILITY_OBSERVER_STUB_H
