/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _HILOG_H
#define _HILOG_H

#include <stdio.h>
// #include <android/log.h>
//  Log type
typedef enum {
    LOG_TYPE_MIN = 0,
    // Log to kmsg, only used by init phase.
    LOG_INIT = 1,
    // Used by core service, framework.
    LOG_CORE = 3,
    LOG_TYPE_MAX
} LogType;

// Log level
typedef enum {
    LOG_LEVEL_MIN = 0,
    LOG_DEBUG = 3,
    LOG_INFO = 4,
    LOG_WARN = 5,
    LOG_ERROR = 6,
    LOG_FATAL = 7,
    LOG_LEVEL_MAX,
} LogLevel;

#define HILOG_DEBUG(type, fmt, ...) printf(fmt "\n", ##__VA_ARGS__)
#define HILOG_INFO(type, fmt, ...) printf(fmt "\n", ##__VA_ARGS__)
#define HILOG_WARN(type, fmt, ...) printf(fmt "\n", ##__VA_ARGS__)
#define HILOG_ERROR(type, fmt, ...) printf(fmt "\n", ##__VA_ARGS__)

// #define HILOG_DEBUG(type, fmt, ...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, fmt, ##__VA_ARGS__)
// #define HILOG_INFO(type, fmt, ...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG, fmt, ##__VA_ARGS__)
// #define HILOG_WARN(type, fmt, ...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, fmt, ##__VA_ARGS__)
// #define HILOG_ERROR(type, fmt, ...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, fmt, ##__VA_ARGS__)

#endif
