/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef DATASHARE_HELPER_H
#define DATASHARE_HELPER_H

#include <iostream>
#include <vector>
#include <memory>
#include <atomic>
#include <string>

#include "data_ability_observer_stub.h"
#include "datashare_predicates.h"
#include "datashare_result_set.h"
#include "uri.h"
#include "refbase.h"
#include "iremote_object.h"

using Uri = OHOS::Uri;
using namespace std;

namespace OHOS {
namespace DataShare {
using string = std::string;

class DatashareBusinessError;

class DataShareHelper : public enable_shared_from_this<DataShareHelper>
{
public:
    /**
     * @brief Destructor.
     */
    virtual ~DataShareHelper() = default;

    /**
     * @brief You can use this method to specify the Uri of the data to operate and set the binding relationship
     * between the ability using the Data template (data share for short) and the associated client process in
     * a DataShareHelper instance.
     *
     * @param token Indicates the System token.
     * @param strUri Indicates the database table or disk file to operate.
     *
     * @return Returns the created DataShareHelper instance.
     */
    static shared_ptr<DataShareHelper> Creator(const sptr<IRemoteObject> &token, const string &strUri);

    /**
     * @brief Query records from the database.
     *
     * @param uri Indicates the path of data to query.
     * @param predicates Indicates filter criteria. You should define the processing logic when this parameter is null.
     * @param columns Indicates the columns to query. If this parameter is null, all columns are queried.
     * @param businessError Indicates the error by query.
     *
     * @return Returns the query result.
     */
    virtual shared_ptr<DataShareResultSet> Query(Uri &uri, const DataSharePredicates &predicates,
                                                 vector<string> &columns,
                                                 DatashareBusinessError *businessError = nullptr) = 0;

    /**
     * @brief Registers an observer to DataObsMgr specified by the given Uri.
     *
     * @param uri, Indicates the path of the data to operate.
     * @param dataObserver, Indicates the IDataAbilityObserver object.
     */
    virtual void RegisterObserver(const Uri &uri, const sptr<AAFwk::DataAbilityObserverStub> &dataObserver) = 0;
};
}  // namespace DataShare
}  // namespace OHOS
#endif  // DATASHARE_HELPER_H