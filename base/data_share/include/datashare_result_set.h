/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef DATASHARE_RESULT_SET_H
#define DATASHARE_RESULT_SET_H

#include <string>

namespace OHOS {
namespace DataShare {
/**
 * This module provides data resultsets.
 */
class DataShareResultSet
{
public:
    DataShareResultSet(){};
    ~DataShareResultSet(){};
    /**
     * @brief Get the data whose value type is string from the database according to the columnIndex.
     *
     * @param columnIndex the zero-based index of the target column.
     * @param value Indicates the value of columnIndex data to put or update.
     *
     * @return Return the value of the requested column as a String.
     */
    int GetString(int columnIndex, std::string &value);
    /**
     * @return Return the numbers of rows in the result set.
     */
    int GetRowCount(int &count);
    /**
     * Move the cursor to the first row.
     *
     * return whether the requested move succeeded.
     */
    int GoToFirstRow();
    /**
     * Returns the zero-based index for the given column name.
     *
     * param columnName the name of the column.
     * return the column index for the given column, or -1 if
     *     the column does not exist.
     */
    int GetColumnIndex(const std::string &columnName, int &columnIndex);
};
}  // namespace DataShare
}  // namespace OHOS

#endif  // DATASHARE_RESULT_SET_H