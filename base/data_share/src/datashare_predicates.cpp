/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#include "datashare_predicates.h"

namespace OHOS {
namespace DataShare {

/**
 * @brief Constructor.
 */
DataSharePredicates::DataSharePredicates()
{
}
/**
 * @brief Destructor.
 */
DataSharePredicates::~DataSharePredicates()
{
}
/**
 * @brief The EqualTo of the predicate.
 *
 * @param field Indicates the target field.
 * @param value Indicates the queried value.
 */
DataSharePredicates *DataSharePredicates::EqualTo(const std::string &field, const std::string &value)
{
    return this;
}

}  // namespace DataShare
}  // namespace OHOS