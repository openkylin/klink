/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#include "datashare_helper.h"

namespace OHOS {
namespace DataShare {
/**
 * @brief You can use this method to specify the Uri of the data to operate and set the binding relationship
 * between the ability using the Data template (data share for short) and the associated client process in
 * a DataShareHelper instance.
 *
 * @param token Indicates the System token.
 * @param strUri Indicates the database table or disk file to operate.
 *
 * @return Returns the created DataShareHelper instance.
 */
shared_ptr<DataShareHelper> DataShareHelper::Creator(const sptr<IRemoteObject> &token, const string &strUri)
{
    // todo
    return nullptr;
}

shared_ptr<DataShareResultSet> DataShareHelper::Query(Uri &uri, const DataSharePredicates &predicates,
                                                      vector<string> &columns, DatashareBusinessError *businessError)
{
    // todo
    return nullptr;
}

void DataShareHelper::RegisterObserver(const Uri &uri, const sptr<AAFwk::DataAbilityObserverStub> &dataObserver)
{
    // todo
}

}  // namespace DataShare
}  // namespace OHOS