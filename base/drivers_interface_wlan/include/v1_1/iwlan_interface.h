/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef OHOS_HDI_WLAN_V1_1_IWLANINTERFACE_H
#define OHOS_HDI_WLAN_V1_1_IWLANINTERFACE_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
typedef enum {
    HDF_SUCCESS = 0,  /**< The operation is successful. */
    HDF_FAILURE = -1, /**< Failed to invoke the OS underlying function. */
} HDF_STATUS;
struct MeasChannelParam {
    int32_t channelId;
    int32_t measTime;
} __attribute__((aligned(8)));

struct MeasChannelResult {
    int32_t channelId;
    int32_t chload;
    int32_t noise;
} __attribute__((aligned(8)));

struct IWlanInterface {
    int32_t (*StartChannelMeas)(struct IWlanInterface *self, const char *ifName,
                                const struct MeasChannelParam *measChannelParam);

    int32_t (*GetChannelMeasResult)(struct IWlanInterface *self, const char *ifName,
                                    struct MeasChannelResult *measChannelResult);
};
struct IWlanInterface *IWlanInterfaceGetInstance(const char *serviceName, bool isStub);
void IWlanInterfaceReleaseInstance(const char *serviceName, struct IWlanInterface *instance, bool isStub);
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif  // OHOS_HDI_WLAN_V1_1_IWLANINTERFACE_H