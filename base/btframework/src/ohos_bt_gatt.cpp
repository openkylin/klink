/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#include "ohos_bt_gatt.h"

#ifdef __cplusplus
extern "C" {
#endif

using namespace std;

namespace OHOS {
namespace Bluetooth {

int BleStopAdv(int advId)
{
    // todo
    return 0;
}

int BleStopScan(int32_t scannerId)
{
    // todo
    return 0;
}

int BleGattRegisterCallbacks(BtGattCallbacks *func)
{
    // todo
    return 0;
}

int BleRegisterScanCallbacks(BleScanCallbacks *func, int32_t *scannerId)
{
    // todo
    return 0;
}

int BleDeregisterScanCallbacks(int32_t scannerId)
{
    // todo
    return true;
}

int SetLpDeviceAdvParam(int duration, int maxExtAdvEvents, int window, int interval, int advHandle)
{
    // todo
    return true;
}

int SetScanReportChannelToLpDevice(int32_t scannerId, bool enable)
{
    // todo
    return true;
}

int EnableSyncDataToLpDevice(void)
{
    // todo
    return true;
}

int DisableSyncDataToLpDevice(void)
{
    // todo
    return true;
}

int GetAdvHandle(int advId, int *advHandle)
{
    // todo
    return true;
}

bool IsLpDeviceAvailable(void)
{
    // todo
    return true;
}

int SetLpDeviceParam(const BtLpDeviceParam *lpDeviceParam)
{
    // todo
    return 0;
}

}  // namespace Bluetooth
}  // namespace OHOS
#ifdef __cplusplus
}
#endif
/** @} */
