/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#include "ohos_bt_gatt_server.h"

#ifdef __cplusplus
extern "C" {
#endif

using namespace std;

namespace OHOS {
namespace Bluetooth {

int BleGattsRegister(BtUuid appUuid)
{
    // todo
    return 0;
}

int BleGattsUnRegister(int serverId)
{
    // todo
    return 0;
}

int BleGattsDisconnect(int serverId, BdAddr bdAddr, int connId)
{
    // todo
    return 0;
}

int BleGattsAddService(int serverId, BtUuid srvcUuid, bool isPrimary, int number)
{
    // todo
    return 0;
}

int BleGattsAddCharacteristic(int serverId, int srvcHandle, BtUuid characUuid, int properties, int permissions)
{
    // todo
    return 0;
}

int BleGattsAddDescriptor(int serverId, int srvcHandle, BtUuid descUuid, int permissions)
{
    // todo
    return 0;
}

int BleGattsStartService(int serverId, int srvcHandle)
{
    // todo
    return 0;
}

int BleGattsStopService(int serverId, int srvcHandle)
{
    // todo
    return 0;
}

int BleGattsDeleteService(int serverId, int srvcHandle)
{
    // todo
    return 0;
}

int BleGattsSendResponse(int serverId, GattsSendRspParam *param)
{
    // todo
    return 0;
}

int BleGattsSendIndication(int serverId, GattsSendIndParam *param)
{
    // todo
    return 0;
}

int BleGattsRegisterCallbacks(BtGattServerCallbacks *func)
{
    // todo
    return 0;
}

}  // namespace Bluetooth
}  // namespace OHOS

#ifdef __cplusplus
}
#endif
/** @} */
