/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#include "ohos_bt_gap.h"
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

using namespace std;

namespace OHOS {
namespace Bluetooth {

bool EnableBle(void)
{
    // todo
    return true;
}

bool DisableBle(void)
{
    // todo
    return true;
}

bool IsBleEnabled()
{
    // todo
    return true;
}

bool GetLocalAddr(unsigned char *mac, unsigned int len)
{
    // todo
    return true;
}

bool PairRequestReply(const BdAddr *bdAddr, int transport, bool accept)
{
    // todo
    return true;
}

bool SetDevicePairingConfirmation(const BdAddr *bdAddr, int transport, bool accept)
{
    // todo
    return true;
}

int GapRegisterCallbacks(BtGapCallBacks *func)
{
    // todo
    return 0;
}

}  // namespace Bluetooth
}  // namespace OHOS
#ifdef __cplusplus
}
#endif
