/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#include "ohos_bt_spp.h"

#ifdef __cplusplus
extern "C" {
#endif

using namespace std;

namespace OHOS {
namespace Bluetooth {

int SppServerCreate(BtCreateSocketPara *socketPara, const char *name, unsigned int len)
{
    // todo
    return 0;
}

int SppServerClose(int serverId)
{
    // todo
    return 0;
}

int SppDisconnect(int clientId)
{
    // todo
    return 0;
}

bool IsSppConnected(int clientId)
{
    // todo
    return 0;
}

int SppServerAccept(int serverId)
{
    // todo
    return 0;
}

int SppGetRemoteAddr(int clientId, BdAddr *remoteAddr)
{
    // todo
    return 0;
}

int SppRead(int clientId, char *buf, const unsigned int bufLen)
{
    // todo
    return 0;
}

int SppWrite(int clientId, const char *data, const unsigned int len)
{
    // todo
    return 0;
}

}  // namespace Bluetooth
}  // namespace OHOS
#ifdef __cplusplus
}
#endif