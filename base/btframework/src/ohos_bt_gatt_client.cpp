/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#include "ohos_bt_gatt_client.h"

#ifdef __cplusplus
extern "C" {
#endif

using namespace std;

namespace OHOS {
namespace Bluetooth {
int BleGattcRegister(BtUuid appUuid)
{
    // todo
    return 0;
}

int BleGattcUnRegister(int clientId)
{
    // todo
    return 0;
}

int BleGattcSetFastestConn(int clientId, bool fastestConnFlag)
{
    // todo
    return 0;
}

int BleGattcConnect(int clientId, BtGattClientCallbacks *func, const BdAddr *bdAddr, bool isAutoConnect,
                    BtTransportType transport)
{
    // todo
    return 0;
}

int BleGattcSetPriority(int clientId, const BdAddr *bdAddr, BtGattPriority priority)
{
    // todo
    return 0;
}

int BleGattcDisconnect(int clientId)
{
    // todo
    return 0;
}

int BleGattcSearchServices(int clientId)
{
    // todo
    return 0;
}

bool BleGattcGetService(int clientId, BtUuid serviceUuid)
{
    // todo
    return 0;
}

int BleGattcWriteCharacteristic(int clientId, BtGattCharacteristic characteristic, BtGattWriteType writeType, int len,
                                const char *value)
{
    // todo
    return 0;
}
int BleGattcConfigureMtuSize(int clientId, int mtuSize)
{
    // todo
    return 0;
}

int BleGattcRegisterNotification(int clientId, BtGattCharacteristic characteristic, bool enable)
{
    // todo
    return 0;
}

}  // namespace Bluetooth
}  // namespace OHOS
#ifdef __cplusplus
}
#endif
/** @} */