/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef OHOS_BT_DEF_H
#    define OHOS_BT_DEF_H
/**
 * @brief Defines the address length of a Bluetooth device.
 *
 */
#    define OHOS_BD_ADDR_LEN 6

/**
 * @brief Defines the device id length
 *
 */
#    define OHOS_ACTIVE_DEVICE_ID_LEN 8

typedef enum {
    OHOS_STATE_CONNECTING = 0x00,
    OHOS_STATE_CONNECTED,
    OHOS_STATE_DISCONNECTING,
    OHOS_STATE_DISCONNECTED,
} BtConnectState;

/**
 * @brief Enumerates Bluetooth statuses.
 *
 * @since 6
 */
typedef enum {
    /** Success */
    OHOS_BT_STATUS_SUCCESS = 0x00,
    /** Failure */
    OHOS_BT_STATUS_FAIL,
    /** Bluetooth not ready */
    OHOS_BT_STATUS_NOT_READY,
    /** Insufficient memory */
    OHOS_BT_STATUS_NOMEM,
    /** System busy */
    OHOS_BT_STATUS_BUSY,
    /** Operation completed */
    OHOS_BT_STATUS_DONE,
    /** Bluetooth not supported by the current version or device */
    OHOS_BT_STATUS_UNSUPPORTED,
    /** Invalid parameters */
    OHOS_BT_STATUS_PARM_INVALID,
    /** Request unhandled */
    OHOS_BT_STATUS_UNHANDLED,
    /** Authentication failure */
    OHOS_BT_STATUS_AUTH_FAILURE,
    /** Remote device shut down */
    OHOS_BT_STATUS_RMT_DEV_DOWN,
    /** Authentication rejected */
    OHOS_BT_STATUS_AUTH_REJECTED
} BtStatus;

/**
 * @brief Enumerates types of characteristic and descriptor write operations performed by the GATT client.
 *
 * @since 6
 */
typedef enum {
    /** Write operation without requiring a response from the server */
    OHOS_GATT_WRITE_NO_RSP = 0x01,
    /** Write operation requiring a response from the server */
    OHOS_GATT_WRITE_DEFAULT = 0x02,
    /** Prepare write requiring a response from the server */
    OHOS_GATT_WRITE_PREPARE = 0x03,
    /** Write operation with an authentication signature */
    OHOS_GATT_WRITE_SIGNED = 0x04
} BtGattWriteType;

/**
 * @brief Defines the Bluetooth address of the device.
 *
 * @since 6
 */
typedef struct {
    /** Bluetooth address */
    unsigned char addr[OHOS_BD_ADDR_LEN];
} BdAddr;

/**
 * @brief Defines the UUID.
 *
 * @since 6
 */
typedef struct {
    /** UUID length */
    unsigned char uuidLen;
    /** UUID field */
    char *uuid;
} BtUuid;

#endif
/** @} */
