/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef FOUNDATION_EVENT_CESFWK_KITS_NATIVE_INCLUDE_COMMON_EVENT_SUPPORT_H
#define FOUNDATION_EVENT_CESFWK_KITS_NATIVE_INCLUDE_COMMON_EVENT_SUPPORT_H

#include <string>

namespace OHOS {
namespace EventFwk {
class CommonEventSupport
{
public:
    static const std::string COMMON_EVENT_BOOT_COMPLETED;
    static const std::string COMMON_EVENT_SCREEN_OFF;
    static const std::string COMMON_EVENT_SCREEN_ON;
    static const std::string COMMON_EVENT_WIFI_CONN_STATE;
    static const std::string COMMON_EVENT_WIFI_POWER_STATE;
    static const std::string COMMON_EVENT_WIFI_HOTSPOT_STATE;
};

}  // namespace EventFwk
}  // namespace OHOS

#endif  // FOUNDATION_EVENT_CESFWK_KITS_NATIVE_INCLUDE_COMMON_EVENT_SUPPORT_H