/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef FOUNDATION_EVENT_CESFWK_KITS_NATIVE_INCLUDE_COMMON_EVENT_SUBSCRIBER_H
#define FOUNDATION_EVENT_CESFWK_KITS_NATIVE_INCLUDE_COMMON_EVENT_SUBSCRIBER_H

#include <string>
#include "want.h"
#include "parcel.h"

namespace OHOS {
namespace EventFwk {
using Want = OHOS::AAFwk::Want;
class MatchingSkills : public Parcelable
{
public:
    MatchingSkills();

    MatchingSkills(const MatchingSkills &matchingSkills);

    ~MatchingSkills();
    /**
     * Marshals this MatchingSkills object into a Parcel.
     *
     * @param parcel Indicates specified Parcel object.
     * @return Returns the marshalling result.
     */
    virtual bool Marshalling(Parcel &parcel) const override;
    /**
     * Adds an event to this MatchingSkills object.
     *
     * @param event Indicates the event.
     */
    void AddEvent(const std::string &event);
};

class CommonEventSubscribeInfo : public Parcelable
{
public:
    /**
     * A constructor used to create a CommonEventSubscribeInfo instance
     * with the matchingSkills parameters passed.
     *
     * @param matchingSkills Indicates the matching skills.
     */
    explicit CommonEventSubscribeInfo(const MatchingSkills &matchingSkills);

    ~CommonEventSubscribeInfo();

    /**
     * Marshals a subscriber info object into a Parcel.
     *
     * @param parcel Indicates specified Parcel object.
     * @return Returns true if success; false otherwise.
     */
    virtual bool Marshalling(Parcel &parcel) const override;
};

class CommonEventSubscriber
{
public:
    /**
     * A constructor used to create a CommonEventSubscriber instance with the
     * subscribeInfo parameter passed.
     *
     * @param subscribeInfo Indicates the subscribeInfo
     */
    CommonEventSubscriber(const CommonEventSubscribeInfo &subscribeInfo);

    ~CommonEventSubscriber();
};
}  // namespace EventFwk
}  // namespace OHOS

#endif  // FOUNDATION_EVENT_CESFWK_KITS_NATIVE_INCLUDE_COMMON_EVENT_SUBSCRIBER_H
