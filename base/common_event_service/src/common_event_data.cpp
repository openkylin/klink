/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#include "common_event_data.h"

#include "message_parcel.h"
#include "string_ex.h"

namespace OHOS {
namespace EventFwk {
CommonEventData::CommonEventData() : code_(0)
{
    // todo
}

CommonEventData::CommonEventData(const Want &want) : want_(want), code_(0)
{
    // todo
}

CommonEventData::CommonEventData(const Want &want, const int32_t &code, const std::string &data)
    : want_(want), code_(code), data_(data)
{
    // todo
}

CommonEventData::~CommonEventData()
{
    // todo
}

int32_t CommonEventData::GetCode() const
{
    return code_;
}

const Want &CommonEventData::GetWant() const
{
    return want_;
}
}  // namespace EventFwk
}  // namespace OHOS