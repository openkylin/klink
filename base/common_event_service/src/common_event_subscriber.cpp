/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#include "common_event_subscriber.h"

namespace OHOS {
namespace EventFwk {
MatchingSkills::MatchingSkills()
{
}

MatchingSkills::MatchingSkills(const MatchingSkills &matchingSkills)
{
}

MatchingSkills::~MatchingSkills()
{
}

bool MatchingSkills::Marshalling(Parcel &parcel) const
{
    // todo
    return true;
}

void MatchingSkills::AddEvent(const std::string &event)
{
    // todo
    return;
}

CommonEventSubscribeInfo::CommonEventSubscribeInfo(const MatchingSkills &matchingSkills)
{
    // todo
}

CommonEventSubscribeInfo::~CommonEventSubscribeInfo()
{
    // todo
}

bool CommonEventSubscribeInfo::Marshalling(Parcel &parcel) const
{
    // todo
    return true;
}

CommonEventSubscriber::CommonEventSubscriber(const CommonEventSubscribeInfo &subscribeInfo)
{
    // todo
}

CommonEventSubscriber::~CommonEventSubscriber()
{
    // todo
}

}  // namespace EventFwk
}  // namespace OHOS