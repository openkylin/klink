/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */
#include "common_event_support.h"

namespace OHOS {
namespace EventFwk {
const std::string CommonEventSupport::COMMON_EVENT_BOOT_COMPLETED = "usual.event.BOOT_COMPLETED";
const std::string CommonEventSupport::COMMON_EVENT_SCREEN_OFF = "usual.event.SCREEN_OFF";
const std::string CommonEventSupport::COMMON_EVENT_SCREEN_ON = "usual.event.SCREEN_ON";
const std::string CommonEventSupport::COMMON_EVENT_WIFI_CONN_STATE = "usual.event.wifi.CONN_STATE";
const std::string CommonEventSupport::COMMON_EVENT_WIFI_POWER_STATE = "usual.event.wifi.POWER_STATE";
const std::string CommonEventSupport::COMMON_EVENT_WIFI_HOTSPOT_STATE = "usual.event.wifi.HOTSPOT_STATE";

}  // namespace EventFwk
}  // namespace OHOS