#ifndef _IPC_REMOTE_OBJECT_H_
#define _IPC_REMOTE_OBJECT_H_

#include "message_parcel.h"
#include "message_option.h"
#include "ipc_types.h"

namespace OHOS {

class IRemoteObject
    : public virtual Parcelable
    , public virtual RefBase
{
public:
    class DeathRecipient : public RefBase
    {
    public:
        virtual void OnRemoteDied(const wptr<IRemoteObject> &object)
        {
        }
    };

    virtual int SendRequest(uint32_t code, MessageParcel &data, MessageParcel &reply, MessageOption &option,
                            int32_t pid = -1);
    virtual bool AddDeathRecipient(const sptr<DeathRecipient> &recipient);
    virtual bool RemoveDeathRecipient(const sptr<DeathRecipient> &recipient);
    virtual bool Marshalling(Parcel &parcel) const override;
    virtual int Dump(int fd, const std::vector<std::u16string> &args);

protected:
    virtual ~IRemoteObject() = default;
};

}  // namespace OHOS

#endif  // _IPC_REMOTE_OBJECT_H_
