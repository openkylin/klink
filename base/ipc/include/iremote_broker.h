#ifndef _IREMOTE_BROKER_H
#define _IREMOTE_BROKER_H

#ifdef __cplusplus

#    include "refbase.h"
#    include "iremote_object.h"

namespace OHOS {
class IRemoteBroker;
template <typename T> class BrokerCreator
{
public:
    BrokerCreator() = default;
    ~BrokerCreator() = default;
    sptr<IRemoteBroker> operator()(const sptr<IRemoteObject> &object)
    {
        T *proxy = new (std::nothrow) T(object);
        if (proxy != nullptr) {
            return static_cast<IRemoteBroker *>(proxy);
        }
        return nullptr;
    };
};

class IRemoteBroker : public virtual RefBase
{
public:
    IRemoteBroker() = default;
    virtual ~IRemoteBroker() override = default;

    /**
     * @brief Obtains a proxy or remote object.
     * @return Returns the RemoteObject if the caller is a RemoteObject;
     * returns the IRemoteObject if the caller is a RemoteProxy object.
     * @since 9
     */
    virtual sptr<IRemoteObject> AsObject() = 0;
};

class BrokerDelegatorBase
{
public:
    BrokerDelegatorBase() = default;
    virtual ~BrokerDelegatorBase() = default;

public:
    bool isSoUnloaded = false;
    std::u16string descriptor_;
};

#    define DECLARE_INTERFACE_DESCRIPTOR(DESCRIPTOR)                       \
        static inline const std::u16string metaDescriptor_ = {DESCRIPTOR}; \
        static inline const std::u16string &GetDescriptor()                \
        {                                                                  \
            return metaDescriptor_;                                        \
        }

class BrokerRegistration
{
    using Constructor = std::function<sptr<IRemoteBroker>(const sptr<IRemoteObject> &object)>;

public:
    /**
     * @brief Get broker registered.
     * @return Returns the BrokerRegistration instance.
     * @since 9
     */
    static BrokerRegistration &Get()
    {
        static BrokerRegistration instance;
        return instance;
    }
    /**
     * @brief Register the broker.
     * @param descriptor Indicates a descriptor the type of string.
     * @param creator Indicates the constructor.
     * @param object Indicates an object of type BrokerDelegatorBase.
     * @return Returns <b>true</b> if registration is successful; returns <b>false</b> otherwise.
     * @since 9
     */
    bool Register(const std::u16string &descriptor, const Constructor &creator, const BrokerDelegatorBase *object)
    {
        if (descriptor.empty()) {
            return false;
        }

        return false;
    }
    void Unregister(const std::u16string &descriptor)
    {
        // todo
    }

    sptr<IRemoteBroker> NewInstance(const std::u16string &descriptor, const sptr<IRemoteObject> &object)
    {
        sptr<IRemoteBroker> broker;
        return broker;
    }

protected:
    BrokerRegistration() = default;
    ~BrokerRegistration() = default;

private:
    BrokerRegistration(const BrokerRegistration &) = delete;
    BrokerRegistration(BrokerRegistration &&) = delete;
    BrokerRegistration &operator=(const BrokerRegistration &) = delete;
    BrokerRegistration &operator=(BrokerRegistration &&) = delete;
};

template <typename T> class BrokerDelegator : public BrokerDelegatorBase
{
public:
    BrokerDelegator();
    ~BrokerDelegator() override;

private:
    BrokerDelegator(const BrokerDelegator &) = delete;
    BrokerDelegator(BrokerDelegator &&) = delete;
    BrokerDelegator &operator=(const BrokerDelegator &) = delete;
    BrokerDelegator &operator=(BrokerDelegator &&) = delete;
};

template <typename T> BrokerDelegator<T>::BrokerDelegator()
{
    const std::u16string descriptor = T::GetDescriptor();
    BrokerRegistration &registration = BrokerRegistration::Get();
    if (registration.Register(descriptor, BrokerCreator<T>(), this)) {
        descriptor_ = T::GetDescriptor();
    }
}

template <typename T> BrokerDelegator<T>::~BrokerDelegator()
{
    if (!isSoUnloaded && !descriptor_.empty()) {
        BrokerRegistration &registration = BrokerRegistration::Get();
        registration.Unregister(descriptor_);
    }
}

template <typename INTERFACE> inline sptr<INTERFACE> iface_cast(const sptr<IRemoteObject> &object)
{
    const std::u16string descriptor = INTERFACE::GetDescriptor();
    BrokerRegistration &registration = BrokerRegistration::Get();
    sptr<IRemoteBroker> broker = registration.NewInstance(descriptor, object);
    return static_cast<INTERFACE *>(broker.GetRefPtr());
}

};  // namespace OHOS

#endif

#endif
