#ifndef _IPC_OBJ_CENTER_H_
#define _IPC_OBJ_CENTER_H_

#include "iremote_object.h"
#include "ipc_object_stub.h"

namespace OHOS {

class IpcObjCenter
{
public:
    static IpcObjCenter &getInstance()
    {
        static IpcObjCenter instance;
        return instance;
    }
    bool Init(bool isServer, IPCObjectStub *stub);
    sptr<IRemoteObject> GetServerStub();
    sptr<IRemoteObject> GetClientStub();

private:
    sptr<IRemoteObject> ipcServcerStub_ = nullptr;
    sptr<IRemoteObject> ipcClientStub_ = nullptr;
    IpcObjCenter();
    ~IpcObjCenter();
};

}  // namespace OHOS

#endif  //_IPC_OBJ_CENTER_H_
