#ifndef _IPC_SKELETON_H_
#define _IPC_SKELETON_H_

#include <sys/types.h>
#include <unistd.h>

#include "parcel.h"
#include "refbase.h"

namespace OHOS {

class IRemoteObject;

class IPCSkeleton
{
public:
    IPCSkeleton() = default;
    ~IPCSkeleton() = default;
    static pid_t GetCallingPid();
    static uid_t GetCallingUid();
    static sptr<IRemoteObject> GetContextObject();
    static bool SetContextObject(const sptr<IRemoteObject> &object);
    static void ClearContextObject();
    static bool SocketListening(bool isServer);
    static int SocketReadFd();
    static bool SocketWriteFd(int fd);
    static bool SetMaxWorkThreadNum(int maxThreadNum);
    /**
     * @brief Get calling token ID of caller.
     * @return Returns the TokenId of caller.
     * @since 9
     */
    static uint32_t GetCallingTokenID();

private:
    static sptr<IRemoteObject> obj_;
    static int socketFd_;
    static bool isServer_;
};

}  // namespace OHOS

#endif  // _IPC_SKELETON_H_
