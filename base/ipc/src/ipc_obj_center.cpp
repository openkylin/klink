#include <thread>
#include <cstring>

#include "ipc_base.h"
#include "ipc_obj_center.h"

namespace OHOS {

IpcObjCenter::IpcObjCenter()
{
}

IpcObjCenter::~IpcObjCenter()
{
}

bool IpcObjCenter::Init(bool isServer, IPCObjectStub *stub)
{
    if (stub == nullptr) {
        IPC_LOG("Invalid stub\n");
        return false;
    }

    if (isServer) {
        this->ipcServcerStub_ = stub;
    } else {
        this->ipcClientStub_ = stub;
    }

    return true;
}

sptr<IRemoteObject> IpcObjCenter::GetServerStub()
{
    return this->ipcServcerStub_;
}

sptr<IRemoteObject> IpcObjCenter::GetClientStub()
{
    return this->ipcClientStub_;
}

}  // namespace OHOS
