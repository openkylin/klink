/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef OHOS_ABILITY_BASE_URI_H
#define OHOS_ABILITY_BASE_URI_H

#include <string>
namespace OHOS {
class Uri
{
public:
    explicit Uri(const std::string &uriString);
    ~Uri() = default;
};
}  // namespace OHOS
#endif  // OHOS_ABILITY_BASE_URI_H_
