/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef HKS_SA_H
#define HKS_SA_H

#include "huks_service_ipc_interface_code.h"

#include "iremote_broker.h"
#include "iremote_stub.h"
#include "nocopyable.h"
#include "system_ability.h"

namespace OHOS {
namespace Security {
namespace Hks {
enum ServiceRunningState { STATE_NOT_START, STATE_RUNNING };
enum ResponseCode {
    HW_NO_ERROR = 0,
    HW_SYSTEM_ERROR = -1,
    HW_PERMISSION_DENIED = -2,
};

class IHksService : public IRemoteBroker
{
public:
    DECLARE_INTERFACE_DESCRIPTOR(u"ohos.security.hks.service");
};

class HksService
{
public:
    virtual ~HksService();

    int OnRemoteRequest(uint32_t code, MessageParcel &data, MessageParcel &reply, MessageOption &option);

    static HksService *GetInstance();

    void initService();

protected:
    void OnStart();
    void OnStop();

private:
    HksService();
    bool Init();

    bool registerToService_;
    ServiceRunningState runningState_;
    static HksService *instance;
};
}  // namespace Hks
}  // namespace Security
}  // namespace OHOS

#endif  // HKS_SA_H
