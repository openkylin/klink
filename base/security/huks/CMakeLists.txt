option(huks_enable_upgrade_key "huks" ON)
option(huks_enable_upgrade_derive_key_alg "huks" ON)
option(enable_user_auth_framework "huks" OFF)
option(enable_hks_coverage "huks" OFF)
option(enable_hks_mock "huks" OFF)
option(use_crypto_lib "openssl | mbedtls" ON)
option(huks_use_mbedtls "huks_engine" ON)
option(support_jsapi "all" OFF)

set(huks_frameworks_path ${CMAKE_CURRENT_SOURCE_DIR}/frameworks/huks_standard/main)
set(huks_engine_path ${CMAKE_CURRENT_SOURCE_DIR}/services/huks_standard/huks_engine/main)
set(huks_service_path ${CMAKE_CURRENT_SOURCE_DIR}/services/huks_standard/huks_service/main)
set(huks_inner_api_path ${CMAKE_CURRENT_SOURCE_DIR}/interfaces/inner_api/huks_standard/main)
set(huks_crypto_engine_openssl_include ${huks_frameworks_path}/crypto_engine/openssl/include)
set(huks_crypto_adapter_include ${CMAKE_CURRENT_SOURCE_DIR}/utils/crypto_adapter)
set(huks_file_operator_include ${CMAKE_CURRENT_SOURCE_DIR}/utils/file_operator)
set(huks_list_include ${CMAKE_CURRENT_SOURCE_DIR}/utils/list)
set(huks_mutex_include ${CMAKE_CURRENT_SOURCE_DIR}/utils/mutex)

set(huks_key_store_standard_path "${dsoftbus_data_path}/service/el1/public/huks_service")

set(defins
    "-DHKS_KEY_VERSION=3"
    "-Wall"
    "-fPIC"
    "-DHKS_CONFIG_KEY_STORE_PATH=\"${huks_key_store_standard_path}\""
    "-D_HARDWARE_ROOT_KEY_"
    "-D_HUKS_LOG_ENABLE_"
    "-DL2_STANDARD"
    "-DOPENSSL_SUPPRESS_DEPRECATED"
    "-DHKS_ENABLE_CLEAN_FILE"
)

if(enable_hks_coverage)
    set(defins
        ${defins}
        "--coverage"
    )
endif()

if (huks_enable_upgrade_key)
    set(defins
        ${defins}
        "-DHKS_ENABLE_UPGRADE_KEY"
    )
    if (huks_enable_upgrade_derive_key_alg)
      # enable upgrade key derivation algorithm from PBKDF2 to HKDF
        set(defins
            ${defins}
            "-DHKS_CHANGE_DERIVE_KEY_ALG_TO_HKDF"
        )
    endif()
endif()

if (enable_user_auth_framework)
    set(defins
        ${defins}
        "-DHKS_SUPPORT_USER_AUTH_ACCESS_CONTROL"
    )
endif()

if (huks_use_mbedtls)
    set(defins
        ${defins}
        "-DHKS_USE_MBEDTLS"
    )
endif()

set(huks_lib
    hilog
    utils
    hitracechain
    hitrace_meter
    ipc_single
    ipc_core
    samgr_proxy
    os_account_innerkits
    accesstoken_sdk
    system_ability_fwk
)

set(huks_frameworks_inc
    "${huks_frameworks_path}/common/include"
    "${huks_frameworks_path}/core/include"
    "${huks_frameworks_path}/os_dependency/ipc/include"
    "${huks_frameworks_path}/os_dependency/sysinfo/include"

)

set(huks_frameworks_src
    "${huks_frameworks_path}/common/src/hks_ability.c"
    "${huks_frameworks_path}/common/src/hks_base_check.c"
    "${huks_frameworks_path}/common/src/hks_check_paramset.c"
    "${huks_frameworks_path}/common/src/hks_common_check.c"
    "${huks_frameworks_path}/common/src/hks_crypto_adapter.c"
    "${huks_frameworks_path}/common/src/hks_param.c"
    "${huks_frameworks_path}/common/src/hks_tags_type_manager.c"
    "${huks_frameworks_path}/common/src/hks_errcode_adapter.c"
    "${huks_frameworks_path}/core/src/hks_local_engine.c"
    "${huks_frameworks_path}/core/src/hks_verifier.c"
    "${huks_frameworks_path}/os_dependency/ipc/src/hks_ipc_check.c"
    "${huks_frameworks_path}/os_dependency/ipc/src/hks_ipc_serialization.c"
    "${huks_frameworks_path}/os_dependency/ipc/src/hks_ipc_slice.c"
    "${huks_frameworks_path}/os_dependency/ipc/src/hks_request.cpp"
    "${huks_frameworks_path}/os_dependency/ipc/src/hks_client_service_ipc.c"
    "${huks_frameworks_path}/os_dependency/posix/hks_mem.c"
)

if (use_crypto_lib)
    set(huks_frameworks_inc
        ${huks_frameworks_inc}
        "${huks_frameworks_path}/crypto_engine/openssl/include"
    )
    set(huks_frameworks_src
        ${huks_frameworks_src}  
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_ability.c"
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_aes.c"
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_bn.c"
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_common.c"
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_curve25519.c"
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_dh.c"
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_dsa.c"
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_ecc.c"
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_ed25519tox25519.c"
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_engine.c"
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_get_main_key.c"
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_hash.c"
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_hmac.c"
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_kdf.c"
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_rsa.c"
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_sm2.c"
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_sm3.c"
        "${huks_frameworks_path}/crypto_engine/openssl/src/hks_openssl_sm4.c"
    )
else()
    set(huks_frameworks_inc
        ${huks_frameworks_inc}
        "${huks_frameworks_path}/crypto_engine/mbedtls/include"
    )
    set(huks_frameworks_src
        ${huks_frameworks_src}  
        "${huks_frameworks_path}/crypto_engine/mbedtls/src/hks_crypto_ed25519.c"
        "${huks_frameworks_path}/crypto_engine/mbedtls/src/hks_mbedtls_ability.c"
        "${huks_frameworks_path}/crypto_engine/mbedtls/src/hks_mbedtls_aes.c"
        "${huks_frameworks_path}/crypto_engine/mbedtls/src/hks_mbedtls_bn.c"
        "${huks_frameworks_path}/crypto_engine/mbedtls/src/hks_mbedtls_common.c"
        "${huks_frameworks_path}/crypto_engine/mbedtls/src/hks_mbedtls_dh.c"
        "${huks_frameworks_path}/crypto_engine/mbedtls/src/hks_mbedtls_dsa.c"
        "${huks_frameworks_path}/crypto_engine/mbedtls/src/hks_mbedtls_ecc.c"
        "${huks_frameworks_path}/crypto_engine/mbedtls/src/hks_mbedtls_ecdh.c"
        "${huks_frameworks_path}/crypto_engine/mbedtls/src/hks_mbedtls_ecdsa.c"
        "${huks_frameworks_path}/crypto_engine/mbedtls/src/hks_mbedtls_engine.c"
        "${huks_frameworks_path}/crypto_engine/mbedtls/src/hks_mbedtls_get_main_key.c"
        "${huks_frameworks_path}/crypto_engine/mbedtls/src/hks_mbedtls_hash.c"
        "${huks_frameworks_path}/crypto_engine/mbedtls/src/hks_mbedtls_hmac.c"
        "${huks_frameworks_path}/crypto_engine/mbedtls/src/hks_mbedtls_kdf.c"
        "${huks_frameworks_path}/crypto_engine/mbedtls/src/hks_mbedtls_rsa.c"
        "${huks_frameworks_path}/crypto_engine/mbedtls/src/hks_mbedtls_x25519.c"
    )
    set(huks_lib
        ${huks_lib}
        mbedtls
    )
endif()

set(huks_interfaces_inc
    "${huks_inner_api_path}/include"
)

set(huks_interfaces_src
    "${huks_inner_api_path}/src/hks_api.c"
    "${huks_inner_api_path}/src/hks_api_adapter.c"
)

set(huks_engine_inc
    "${huks_engine_path}/core/include"
    "${huks_engine_path}/core_dependency/include"
    "${huks_engine_path}/device_cert_manager/include"
)

set(huks_engine_src
    "${huks_engine_path}/core_dependency/src/hks_chipset_platform_key_hardcoded.c"
    "${huks_engine_path}/core_dependency/src/hks_core_hal_api.c"
    "${huks_engine_path}/core_dependency/src/hks_core_useriam_wrap.cpp"
    "${huks_engine_path}/core/src/hks_auth.c"
    "${huks_engine_path}/core/src/hks_chipset_platform_decrypt.c"
    "${huks_engine_path}/core/src/hks_core_interfaces.c"
    "${huks_engine_path}/core/src/hks_core_service.c"
    "${huks_engine_path}/core/src/hks_core_service_three_stage.c"
    "${huks_engine_path}/core/src/hks_keyblob.c"
    "${huks_engine_path}/core/src/hks_keynode.c"
    "${huks_engine_path}/core/src/hks_secure_access.c"
    "${huks_engine_path}/core/src/hks_sm_import_wrap_key.c"
    "${huks_engine_path}/core/src/hks_upgrade_key.c"
    "${huks_engine_path}/device_cert_manager/src/dcm_asn1.c"
    "${huks_engine_path}/device_cert_manager/src/dcm_attest.c"
    "${huks_engine_path}/device_cert_manager/src/dcm_attest_utils.c"
)

set(huks_services_inc
    "${huks_service_path}/core/include"
    "${huks_service_path}/os_dependency/idl/ipc"
    "${huks_service_path}/os_dependency/idl/passthrough"
    "${huks_service_path}/os_dependency/sa"
    "${huks_service_path}/systemapi_wrap/hisysevent_wrapper/include"
    "${huks_service_path}/systemapi_wrap/hitrace_meter_wrapper/include"
)

set(huks_services_src
    "${huks_service_path}/core/src/hks_client_check.c"
    "${huks_service_path}/core/src/hks_client_service.c"
    "${huks_service_path}/core/src/hks_client_service_util.c"
    "${huks_service_path}/core/src/hks_session_manager.c"
    "${huks_service_path}/core/src/hks_storage.c"
    "${huks_service_path}/core/src/hks_storage_file_lock.c"
    "${huks_service_path}/core/src/hks_upgrade_helper.c"
    "${huks_service_path}/core/src/hks_upgrade_key_accesser.c"
    "${huks_service_path}/core/src/hks_lock.c"
    "${huks_service_path}/core/src/hks_hitrace.c"
    "${huks_service_path}/core/src/hks_report.c"  
    "${huks_service_path}/os_dependency/idl/ipc/hks_ipc_serialization.c"
    "${huks_service_path}/os_dependency/idl/ipc/hks_ipc_service.c"
    "${huks_service_path}/os_dependency/idl/ipc/hks_response.cpp"
    "${huks_service_path}/os_dependency/idl/passthrough/huks_access.c"
    "${huks_service_path}/os_dependency/idl/passthrough/huks_core_static_hal.c"
    "${huks_service_path}/os_dependency/posix/hks_rwlock.c"
    "${huks_service_path}/os_dependency/sa/hks_sa.cpp"
    "${huks_service_path}/systemapi_wrap/hisysevent_wrapper/src/hisysevent_wrapper.cpp"
    "${huks_service_path}/systemapi_wrap/hisysevent_wrapper/src/hks_report_wrapper.c"
    "${huks_service_path}/systemapi_wrap/hitrace_meter_wrapper/src/hitrace_meter_wrapper.cpp"
)

if (support_jsapi)
    set(huks_services_src
        ${huks_services_src}
        "${huks_service_path}/os_dependency/sa/hks_event_observer.cpp"
    )
    set(defins
        ${defins}
        "-DSUPPORT_COMMON_EVENT"
        "-DHAS_OS_ACCOUNT_PART"
    )
    set(huks_lib
        ${huks_lib}
        want
        cesfwk_innerkits
        os_account_innerkits
    )
endif()

if(enable_hks_mock)
    set(huks_services_inc
        ${huks_services_inc}
        "${huks_service_path}/systemapi_wrap/useridm/inc"
    )
    set(huks_services_src
        ${huks_services_src}
        "${huks_service_path}/systemapi_mock/src/hks_useridm_api_mock.cpp"
        "${huks_service_path}/systemapi_wrap/useridm/src/hks_useridm_api_wrap.cpp"
    )
endif()

set(huks_utils_inc
    "${CMAKE_CURRENT_SOURCE_DIR}/utils/condition"
    "${CMAKE_CURRENT_SOURCE_DIR}/utils/crypto_adapter"
    "${CMAKE_CURRENT_SOURCE_DIR}/utils/file_operator"
    "${CMAKE_CURRENT_SOURCE_DIR}/utils/list"
    "${CMAKE_CURRENT_SOURCE_DIR}/utils/mutex"
)

set(huks_utils_src
    "${CMAKE_CURRENT_SOURCE_DIR}/utils/condition/hks_condition.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/utils/crypto_adapter/hks_client_service_adapter.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/utils/crypto_adapter/hks_client_service_adapter_common.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/utils/file_operator/hks_file_operator.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/utils/list/hks_double_list.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/utils/mutex/hks_mutex.c"
)

include_directories(
    "${huks_interfaces_inc}"
    "${huks_frameworks_inc}"
    "${huks_engine_inc}"
    "${huks_services_inc}"
    "${huks_utils_inc}"
)

add_library(
    hukssdk
    SHARED
    "${huks_interfaces_src}"
    "${huks_frameworks_src}"
    "${huks_engine_src}"
    "${huks_services_src}"
    "${huks_utils_src}"
)

set(LIBRARY_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/output/lib/base)

target_compile_options(hukssdk PRIVATE ${defins})

target_include_directories(hukssdk PUBLIC ${top_path}/base/include)
target_include_directories(hukssdk PUBLIC ${top_path}/third_party/include)
target_include_directories(hukssdk PUBLIC ${openssl_include_path})
target_link_directories(hukssdk PUBLIC /opt/dsoftbus/lib)
target_link_directories(hukssdk PUBLIC ${openssl_lib_path})

target_link_libraries(hukssdk
                      PUBLIC
                      ${huks_lib}
                      ${huks_crypto_engine_standard_static}
                      ${openssl_libs}
)

unset(huks_crypto_engine_standard_static CACHE)