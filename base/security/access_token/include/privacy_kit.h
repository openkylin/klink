/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

/**
 * @addtogroup Privacy
 * @{
 *
 * @brief Provides sensitive permissions access management.
 *
 * @since 8.0
 * @version 8.0
 */

/**
 * @file privacy_kit.h
 *
 * @brief Declares PrivacyKit interfaces.
 *
 * @since 8.0
 * @version 8.0
 */

#ifndef INTERFACES_INNER_KITS_PRIVACY_KIT_H
#define INTERFACES_INNER_KITS_PRIVACY_KIT_H

#include <string>

#include "access_token.h"

namespace OHOS {
namespace Security {
namespace AccessToken {
/**
 * @brief Declares PrivacyKit class
 */
class PrivacyKit
{
public:
    /**
     * @brief Add input tokenID access input permission record.
     * @param tokenID token id
     * @param permissionName permission nanme
     * @param successCount access success count
     * @param failCount fail success count
     * @return error code, see privacy_error.h
     */
    static int32_t AddPermissionUsedRecord(AccessTokenID tokenID, const std::string &permissionName,
                                           int32_t successCount, int32_t failCount);
};
}  // namespace AccessToken
}  // namespace Security
}  // namespace OHOS
#endif
