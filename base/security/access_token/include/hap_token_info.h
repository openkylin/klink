/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef ACCESSTOKEN_HAP_TOKEN_INFO_H
#define ACCESSTOKEN_HAP_TOKEN_INFO_H

#include "access_token.h"
#include <string>

namespace OHOS {
namespace Security {
namespace AccessToken {

/**
 * @brief Declares hap token info class
 */
class HapTokenInfo final
{
public:
    /**
     * apl level, for details about the valid values,
     * see the definition of ATokenAplEnum in the access_token.h file.
     */
    ATokenAplEnum apl;
    char ver;
    int userID;
    std::string bundleName;
    /** which version of the SDK is used to develop this hap */
    int32_t apiVersion;
    /** instcance index */
    int instIndex;
    /**
     * dlp type, for details about the valid values,
     * see the definition of HapDlpType in the access_token.h file.
     */
    int dlpType;
    std::string appID;
    std::string deviceID;
    AccessTokenID tokenID;
    /** token attribute */
    AccessTokenAttr tokenAttr;
};

}  // namespace AccessToken
}  // namespace Security
}  // namespace OHOS
#endif  // ACCESSTOKEN_HAP_TOKEN_INFO_H
