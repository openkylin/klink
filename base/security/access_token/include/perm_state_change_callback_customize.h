/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

/**
 * @addtogroup AccessToken
 * @{
 *
 * @brief Provides permission management interfaces.
 *
 * Provides tokenID-based application permission verification mechanism.
 * When an application accesses sensitive data or APIs, this module can check
 * whether the application has the corresponding permission. Allows applications
 * to query their access token information or APL levcels based on token IDs.
 *
 * @since 7.0
 * @version 7.0
 */

/**
 * @file perm_state_change_callback_customize.h
 *
 * @brief Declares permission state change callback class.
 *
 * @since 8.0
 * @version 8.0
 */

#ifndef INTERFACES_INNER_KITS_PERM_STATE_CALLBACK_CUSTOMIZE_H
#define INTERFACES_INNER_KITS_PERM_STATE_CALLBACK_CUSTOMIZE_H

#include <string>
#include <vector>
#include "access_token.h"

namespace OHOS {
namespace Security {
namespace AccessToken {

/** define tokenID list max size */
#define TOKENIDS_LIST_SIZE_MAX 1024
/** define permission list max size */
#define PERMS_LIST_SIZE_MAX 1024

/**
 * @brief Declares permission state change info struct
 */
struct PermStateChangeInfo {
    /**
     * permission state change type, for details about the valid values,
     * see the definition of ActiveChangeType in the active_change_response_info.h file.
     */
    int32_t permStateChangeType;
    AccessTokenID tokenID;
    std::string permissionName;
};

/**
 * @brief Declares permission state change scope struct
 */
struct PermStateChangeScope {
    /**
     * indicates which tokenID to listen the permission state change,
     * empty means listen all tokenIDs in the device
     */
    std::vector<AccessTokenID> tokenIDs;
    /**
     * indicates which permission to listen the state change,
     * empty means listen all permission state changes in the device
     */
    std::vector<std::string> permList;
};

/**
 * @brief Declares permission state change callback customize class
 */
class PermStateChangeCallbackCustomize
{
public:
    /**
     * @brief Constructor without any param.
     */
    PermStateChangeCallbackCustomize();
    /**
     * @brief Constructor with param.
     * @param scopeInfo struct PermStateChangeScope quote
     */
    explicit PermStateChangeCallbackCustomize(const PermStateChangeScope &scopeInfo);
    /**
     * @brief Destructor without any param.
     */
    virtual ~PermStateChangeCallbackCustomize();

    /**
     * @brief Pure virtual function for callback.
     * @param result PermStateChangeInfo quote
     */
    virtual void PermStateChangeCallback(PermStateChangeInfo &result) = 0;

    /**
     * @brief Get private variable scopeInfo_.
     * @param scopeInfo struct PermStateChangeScope quote as return value
     */
    void GetScope(PermStateChangeScope &scopeInfo) const;

private:
    /** private variable struct PermStateChangeScope */
    PermStateChangeScope scopeInfo_;
};
}  // namespace AccessToken
}  // namespace Security
}  // namespace OHOS

#endif  // INTERFACES_INNER_KITS_PERM_STATE_CALLBACK_CUSTOMIZE_H