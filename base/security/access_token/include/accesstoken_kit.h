/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

/**
 * @addtogroup AccessToken
 * @{
 *
 * @brief Provides permission management.
 *
 * Provides tokenID-based application permission verification mechanism.
 * When an application accesses sensitive data or APIs, this module can check
 * whether the application has the corresponding permission. Allows applications
 * to query their access token information or APL levcels based on token IDs.
 *
 * @since 7.0
 * @version 7.0
 */

/**
 * @file accesstoken_kit.h
 *
 * @brief Declares access token interfaces.
 *
 * @since 7.0
 * @version 7.0
 */

#ifndef INTERFACES_INNER_KITS_ACCESSTOKEN_KIT_H
#define INTERFACES_INNER_KITS_ACCESSTOKEN_KIT_H

#include <string>
#include <vector>
#include <memory>
#include <cstring>

#include "access_token.h"
#include "hap_token_info.h"
#include "perm_state_change_callback_customize.h"

namespace OHOS {
namespace Security {
namespace AccessToken {

/**
 * @brief Declares native token info class
 */
class NativeTokenInfo final
{
public:
    /**
     * apl level, for details about the valid values,
     * see the definition of ATokenAplEnum in the access_token.h file.
     */
    ATokenAplEnum apl;
    unsigned char ver;
    /** native process name */
    std::string processName;
    /** capsbility list */
    std::vector<std::string> dcap;
    AccessTokenID tokenID;
    /** token attribute */
    AccessTokenAttr tokenAttr;
    /** native process access control permission list */
    std::vector<std::string> nativeAcls;
};

/**
 * @brief Declares AccessTokenKit class
 */
class AccessTokenKit
{
public:
    /**
     * @brief Get token type from flag in tokenId, which doesn't depend
     *        on ATM service, with uint_64 parameters.
     * @param tokenID token id
     * @return token type enum, see access_token.h
     */
    static ATokenTypeEnum GetTokenTypeFlag(FullTokenID tokenID);
    /**
     * @brief Check if the input tokenID has been granted the input permission.
     * @param tokenID token id
     * @param permissionName permission to be checked
     * @return enum PermissionState, see access_token.h
     */
    static int VerifyAccessToken(AccessTokenID tokenID, const std::string &permissionName);
    /**
     * @brief Get hap token info by token id.
     * @param tokenID token id
     * @param hapTokenInfoRes HapTokenInfo quote, as query result
     * @return error code, see access_token_error.h
     */
    static int GetHapTokenInfo(AccessTokenID tokenID, HapTokenInfo &hapTokenInfoRes);
    /**
     * @brief Get native token info by token id.
     * @param tokenID token id
     * @param nativeTokenInfoRes NativeTokenInfo quote, as query result
     * @return error code, see access_token_error.h
     */
    static int GetNativeTokenInfo(AccessTokenID tokenID, NativeTokenInfo &nativeTokenInfoRes);
    /**
     * @brief Register permission state change callback.
     * @param callback smart point of class PermStateChangeCallbackCustomize quote
     * @return error code, see access_token_error.h
     */
    static int32_t RegisterPermStateChangeCallback(const std::shared_ptr<PermStateChangeCallbackCustomize> &callback);
};
}  // namespace AccessToken
}  // namespace Security
}  // namespace OHOS
#endif
