execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/ability_base/include 1)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/ability_runtime/include 1)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/security/access_token/include 1)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/begetutil/include 1)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/btframework/include 1)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/bundle_framework/include 1)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/c_utils/include 1)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/common_event_service/include 1)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/data_share/include 1)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/security/device_auth/interfaces/inner_api 1)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/drivers_interface_wlan/include/v1_1 0)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/hicollie_native/include/xcollie 0)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/hilog/include/hilog 0)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/hiviewdfx/hisysevent/include 1)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/hiviewdfx/hitrace/include/hitrace 0)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/hiviewdfx/hitrace/include 1)
execute_process(COMMAND sh ${top_path}/lninclude.sh 
                           ${CMAKE_CURRENT_SOURCE_DIR} 
                           ${CMAKE_CURRENT_SOURCE_DIR}/security/huks/interfaces/inner_api/huks_standard/main/include 1)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/ipc/include 1)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/os_account/include 1)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/safwk/include 1)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/samgr/include 1)
execute_process(COMMAND sh ${top_path}/lninclude.sh ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/wifi/include 1)

add_subdirectory(hilog)
add_subdirectory(hiviewdfx/hitrace)
add_subdirectory(hiviewdfx/hisysevent)
add_subdirectory(begetutil)
add_subdirectory(c_utils)
add_subdirectory(ipc)
add_subdirectory(samgr)
add_subdirectory(ability_base)
add_subdirectory(ability_runtime)
add_subdirectory(data_share)
add_subdirectory(relational_store)
add_subdirectory(os_account)
add_subdirectory(common_event_service)
add_subdirectory(wifi)
add_subdirectory(drivers_interface_wlan)
add_subdirectory(hicollie_native)
add_subdirectory(napi)
add_subdirectory(btframework)
add_subdirectory(safwk)
add_subdirectory(bundle_framework)
