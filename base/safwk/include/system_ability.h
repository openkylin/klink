/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file was modified by KylinSoft Co. on 2023.
 */

#ifndef _SYSTEM_ABILITY_H
#define _SYSTEM_ABILITY_H

#ifdef __cplusplus
#    include <string>
#    include <vector>
#    include <memory>
#    include <mutex>
#    include "parcel.h"

#    define DECLEAR_SYSTEM_ABILITY(className)       \
    public:                                         \
        virtual std::string GetClassName() override \
        {                                           \
            return #className;                      \
        }

#    define DECLEAR_BASE_SYSTEM_ABILITY(className) \
    public:                                        \
        virtual std::string GetClassName() = 0;

#    define DECLARE_SYSTEM_ABILITY(className)       \
    public:                                         \
        virtual std::string GetClassName() override \
        {                                           \
            return #className;                      \
        }

#    define DECLARE_BASE_SYSTEM_ABILITY(className) \
    public:                                        \
        virtual std::string GetClassName() = 0;

namespace OHOS {

#    define REGISTER_SYSTEM_ABILITY_BY_ID(abilityClassName, systemAbilityId, runOnCreate) \
        const bool abilityClassName##_##RegisterResult =                                  \
            SystemAbility::MakeAndRegisterAbility(new abilityClassName(systemAbilityId, runOnCreate));

class IRemoteObject;

class SystemAbility
{
    DECLARE_BASE_SYSTEM_ABILITY(SystemAbility);

public:
    static bool MakeAndRegisterAbility(SystemAbility *systemAbility)
    {
        return true;
    }
    bool Publish(sptr<IRemoteObject> systemAbility)
    {
        return true;
    }

protected:
    SystemAbility(bool runOnCreate = false);
    SystemAbility(int32_t systemAbilityId, bool runOnCreate = false);
    virtual ~SystemAbility();
    virtual void OnStart();
    virtual void OnStop();
    virtual void OnAddSystemAbility(int32_t systemAbilityId, const std::string &deviceId);
    virtual void OnRemoveSystemAbility(int32_t systemAbilityId, const std::string &deviceId);
};

};  // namespace OHOS
#endif

#endif
