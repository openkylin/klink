#!/bin/bash
echo "$(pwd)/build"
if [ -d "$(pwd)/build" ]; then
    rm -rf ./build/ ./output/ ./base/include/ ./dsoftbus/include/ ./third_party/include/
fi