/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>
#include <signal.h>
#include <time.h>
#include "securec.h"
#include "softbus_errcode.h"
#include "softbus_adapter_log.h"
#include "softbus_bus_center.h"

#define PKGNAME_MAX_LEN 128
#define DEFAULT_ID 1
#define LNN_CAPABILITY "osdCapability"

const uint16_t MAX_ID = 65535;

static char g_pkgName[PKGNAME_MAX_LEN];

static SubscribeInfo g_disc_sInfo = {.subscribeId = DEFAULT_ID,
                                     .mode = DISCOVER_MODE_ACTIVE,
                                     .medium = COAP,
                                     .freq = HIGH,
                                     .isSameAccount = false,
                                     .isWakeRemote = false,
                                     .capability = LNN_CAPABILITY,
                                     .capabilityData = (unsigned char *)LNN_CAPABILITY,
                                     .dataLen = strlen(LNN_CAPABILITY)

};

static PublishInfo s_publishInfo = {.publishId = DEFAULT_ID,
                                    .mode = DISCOVER_MODE_ACTIVE,
                                    .medium = COAP,
                                    .freq = HIGH,
                                    .capability = LNN_CAPABILITY,
                                    .ranging = false};

static void OnSoftBusDeviceOnline(NodeBasicInfo *info)
{
    if (info == nullptr) {
        LOG_ERR("[IPC_DEMO]OnSoftBusDeviceOnline: info is nullptr.");
        return;
    }
    LOG_INFO("[IPC_DEMO]OnSoftBusDeviceOnline: device %s online", info->deviceName);
}

static void OnSoftbusDeviceOffline(NodeBasicInfo *info)
{
    if (info == nullptr) {
        LOG_ERR("[IPC_DEMO]OnSoftbusDeviceOffline: info is nullptr.");
        return;
    }

    LOG_INFO("[IPC_DEMO]OnSoftbusDeviceOffline :device %s offline", info->deviceName);
}

static void OnSoftbusDeviceInfoChanged(NodeBasicInfoType type, NodeBasicInfo *info)
{
    if (info == nullptr) {
        LOG_ERR("[IPC_DEMO]OnSoftbusDeviceInfoChanged: info is nullptr.");
        return;
    }
    LOG_INFO("[IPC_DEMO]OnSoftbusDeviceInfoChanged");
}

static INodeStateCb s_softbusNodeStateCb = {.events = EVENT_NODE_STATE_ONLINE | EVENT_NODE_STATE_OFFLINE |
                                                      EVENT_NODE_STATE_INFO_CHANGED,
                                            .onNodeOnline = OnSoftBusDeviceOnline,
                                            .onNodeOffline = OnSoftbusDeviceOffline,
                                            .onNodeBasicInfoChanged = OnSoftbusDeviceInfoChanged};

static void OnDevCenterPublishResult(int publishId, PublishResult result)
{
    LOG_INFO("[IPC_DEMO]OnDevCenterPublishResult, publishId: %d, result: %d.", publishId, result);
}

static IPublishCb s_devicePublishCb = {
    .OnPublishResult = OnDevCenterPublishResult,
};

void OnSoftbusDeviceFound(const DeviceInfo *device)
{
    if (device == nullptr) {
        LOG_INFO("[IPC_DEMO]OnSoftbusDeviceFound, error device is null");
        return;
    }
    LOG_INFO("[IPC_DEMO]OnSoftbusDeviceFound :notify found device: %s found", device->devName);
}

void OnSoftbusDiscoveryResult(int subscribeId, RefreshResult result)
{
    LOG_INFO("[IPC_DEMO]OnSoftbusDiscoveryResult subscribeId: %d, result: %d", subscribeId, result);
}

IRefreshCallback s_softbusDiscoveryCb = {
    .OnDeviceFound = OnSoftbusDeviceFound,
    .OnDiscoverResult = OnSoftbusDiscoveryResult,
};

void StopClient(int signo)
{
    LOG_INFO("[IPC_DEMO]StopRefreshLNN begin, subscribeId: %d.", (int32_t)g_disc_sInfo.subscribeId);
    int32_t ret = StopRefreshLNN(g_pkgName, g_disc_sInfo.subscribeId);
    if (ret != SOFTBUS_OK) {
        LOG_ERR("[IPC_DEMO]StopRefreshLNN failed, ret: %d.", ret);
        return;
    }
    LOG_INFO("[IPC_DEMO]UnPublishDiscovery begin, publishId: %d.", s_publishInfo.publishId);
    ret = StopPublishLNN(g_pkgName, s_publishInfo.publishId);
    if (ret != SOFTBUS_OK) {
        LOG_ERR("[IPC_DEMO]StopPublishLNN failed with ret: %d.", ret);
        return;
    }
    exit(0);
    return;
}

int main(int argc, char *argv[])
{
    printf("Please enter the client package name: ");
    scanf("%s", g_pkgName);

    int32_t ret = RegNodeDeviceStateCb(g_pkgName, &s_softbusNodeStateCb);
    if (ret != SOFTBUS_OK) {
        LOG_ERR("[DeviceManagerCenter]RegNodeDeviceStateCb failed, ret: %d.", ret);
        return -1;
    }

    srand(time(NULL));
    s_publishInfo.publishId = rand() % MAX_ID;
    ret = PublishLNN(g_pkgName, &s_publishInfo, &s_devicePublishCb);
    if (ret != SOFTBUS_OK) {
        LOG_ERR("[IPC_DEMO]PublishLNN failed, ret: %d.", ret);
        return -1;
    }

    srand(time(NULL));
    g_disc_sInfo.subscribeId = rand() % MAX_ID;
    ret = RefreshLNN(g_pkgName, &g_disc_sInfo, &s_softbusDiscoveryCb);
    if (ret != SOFTBUS_OK) {
        LOG_ERR("[IPC_DEMO]RefreshLNN failed, ret: %d.", ret);
        return -1;
    }

    while (1) {
        signal(SIGINT, StopClient);
        pause();
    }
    return 0;
}
