/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <thread>
#include "softbus_errcode.h"
#include "softbus_log.h"
#include "discovery_service.h"
#include "softbus_bus_center.h"
#include "device_auth.h"
#include "device_auth_defines.h"
#include "inner_session.h"
#include "session.h"
#include "cJSON.h"
#include <stdlib.h>
#include <inttypes.h>
#include "securec.h"
#include "json_utils.h"

#define TEST_ASSERT_TRUE(ret)        \
    if (ret) {                       \
        LOG_INFO("[test][succ]\n");  \
        g_succTestCount++;           \
    } else {                         \
        LOG_INFO("[test][error]\n"); \
        g_failTestCount++;           \
    }

static int g_succTestCount = 0;
static int g_failTestCount = 0;
static int g_devieceFoundCount = 0;

#define DECIMAL 10

#define DISC_SUBSCRIBE_ID 9

static char const *g_pkgName = "ohos.samples.distributedcalc";
constexpr const char *TAG_GROUP_ID = "groupId";
constexpr const char *TAG_GROUP_NAME = "GROUPNAME";
constexpr const char *TAG_REQUEST_ID = "REQUESTID";
constexpr const char *TAG_DEVICE_ID = "DEVICEID";
constexpr const char *TAG_AUTH_TYPE = "AUTHTYPE";
constexpr const char *TAG_CRYPTO_SUPPORT = "CRYPTOSUPPORT";
constexpr const char *TAG_VER = "ITF_VER";
constexpr const char *TAG_MSG_TYPE = "MSG_TYPE";
constexpr const char *TAG_LOCAL_DEVICE_ID = "LOCALDEVICEID";
constexpr const char *DM_ITF_VER = "1.1";
constexpr const char *DM_PKG_NAME = "ohos.distributedhardware.devicemanager";
constexpr const char *DM_SESSION_NAME = "ohos.distributedhardware.devicemanager.resident";
const static char *DM_CAPABILITY_OSD = "osdCapability";
#define LNN_DISC_CAPABILITY "ddmpCapability"
#define DCONN_AUTH_APPID "dconn_app"
#define DCONN_AUTH_GROUP_NAME "dconn_group"

static SubscribeInfo g_disc_sInfo = {.subscribeId = DISC_SUBSCRIBE_ID,
                                     .mode = DISCOVER_MODE_ACTIVE,
                                     .medium = COAP,
                                     .freq = HIGH,
                                     .isSameAccount = false,
                                     .isWakeRemote = false,
                                     .capability = LNN_DISC_CAPABILITY,
                                     .capabilityData = (unsigned char *)LNN_DISC_CAPABILITY,
                                     .dataLen = strlen(LNN_DISC_CAPABILITY)

};

static char g_ip[IP_STR_MAX_LEN];
int g_port;

static bool OnGmTransmit(int64_t requestId, const uint8_t *data, uint32_t dataLen);
static void OnGmSessionKeyReturned(int64_t requestId, const uint8_t *sessionKey, uint32_t sessionKeyLen);
static void OnGmFinish(int64_t requestId, int operationCode, const char *returnData);
static void OnGmError(int64_t requestId, int operationCode, int errorCode, const char *errorReturn);
static char *OnGmRequest(int64_t requestId, int operationCode, const char *reqParams);

static const DeviceAuthCallback g_groupManagerCallback = {
    .onTransmit = OnGmTransmit,
    .onSessionKeyReturned = OnGmSessionKeyReturned,
    .onFinish = OnGmFinish,
    .onError = OnGmError,
    .onRequest = OnGmRequest,
};

static bool OnGmTransmit(int64_t requestId, const uint8_t *data, uint32_t dataLen)
{
    LOG_INFO("OnGmTransmit: requestId: %d, data: %s\n", requestId, (const char *)data);
    return true;
}

static void OnGmSessionKeyReturned(int64_t requestId, const uint8_t *sessionKey, uint32_t sessionKeyLen)
{
    LOG_INFO("OnGmSessionKeyReturned: requestId: %d, sessionKey: %s", requestId, (const char *)sessionKey);
}

static void OnGmFinish(int64_t requestId, int operationCode, const char *returnData)
{
    LOG_INFO("OnGmSessionKeyReturned: requestId: %d, operationCode: %d, returnData: %s", requestId, operationCode,
             returnData);
}

static void OnGmError(int64_t requestId, int operationCode, int errorCode, const char *errorReturn)
{
    LOG_INFO("OnGmSessionKeyReturned: requestId: %d, operationCode: %d, errorCode: %d, errorReturn: %s", requestId,
             operationCode, errorCode, errorReturn);
}

static char *OnGmRequest(int64_t requestId, int operationCode, const char *reqParams)
{
    LOG_INFO("OnGmSessionKeyReturned: requestId: %d, operationCode: %d, reqParams: %s", requestId, operationCode,
             reqParams);
}

static void DiscDeviceFound(const DeviceInfo *device)
{
    g_devieceFoundCount++;
    LOG_INFO("[TestCallback]DiscDeviceFound success! index = %d, device info devId=%s, accountHash=%s, devType=%d, "
             "devName=%s,  capabilityBitMap=%d, addrNum=%d",
             g_devieceFoundCount, device->devId, device->accountHash, device->devType, device->devName,
             device->capabilityBitmap[0], device->addrNum);

    static bool hichainInited = false;
    if (strcpy_s(g_ip, IP_STR_MAX_LEN, device[0].addr->info.ip.ip) != EOK) {
        LOG_INFO("ip strcpy_s failed!");
    }
    g_port = (int)device[0].addr->info.ip.port;

    if (!hichainInited) {
        LOG_INFO("Hichain already initialized.");
        // 设置回调函数
        int32_t result = GetGmInstance()->regCallback(DM_PKG_NAME, &g_groupManagerCallback);
        if (result != HC_SUCCESS) {
            LOG_INFO("gm regCallback failed: %" PRIu32, result);
            return;
        }
        hichainInited = true;
        NodeBasicInfo info;
        GetLocalNodeDeviceInfo(DM_PKG_NAME, &info);
        LOG_INFO("GetLocalNodeDeviceInfo==========================");
        LOG_INFO("name:%s\n", info.deviceName);
        LOG_INFO("deviceTypeId:%d\n", info.deviceTypeId);
        LOG_INFO("networkId:%s\n", info.networkId);
        LOG_INFO("==============================\n");

        OpenAuthSession(DM_SESSION_NAME, device->addr, device->addrNum, "");
    }
}

static void DiscoverFailed(int subscribeId, DiscoveryFailReason failReason)
{
    LOG_INFO("[TestCallback]DiscoverFailed! subscribeId = %d, failReason = %d", subscribeId, failReason);
}

static void DiscoverySuccess(int subscribeId)
{
    LOG_INFO("[TestCallback]DiscoverySuccess! subscribeId = %d", subscribeId);
}

static IDiscoveryCallback g_subscribeCb = {
    .OnDeviceFound = DiscDeviceFound, .OnDiscoverFailed = DiscoverFailed, .OnDiscoverySuccess = DiscoverySuccess};

static int32_t DiscStartDiscovery()
{
    g_devieceFoundCount = 0;
    int ret = StartDiscovery(DM_PKG_NAME, &g_disc_sInfo, &g_subscribeCb);
    TEST_ASSERT_TRUE(ret == 0);
    return ret;
}

static int32_t DiscStopDiscovery()
{
    int ret = StopDiscovery(DM_PKG_NAME, g_disc_sInfo.subscribeId);
    TEST_ASSERT_TRUE(ret == 0);
    return ret;
}

static int OnSessionOpened(int sessionId, int result)
{
    LOG_INFO("OnSessionOpened! sessionId = %d, result = %d", sessionId, result);
    cJSON *rspJson = cJSON_CreateObject();
    if (rspJson == NULL) {
        LOG_INFO("Failed to allocate rspJson memory.");
        return NULL;
    }

    AddIntToJson(rspJson, TAG_AUTH_TYPE, 1);
    AddBoolToJson(rspJson, TAG_CRYPTO_SUPPORT, false);
    AddStringToJson(rspJson, TAG_VER, DM_ITF_VER);
    AddStringToJson(rspJson, TAG_LOCAL_DEVICE_ID, "168BCD7BF6A368494A7E9C9B5A0D1C5153706CC0C555279BA8C759CA9FD5A1BF");
    AddIntToJson(rspJson, TAG_MSG_TYPE, 80);
    AddIntToJson(rspJson, "REPLY", -20022);

    char *rspBuffer = cJSON_PrintUnformatted(rspJson);
    if (rspBuffer == NULL) {
        LOG_INFO("Failed to pack rspJson to string.");
        cJSON_Delete(rspJson);
        return NULL;
    }
    cJSON_Delete(rspJson);

    SendBytes(sessionId, rspBuffer, strlen(rspBuffer));
    return 0;
}

static void OnSessionClosed(int sessionId)
{
    LOG_INFO("OnSessionClosed! sessionId = %d", sessionId);
}

static void OnBytesReceived(int sessionId, const void *data, unsigned int dataLen)
{
    LOG_INFO("OnBytesReceived! sessionId = %d, data: %s", sessionId, (const char *)data);

    cJSON *payload = cJSON_Parse((const char *)data);
    if (payload == NULL) {
        LOG_INFO("Cannot parse device ID message");
        cJSON_Delete(payload);
        return;
    }

    int32_t msgType;
    int32_t result = GetIntFromJson(payload, TAG_MSG_TYPE, &msgType);
    if (result != 0) {
        LOG_INFO("Cannot get msg type from message");
        cJSON_Delete(payload);
        return;
    }
    if (msgType == 90) {
        cJSON *rspJson = cJSON_CreateObject();
        if (rspJson == NULL) {
            LOG_INFO("Failed to allocate rspJson memory.");
            return;
        }

        AddStringToJson(rspJson, "APPDESC", "");
        AddStringToJson(rspJson, "APPNAME", "");
        AddStringToJson(rspJson, "APPOPERATION", "");
        AddIntToJson(rspJson, TAG_AUTH_TYPE, 1);
        AddStringToJson(rspJson, "CUSTOMDESC", "");
        AddStringToJson(rspJson, "DEVICEID", "a97d7bd5");
        AddStringToJson(rspJson, "HOST", "ohos.samples.distributedcalc");
        AddIntToJson(rspJson, "INDEX", 0);
        AddStringToJson(rspJson, TAG_VER, DM_ITF_VER);
        AddStringToJson(rspJson, TAG_LOCAL_DEVICE_ID,
                        "168BCD7BF6A368494A7E9C9B5A0D1C5153706CC0C555279BA8C759CA9FD5A1BF");
        AddIntToJson(rspJson, "LOCALDEVICETYPE", 0);
        AddIntToJson(rspJson, "MSG_TYPE", 100);
        AddStringToJson(rspJson, "REQUESTER", "kylin");
        AddStringToJson(rspJson, "TARGET", "ohos.samples.distributedcalc");
        AddIntToJson(rspJson, "THUMSIZE", 0);
        AddInt64StringToJson(rspJson, "TOKEN", 68694333);
        AddIntToJson(rspJson, "VISIBILITY", 0);
        AddIntToJson(rspJson, "SLICE", 1);

        char *rspBuffer = cJSON_PrintUnformatted(rspJson);
        if (rspBuffer == NULL) {
            LOG_INFO("Failed to pack rspJson to string.");
            cJSON_Delete(rspJson);
            return;
        }
        cJSON_Delete(rspJson);

        SendBytes(sessionId, rspBuffer, strlen(rspBuffer));
    } else if (msgType == 200) {
        char pinCode[16];
        LOG_INFO("Please input pinCode:");
        if (scanf_s("%s", pinCode, 16) <= 0) {
            LOG_INFO("Input parameter error.\n");
            return;
        }
        if (pinCode == NULL || strlen(pinCode) > 16) {
            LOG_INFO("Invalid auth key.");
            return;
        }

        const char *deviceId = GetStringFromJson(payload, "DEVICEID");
        if (deviceId == NULL) {
            LOG_INFO("Cannot get device ID from message");
            cJSON_Delete(payload);
            return;
        }

        const char *groupId = GetStringFromJson(payload, FIELD_GROUP_ID);
        if (groupId == NULL) {
            LOG_INFO("Cannot get groupId from message");
            cJSON_Delete(payload);
            return;
        }

        const char *groupName = GetStringFromJson(payload, "GROUPNAME");
        if (groupName == NULL) {
            LOG_INFO("Cannot get groupName from message");
            cJSON_Delete(payload);
            return;
        }

        int32_t requestId;
        int32_t result = GetIntFromJson(payload, "REQUESTID", &requestId);
        if (result != 0) {
            LOG_INFO("Cannot get request ID from message: ret == %d, id = %lld", result, requestId);
            cJSON_Delete(payload);
            return;
        }

        // 1. 构造参数
        cJSON *connectParams = cJSON_CreateObject();
        if (connectParams == NULL) {
            LOG_INFO("Failed to allocate addParams memory.");
            return;
        }

        AddStringToJson(connectParams, "DEVICE_ID", deviceId);
        AddStringToJson(connectParams, "WIFI_IP", g_ip);  // 点对点群组类型
        AddIntToJson(connectParams, "WIFI_PORT", g_port);

        cJSON *addParams = cJSON_CreateObject();
        if (addParams == NULL) {
            LOG_INFO("Failed to allocate addParams memory.");
            return;
        }

        AddStringToJson(addParams, FIELD_GROUP_ID, groupId);
        AddIntToJson(addParams, FIELD_GROUP_TYPE, 256);  // 点对点群组类型
        AddStringToJson(addParams, FIELD_PIN_CODE, pinCode);
        AddBoolToJson(addParams, FIELD_IS_ADMIN, false);
        AddStringToJson(addParams, FIELD_DEVICE_ID, "168BCD7BF6A368494A7E9C9B5A0D1C5153706CC0C555279BA8C759CA9FD5A1BF");
        AddStringToJson(addParams, FIELD_GROUP_NAME, groupName);
        AddStringToJson(addParams, FIELD_CONNECT_PARAMS, cJSON_PrintUnformatted(connectParams));

        char *addParamsStr = cJSON_PrintUnformatted(addParams);
        LOG_INFO("addParamsStr===============\n%s\n=========================", addParamsStr);
        if (addParamsStr == NULL) {
            LOG_INFO("Failed to pack addParams to string.");
            cJSON_Delete(addParams);
            return;
        }

        // 2. 异步邀请加入
        int32_t ret = GetGmInstance()->addMemberToGroup(DEFAULT_OS_ACCOUNT, requestId, DM_PKG_NAME, addParamsStr);

        // 3. 返回结果，释放资源
        cJSON_Delete(addParams);
        cJSON_free(addParamsStr);
        if (ret != HC_SUCCESS) {
            LOG_INFO("Invite peer to group failed");
            return;
        }

        LOG_INFO("addMemberToGroup sucess");
    }
    cJSON_Delete(payload);
}

int main(int argc, char *argv[])
{
    // 设置设备认证服务
    int32_t ret = InitDeviceAuthService();
    if (ret != HC_SUCCESS) {
        LOG_INFO("init hc service failed: %d", ret);
        return -1;
    }

    // 初始化 gm 实例
    if (GetGmInstance() == NULL) {
        LOG_INFO("GetGmInstance failed");
        return -1;
    }

    ISessionListener sessionListener = {.OnSessionOpened = OnSessionOpened,
                                        .OnSessionClosed = OnSessionClosed,
                                        .OnBytesReceived = OnBytesReceived,
                                        .OnMessageReceived = nullptr,
                                        .OnStreamReceived = nullptr};
    LOG_INFO("SoftbusListener constructor.");
    ret = CreateSessionServer(DM_PKG_NAME, DM_SESSION_NAME, &sessionListener, true);
    if (ret != 0) {
        LOG_INFO("[SOFTBUS]CreateSessionServer failed, ret: %d.", ret);
    } else {
        LOG_INFO("[SOFTBUS]CreateSessionServer ok.");
    }
    DiscStartDiscovery();
    // NodeBasicInfo *info = NULL;
    // int32_t num = 0;
    // GetAllNodeDeviceInfo(g_pkgName, &info, &num);
    // printf("GetAllNodeDeviceInfo num === %d\n", num);
    // for (int32_t i = 0; i < num; i++) {
    //     printf("Device%d======================\n");
    //     printf("name:%s\n", info[i].deviceName);
    //     printf("deviceTypeId:%d\n", info[i].deviceTypeId);
    //     printf("networkId:%s\n", info[i].networkId);
    //     printf("==============================\n");
    // }

    while (1) {
        pause();
    }
    return 0;
}
