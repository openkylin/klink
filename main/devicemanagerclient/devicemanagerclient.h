/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DISTRIBUTEDAUTHENTICATION_H
#define DISTRIBUTEDAUTHENTICATION_H

#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QListWidget>
#include <QMessageBox>
#include <QInputDialog>
#include <QPoint>
#include "devicemgr_comm.h"
#include "screenservice.h"
#include "display.h"

class DistributedDauthentication : public QWidget
{
    Q_OBJECT
public:
    ~DistributedDauthentication();
    static DistributedDauthentication &GetInstance();

Q_SIGNALS:
    void SigAddDscoveryDevInfo(const AuthDeviceInfo &);
    void SigAddConnectDevInfo(const AuthDeviceInfo &);
    void SigRemoveConnectDevInfo(const AuthDeviceInfo &);
    void SigChangeConnectDevInfo(const AuthDeviceInfo &);
    void SigGetUserOperation(const QString &deName);
    void SigCancelDisplay();
    void SigShowAuthStateUI(int);
    void SigShowAuthInfoDialog(const QString &);
    void SigInputAuthInfoDialog();

private Q_SLOTS:
    void OnNeedDisplay(bool isDisplay);
    void OnNewFrame(const char *data, int size);

    void OnAddDscoveryDevInfo(const AuthDeviceInfo &info);
    void OnAddConnectDevInfo(const AuthDeviceInfo &info);
    void OnRemoveConnectDevInfo(const AuthDeviceInfo &info);
    void OnChangeConnectDevInfo(const AuthDeviceInfo &info);
    void OnGetUserOperation(const QString &devName);
    void OnCancelDisplay();
    void OnShowAuthStateUI(int msgType);
    void OnShowAuthInfoDialog(const QString &info);
    void OnInputAuthInfoDialog();

    void OnRefreshBtnClicked();
    void OnDiscoveryBtnClicked();
    void OnDiscoveryDevItemClicked(QListWidgetItem *item);
    void OnCustomContextMenuRequested(const QPoint &pos);

protected:
    void paintEvent(QPaintEvent *event) override;
    void closeEvent(QCloseEvent *event) override;

private:
    DistributedDauthentication(QWidget *parent = nullptr);
    void InitUI();
    void RemoveDiscoveryDev(const QString &devId);

private:
    ScreenService *screenService_ = nullptr;
    Display *display_ = nullptr;

    QPushButton *refreshBtn_ = nullptr;
    QPushButton *discoveryBtn_ = nullptr;
    QListWidget *connectDevListWin_ = nullptr;
    QListWidget *discoveryDevListWin_ = nullptr;
    QMessageBox *authMessageBox_ = nullptr;
    QInputDialog *inputAuthInfoBox_ = nullptr;
};

#endif  // DISTRIBUTEDAUTHENTICATION_H
