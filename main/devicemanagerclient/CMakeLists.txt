set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

option(add_session_test "session test" OFF)

find_package(QT NAMES Qt5 COMPONENTS Widgets LinguistTools REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Widgets LinguistTools REQUIRED)

set(screen_inc "screen/include")
set(screen_src "screen/src")

include_directories(${screen_inc})

set(src
  "devicemanagerclient.h"
  "devicemanagerclient.cpp"
  "${screen_inc}/screenservice.h"
  "${screen_inc}/display.h"
  "${screen_src}/screenservice.cpp"
  "${screen_src}/display.cpp"
  "main.cpp"
)

set(lib
  Qt5::Widgets
  devicemgr_sdk
  devicemgrutils
  softbus_client
)

if(add_session_test)
  message(STATUS "connectivity_devicemgr_client add session test ...")
  set(src
      ${src}
      "sessiontest/sessiontest.h"
      "sessiontest/sessiontest.cpp"
  )
  add_compile_options("-DSESSION_TEST")
  find_package(OpenCV REQUIRED)
  set(lib
    ${lib}
    ${OpenCV_LIBS}
  )
  execute_process(COMMAND sh ${top_path}/main/devicemanagerclient/test.sh ${top_path}/main/devicemanagerclient)
endif()

add_executable(connectivity_devicemgr_client
                ${src}
              )

target_sources(connectivity_devicemgr_client PRIVATE "resources/resources.qrc")

set(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/output/bin)

target_include_directories(connectivity_devicemgr_client PUBLIC ${top_path}/device_manager/interface)
target_include_directories(connectivity_devicemgr_client PUBLIC ${top_path}/dsoftbus/include)

target_link_directories(connectivity_devicemgr_client PUBLIC /opt/dsoftbuslib)

target_link_libraries (
    connectivity_devicemgr_client
    PRIVATE
    ${lib}
)