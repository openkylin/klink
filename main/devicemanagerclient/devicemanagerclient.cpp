/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "devicemanagerclient.h"

#include <QIcon>
#include <QString>
#include <QPainter>
#include <QStyleOption>
#include <QBitmap>
#include <QPalette>
#include <QLabel>
#include <QFont>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QListWidgetItem>
#include <QInputDialog>
#include <QVariant>
#include <QMenu>
#include <QAction>
#include <string>
#include <unistd.h>
#include <vector>
#include <mutex>
#include "devicemanagersdk.h"

#ifdef SESSION_TEST
#    include "./sessiontest/sessiontest.h"
#endif  // SESSION_TEST

#define DISC_SUBSCRIBE_ID 100
#define PUBLISH_ID 4802
#define DEVICE_ID_ROLE 101
#define NETWORK_ID_ROLE 201

static std::mutex mtx;
static int32_t s_pinCode = 0;
static int32_t s_opt = 0;

const char *CAPABILITY = "osdCapability";
const char *PKG_NAME = "com.kylin.device.magr";

static AuthDeviceInfo s_local_deviceinfo;

static SubscribeInfo s_disc_sInfo = {.subscribeId = DISC_SUBSCRIBE_ID,
                                     .mode = DISCOVER_MODE_ACTIVE,
                                     .medium = COAP,
                                     .freq = HIGH,
                                     .isSameAccount = false,
                                     .isWakeRemote = false,
                                     .capability = CAPABILITY,
                                     .capabilityData = nullptr,
                                     .dataLen = 0};

static void OnDeviceChanged(AuthDeviceState state, const AuthDeviceInfo &info)
{
    switch (state) {
        case AuthDeviceState::DEVICE_STATE_ONLINE: {
            Q_EMIT DistributedDauthentication::GetInstance().SigAddConnectDevInfo(info);
        } break;
        case AuthDeviceState::DEVICE_STATE_OFFLINE: {
            Q_EMIT DistributedDauthentication::GetInstance().SigRemoveConnectDevInfo(info);
        } break;
        case AuthDeviceState::DEVICE_INFO_CHANGED: {
            Q_EMIT DistributedDauthentication::GetInstance().SigChangeConnectDevInfo(info);
        } break;
    }
}

static ISoftbusStateCallback s_deviceStateCb = {.OnDeviceChanged = OnDeviceChanged};

static void OnDeviceFound(const AuthDeviceInfo &info, bool isOnline)
{
    if (isOnline) {
        Q_EMIT DistributedDauthentication::GetInstance().SigAddConnectDevInfo(info);
    } else {
        Q_EMIT DistributedDauthentication::GetInstance().SigAddDscoveryDevInfo(info);
    }
}

static void OnDiscoverySuccess(int32_t subscribeId)
{
}

static void OnDiscoveryFailed(int32_t subscribeId, int32_t failedReason)
{
}

static ISoftbusDiscoveryCallback s_discoveryCb = {
    .OnDeviceFound = OnDeviceFound, .OnDiscoverySuccess = OnDiscoverySuccess, .OnDiscoveryFailed = OnDiscoveryFailed};

static void OnPublishResult(int32_t publishId, int32_t publishResult)
{
}

static ISoftbusPublishCallback s_publishCb = {.OnPublishResult = OnPublishResult};

void OnShowAuthState(int32_t msgType)
{
    Q_EMIT DistributedDauthentication::GetInstance().SigShowAuthStateUI((int)msgType);
}

void OnShowAuthInfo(int32_t code)
{
    Q_EMIT DistributedDauthentication::GetInstance().SigShowAuthInfoDialog(QString::number(code));
}

int32_t OnUserOperation(const char *remoteName)
{
    Q_EMIT DistributedDauthentication::GetInstance().SigGetUserOperation(remoteName);
    s_opt = -1;
    while (1) {
        std::unique_lock<std::mutex> lock(mtx);
        if (s_opt != -1) {
            break;
        }
    }

    return s_opt;
}

void OnCancelDisplay()
{
    Q_EMIT DistributedDauthentication::GetInstance().SigCancelDisplay();
}

int32_t OnInputAuthInfo()
{
    Q_EMIT DistributedDauthentication::GetInstance().SigInputAuthInfoDialog();

    s_pinCode = -1;
    while (1) {
        std::unique_lock<std::mutex> lock(mtx);
        if (s_pinCode != -1) {
            break;
        }
    }
    return s_pinCode;
}

static IUIStateUpdateCallback s_uiStateUpdateCb = {.OnShowAuthState = OnShowAuthState,
                                                   .OnShowAuthInfo = OnShowAuthInfo,
                                                   .OnUserOperation = OnUserOperation,
                                                   .OnCancelDisplay = OnCancelDisplay,
                                                   .OnInputAuthInfo = OnInputAuthInfo};

QString GetDeviceTypeIcon(AuthDeviceType type)
{
    switch (type) {
        case AuthDeviceType::DEVICE_TYPE_PHONE:
            return QString(":/phone.svg");
        case AuthDeviceType::DEVICE_TYPE_PC:
            return QString(":/pc.svg");
        default:
            return QString(":/pc.svg");
            break;
    }
}

DistributedDauthentication &DistributedDauthentication::GetInstance()
{
    static DistributedDauthentication instance;
    return instance;
}

DistributedDauthentication::DistributedDauthentication(QWidget *parent) : QWidget(parent)
{
#ifdef SESSION_TEST
    TestCreateSessionServer();
#endif  // SESSION_TEST

    qRegisterMetaType<AuthDeviceInfo>("AuthDeviceInfo");

    connect(this, &DistributedDauthentication::SigAddDscoveryDevInfo, this,
            &DistributedDauthentication::OnAddDscoveryDevInfo);
    connect(this, &DistributedDauthentication::SigAddConnectDevInfo, this,
            &DistributedDauthentication::OnAddConnectDevInfo);
    connect(this, &DistributedDauthentication::SigRemoveConnectDevInfo, this,
            &DistributedDauthentication::OnRemoveConnectDevInfo);
    connect(this, &DistributedDauthentication::SigChangeConnectDevInfo, this,
            &DistributedDauthentication::OnChangeConnectDevInfo);
    connect(this, &DistributedDauthentication::SigGetUserOperation, this,
            &DistributedDauthentication::OnGetUserOperation);
    connect(this, &DistributedDauthentication::SigCancelDisplay, this, &DistributedDauthentication::OnCancelDisplay);
    connect(this, &DistributedDauthentication::SigShowAuthStateUI, this,
            &DistributedDauthentication::OnShowAuthStateUI);
    connect(this, &DistributedDauthentication::SigShowAuthInfoDialog, this,
            &DistributedDauthentication::OnShowAuthInfoDialog);
    connect(this, &DistributedDauthentication::SigInputAuthInfoDialog, this,
            &DistributedDauthentication::OnInputAuthInfoDialog);

    RegisterDeviceStateCallback(PKG_NAME, &s_deviceStateCb);
    RegisterUIStateCallback(PKG_NAME, &s_uiStateUpdateCb);
    GetLocalDeviceInfo(PKG_NAME, s_local_deviceinfo);

    screenService_ = &ScreenService::GetInstance();
    connect(screenService_, &ScreenService::SigNeedDisplay, this, &DistributedDauthentication::OnNeedDisplay);
    connect(screenService_, &ScreenService::SigNewFrame, this, &DistributedDauthentication::OnNewFrame);

    InitUI();
}

void DistributedDauthentication::OnNeedDisplay(bool isDisplay)
{
    if (isDisplay) {
        if (display_ == nullptr) {
            display_ = new Display;
            connect(display_, &Display::SigClose, [this]() { screenService_->CloseScreenSession(); });
            display_->show();
        }
    } else {
        if (display_ != nullptr) {
            display_->deleteLater();
            display_ = nullptr;
        }
    }
}

void DistributedDauthentication::OnNewFrame(const char *data, int size)
{
    if (display_ != nullptr) {
        display_->UpdateImage(data, size);
    }
}

void DistributedDauthentication::OnGetUserOperation(const QString &devName)
{
    authMessageBox_ = new QMessageBox(this);
    authMessageBox_->setIcon(QMessageBox::Question);
    authMessageBox_->setWindowTitle("认证请求");
    QString txtStr = devName + QString("请求连接本机， 是否信任此设备？");
    authMessageBox_->setText(txtStr);
    authMessageBox_->addButton("信任", QMessageBox::AcceptRole);
    authMessageBox_->addButton("不信任", QMessageBox::RejectRole);
    int ret = authMessageBox_->exec();
    std::lock_guard<std::mutex> lock(mtx);
    s_opt = ret;
    authMessageBox_->deleteLater();
    authMessageBox_ = nullptr;
    return;
}

void DistributedDauthentication::OnAddDscoveryDevInfo(const AuthDeviceInfo &info)
{
    if (discoveryDevListWin_ != nullptr) {
        int countNum = discoveryDevListWin_->count();
        for (int i = 0; i < countNum; i++) {
            QListWidgetItem *item = discoveryDevListWin_->item(i);
            QString devId = item->data(DEVICE_ID_ROLE).toString();
            if (devId == QString(info.deviceId)) {
                return;
            }
        }
        QListWidgetItem *listitem = new QListWidgetItem;
        listitem->setData(DEVICE_ID_ROLE, QString(info.deviceId));
        listitem->setText(QString(info.deviceName));
        listitem->setIcon(QIcon(GetDeviceTypeIcon((AuthDeviceType)info.deviceTypeId)));
        discoveryDevListWin_->addItem(listitem);
    }
}

void DistributedDauthentication::RemoveDiscoveryDev(const QString &devId)
{
    if (discoveryDevListWin_ != nullptr) {
        int countNum = discoveryDevListWin_->count();
        for (int i = 0; i < countNum; i++) {
            QListWidgetItem *item = discoveryDevListWin_->item(i);
            QString deviceId = item->data(DEVICE_ID_ROLE).toString();
            if (devId == deviceId) {
                discoveryDevListWin_->removeItemWidget(item);
                delete item;
                discoveryDevListWin_->update();
                discoveryDevListWin_->show();
                return;
            }
        }
    }
}

void DistributedDauthentication::OnAddConnectDevInfo(const AuthDeviceInfo &info)
{
    RemoveDiscoveryDev(info.deviceId);
    if (connectDevListWin_ != nullptr) {
        int countNum = connectDevListWin_->count();
        for (int i = 0; i < countNum; i++) {
            QListWidgetItem *item = connectDevListWin_->item(i);
            QString networkId = item->data(NETWORK_ID_ROLE).toString();
            if (networkId == QString(info.networkId)) {
                return;
            }
        }
        QListWidgetItem *listitem = new QListWidgetItem;
        listitem->setData(NETWORK_ID_ROLE, QString(info.networkId));
        listitem->setText(QString(info.deviceName));
        listitem->setIcon(QIcon(GetDeviceTypeIcon((AuthDeviceType)info.deviceTypeId)));
        connectDevListWin_->addItem(listitem);
    }
}

void DistributedDauthentication::OnRemoveConnectDevInfo(const AuthDeviceInfo &info)
{
    if (connectDevListWin_ != nullptr) {
        int countNum = connectDevListWin_->count();
        for (int i = 0; i < countNum; i++) {
            QListWidgetItem *item = connectDevListWin_->item(i);
            QString networkId = item->data(NETWORK_ID_ROLE).toString();
            if (networkId == QString(info.networkId)) {
                connectDevListWin_->removeItemWidget(item);
                return;
            }
        }
    }
}

void DistributedDauthentication::OnChangeConnectDevInfo(const AuthDeviceInfo &info)
{
    if (connectDevListWin_ != nullptr) {
        int countNum = connectDevListWin_->count();
        for (int i = 0; i < countNum; i++) {
            QListWidgetItem *item = connectDevListWin_->item(i);
            QString networkId = item->data(NETWORK_ID_ROLE).toString();
            if (networkId == QString(info.networkId)) {
                item->setText(QString(info.deviceName));
                item->setIcon(QIcon(GetDeviceTypeIcon((AuthDeviceType)info.deviceTypeId)));
                return;
            }
        }
    }
}

void DistributedDauthentication::OnCancelDisplay()
{
    if (authMessageBox_ != nullptr) {
        authMessageBox_->hide();
        authMessageBox_->deleteLater();
        authMessageBox_ = nullptr;
    }
}

void DistributedDauthentication::OnShowAuthStateUI(int msgType)
{
    if (authMessageBox_ != nullptr) {
        authMessageBox_->hide();
        authMessageBox_->deleteLater();
        authMessageBox_ = nullptr;
    }
    switch ((UiStateMsg)msgType) {
        case UiStateMsg::MSG_PIN_CODE_ERROR:
            authMessageBox_ = new QMessageBox(this);
            authMessageBox_->critical(this, "PIN码认证", "PIN码输入错误");
            authMessageBox_->exec();
            authMessageBox_->deleteLater();
            authMessageBox_ = nullptr;
            break;
        case UiStateMsg::MSG_PIN_CODE_SUCCESS:
            /* code */
            break;
        case UiStateMsg::MSG_CANCEL_PIN_CODE_SHOW:
            /* code */
            break;
        case UiStateMsg::MSG_CANCEL_PIN_CODE_INPUT:
            /* code */
            break;
        case UiStateMsg::MSG_DOING_AUTH:
            /* code */
            break;
        default:
            break;
    }
}

void DistributedDauthentication::OnShowAuthInfoDialog(const QString &info)
{
    authMessageBox_ = new QMessageBox(this);
    authMessageBox_->setAttribute(Qt::WA_DeleteOnClose);
    authMessageBox_->setWindowTitle("PIN码认证");
    QString txt = QString("请在设备端输入连接码进行连接\n") + info;
    authMessageBox_->setText(txt);
    authMessageBox_->show();
}

void DistributedDauthentication::OnInputAuthInfoDialog()
{
    inputAuthInfoBox_ = new QInputDialog(this);
    inputAuthInfoBox_->setWindowTitle("PIN码认证");
    inputAuthInfoBox_->setLabelText("请输入PIN码:");
    inputAuthInfoBox_->setInputMode(QInputDialog::TextInput);
    inputAuthInfoBox_->setCancelButtonText("取消");
    inputAuthInfoBox_->setOkButtonText("确认");
    int ret = inputAuthInfoBox_->exec();
    int32_t code = 0;
    if (ret == QInputDialog::Accepted) {
        code = inputAuthInfoBox_->textValue().toInt();
    } else {
        code = 0;
    }
    std::lock_guard<std::mutex> lock(mtx);
    s_pinCode = code;
    inputAuthInfoBox_->deleteLater();
    inputAuthInfoBox_ = nullptr;
    return;
}

void DistributedDauthentication::OnRefreshBtnClicked()
{
    const QPalette oldPal = refreshBtn_->palette();
    QPalette newPal;
    newPal.setColor(QPalette::Button, QColor(255, 0, 0));
    refreshBtn_->setPalette(newPal);
    int devNum = 0;
    AuthDeviceInfo *deviceInfo = nullptr;
    (void)GetTrustedDeviceList(PKG_NAME, &deviceInfo, &devNum);
    if (connectDevListWin_ != nullptr) {
        connectDevListWin_->clear();
        for (int i = 0; i < devNum; i++) {
            AuthDeviceInfo *info = deviceInfo + i;
            OnAddConnectDevInfo(*info);
        }
    }
    refreshBtn_->setPalette(oldPal);
}

void DistributedDauthentication::OnDiscoveryBtnClicked()
{
    if (discoveryBtn_->text() == QString("发现")) {
        discoveryBtn_->setText("停止");
        srand(time(NULL));
        s_disc_sInfo.subscribeId = rand() % 65535;
        StartDeviceDiscovery(PKG_NAME, s_disc_sInfo, &s_discoveryCb);
    } else {
        discoveryBtn_->setText("发现");
        StopDeviceDiscovery(PKG_NAME, s_disc_sInfo.subscribeId);
    }
}

void DistributedDauthentication::OnDiscoveryDevItemClicked(QListWidgetItem *item)
{
    QString devId = item->data(DEVICE_ID_ROLE).toString();
    AuthenticateDevice(PKG_NAME, devId.toStdString().c_str());
}

void DistributedDauthentication::OnCustomContextMenuRequested(const QPoint &pos)
{
    QListWidgetItem *item = connectDevListWin_->itemAt(pos);
    if (item != nullptr) {
        QMenu menu(connectDevListWin_);
        QAction *screenAction = menu.addAction("投屏");
        QAction *delAction = menu.addAction("删除");
        connect(screenAction, &QAction::triggered, [item]() {
            // todo
        });
        connect(delAction, &QAction::triggered, [item]() {
            QString networkId = item->data(NETWORK_ID_ROLE).toString();
            UnAuthenticateDevice(PKG_NAME, networkId.toStdString().c_str());
        });

#ifdef SESSION_TEST
        QAction *testAction1 = menu.addAction("发送字节数据测试");
        connect(testAction1, &QAction::triggered, [item]() {
            QString networkId = item->data(NETWORK_ID_ROLE).toString();
            TestOpenSession(TYPE_BYTES, networkId.toStdString().c_str());
        });
        QAction *testAction2 = menu.addAction("发送消息测试");
        connect(testAction2, &QAction::triggered, [item]() {
            QString networkId = item->data(NETWORK_ID_ROLE).toString();
            TestOpenSession(TYPE_MESSAGE, networkId.toStdString().c_str());
        });
        QAction *testAction3 = menu.addAction("发送流数据测试");
        connect(testAction3, &QAction::triggered, [item]() {
            QString networkId = item->data(NETWORK_ID_ROLE).toString();
            TestOpenSession(TYPE_STREAM, networkId.toStdString().c_str());
        });
        QAction *testAction4 = menu.addAction("发送文件测试");
        connect(testAction4, &QAction::triggered, [item]() {
            QString networkId = item->data(NETWORK_ID_ROLE).toString();
            TestOpenSession(TYPE_FILE, networkId.toStdString().c_str());
        });
#endif  // SESSION_TEST

        menu.exec(QCursor::pos());
    }
}

DistributedDauthentication::~DistributedDauthentication()
{
}

void DistributedDauthentication::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QStyleOption opt;
    opt.initFrom(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);  // 绘制样式

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);  // 反锯齿;
    QColor mainColor;
    mainColor = QColor("#F6F6F6");
    painter.setBrush(QBrush(mainColor));
    painter.setPen(Qt::transparent);
    QRect rect = this->rect();
    rect.setWidth(rect.width());
    rect.setHeight(rect.height());
    painter.drawRoundedRect(rect, 8, 8);
    QWidget::paintEvent(event);

    return;
}

void DistributedDauthentication::closeEvent(QCloseEvent *event)
{
    screenService_ = nullptr;
    if (display_ != nullptr) {
        display_->deleteLater();
        display_ = nullptr;
    }
#ifdef SESSION_TEST
    TestCloseSession();
    TestRemoveSessionServer();
#endif  // SESSION_TEST
    DestoryClient(PKG_NAME);

    QWidget::closeEvent(event);
}

void DistributedDauthentication::InitUI()
{
    QPushButton *devTypeIcon = new QPushButton(this);
    devTypeIcon->setIconSize(QSize(48, 48));
    QString titleIconStyle = "QPushButton{border:0px;border-radius:4px;background:transparent;}"
                             "QPushButton:Hover{border:0px;border-radius:4px;background:transparent;}"
                             "QPushButton:Pressed{border:0px;border-radius:4px;background:transparent;}";
    devTypeIcon->setStyleSheet(titleIconStyle);
    devTypeIcon->setIcon(QIcon(GetDeviceTypeIcon((AuthDeviceType)s_local_deviceinfo.deviceTypeId)));
    devTypeIcon->setContentsMargins(8, 8, 8, 8);

    QFont font;
    font.setBold(true);
    font.setPointSizeF(20);
    QLabel *devNameLab = new QLabel(this);
    devNameLab->setText(QString(s_local_deviceinfo.deviceName));

    QHBoxLayout *hLayout1 = new QHBoxLayout;
    hLayout1->addWidget(devTypeIcon);
    hLayout1->addSpacing(8);
    hLayout1->addWidget(devNameLab);
    hLayout1->addStretch();

    QLabel *connectedDescribeLab = new QLabel(this);
    connectedDescribeLab->setText("已连接设备");

    refreshBtn_ = new QPushButton(this);
    connect(refreshBtn_, &QPushButton::clicked, this, &DistributedDauthentication::OnRefreshBtnClicked);
    refreshBtn_->setText("刷新");

    QHBoxLayout *hLayout2 = new QHBoxLayout;
    hLayout2->setSpacing(8);
    hLayout2->setMargin(8);
    hLayout2->addWidget(connectedDescribeLab);
    hLayout2->addStretch();
    hLayout2->addWidget(refreshBtn_);

    connectDevListWin_ = new QListWidget(this);
    connectDevListWin_->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(connectDevListWin_, &QListWidget::customContextMenuRequested, this,
            &DistributedDauthentication::OnCustomContextMenuRequested);

    QLabel *availableDevDescribeLab = new QLabel(this);
    availableDevDescribeLab->setText("可用设备");

    discoveryBtn_ = new QPushButton(this);
    connect(discoveryBtn_, &QPushButton::clicked, this, &DistributedDauthentication::OnDiscoveryBtnClicked);
    discoveryBtn_->setText("发现");

    QHBoxLayout *hLayout3 = new QHBoxLayout;
    hLayout3->setSpacing(8);
    hLayout3->setMargin(8);
    hLayout3->addWidget(availableDevDescribeLab);
    hLayout3->addStretch();
    hLayout3->addWidget(discoveryBtn_);

    discoveryDevListWin_ = new QListWidget(this);
    connect(discoveryDevListWin_, &QListWidget::itemClicked, this,
            &DistributedDauthentication::OnDiscoveryDevItemClicked);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addLayout(hLayout1);
    mainLayout->addLayout(hLayout2);
    mainLayout->addWidget(connectDevListWin_);
    mainLayout->addLayout(hLayout3);
    mainLayout->addWidget(discoveryDevListWin_);

    setLayout(mainLayout);
}