/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "display.h"
#include "screenlog.h"
#include <QPainter>
#include <QIcon>
#include <QString>

Display::Display(QWidget *parent) : QWidget(parent)
{
    setWindowTitle("设备投屏");
    setWindowIcon(QIcon(":/app.png"));
    setFixedSize(432, 798);
}

Display::~Display()
{
}

void Display::UpdateImage(const char *data, int size)
{
    if (!image_.loadFromData((const unsigned char *)data, size, "JPEG")) {
        SCREEN_LOG("UpdateImage failed!");
        return;
    }
    update();
}

void Display::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.drawImage(rect(), image_);
}

void Display::closeEvent(QCloseEvent *event)
{
    Q_EMIT SigClose();
}