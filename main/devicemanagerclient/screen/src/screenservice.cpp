/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "screenservice.h"
#include "screenlog.h"

const char *SCREEN_PKGNAME = "kylin.data";
const char *SCREEN_SESSION_NAME = "kylin.screen";

static int32_t s_sessionId = 0;

ISessionListener ScreenService::sessionListener_ = {.OnSessionOpened = ScreenService::OnSessionOpened,
                                                    .OnSessionClosed = ScreenService::OnSessionClosed,
                                                    .OnBytesReceived = ScreenService::OnBytesReceived,
                                                    .OnMessageReceived = ScreenService::OnMessageReceived,
                                                    .OnStreamReceived = ScreenService::OnStreamReceived,
                                                    .OnQosEvent = ScreenService::OnQosEvent};

ScreenService::ScreenService(QObject *parent) : QObject(parent)
{
    (void)CreateSessionServer(SCREEN_PKGNAME, SCREEN_SESSION_NAME, &sessionListener_, true);
}

ScreenService::~ScreenService()
{
    CloseSession(s_sessionId);
    s_sessionId = 0;
    RemoveSessionServer(SCREEN_PKGNAME, SCREEN_SESSION_NAME);
}

void ScreenService::CloseScreenSession()
{
    CloseSession(s_sessionId);
    s_sessionId = 0;
}

int32_t ScreenService::OnSessionOpened(int32_t sessionId, int32_t result)
{
    SCREEN_LOG("OnSessionOpened, result: %d.", result);
    if (result != -1) {
        s_sessionId = sessionId;
        Q_EMIT ScreenService::GetInstance().SigNeedDisplay(true);
    }
    return 0;
}

void ScreenService::OnSessionClosed(int32_t sessionId)
{
    SCREEN_LOG("OnSessionClosed");

    Q_EMIT ScreenService::GetInstance().SigNeedDisplay(false);
}

void ScreenService::OnBytesReceived(int32_t sessionId, const void *data, uint32_t dataLen)
{
    SCREEN_LOG("OnBytesReceived: %s", (char *)data);
}

void ScreenService::OnStreamReceived(int32_t sessionId, const StreamData *data, const StreamData *ext,
                                     const StreamFrameInfo *frameInfo)
{
    if (data == nullptr) {
        SCREEN_LOG("OnStreamReceived Stream data is null");
        return;
    }
    SCREEN_LOG("OnStreamReceived Stream data size: %d", data->bufLen);
    Q_EMIT ScreenService::GetInstance().SigNewFrame(data -> buf, data->bufLen);
}

void ScreenService::OnMessageReceived(int sessionId, const void *data, unsigned int dataLen)
{
    SCREEN_LOG("OnMessageReceived: %s", (char *)data);
}

void ScreenService::OnQosEvent(int sessionId, int eventId, int tvCount, const QosTv *tvList)
{
    SCREEN_LOG("OnQosEvent event: %d", eventId);
}