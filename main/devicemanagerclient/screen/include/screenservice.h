/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SCREENSERVICE_H
#define SCREENSERVICE_H

#include <QObject>
#include <QThread>
#include <string.h>
#include "session.h"

class ScreenService : public QObject
{
    Q_OBJECT
public:
    static ScreenService &GetInstance()
    {
        static ScreenService instance;
        return instance;
    }
    ~ScreenService();

    void CloseScreenSession();

    static int32_t OnSessionOpened(int32_t sessionId, int32_t result);
    static void OnSessionClosed(int32_t sessionId);
    static void OnBytesReceived(int32_t sessionId, const void *data, uint32_t dataLen);
    static void OnStreamReceived(int32_t sessionId, const StreamData *data, const StreamData *ext,
                                 const StreamFrameInfo *frameInfo);
    static void OnMessageReceived(int sessionId, const void *data, unsigned int dataLen);
    static void OnQosEvent(int sessionId, int eventId, int tvCount, const QosTv *tvList);

Q_SIGNALS:
    void SigNeedDisplay(bool);
    void SigNewFrame(const char *, int);

private:
    ScreenService(QObject *parent = nullptr);

private:
    static ISessionListener sessionListener_;
};

#endif  // SCREENSERVICE_H