/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "sessiontest.h"

#include <unistd.h>
#include <inttypes.h>
#include <fstream>
#include <vector>
#include <opencv2/opencv.hpp>
#include "securec.h"
#include "session.h"
#include "softbus_common.h"
#include "devicemgr_comm.h"
#include "softbus_adapter_log.h"

const char *SESSION_TEST_NAME = "kylin.test.session";
const char *TEST_PKG_NAME = "kylin.test";
static int32_t g_testSessionId = 0;
static int g_dataType = -1;
static bool g_isSend = false;

static int32_t SessionTestOnSessionOpened(int32_t sessionId, int32_t result)
{
    LOG_INFO("[SessionTest]SessionTestOnSessionOpened, result: %d.", result);
    if (result != -1) {
        g_testSessionId = sessionId;
        if (g_isSend) {
            switch (g_dataType) {
                case TYPE_BYTES: {
                    TestSendBytes();
                } break;

                case TYPE_MESSAGE: {
                    TestSendMessage();
                } break;
                case TYPE_STREAM: {
                    TestSendStream();
                } break;
                case TYPE_FILE: {
                    TestSendFile();
                } break;
                default:
                    break;
            }
        }
    }
    return DEV_CENTER_OK;
}

static void SessionTestOnSessionClosed(int32_t sessionId)
{
    LOG_INFO("[SessionTest]SessionTestOnSessionClosed");
}

static void SessionTestOnBytesReceived(int32_t sessionId, const void *data, uint32_t dataLen)
{
    LOG_INFO("[SessionTest]SessionTestOnBytesReceived: %s", (char *)data);
}

static void SessionTestOnStreamReceived(int32_t sessionId, const StreamData *data, const StreamData *ext,
                                        const StreamFrameInfo *frameInfo)
{
    if (data == nullptr) {
        LOG_INFO("[SessionTest]SessionTestOnStreamReceived Stream data is null");
        return;
    }
    LOG_INFO("[SessionTest]SessionTestOnStreamReceived Stream data size: %d", data->bufLen);
    std::vector<uchar> buffer(data->bufLen);
    int size = data->bufLen;
    for (int i = 0; i < size; i++) {
        buffer[i] = (uchar)(data->buf[i]);
    }
    cv::Mat decodedImage = cv::imdecode(cv::Mat(buffer), cv::IMREAD_COLOR);
    if (decodedImage.empty()) {
        LOG_INFO("[SessionTest]SessionTestOnStreamReceived failed");
        return;
    }
    // 创建窗口
    cv::namedWindow("Image Display", cv::WINDOW_AUTOSIZE);

    // 显示图片
    cv::imshow("Image Display", decodedImage);

    // 等待按键事件
    cv::waitKey(0);
}

static void SessionTestOnMessageReceived(int sessionId, const void *data, unsigned int dataLen)
{
    LOG_INFO("[SessionTest]SessionTestOnMessageReceived: %s", (char *)data);
}

static void SessionTestOnQosEvent(int sessionId, int eventId, int tvCount, const QosTv *tvList)
{
    LOG_INFO("[SessionTest]SessionTestOnQosEvent event: %d", eventId);
}

static ISessionListener s_sessionListener = {.OnSessionOpened = SessionTestOnSessionOpened,
                                             .OnSessionClosed = SessionTestOnSessionClosed,
                                             .OnBytesReceived = SessionTestOnBytesReceived,
                                             .OnMessageReceived = SessionTestOnMessageReceived,
                                             .OnStreamReceived = SessionTestOnStreamReceived,
                                             .OnQosEvent = SessionTestOnQosEvent};

static int SessionTestOnSendFileProcess(int sessionId, uint64_t bytesUpload, uint64_t bytesTotal)
{
    LOG_INFO("[SessionTest]SessionTestOnSendFileProcess, Upload = %llu/%llu", bytesUpload, bytesTotal);
    return 0;
}

static int SessionTestOnSendFileFinished(int sessionId, const char *firstFile)
{
    LOG_INFO("[SessionTest]SessionTestOnSendFileFinished file = %s", firstFile);
    return 0;
}

static void SessionTestOnFileTransError(int sessionId)
{
    LOG_INFO("[SessionTest]SessionTestOnFileTransError");
}

static IFileSendListener g_fileSendListener = {
    .OnSendFileProcess = SessionTestOnSendFileProcess,
    .OnSendFileFinished = SessionTestOnSendFileFinished,
    .OnFileTransError = SessionTestOnFileTransError,
};

static int SessionTestOnReceiveFileStarted(int sessionId, const char *files, int fileCnt)
{
    LOG_INFO("[SessionTest]SessionTestOnReceiveFileStarted, first file = %s, fileCnt = %d", files, fileCnt);
    return 0;
}

static int SessionTestOnReceiveFileProcess(int sessionId, const char *firstFile, uint64_t bytesUpload,
                                           uint64_t bytesTotal)
{
    LOG_INFO("[SessionTest]SessionTestOnReceiveFileProcess, first file = %s, Upload = %llu/%llu", firstFile,
             bytesUpload, bytesTotal);
    return 0;
}

static void SessionTestOnReceiveFileFinished(int sessionId, const char *files, int fileCnt)
{
    LOG_INFO("[SessionTest]SessionTestOnReceiveFileFinished, first file = %s, fileCnt = %d", files, fileCnt);
}

// Callbacks related to file receiving
static IFileReceiveListener g_fileRecvListener = {
    .OnReceiveFileStarted = SessionTestOnReceiveFileStarted,
    .OnReceiveFileProcess = SessionTestOnReceiveFileProcess,
    .OnReceiveFileFinished = SessionTestOnReceiveFileFinished,
    .OnFileTransError = SessionTestOnFileTransError,
};

int32_t TestCreateSessionServer()
{
    int32_t ret = SetFileSendListener(TEST_PKG_NAME, SESSION_TEST_NAME, &g_fileSendListener);
    if (ret != DEV_CENTER_OK) {
        LOG_ERR("[SessionTest]SetFileSendListener failed, ret: %d.", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    ret = SetFileReceiveListener(TEST_PKG_NAME, SESSION_TEST_NAME, &g_fileRecvListener, "/tmp/");
    if (ret != DEV_CENTER_OK) {
        LOG_ERR("[SessionTest]SetFileReceiveListener failed, ret: %d.", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    ret = CreateSessionServer(TEST_PKG_NAME, SESSION_TEST_NAME, &s_sessionListener, true);
    if (ret != DEV_CENTER_OK) {
        LOG_ERR("[SessionTest]CreateSessionServer failed, ret: %d.", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    return DEV_CENTER_OK;
}

int32_t TestRemoveSessionServer()
{
    int32_t ret = RemoveSessionServer(TEST_PKG_NAME, SESSION_TEST_NAME);
    if (ret != DEV_CENTER_OK) {
        LOG_ERR("[SessionTest]RemoveSessionServer failed, ret: %d.", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    return DEV_CENTER_OK;
}

int32_t TestOpenSession(int type, const char *peerNetworkId)
{
    g_dataType = type;
    g_isSend = true;
    SessionAttribute attr = {0};
    attr.dataType = type;
    if (type == TYPE_STREAM) {
        attr.attr.streamAttr.streamType = COMMON_VIDEO_STREAM;
    }
    attr.linkTypeNum = LINK_TYPE_MAX;
    LinkType linkTypeList[LINK_TYPE_MAX] = {
        LINK_TYPE_WIFI_P2P,
        LINK_TYPE_WIFI_WLAN_5G,
        LINK_TYPE_WIFI_WLAN_2G,
        LINK_TYPE_BR,
    };
    int32_t ret = memcpy_s(attr.linkType, sizeof(attr.linkType), linkTypeList, sizeof(linkTypeList));
    if (ret != EOK) {
        LOG_ERR("[SessionTest]OpenSession Data copy failed.");
        return ERR_DEV_CENTER_FAILED;
    }
    int32_t sessionId = OpenSession(SESSION_TEST_NAME, SESSION_TEST_NAME, peerNetworkId, "0", &attr);
    if (sessionId < 0) {
        LOG_ERR("[SessionTest]OpenSession failed sessionname: %s", SESSION_TEST_NAME);
        return ERR_DEV_CENTER_FAILED;
    }
    g_testSessionId = sessionId;
    LOG_INFO("[SessionTest]OpenSession success sessionname: %s", SESSION_TEST_NAME);
    return DEV_CENTER_OK;
}

int32_t TestCloseSession()
{
    if (g_testSessionId < 0) {
        LOG_WARN("[SessionTest]CloseSession No Session");
        return ERR_DEV_CENTER_NOT_INIT;
    }
    CloseSession(g_testSessionId);
    return DEV_CENTER_OK;
}

int32_t TestSendBytes()
{
    const char *data = "SendBytes Test";
    int32_t ret = SendBytes(g_testSessionId, data, strlen(data));
    if (ret != DEV_CENTER_OK) {
        LOG_ERR("[SessionTest]SendBytes failed ret: %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    return DEV_CENTER_OK;
}

int32_t TestSendMessage()
{
    const char *data = "SendMessage Test";
    int32_t ret = SendMessage(g_testSessionId, data, strlen(data));
    if (ret != DEV_CENTER_OK) {
        LOG_ERR("[SessionTest]SendMessage failed ret: %d", ret);
    }

    const char *memcmd = "[REQEST]GetMemInfo";
    ret = SendMessage(g_testSessionId, memcmd, strlen(memcmd));
    if (ret != DEV_CENTER_OK) {
        LOG_ERR("[SessionTest]TestGetMeminfo failed ret: %d", ret);
    }

    const char *cpucmd = "[REQEST]GetCpuInfo";
    ret = SendMessage(g_testSessionId, cpucmd, strlen(cpucmd));
    if (ret != DEV_CENTER_OK) {
        LOG_ERR("[SessionTest]TestGetMeminfo failed ret: %d", ret);
    }
    return DEV_CENTER_OK;
}

int32_t TestSendStream()
{
    cv::Mat image = cv::imread("/tmp/sessiontest/test.jpg");
    if (image.empty()) {
        LOG_ERR("[SessionTest]SendStream load image failed");
        return ERR_DEV_CENTER_FAILED;
    }

    // 将图片转换为流数据
    std::vector<uchar> buffer;
    cv::imencode(".jpg", image, buffer);

    StreamData data = {reinterpret_cast<char *>(buffer.data()), buffer.size()};
    StreamData ext = {0};
    StreamFrameInfo frameInfo = {0};
    int32_t ret = SendStream(g_testSessionId, &data, &ext, &frameInfo);
    if (ret != DEV_CENTER_OK) {
        LOG_ERR("[SessionTest]SendStream failed ret: %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    return DEV_CENTER_OK;
}

int32_t TestSendFile()
{
    const char *sfileList[] = {
        "/tmp/sessiontest/test.jpg",
        "/tmp/sessiontest/app.png",
        "/tmp/sessiontest/pc.svg",
    };
    int32_t ret = SendFile(g_testSessionId, sfileList, NULL, 3);
    if (ret != DEV_CENTER_OK) {
        LOG_ERR("[SessionTest]TestSendFile failed ret: %d", ret);
        return ERR_DEV_CENTER_FAILED;
    }
    return DEV_CENTER_OK;
}
