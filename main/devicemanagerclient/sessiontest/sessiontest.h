/*
 * Copyright (c) 2023-2024 KylinSoft Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SESSION_TEST_H
#define SESSION_TEST_H

#include <stdint.h>
#include <string.h>

int32_t TestCreateSessionServer();
int32_t TestRemoveSessionServer();
int32_t TestOpenSession(int type, const char *peerNetworkId);
int32_t TestCloseSession();
int32_t TestSendBytes();
int32_t TestSendMessage();
int32_t TestSendStream();
int32_t TestSendFile();

#endif  // SESSION_TEST_H
