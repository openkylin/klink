# Copyright&LICENSE

Copyright (c) 2023-2024 KylinSoft Co., Ltd.

kylin-dsoftbus-standard is licensed under the Apache License, Version 2.0 (the "License");

Please check the LICENSE file for more information.

kylin-dsoftbus-standard fork from openharmony dsoftbus;

You can access the source open source software address through this URL：

https://gitee.com/openharmony/communication_dsoftbus

# 接口文档（linux开发）

------

sdk名称：互联互通应用sdk

版本：v1.0.0

发布日期：2024.04.30

开发团队：麒麟软件有限公司星光麒麟事业部

联系邮箱：[xxx@kylinos.cn](mailto:xxx@kylinos.cn)

------

## 1. 简介

互联互通SDK为应用接入互联互通功能提供了便捷的编程接口，支持C/C++语言。通过集成此SDK，开发者可以快速实现应用接入互联互通体系，简化开发流程，提高开发效率。本文档详细介绍了SDK的核心接口及其参数说明、示例代码以及常见问题处理。

## 2. 快速开始

（1）编译

```bash
cd src-path # 进入源码路径
# 编译依赖
# 查看build-depends文件
# 这里需要使用3.0.0以上版本的openssl库
# 这里在third_party/openssl/下提供已编译的openssl库，仅支持linux使用，其他平台需要自行编译
# 执行
sudo bash ./third_party/openssl/openssl_install.sh ./third_party/openssl [amd|arm]
# 进行动态库植入
# 如果你是其他平台或架构，请自己编译openssl版本，非常抱歉
mkdir build & cd build
cmake ..
make
sudo make install
```

（2）运行

目前需要自己将klink的服务拉起

```bash
# 启动软总线服务
sudo /opt/dsoftbus/bin/connectivity_softbus_server
# 启动设备管理服务
sudo /opt/dsoftbus/bin/connectivity_devicemgr_service
# 启动认证客户端（可以根据sdk接口自己实现）
sudo /opt/dsoftbus/bin/connectivity_devicemgr_client
```

（3）cmake链接库说明

```cmake
find_package(PkgConfig REQUIRED)
pkg_check_modules(SOFTBUS_CLIENT REQUIRED softbus_client)
pkg_check_modules(DEVICEMGR_SDK REQUIRED devicemgr_sdk)

include_directories(${SOFTBUS_CLIENT_INCLUDE_DIRS})
include_directories(${DEVICEMGR_SDK_INCLUDE_DIRS})

target_link_libraries (
    ${PROJECT_NAME}
    PUBLIC
    ${SOFTBUS_CLIENT_LIBRARIES}
    ${DEVICEMGR_SDK_LIBRARIES}
    )
```

然后就可以进行klink业务的开发了

## 3. 核心接口

#### （1）获取本机信息接口

**功能描述：**业务层可以通过此接口获取本设备的可用信息。

**接口：**

```c++
typedef struct AuthDeviceInfo {
    // 设备ID
    char deviceId[DEV_CENTER_MAX_DEVICE_ID_LEN];
    // 设备名称
    char deviceName[DEV_CENTER_MAX_DEVICE_NAME_LEN];
    // 设备类型
    uint16_t deviceTypeId;
    // 设备组网ID
    char networkId[DEV_CENTER_MAX_DEVICE_ID_LEN];
    // 发现距离
    int32_t range;
    int32_t networkType;
    AuthForm authForm;
} AuthDeviceInfo;

int32_t GetLocalDeviceInfo(const char *pkgName, AuthDeviceInfo &info);
```

**请求参数：**        

| 参数名  | 类型             | 是否必填 | 描述                   |
| ------- | ---------------- | -------- | ---------------------- |
| pkgName | const char *     | 是       | 业务侧应用名           |
| info    | AuthDeviceInfo & | 是       | 输出参数，输出设备信息 |

**返回值：**成功返回0

**使用说明：**

```c++
// 包含头文件
#include <devicemgr_comm.h>
#include <devicemanagersdk.h>

const char *PKG_NAME = "test";
AuthDeviceInfo g_local_deviceinfo;

if (GetLocalDeviceInfo(PKG_NAME, &g_local_deviceinfo) == 0) 
{
    // 获取信息成功
}
```

#### （2）注册/注销设备监听接口

**功能描述：**业务层需要监听（或不再监听）网络中的节点状态变化等事件时，使用以下节点（及设备）注册和注销接口。

**接口：**

```c++
// 设备上线下通知回调
typedef struct {
    void (*OnDeviceChanged)(AuthDeviceState state, const AuthDeviceInfo &info);
} ISoftbusStateCallback;

// 注册设备监听回调
int32_t RegisterDeviceStateCallback(const char *pkgName, const ISoftbusStateCallback *callback);
// 注销设备监听回调注册
int32_t UnRegisterDeviceStateCallback(const char *pkgName);
```

**请求参数：**        

| 参数名  | 类型                          | 是否必填 | 描述                     |
| ------- | ----------------------------- | -------- | ------------------------ |
| pkgName | const char *                  | 是       | 业务侧应用名             |
| info    | const ISoftbusStateCallback * | 是       | 设备状态变化通知回调函数 |

**返回值：**成功返回0

**使用说明：**

```c++
// 包含头文件
#include <devicemgr_comm.h>
#include <devicemanagersdk.h>

const char *PKG_NAME = "test";

void OnDeviceChanged(AuthDeviceState state, const AuthDeviceInfo &info)
{
    switch (state) {
        case AuthDeviceState::DEVICE_STATE_ONLINE: {
            // 节点上线
        } break;
        case AuthDeviceState::DEVICE_STATE_OFFLINE: {
            // 节点下线
        } break;
        case AuthDeviceState::DEVICE_INFO_CHANGED: {
            // 节点信息改变
        } break;
    }
}

ISoftbusStateCallback g_callback = {.OnDeviceChanged = OnDeviceChanged};

if (RegisterDeviceStateCallback(PKG_NAME, &g_callback) == 0) 
{
    // 成功注册
}

if (UnRegisterDeviceStateCallback(PKG_NAME) == 0) 
{
    // 成功注销
}
```

#### （3）设备发现接口

**功能描述：**为业务侧提供订阅发现/停止发现附近设备的能力

**接口：**

```c++
// 订阅信息
typedef struct {
    // 订阅ID
    int subscribeId;
    // 发现模式 主动 | 被动
    DiscoverMode mode;
    // 发现协议
    ExchangeMedium medium;
    // 发现频率
    ExchangeFreq freq;
    // 是否仅订阅相同账户设备
    bool isSameAccount;
    // 是否发现睡眠设备
    bool isWakeRemote;
    // 订阅能力
    const char *capability;
    // 订阅能力数据
    unsigned char *capabilityData;
    unsigned int dataLen;
} SubscribeInfo;

// 发现通知回调
typedef struct {
    // 发现设备通知
    void (*OnDeviceFound)(const AuthDeviceInfo &info, bool isOnline);
    // 订阅成功通知
    void (*OnDiscoverySuccess)(int32_t subscribeId);
    // 订阅失败通知
    void (*OnDiscoveryFailed)(int32_t subscribeId, int32_t failedReason);
} ISoftbusDiscoveryCallback;

// 订阅发现
int32_t StartDeviceDiscovery(const char *pkgName, const SubscribeInfo &subscribeInfo,
                             const ISoftbusDiscoveryCallback *callback);
// 停止发现
int32_t StopDeviceDiscovery(const char *pkgName, int32_t subscribeId);
```

**请求参数：**

| 参数名        | 类型                              | 是否必填 | 描述             |
| ------------- | --------------------------------- | -------- | ---------------- |
| pkgName       | const char *                      | 是       | 业务侧应用名     |
| subscribeInfo | const SubscribeInfo &             | 是       | 订阅信息         |
| callback      | const ISoftbusDiscoveryCallback * | 是       | 发现设备回调函数 |
| subscribeId   | int32_t                           | 是       | 订阅ID           |

**返回值：**成功返回0

**使用说明：**

```c++
// 包含头文件
#include <devicemgr_comm.h>
#include <devicemanagersdk.h>

const char *PKG_NAME = "test";

void OnDeviceFound(const AuthDeviceInfo &info, bool isOnline)
{
    // 设备信息 info
    // 是否在线 isOnline
}

void OnDiscoverySuccess(int32_t subscribeId)
{
    // 成功订阅
}

void OnDiscoveryFailed(int32_t subscribeId, int32_t failedReason)
{
    // 订阅失败
}

ISoftbusDiscoveryCallback g_callback = {.OnDeviceFound = OnDeviceFound,
                                        .OnDiscoverySuccess = OnDiscoverySuccess,
                                        .OnDiscoveryFailed = OnDiscoveryFailed};

SubscribeInfo g_Info = {.subscribeId = DISC_SUBSCRIBE_ID,
                              .mode = DiscoverMode::DISCOVER_MODE_ACTIVE,
                              .medium = ExchangeMedium::COAP,
                              .freq = ExchangeFreq::HIGH,
                              .isSameAccount = false,
                              .isWakeRemote = false,
                              .capability = CAPABILITY,
                              .capabilityData = nullptr,
                              .dataLen = 0};

if (StartDeviceDiscovery(PKG_NAME, g_Info, &g_callback) == 0) 
{
    // 成功
}

if ()
{
    // 成功
}

```

#### （4）注册/注销UI操作接口

**功能描述：**业务层需要进行设备认证操作时需要注册UI的处理，业务侧认证结束后需要注销UI的处理

**接口：**

```c++
// UI操作处理回调通知
typedef struct {
    // 认证状态变化操作 
    void (*OnShowAuthState)(int32_t msgType);
    // 显示认证凭证操作
    void (*OnShowAuthInfo)(int32_t code);
    // 获取用户认证授权操作
    int32_t (*OnUserOperation)(const char *remoteName);
    // 取消UI显示操作
    void (*OnCancelDisplay)();
    // 用户输入认证凭证操作
    int32_t (*OnInputAuthInfo)();
} IUIStateUpdateCallback;

// 注册认证时的UI操作处理
int32_t RegisterUIStateCallback(const char *pkgName, const IUIStateUpdateCallback *callback);
// 注销认证时的UI操作处理
int32_t UnRegisterUIStateCallback(const char *pkgName);
```

**请求参数：**        

| 参数名   | 类型                           | 是否必填 | 描述               |
| -------- | ------------------------------ | -------- | ------------------ |
| pkgName  | const char *                   | 是       | 业务侧应用名       |
| callback | const IUIStateUpdateCallback * | 是       | UI操作处理回调函数 |

**返回值：**成功返回0

**使用说明：**

```c++
// 包含头文件
#include <devicemgr_comm.h>
#include <devicemanagersdk.h>
const char *PKG_NAME = "test";
void OnShowAuthState(int32_t msgType)
{     
    switch ((UiStateMsg)msgType) {
        case UiStateMsg::MSG_PIN_CODE_ERROR:            
            // PIN码输入错误            
            break;        
        case UiStateMsg::MSG_PIN_CODE_SUCCESS:            
            // PIN码正确            
            break;        
        case UiStateMsg::MSG_CANCEL_PIN_CODE_SHOW:            
            // 取消PIN码显示            
            break;        
        case UiStateMsg::MSG_CANCEL_PIN_CODE_INPUT:            
            // 取消PIN码输入            
            break;        
        case UiStateMsg::MSG_DOING_AUTH:            
            // 认证完成            
            break;        
        default:            
            break;    
    }
}

void OnShowAuthInfo(int32_t code)
{    
    // 显示code
}

int32_t OnUserOperation(const char *remoteName)
{    
    // 设备remote发起认证    
    // 信任返回 0
}

void OnCancelDisplay()
{    
    // 取消当前UI操作
}

int32_t OnInputAuthInfo()
{    
    //返回输入的认证凭据
}

IUIStateUpdateCallback g_callback = {.OnShowAuthState = OnShowAuthState,                                     
                                     .OnShowAuthInfo = OnShowAuthInfo,                                     
                                     .OnUserOperation = OnUserOperation,                                     
                                     .OnCancelDisplay = OnCancelDisplay,                                     
                                     .OnInputAuthInfo = OnInputAuthInfo};

if (RegisterUIStateCallback(PKG_NAME, &g_callback) == 0) {    
    // 成功注册
}

if (UnRegisterUIStateCallback(PKG_NAME) == 0) {    
    // 成功注销
}
```

#### （5）设备认证接口

**功能描述：**业务侧对已发现设备进行可信认证，使用时需提前注册UI操作接口，见上文。

**接口：**

```c++
int32_t AuthenticateDevice(const char *pkgName, const char *deviceId);
```

**请求参数：**

| 参数名   | 类型         | 是否必填 | 描述                                         |
| -------- | ------------ | -------- | -------------------------------------------- |
| pkgName  | const char * | 是       | 业务侧应用名                                 |
| deviceId | const char * | 是       | 设备ID，由设备发现的AuthDeviceInfo数据中提供 |

**返回值：**

**使用说明：**

```c++
// 包含头文件
#include <devicemgr_comm.h>
#include <devicemanagersdk.h>
const char *PKG_NAME = "test";

if (AuthenticateDevice(PKG_NAME, deviceId) == 0)
{     
    // 认证操作成功
}
```

#### （6）删除可信设备接口

**功能描述：**业务侧删除已认证设备

**接口：**

```c++
int32_t UnAuthenticateDevice(const char *pkgName, const char *networkId);
```

**请求参数：**

| 参数名    | 类型         | 是否必填 | 描述                                             |
| --------- | ------------ | -------- | ------------------------------------------------ |
| pkgName   | const char * | 是       | 业务侧应用名                                     |
| networkId | const char * | 是       | 网络ID，认证成功从节点上线的AuthDeviceInfo中获取 |

**返回值：**成功返回0

**使用说明：**

```c++
// 包含头文件
#include <devicemgr_comm.h>
#include <devicemanagersdk.h>
const char *PKG_NAME = "test";
if (UnAuthenticateDevice(PKG_NAME, networkId) == 0){     
    // 删除可信设备操作成功
}
```

#### （7）获取可信设备列表接口

**功能描述：**业务侧获取附近所有可信任设备信息列表

**接口：**

```c++
int32_t GetTrustedDeviceList(const char *pkgName, std::vector<AuthDeviceInfo> &deviceInfoList);
```

**请求参数：**

| 参数名     | 类型              | 是否必填 | 描述                       |
| ---------- | ----------------- | -------- | -------------------------- |
| pkgName    | const char *      | 是       | 业务侧应用名               |
| deviceInfo | AuthDeviceInfo ** | 是       | 输出参数，可信设备列表信息 |
| infoNum    | int *             | 是       | 输出参数，设备数量         |

**返回值：**成功返回0

**使用说明：**

```c++
// 包含头文件
#include <devicemgr_comm.h>
#include <devicemanagersdk.h>
const char *PKG_NAME = "test";
int devNum = 0;
AuthDeviceInfo *deviceInfo = nullptr;
if (GetTrustedDeviceList(PKG_NAME, &deviceInfo, &devNum) == 0)
{     
    // 获取成功
}
```

#### （8）注销服务端业务管理接口

**功能描述：**业务侧注销在设备管理模块注册的所有操作

**接口：**

```c++
int32_t DestoryClient(const char *pkgName);
```

**请求参数：**

| 参数名  | 类型         | 是否必填 | 描述         |
| ------- | ------------ | -------- | ------------ |
| pkgName | const char * | 是       | 业务侧应用名 |

**返回值：**成功返回0

**使用说明：**

```c++
// 包含头文件
#include <devicemgr_comm.h>
#include <devicemanagersdk.h>
const char *PKG_NAME = "test";
if (DestoryClient(PKG_NAME) == 0){     
    // 注销成功
}
```

#### （9）创建会话传输接口

**功能描述：**业务侧创建会话传输

**接口：**

```c++
// 会话管理通知回调
typedef struct {    
    // 会话已打开通知    
    int (*OnSessionOpened)(int sessionId, int result);    
    // 会话已关闭通知    
    void (*OnSessionClosed)(int sessionId);    
    // 收到字节数据通知    
    void (*OnBytesReceived)(int sessionId, const void *data, unsigned int dataLen);    
    // 收到消息数据通知    
    void (*OnMessageReceived)(int sessionId, const void *data, unsigned int dataLen);    
    // 收到流数据通知    
    void (*OnStreamReceived)(int sessionId, const StreamData *data, const StreamData *ext, const StreamFrameInfo *param);    
    // 暂未开发    
    void (*OnQosEvent)(int sessionId, int eventId, int tvCount, const QosTv *tvList);
} ISessionListener;

int CreateSessionServer(const char *pkgName, const char *sessionName, const ISessionListener *listener, bool isProxy);
```

**请求参数：**

| 参数名      | 类型                     | 是否必填 | 描述                 |
| ----------- | ------------------------ | -------- | -------------------- |
| pkgName     | const char *             | 是       | 业务侧应用名         |
| sessionName | const char *             | 是       | 会话名               |
| listener    | const ISessionListener * | 是       | 会话管理通知回调函数 |
| isProxy     | bool                     | 是       | 填入true             |

**返回值：**成功返回0，失败返回-1

**使用说明：**

```c++
// 包含头文件
#include <session.h>

const char *PKG_NAME = "test";
const char *SESSION_TEST_NAME = "test.session";

int32_t OnSessionOpened(int32_t sessionId, int32_t result)
{    
    // 会话已打开
}

void OnSessionClosed(int32_t sessionId)
{    
    // 会话已关闭
}

void OnBytesReceived(int32_t sessionId, const void *data, uint32_t dataLen)
{    
    // 处理字节数据
}

void OnStreamReceived(int32_t sessionId, const StreamData *data, const StreamData *ext, const StreamFrameInfo *frameInfo)
{    
    // 处理流数据
}

void OnMessageReceived(int sessionId, const void *data, unsigned int dataLen)
{    
    // 处理消息数据
}

void OnQosEvent(int sessionId, int eventId, int tvCount, const QosTv *tvList)
{    
    // 暂未开发
}

ISessionListener s_sessionListener = {.OnSessionOpened = OnSessionOpened,
                                      .OnSessionClosed = OnSessionClosed,
                                      .OnBytesReceived = OnBytesReceived,
                                      .OnMessageReceived = OnMessageReceived,
                                      .OnStreamReceived = OnStreamReceived,
                                      .OnQosEvent = OnQosEvent};

if (CreateSessionServer(PKG_NAME, SESSION_TEST_NAME, &s_sessionListener, true) == 0) {     
    // 创建成功
}
```

#### （10）删除会话传输接口

**功能描述：**业务侧删除会话传输

**接口：**

```c++
int RemoveSessionServer(const char *pkgName, const char *sessionName);
```

**请求参数：**

| 参数名      | 类型         | 是否必填 | 描述         |
| ----------- | ------------ | -------- | ------------ |
| pkgName     | const char * | 是       | 业务侧应用名 |
| sessionName | const char * | 是       | 会话名       |

**返回值：**成功返回0，失败返回-1

**使用说明：**

```c++
// 包含头文件
#include <session.h>
const char *PKG_NAME = "test";
const char *SESSION_TEST_NAME = "test.session";
if (RemoveSessionServer(DEV_CENTER_PKG_NAME, SESSION_TEST_NAME) == 0){     
    // 删除会话成功
}
```

#### （11）开启会话传输接口

**功能描述：**业务侧打开会话传输

**接口：**

```c++
// 会话属性描述
typedef struct {    
    // 传输数据类型    
    int dataType;    
    // 链路数量    
    int linkTypeNum;    
    // 链路类型    
    LinkType linkType[LINK_TYPE_MAX];    
    union {        
        // 流数据类型        
        struct StreamAttr {            
            int streamType;        
        } streamAttr;    
    } attr;    
    uint8_t *fastTransData;    
    uint16_t fastTransDataSize;
} SessionAttribute;

int OpenSession(const char *mySessionName, const char *peerSessionName, const char *peerNetworkId, const char *groupId,                const SessionAttribute *attr);
```

**请求参数：**

| 参数名          | 类型                     | 是否必填 | 描述                  |
| --------------- | ------------------------ | -------- | --------------------- |
| mySessionName   | const char *             | 是       | 业务侧应用名          |
| peerSessionName | const char *             | 是       | 连接对端设备会话名    |
| peerNetworkId   | const char *             | 是       | 连接对端设备networkId |
| groupId         | const char *             | 是       | 群组ID，没有填"0"     |
| attr            | const SessionAttribute * | 是       | 会话属性描述信息      |

**返回值：**成功返回sessionId，失败返回-1

**使用说明：**

```c++
// 包含头文件
#include <session.h>
const char *PKG_NAME = "test";
const char *SESSION_TEST_NAME = "test.session";
SessionAttribute attr = {0};
attr.dataType = SessionType::TYPE_BYTES; // 字节数据
if (type == TYPE_STREAM) {    
    // 设置流数据格式    
    attr.attr.streamAttr.streamType = COMMON_VIDEO_STREAM;
}
attr.linkTypeNum = LINK_TYPE_MAX;
LinkType linkTypeList[LINK_TYPE_MAX] = {    
    LINK_TYPE_WIFI_P2P,    
    LINK_TYPE_WIFI_WLAN_5G,    
    LINK_TYPE_WIFI_WLAN_2G,    
    LINK_TYPE_BR,
};
int ret = memcpy(attr.linkType, linkTypeList, sizeof(attr.linkType));
if (ret != 0) {    
    // 错误    
    return ;
}
int sessionId = OpenSession(SESSION_TEST_NAME, SESSION_TEST_NAME, peerNetworkId, "0", &attr);
if (sessionId < 0) {    
    // 错误    
    return;
}
// 保存sessionId
```

#### （12）关闭会话传输接口

**功能描述：**业务侧关闭会话传输

**接口：**

```c++
void CloseSession(int sessionId);
```

**请求参数：**

| 参数名    | 类型 | 是否必填 | 描述                   |
| --------- | ---- | -------- | ---------------------- |
| sessionId | int  | 是       | 打开会话时返回的会话ID |

**返回值：**无

**使用说明：**无

#### （13）发送字节数据接口

**功能描述：**业务侧基于打开会话的sessionId进行字节数据传输

**接口：**

```c++
int SendBytes(int sessionId, const void *data, unsigned int len);
```

**请求参数：**

| 参数名    | 类型         | 是否必填 | 描述             |
| --------- | ------------ | -------- | ---------------- |
| sessionId | int          | 是       | 会话ID           |
| data      | const void * | 是       | 待发送的字节数据 |
| len       | unsigned int | 是       | 数据长度         |

**返回值：**成功返回0

**使用说明：**

```c++
// 包含头文件
#include <session.h>
const char *data = "SendBytes Test";
if (SendBytes(sessionId, data, strlen(data)) == 0) {    
    // 数据发送成功
}
```

#### （14）发送消息数据接口

**功能描述：**业务侧基于打开会话的sessionId进行消息数据传输

**接口：**

```c++
int SendMessage(int sessionId, const void *data, unsigned int len);
```

**请求参数：**

| 参数名    | 类型         | 是否必填 | 描述             |
| --------- | ------------ | -------- | ---------------- |
| sessionId | int          | 是       | 会话ID           |
| data      | const void * | 是       | 待发送的消息数据 |
| len       | unsigned int | 是       | 数据长度         |

**返回值：**成功返回0

**使用说明：**无

#### （15）发送流数据接口

**功能描述：**业务侧基于打开会话的sessionId进行流数据传输

**接口：**

```c++
// 流数据描述
typedef struct {    
    // 指向用于存储流数据的缓冲区的指针    
    char *buf;    
    // 缓冲区大小    
    int bufLen; 
} StreamData;
// 帧信息
typedef struct {    
    // 帧类型    
    int frameType;    
    // 时间戳    
    int64_t timeStamp;    
    // 序列号    
    int seqNum;    
    // 切片序列号    
    int seqSubNum;    
    // 视频编码级别    
    int level;    
    // 位图    
    int bitMap;    
    int tvCount;    
    TV *tvList;
} StreamFrameInfo;
int SendStream(int sessionId, const StreamData *data, const StreamData *ext, const StreamFrameInfo *param);
```

**请求参数：**

| 参数名    | 类型                    | 是否必填 | 描述                 |
| --------- | ----------------------- | -------- | -------------------- |
| sessionId | int                     | 是       | 会话ID               |
| data      | const StreamData *      | 是       | 待发送的流数据的指针 |
| ext       | const StreamData *      | 是       | 扩展流数据指针       |
| param     | const StreamFrameInfo * | 是       | 流帧信息的指针       |

**返回值：**成功返回0

**使用说明：**

```c++
// 包含头文件
#include <session.h>
// 将图片转换为流数据
std::vector<uchar> buffer;
// 设置流数据指针
StreamData data = {reinterpret_cast<char *>(buffer.data()), buffer.size()};
StreamData ext = {0};
StreamFrameInfo frameInfo = {0};
if (SendStream(sessionId, &data, &ext, &frameInfo) == 0) {    
    // 流数据发送成功
}
```

#### （16）发送文件接口

**功能描述：**业务侧发送文件

**接口：**

```c++
// 文件接收管理回调函数
typedef struct {    
    // 接收到文件通知    
    int (*OnReceiveFileStarted)(int sessionId, const char *files, int fileCnt);    
    // 文件接收处理    
    int (*OnReceiveFileProcess)(int sessionId, const char *firstFile, uint64_t bytesUpload, uint64_t bytesTotal);    
    // 文件接收完成    
    void (*OnReceiveFileFinished)(int sessionId, const char *files, int fileCnt);    
    // 文件接收失败    
    void (*OnFileTransError)(int sessionId);
} IFileReceiveListener;
// 设置接收文件管理
int SetFileReceiveListener(const char *pkgName, const char *sessionName, const IFileReceiveListener *recvListener,                           const char *rootDir);
// 发送文件管理回调函数
typedef struct {    
    // 发送文件处理    
    int (*OnSendFileProcess)(int sessionId, uint64_t bytesUpload, uint64_t bytesTotal);    
    // 文件发送完成    
    int (*OnSendFileFinished)(int sessionId, const char *firstFile);    
    // 文件发送失败    
    void (*OnFileTransError)(int sessionId);
} IFileSendListener;
// 设置文件发送管理
int SetFileSendListener(const char *pkgName, const char *sessionName, const IFileSendListener *sendListener);
// 发送文件
// 如果需要使用该接口
// 在创建session之前需要
// 使用上文的SetFileReceiveListener接口设置文件接收处理
// 使用上文的SetFileSendListener接口设置文件发送处理
int SendFile(int sessionId, const char *sFileList[], const char *dFileList[], uint32_t fileCnt);
```

**请求参数：**

| 参数名       | 类型                         | 是否必填 | 描述                     |
| ------------ | ---------------------------- | -------- | ------------------------ |
| pkgName      | const char *                 | 是       | 业务侧应用名             |
| sessionName  | const char *                 | 是       | 会话名                   |
| recvListener | const IFileReceiveListener * | 是       | 接收文件管理指针         |
| sendListener | const IFileSendListener *    | 是       | 发送文件管理指针         |
| sessionId    | int                          | 是       | 会话ID                   |
| sFileList    | const char **                | 是       | 指向要发送的源文件的指针 |
| dFileList    | const char **                | 是       | 指向目标文件的指针       |
| fileCnt      | uint32_t                     | 是       | 发送的文件数             |

**返回值：**成功返回0

**使用说明：**

```c++
// 包含头文件
#include <session.h>
const char *PKG_NAME = "test";
const char *SESSION_TEST_NAME = "test.session";
int OnSendFileProcess(int sessionId, uint64_t bytesUpload, uint64_t bytesTotal)
{    
    // bytesUpload 发送的文件大小    
    // bytesTotal 文件总大小    
    return 0;
}
int OnSendFileFinished(int sessionId, const char *firstFile)
{    
    // firstFile 发送的第一个文件指针    
    return 0;
}
void OnFileTransError(int sessionId)
{    
    // 错误处理
}
int OnReceiveFileStarted(int sessionId, const char *files, int fileCnt){    
    // files 要接受的文件的指针    
    // fileCnt 文件数量    
    return 0;
}
int OnReceiveFileProcess(int sessionId, const char *firstFile, uint64_t bytesUpload, uint64_t bytesTotal)
{    
    // 要接收的第一个文件的指针    
    // bytesUpload 接收的文件大小    
    // bytesTotal 接收文件总大小    
    return 0;
}
void OnReceiveFileFinished(int sessionId, const char *files, int fileCnt)
{    
    // files 指向接收到的文件指针    
    // fileCnt 文件数量
}
IFileSendListener g_fileSendListener = {.OnSendFileProcess = OnSendFileProcess,
                                        .OnSendFileFinished = OnSendFileFinished,
                                        .OnFileTransError = OnFileTransError};
IFileReceiveListener g_fileRecvListener = {.OnReceiveFileStarted = OnReceiveFileStarted,
                                           .OnReceiveFileProcess = OnReceiveFileProcess,
                                           .OnReceiveFileFinished = OnReceiveFileFinished,
                                           .OnFileTransError = OnFileTransError};
if (SetFileSendListener(PKG_NAME, SESSION_TEST_NAME, &g_fileSendListener) == 0) {    
    // 设置成功
}
if (SetFileReceiveListener(PKG_NAME, SESSION_TEST_NAME, &g_fileRecvListener, "/tmp/") == 0) {    
    // 设置成功
}
const char *sfileList[] = {        
    "/tmp/sessiontest/test.jpg",        
    "/tmp/sessiontest/app.png",
    "/tmp/sessiontest/pc.svg",};
if (SendFile(g_testSessionId, sfileList, NULL, 3) == 0) {    
    // 发送成功
}
```

