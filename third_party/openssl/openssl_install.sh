#!/bin/bash

mkdir -p /opt/dsoftbus/openssl/include
mkdir -p /opt/dsoftbus/openssl/lib

cp -ar $1/include/*  /opt/dsoftbus/openssl/include/
cp -a $1/lib/$2/lib* /opt/dsoftbus/openssl/lib/

ln -s /opt/dsoftbus/openssl/lib/libcrypto.so.3 /opt/dsoftbus/openssl/lib/libcrypto.so 1>/dev/null 2>/dev/null
ln -s /opt/dsoftbus/openssl/lib/libssl.so.3 /opt/dsoftbus/openssl/lib/libssl.so 1>/dev/null 2>/dev/null